<?php

namespace Phareos\DeskNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\DeskNetServiceBundle\Entity\dept
 *
 * @ORM\Table(name="desk_dept")
 * @ORM\Entity(repositoryClass="Phareos\DeskNetServiceBundle\Entity\deptRepository")
 */
class dept
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nom
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     */
    private $nom;
	
	/**
     * @var string $client
     *
     * @ORM\Column(name="client", type="string", length=255, nullable=true)
     */
    private $client;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }
	
	/**
     * Set client
     *
     * @param string $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * Get client
     *
     * @return string 
     */
    public function getClient()
    {
        return $this->client;
    }
}