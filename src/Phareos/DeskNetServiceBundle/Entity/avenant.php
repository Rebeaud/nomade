<?php

namespace Phareos\DeskNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\DeskNetServiceBundle\Entity\avenant
 *
 * @ORM\Table(name="desk_avenant")
 * @ORM\Entity(repositoryClass="Phareos\DeskNetServiceBundle\Entity\avenantRepository")
 */
class avenant
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
	 * @ORM\ManyToOne(targetEntity="collaborateur", inversedBy="collabavenant", cascade={"remove"})
	 * @ORM\JoinColumn(name="collaborateur_id", referencedColumnName="id")
	 */
	protected $collaborateur;

    /**
     * @var datetime $datedemande
     *
     * @ORM\Column(name="datedemande", type="datetime", nullable=true)
     */
    private $datedemande;

    /**
     * @var string $typeavenant
     *
     * @ORM\Column(name="typeavenant", type="string", length=255, nullable=true)
     */
    private $typeavenant;

    /**
     * @var text $commentaire
     *
     * @ORM\Column(name="commentaire", type="text", nullable=true)
     */
    private $commentaire;

    /**
     * @var boolean $confirmok
     *
     * @ORM\Column(name="confirmok", type="boolean", nullable=true)
     */
    private $confirmok;

    /**
     * @var datetime $dateok
     *
     * @ORM\Column(name="dateok", type="datetime", nullable=true)
     */
    private $dateok;

    /**
     * @var datetime $datecrea
     *
     * @ORM\Column(name="datecrea", type="datetime", nullable=true)
     */
    private $datecrea;

    /**
     * @var datetime $datemaj
     *
     * @ORM\Column(name="datemaj", type="datetime", nullable=true)
     */
    private $datemaj;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datedemande
     *
     * @param datetime $datedemande
     */
    public function setDatedemande($datedemande)
    {
        $this->datedemande = $datedemande;
    }

    /**
     * Get datedemande
     *
     * @return datetime 
     */
    public function getDatedemande()
    {
        return $this->datedemande;
    }

    /**
     * Set typeavenant
     *
     * @param string $typeavenant
     */
    public function setTypeavenant($typeavenant)
    {
        $this->typeavenant = $typeavenant;
    }

    /**
     * Get typeavenant
     *
     * @return string 
     */
    public function getTypeavenant()
    {
        return $this->typeavenant;
    }

    /**
     * Set commentaire
     *
     * @param text $commentaire
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;
    }

    /**
     * Get commentaire
     *
     * @return text 
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * Set confirmok
     *
     * @param boolean $confirmok
     */
    public function setConfirmok($confirmok)
    {
        $this->confirmok = $confirmok;
    }

    /**
     * Get confirmok
     *
     * @return boolean 
     */
    public function getConfirmok()
    {
        return $this->confirmok;
    }

    /**
     * Set dateok
     *
     * @param datetime $dateok
     */
    public function setDateok($dateok)
    {
        $this->dateok = $dateok;
    }

    /**
     * Get dateok
     *
     * @return datetime 
     */
    public function getDateok()
    {
        return $this->dateok;
    }

    /**
     * Set datecrea
     *
     * @param datetime $datecrea
     */
    public function setDatecrea($datecrea)
    {
        $this->datecrea = $datecrea;
    }

    /**
     * Get datecrea
     *
     * @return datetime 
     */
    public function getDatecrea()
    {
        return $this->datecrea;
    }

    /**
     * Set datemaj
     *
     * @param datetime $datemaj
     */
    public function setDatemaj($datemaj)
    {
        $this->datemaj = $datemaj;
    }

    /**
     * Get datemaj
     *
     * @return datetime 
     */
    public function getDatemaj()
    {
        return $this->datemaj;
    }

    /**
     * Set collaborateur
     *
     * @param Phareos\DeskNetServiceBundle\Entity\collaborateur $collaborateur
     */
    public function setCollaborateur(\Phareos\DeskNetServiceBundle\Entity\collaborateur $collaborateur)
    {
        $this->collaborateur = $collaborateur;
    }

    /**
     * Get collaborateur
     *
     * @return Phareos\DeskNetServiceBundle\Entity\collaborateur 
     */
    public function getCollaborateur()
    {
        return $this->collaborateur;
    }
}