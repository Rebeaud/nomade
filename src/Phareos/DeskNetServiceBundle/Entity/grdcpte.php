<?php

namespace Phareos\DeskNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Phareos\DeskNetServiceBundle\Entity\grdcpte
 *
 * @ORM\Table(name="desk_grdcpte")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Phareos\DeskNetServiceBundle\Entity\grdcpteRepository")
 */
class grdcpte
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
	 * @ORM\OneToMany(targetEntity="client", mappedBy="grdcpte", cascade={"remove", "persist"})
	 */
	protected $grdcpteclient;
	
	/**
	 * @ORM\ManyToMany(targetEntity="dept", cascade={"persist"})
	 */
	private $departements;
	
	/**
     * @var string $Nom
     *
     * @ORM\Column(name="Nom", type="string", length=255)
     */
    private $Nom;

    /**
     * @var string $adres1
     *
     * @ORM\Column(name="adres1", type="string", length=255, nullable=true)
     */
    private $adres1;

    /**
     * @var string $adres2
     *
     * @ORM\Column(name="adres2", type="string", length=255, nullable=true)
     */
    private $adres2;
	
	/**
     * @var string $cp
     *
     * @ORM\Column(name="cp", type="string", length=255, nullable=true)
     */
    private $cp;
	
	/**
     * @var string $ville
     *
     * @ORM\Column(name="ville", type="string", length=255, nullable=true)
     */
    private $ville;

    /**
     * @var string $tel
     *
     * @ORM\Column(name="tel", type="string", length=255, nullable=true)
     */
    private $tel;

    /**
     * @var string $fax
     *
     * @ORM\Column(name="fax", type="string", length=255, nullable=true)
     */
    private $fax;

    /**
     * @var string $mail
     *
     * @ORM\Column(name="mail", type="string", length=255, nullable=true)
     */
    private $mail;

    /**
     * @var string $acheteur
     *
     * @ORM\Column(name="acheteur", type="string", length=255, nullable=true)
     */
    private $acheteur;
	
	/**
     * @var string $prenach
     *
     * @ORM\Column(name="prenach", type="string", length=255, nullable=true)
     */
    private $prenach;
	
	/**
     * @var string $achcivil
     *
     * @ORM\Column(name="achcivil", type="string", length=255, nullable=true)
     */
    private $achcivil;

    /**
     * @var string $achtel
     *
     * @ORM\Column(name="achtel", type="string", length=255, nullable=true)
     */
    private $achtel;

    /**
     * @var string $achmail
     *
     * @ORM\Column(name="achmail", type="string", length=255, nullable=true)
     */
    private $achmail;

    /**
     * @var integer $nbragences
     *
     * @ORM\Column(name="nbragences", type="integer", nullable=true)
     */
    private $nbragences;
	
	/**
     * @var string $societeuser
     *
     * @ORM\Column(name="societeuser", type="string", length=255, nullable=true)
     */
    private $societeuser;
	
	/**
     * @var string $dept
     *
     * @ORM\Column(name="dept", type="string", length=255, nullable=true)
     */
    private $dept;
	
	/**
     * @var string $cdcpdf
     * @Assert\File( maxSize = "4M", mimeTypes = {"application/pdf", "application/x-pdf", "application/vnd.ms-excel"}, mimeTypesMessage = "SVP Uploader un fichier PDF")
     * @ORM\Column(name="cdcpdf", type="string", length=255, nullable=true)
     */
    private $cdcpdf;
	
	/**
     * @var string $cdbpdf
     * @Assert\File( maxSize = "4M", mimeTypes = {"application/pdf", "application/x-pdf", "application/vnd.ms-excel"}, mimeTypesMessage = "SVP Uploader un fichier PDF")
     * @ORM\Column(name="cdbpdf", type="string", length=255, nullable=true)
     */
    private $cdbpdf;
	
	/**
     * @var string $contratpdf
     * @Assert\File( maxSize = "4M", mimeTypes = {"application/pdf", "application/x-pdf", "application/vnd.ms-excel"}, mimeTypesMessage = "SVP Uploader un fichier PDF")
     * @ORM\Column(name="contratpdf", type="string", length=255, nullable=true)
     */
    private $contratpdf;
	
	
	/**
     * @var string $planprevpdf
     * @Assert\File( maxSize = "4M", mimeTypes = {"application/pdf", "application/x-pdf", "application/vnd.ms-excel"}, mimeTypesMessage = "SVP Uploader un fichier PDF")
     * @ORM\Column(name="planprevpdf", type="string", length=255, nullable=true)
     */
    private $planprevpdf;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Nom
     *
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->Nom = $nom;
    }

    /**
     * Get Nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->Nom;
    }

    /**
     * Set adres1
     *
     * @param string $adres1
     */
    public function setAdres1($adres1)
    {
        $this->adres1 = $adres1;
    }

    /**
     * Get adres1
     *
     * @return string 
     */
    public function getAdres1()
    {
        return $this->adres1;
    }

    /**
     * Set adres2
     *
     * @param string $adres2
     */
    public function setAdres2($adres2)
    {
        $this->adres2 = $adres2;
    }

    /**
     * Get adres2
     *
     * @return string 
     */
    public function getAdres2()
    {
        return $this->adres2;
    }
	
	/**
     * Set cp
     *
     * @param string $cp
     */
    public function setCp($cp)
    {
        $this->cp = $cp;
    }

    /**
     * Get cp
     *
     * @return string 
     */
    public function getCp()
    {
        return $this->cp;
    }
	
	/**
     * Set ville
     *
     * @param string $ville
     */
    public function setVille($ville)
    {
        $this->ville = $ville;
    }

    /**
     * Get ville
     *
     * @return string 
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set tel
     *
     * @param string $tel
     */
    public function setTel($tel)
    {
        $this->tel = $tel;
    }

    /**
     * Get tel
     *
     * @return string 
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set fax
     *
     * @param string $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * Get fax
     *
     * @return string 
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set mail
     *
     * @param string $mail
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    /**
     * Get mail
     *
     * @return string 
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set acheteur
     *
     * @param string $acheteur
     */
    public function setAcheteur($acheteur)
    {
        $this->acheteur = $acheteur;
    }

    /**
     * Get acheteur
     *
     * @return string 
     */
    public function getAcheteur()
    {
        return $this->acheteur;
    }
	
	/**
     * Set achcivil
     *
     * @param string $achcivil
     */
    public function setAchcivil($achcivil)
    {
        $this->achcivil = $achcivil;
    }

    /**
     * Get achcivil
     *
     * @return string 
     */
    public function getAchcivil()
    {
        return $this->achcivil;
    }

    /**
     * Set achtel
     *
     * @param string $achtel
     */
    public function setAchtel($achtel)
    {
        $this->achtel = $achtel;
    }

    /**
     * Get achtel
     *
     * @return string 
     */
    public function getAchtel()
    {
        return $this->achtel;
    }

    /**
     * Set achmail
     *
     * @param string $achmail
     */
    public function setAchmail($achmail)
    {
        $this->achmail = $achmail;
    }

    /**
     * Get achmail
     *
     * @return string 
     */
    public function getAchmail()
    {
        return $this->achmail;
    }

    /**
     * Set nbragences
     *
     * @param integer $nbragences
     */
    public function setNbragences($nbragences)
    {
        $this->nbragences = $nbragences;
    }

    /**
     * Get nbragences
     *
     * @return integer 
     */
    public function getNbragences()
    {
        return $this->nbragences;
    }
	
	/**
     * Set societeuser
     *
     * @param string $societeuser
     */
    public function setSocieteuser($societeuser)
    {
        $this->societeuser = $societeuser;
    }

    /**
     * Get societeuser
     *
     * @return string 
     */
    public function getSocieteuser()
    {
        return $this->societeuser;
    }

	/**
     * Set dept
     *
     * @param string $dept
     */
    public function setDept($dept)
    {
        $this->dept = $dept;
    }

    /**
     * Get dept
     *
     * @return string 
     */
    public function getDept()
    {
        return $this->dept;
    }
	
	/**
     * Set cdcpdf
     *
     * @param string $cdcpdf
     */
    public function setCdcpdf($cdcpdf)
    {
        $this->cdcpdf = $cdcpdf;
    }

    /**
     * Get cdcpdf
     *
     * @return string 
     */
    public function getCdcpdf()
    {
        return $this->cdcpdf;
    }
	
	/**
     * Set cdbpdf
     *
     * @param string $cdbpdf
     */
    public function setCdbpdf($cdbpdf)
    {
        $this->cdbpdf = $cdbpdf;
    }

    /**
     * Get cdbpdf
     *
     * @return string 
     */
    public function getCdbpdf()
    {
        return $this->cdbpdf;
    }
	
	/**
     * Set contratpdf
     *
     * @param string $contratpdf
     */
    public function setContratpdf($contratpdf)
    {
        $this->contratpdf = $contratpdf;
    }

    /**
     * Get contratpdf
     *
     * @return string 
     */
    public function getContratpdf()
    {
        return $this->contratpdf;
    }
	
	/**
     * Set planprevpdf
     *
     * @param string $planprevpdf
     */
    public function setPlanprevpdf($planprevpdf)
    {
        $this->planprevpdf = $planprevpdf;
    }

    /**
     * Get planprevpdf
     *
     * @return string 
     */
    public function getPlanprevpdf()
    {
        return $this->planprevpdf;
    }
	
	protected function getTmpUploadRootCdcPdfDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/upload/desk/grdcpte/cdc/';
    }
	
	protected function getTmpUploadRootCdbPdfDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/upload/desk/grdcpte/cdb/';
    }
	
	protected function getTmpUploadRootContratPdfDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/upload/desk/grdcpte/contrat/';
    }
	
	protected function getTmpUploadRootPlanprevPdfDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/upload/desk/grdcpte/planprev/';
    }
	
	protected function getUploadRootCdcPdfDir() {
        // the absolute directory path where uploaded documents should be saved
        return $this->getTmpUploadRootCdcPdfDir().$this->getId()."/";
    }
	
	protected function getUploadRootCdbPdfDir() {
        // the absolute directory path where uploaded documents should be saved
        return $this->getTmpUploadRootCdbPdfDir().$this->getId()."/";
    }
	
	protected function getUploadRootContratPdfDir() {
        // the absolute directory path where uploaded documents should be saved
        return $this->getTmpUploadRootContratPdfDir().$this->getId()."/";
    }
	
	protected function getUploadRootPlanprevPdfDir() {
        // the absolute directory path where uploaded documents should be saved
        return $this->getTmpUploadRootPlanprevPdfDir().$this->getId()."/";
    }
	
	public function getFullCdcPdfPath() {
        return null === $this->cdcpdf ? null : $this->getUploadRootCdcPdfDir(). $this->cdcpdf;
    }
	
	public function getFullCdbPdfPath() {
        return null === $this->cdbpdf ? null : $this->getUploadRootCdbPdfDir(). $this->cdbpdf;
    }
	
	public function getFullContratPdfPath() {
        return null === $this->contratpdf ? null : $this->getUploadRootContratPdfDir(). $this->contratpdf;
    }
	
	public function getFullPlanprevPdfPath() {
        return null === $this->planprevpdf ? null : $this->getUploadRootPlanprevPdfDir(). $this->planprevpdf;
    }
	
	
	
	
	
	/**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function uploadCdcPdf() {
        // the file property can be empty if the field is not required
        if (null === $this->cdcpdf) {
            return;
        }
        if(!$this->id){
            $this->cdcpdf->move($this->getTmpUploadRootCdcPdfDir(), $this->cdcpdf->getClientOriginalName());
        }else{
            $this->cdcpdf->move($this->getUploadRootCdcPdfDir(), $this->cdcpdf->getClientOriginalName());
        }
        $this->setCdcpdf($this->cdcpdf->getClientOriginalName());
    }
 
    /**
     * @ORM\PostPersist()
     */
    public function moveCdcPdf()
    {
        if (null === $this->cdcpdf) {
            return;
        }
        if(!is_dir($this->getUploadRootCdcPdfDir())){
            mkdir($this->getUploadRootCdcPdfDir());
        }
        copy($this->getTmpUploadRootCdcPdfDir().$this->cdcpdf, $this->getFullCdcPdfPath());
        unlink($this->getTmpUploadRootCdcPdfDir().$this->cdcpdf);
    }
	
	/**
     * @ORM\PreRemove()
     */
    public function removeCdcPdf()
    {
        unlink($this->getFullCdcPdfPath());
        rmdir($this->getUploadRootCdcPdfDir());
    }
	
	
	
	
	
	
	
	/**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function uploadCdbPdf() {
        // the file property can be empty if the field is not required
        if (null === $this->cdbpdf) {
            return;
        }
        if(!$this->id){
            $this->cdbpdf->move($this->getTmpUploadRootCdbPdfDir(), $this->cdbpdf->getClientOriginalName());
        }else{
            $this->cdbpdf->move($this->getUploadRootCdbPdfDir(), $this->cdbpdf->getClientOriginalName());
        }
        $this->setCdbpdf($this->cdbpdf->getClientOriginalName());
    }
 
    /**
     * @ORM\PostPersist()
     */
    public function moveCdbPdf()
    {
        if (null === $this->cdbpdf) {
            return;
        }
        if(!is_dir($this->getUploadRootCdbPdfDir())){
            mkdir($this->getUploadRootCdbPdfDir());
        }
        copy($this->getTmpUploadRootCdbPdfDir().$this->cdbpdf, $this->getFullCdbPdfPath());
        unlink($this->getTmpUploadRootCdbPdfDir().$this->cdbpdf);
    }
	
	/**
     * @ORM\PreRemove()
     */
    public function removeCdbPdf()
    {
        unlink($this->getFullCdbPdfPath());
        rmdir($this->getUploadRootCdbPdfDir());
    }
	
	
	
	
	
	
	
	/**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function uploadContratPdf() {
        // the file property can be empty if the field is not required
        if (null === $this->contratpdf) {
            return;
        }
        if(!$this->id){
            $this->contratpdf->move($this->getTmpUploadRootContratPdfDir(), $this->contratpdf->getClientOriginalName());
        }else{
            $this->contratpdf->move($this->getUploadRootContratPdfDir(), $this->contratpdf->getClientOriginalName());
        }
        $this->setContratpdf($this->contratpdf->getClientOriginalName());
    }
 
    /**
     * @ORM\PostPersist()
     */
    public function moveContratPdf()
    {
        if (null === $this->contratpdf) {
            return;
        }
        if(!is_dir($this->getUploadRootContratPdfDir())){
            mkdir($this->getUploadRootContratPdfDir());
        }
        copy($this->getTmpUploadRootContratPdfDir().$this->contratpdf, $this->getFullContratPdfPath());
        unlink($this->getTmpUploadRootContratPdfDir().$this->contratpdf);
    }
	
	/**
     * @ORM\PreRemove()
     */
    public function removeContratPdf()
    {
        unlink($this->getFullContratPdfPath());
        rmdir($this->getUploadRootContratPdfDir());
    }
	
	
	
	
	
	
	
	/**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function uploadPlanprevPdf() {
        // the file property can be empty if the field is not required
        if (null === $this->planprevpdf) {
            return;
        }
        if(!$this->id){
            $this->planprevpdf->move($this->getTmpUploadRootPlanprevPdfDir(), $this->planprevpdf->getClientOriginalName());
        }else{
            $this->planprevpdf->move($this->getUploadRootPlanprevPdfDir(), $this->planprevpdf->getClientOriginalName());
        }
        $this->setPlanprevpdf($this->planprevpdf->getClientOriginalName());
    }
 
    /**
     * @ORM\PostPersist()
     */
    public function movePlanprevPdf()
    {
        if (null === $this->planprevpdf) {
            return;
        }
        if(!is_dir($this->getUploadRootPlanprevPdfDir())){
            mkdir($this->getUploadRootPlanprevPdfDir());
        }
        copy($this->getTmpUploadRootPlanprevPdfDir().$this->planprevpdf, $this->getFullPlanprevPdfPath());
        unlink($this->getTmpUploadRootPlanprevPdfDir().$this->planprevpdf);
    }
	
	/**
     * @ORM\PreRemove()
     */
    public function removePlanprevPdf()
    {
        unlink($this->getFullPlanprevPdfPath());
        rmdir($this->getUploadRootPlanprevPdfDir());
    }
	
    public function __construct()
    {
        $this->grdcpteclient = new \Doctrine\Common\Collections\ArrayCollection();
		$this->departements = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add grdcpteclient
     *
     * @param Phareos\DeskNetServiceBundle\Entity\client $grdcpteclient
     */
    public function addclient(\Phareos\DeskNetServiceBundle\Entity\client $grdcpteclient)
    {
        $this->grdcpteclient[] = $grdcpteclient;
    }

    
	
	/**
     * Get grdcpteclient
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getGrdcpteclient()
    {
        return $this->grdcpteclient;
    }
	
	/**
	* Set grdcpteclient
	*
	* @param \Doctrine\Common\Collections\Collection $grdcpteclient
	*/
	public function setClient(\Doctrine\Common\Collections\Collection $grdcpteclient)
	{
		$this->grdcpteclient = $grdcpteclient;
	}
	
	
	

    /**
     * Set prenach
     *
     * @param string $prenach
     */
    public function setPrenach($prenach)
    {
        $this->prenach = $prenach;
    }

    /**
     * Get prenach
     *
     * @return string 
     */
    public function getPrenach()
    {
        return $this->prenach;
    }
	
	/**
     * Add departements
     *
     * @param Phareos\DeskNetServiceBundle\Entity\dept $departements
     */
    public function adddept(\Phareos\DeskNetServiceBundle\Entity\dept $departements)
    {
        $this->departements[] = $departements;
    }

    /**
     * Get departements
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getDepartements()
    {
        return $this->departements;
    }

}