<?php

namespace Phareos\DeskNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\DeskNetServiceBundle\Entity\collaborateur
 *
 * @ORM\Table(name="desk_collabo")
 * @ORM\Entity(repositoryClass="Phareos\DeskNetServiceBundle\Entity\collaborateurRepository")
 */
class collaborateur
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
	 * @ORM\ManyToMany(targetEntity="dept", cascade={"persist"})
	 */
	private $departements;
	
	/**
	 * @ORM\OneToMany(targetEntity="contrats", mappedBy="collaborateur", cascade={"remove", "persist"})
	 */
	protected $collabcontrat;
	
	/**
	 * @ORM\OneToMany(targetEntity="avenant", mappedBy="collaborateur", cascade={"remove", "persist"})
	 */
	protected $collabavenant;

    /**
     * @var string $nom
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string $prenom
     *
     * @ORM\Column(name="prenom", type="string", length=255, nullable=true)
     */
    private $prenom;

    /**
     * @var string $typeposte
     *
     * @ORM\Column(name="typeposte", type="string", length=255, nullable=true)
     */
    private $typeposte;

    /**
     * @var string $adres1
     *
     * @ORM\Column(name="adres1", type="string", length=255, nullable=true)
     */
    private $adres1;

    /**
     * @var string $adres2
     *
     * @ORM\Column(name="adres2", type="string", length=255, nullable=true)
     */
    private $adres2;

    /**
     * @var string $cp
     *
     * @ORM\Column(name="cp", type="string", length=255, nullable=true)
     */
    private $cp;

    /**
     * @var string $ville
     *
     * @ORM\Column(name="ville", type="string", length=255, nullable=true)
     */
    private $ville;

    /**
     * @var string $tel
     *
     * @ORM\Column(name="tel", type="string", length=255, nullable=true)
     */
    private $tel;

    /**
     * @var string $mobile
     *
     * @ORM\Column(name="mobile", type="string", length=255, nullable=true)
     */
    private $mobile;

    /**
     * @var string $fax
     *
     * @ORM\Column(name="fax", type="string", length=255, nullable=true)
     */
    private $fax;

    /**
     * @var string $mail
     *
     * @ORM\Column(name="mail", type="string", length=255, nullable=true)
     */
    private $mail;

    /**
     * @var date $datenaissance
     *
     * @ORM\Column(name="datenaissance", type="date", nullable=true)
     */
    private $datenaissance;

    /**
     * @var string $lieunaissance
     *
     * @ORM\Column(name="lieunaissance", type="string", length=255, nullable=true)
     */
    private $lieunaissance;

    /**
     * @var string $situatfam
     *
     * @ORM\Column(name="situatfam", type="string", length=255, nullable=true)
     */
    private $situatfam;

    /**
     * @var string $nomjeunefille
     *
     * @ORM\Column(name="nomjeunefille", type="string", length=255, nullable=true)
     */
    private $nomjeunefille;

    /**
     * @var string $numsecu
     *
     * @ORM\Column(name="numsecu", type="string", length=255, nullable=true)
     */
    private $numsecu;

    /**
     * @var date $datevismed
     *
     * @ORM\Column(name="datevismed", type="date", nullable=true)
     */
    private $datevismed;

    /**
     * @var string $handicap
     *
     * @ORM\Column(name="handicap", type="string", length=255, nullable=true)
     */
    private $handicap;

    /**
     * @var string $annexesept
     *
     * @ORM\Column(name="annexesept", type="string", length=255, nullable=true)
     */
    private $annexesept;

    /**
     * @var decimal $txhoraire
     *
     * @ORM\Column(name="txhoraire", type="decimal", nullable=true)
     */
    private $txhoraire;

    /**
     * @var integer $echelon
     *
     * @ORM\Column(name="echelon", type="integer", nullable=true)
     */
    private $echelon;

    /**
     * @var integer $nivechelon
     *
     * @ORM\Column(name="nivechelon", type="integer", nullable=true)
     */
    private $nivechelon;

    /**
     * @var integer $anciennete
     *
     * @ORM\Column(name="anciennete", type="integer", nullable=true)
     */
    private $anciennete;

    /**
     * @var string $cartesejour
     *
     * @ORM\Column(name="cartesejour", type="string", length=255, nullable=true)
     */
    private $cartesejour;

    /**
     * @var string $autortravail
     *
     * @ORM\Column(name="autortravail", type="string", length=255, nullable=true)
     */
    private $autortravail;

    /**
     * @var string $carteident
     *
     * @ORM\Column(name="carteident", type="string", length=255, nullable=true)
     */
    private $carteident;

    /**
     * @var string $sexe
     *
     * @ORM\Column(name="sexe", type="string", length=255, nullable=true)
     */
    private $sexe;

    /**
     * @var string $perimetre
     *
     * @ORM\Column(name="perimetre", type="string", length=255, nullable=true)
     */
    private $perimetre;

    /**
     * @var string $nationalite
     *
     * @ORM\Column(name="nationalite", type="string", length=255, nullable=true)
     */
    private $nationalite;

    /**
     * @var string $permis
     *
     * @ORM\Column(name="permis", type="string", length=255, nullable=true)
     */
    private $permis;

    /**
     * @var string $vehicule
     *
     * @ORM\Column(name="vehicule", type="string", length=255, nullable=true)
     */
    private $vehicule;

    /**
     * @var string $complement
     *
     * @ORM\Column(name="complement", type="string", length=255, nullable=true)
     */
    private $complement;

    /**
     * @var string $attestation
     *
     * @ORM\Column(name="attestation", type="string", length=255, nullable=true)
     */
    private $attestation;

    /**
     * @var decimal $accompte
     *
     * @ORM\Column(name="accompte", type="decimal", nullable=true)
     */
    private $accompte;

    /**
     * @var decimal $annuledue
     *
     * @ORM\Column(name="annuledue", type="decimal", nullable=true)
     */
    private $annuledue;

    /**
     * @var date $datefincsj
     *
     * @ORM\Column(name="datefincsj", type="date", nullable=true)
     */
    private $datefincsj;

    /**
     * @var string $motifpi
     *
     * @ORM\Column(name="motifpi", type="string", length=255, nullable=true)
     */
    private $motifpi;

    /**
     * @var string $tailletshirt
     *
     * @ORM\Column(name="tailletshirt", type="string", length=255, nullable=true)
     */
    private $tailletshirt;

    /**
     * @var text $commentaires
     *
     * @ORM\Column(name="commentaires", type="text", nullable=true)
     */
    private $commentaires;
	
	/**
     * @var string $societeuser
     *
     * @ORM\Column(name="societeuser", type="string", length=255, nullable=true)
     */
    private $societeuser;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * Get prenom
     *
     * @return string 
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set typeposte
     *
     * @param string $typeposte
     */
    public function setTypeposte($typeposte)
    {
        $this->typeposte = $typeposte;
    }

    /**
     * Get typeposte
     *
     * @return string 
     */
    public function getTypeposte()
    {
        return $this->typeposte;
    }

    /**
     * Set adres1
     *
     * @param string $adres1
     */
    public function setAdres1($adres1)
    {
        $this->adres1 = $adres1;
    }

    /**
     * Get adres1
     *
     * @return string 
     */
    public function getAdres1()
    {
        return $this->adres1;
    }

    /**
     * Set adres2
     *
     * @param string $adres2
     */
    public function setAdres2($adres2)
    {
        $this->adres2 = $adres2;
    }

    /**
     * Get adres2
     *
     * @return string 
     */
    public function getAdres2()
    {
        return $this->adres2;
    }

    /**
     * Set cp
     *
     * @param string $cp
     */
    public function setCp($cp)
    {
        $this->cp = $cp;
    }

    /**
     * Get cp
     *
     * @return string 
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set ville
     *
     * @param string $ville
     */
    public function setVille($ville)
    {
        $this->ville = $ville;
    }

    /**
     * Get ville
     *
     * @return string 
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set tel
     *
     * @param string $tel
     */
    public function setTel($tel)
    {
        $this->tel = $tel;
    }

    /**
     * Get tel
     *
     * @return string 
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * Get mobile
     *
     * @return string 
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set fax
     *
     * @param string $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * Get fax
     *
     * @return string 
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set mail
     *
     * @param string $mail
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    /**
     * Get mail
     *
     * @return string 
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set datenaissance
     *
     * @param date $datenaissance
     */
    public function setDatenaissance($datenaissance)
    {
        $this->datenaissance = $datenaissance;
    }

    /**
     * Get datenaissance
     *
     * @return date 
     */
    public function getDatenaissance()
    {
        return $this->datenaissance;
    }

    /**
     * Set lieunaissance
     *
     * @param string $lieunaissance
     */
    public function setLieunaissance($lieunaissance)
    {
        $this->lieunaissance = $lieunaissance;
    }

    /**
     * Get lieunaissance
     *
     * @return string 
     */
    public function getLieunaissance()
    {
        return $this->lieunaissance;
    }

    /**
     * Set situatfam
     *
     * @param string $situatfam
     */
    public function setSituatfam($situatfam)
    {
        $this->situatfam = $situatfam;
    }

    /**
     * Get situatfam
     *
     * @return string 
     */
    public function getSituatfam()
    {
        return $this->situatfam;
    }

    /**
     * Set nomjeunefille
     *
     * @param string $nomjeunefille
     */
    public function setNomjeunefille($nomjeunefille)
    {
        $this->nomjeunefille = $nomjeunefille;
    }

    /**
     * Get nomjeunefille
     *
     * @return string 
     */
    public function getNomjeunefille()
    {
        return $this->nomjeunefille;
    }

    /**
     * Set numsecu
     *
     * @param string $numsecu
     */
    public function setNumsecu($numsecu)
    {
        $this->numsecu = $numsecu;
    }

    /**
     * Get numsecu
     *
     * @return string 
     */
    public function getNumsecu()
    {
        return $this->numsecu;
    }

    /**
     * Set datevismed
     *
     * @param date $datevismed
     */
    public function setDatevismed($datevismed)
    {
        $this->datevismed = $datevismed;
    }

    /**
     * Get datevismed
     *
     * @return date 
     */
    public function getDatevismed()
    {
        return $this->datevismed;
    }

    /**
     * Set handicap
     *
     * @param string $handicap
     */
    public function setHandicap($handicap)
    {
        $this->handicap = $handicap;
    }

    /**
     * Get handicap
     *
     * @return string 
     */
    public function getHandicap()
    {
        return $this->handicap;
    }

    /**
     * Set annexesept
     *
     * @param string $annexesept
     */
    public function setAnnexesept($annexesept)
    {
        $this->annexesept = $annexesept;
    }

    /**
     * Get annexesept
     *
     * @return string 
     */
    public function getAnnexesept()
    {
        return $this->annexesept;
    }

    /**
     * Set txhoraire
     *
     * @param decimal $txhoraire
     */
    public function setTxhoraire($txhoraire)
    {
        $this->txhoraire = $txhoraire;
    }

    /**
     * Get txhoraire
     *
     * @return decimal 
     */
    public function getTxhoraire()
    {
        return $this->txhoraire;
    }

    /**
     * Set echelon
     *
     * @param integer $echelon
     */
    public function setEchelon($echelon)
    {
        $this->echelon = $echelon;
    }

    /**
     * Get echelon
     *
     * @return integer 
     */
    public function getEchelon()
    {
        return $this->echelon;
    }

    /**
     * Set nivechelon
     *
     * @param integer $nivechelon
     */
    public function setNivechelon($nivechelon)
    {
        $this->nivechelon = $nivechelon;
    }

    /**
     * Get nivechelon
     *
     * @return integer 
     */
    public function getNivechelon()
    {
        return $this->nivechelon;
    }

    /**
     * Set anciennete
     *
     * @param integer $anciennete
     */
    public function setAnciennete($anciennete)
    {
        $this->anciennete = $anciennete;
    }

    /**
     * Get anciennete
     *
     * @return integer 
     */
    public function getAnciennete()
    {
        return $this->anciennete;
    }

    /**
     * Set cartesejour
     *
     * @param string $cartesejour
     */
    public function setCartesejour($cartesejour)
    {
        $this->cartesejour = $cartesejour;
    }

    /**
     * Get cartesejour
     *
     * @return string 
     */
    public function getCartesejour()
    {
        return $this->cartesejour;
    }

    /**
     * Set autortravail
     *
     * @param string $autortravail
     */
    public function setAutortravail($autortravail)
    {
        $this->autortravail = $autortravail;
    }

    /**
     * Get autortravail
     *
     * @return string 
     */
    public function getAutortravail()
    {
        return $this->autortravail;
    }

    /**
     * Set carteident
     *
     * @param string $carteident
     */
    public function setCarteident($carteident)
    {
        $this->carteident = $carteident;
    }

    /**
     * Get carteident
     *
     * @return string 
     */
    public function getCarteident()
    {
        return $this->carteident;
    }

    /**
     * Set sexe
     *
     * @param string $sexe
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;
    }

    /**
     * Get sexe
     *
     * @return string 
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * Set perimetre
     *
     * @param string $perimetre
     */
    public function setPerimetre($perimetre)
    {
        $this->perimetre = $perimetre;
    }

    /**
     * Get perimetre
     *
     * @return string 
     */
    public function getPerimetre()
    {
        return $this->perimetre;
    }

    /**
     * Set nationalite
     *
     * @param string $nationalite
     */
    public function setNationalite($nationalite)
    {
        $this->nationalite = $nationalite;
    }

    /**
     * Get nationalite
     *
     * @return string 
     */
    public function getNationalite()
    {
        return $this->nationalite;
    }

    /**
     * Set permis
     *
     * @param string $permis
     */
    public function setPermis($permis)
    {
        $this->permis = $permis;
    }

    /**
     * Get permis
     *
     * @return string 
     */
    public function getPermis()
    {
        return $this->permis;
    }

    /**
     * Set vehicule
     *
     * @param string $vehicule
     */
    public function setVehicule($vehicule)
    {
        $this->vehicule = $vehicule;
    }

    /**
     * Get vehicule
     *
     * @return string 
     */
    public function getVehicule()
    {
        return $this->vehicule;
    }

    /**
     * Set complement
     *
     * @param string $complement
     */
    public function setComplement($complement)
    {
        $this->complement = $complement;
    }

    /**
     * Get complement
     *
     * @return string 
     */
    public function getComplement()
    {
        return $this->complement;
    }

    /**
     * Set attestation
     *
     * @param string $attestation
     */
    public function setAttestation($attestation)
    {
        $this->attestation = $attestation;
    }

    /**
     * Get attestation
     *
     * @return string 
     */
    public function getAttestation()
    {
        return $this->attestation;
    }

    /**
     * Set accompte
     *
     * @param decimal $accompte
     */
    public function setAccompte($accompte)
    {
        $this->accompte = $accompte;
    }

    /**
     * Get accompte
     *
     * @return decimal 
     */
    public function getAccompte()
    {
        return $this->accompte;
    }

    /**
     * Set annuledue
     *
     * @param decimal $annuledue
     */
    public function setAnnuledue($annuledue)
    {
        $this->annuledue = $annuledue;
    }

    /**
     * Get annuledue
     *
     * @return decimal 
     */
    public function getAnnuledue()
    {
        return $this->annuledue;
    }

    /**
     * Set datefincsj
     *
     * @param date $datefincsj
     */
    public function setDatefincsj($datefincsj)
    {
        $this->datefincsj = $datefincsj;
    }

    /**
     * Get datefincsj
     *
     * @return date 
     */
    public function getDatefincsj()
    {
        return $this->datefincsj;
    }

    /**
     * Set motifpi
     *
     * @param string $motifpi
     */
    public function setMotifpi($motifpi)
    {
        $this->motifpi = $motifpi;
    }

    /**
     * Get motifpi
     *
     * @return string 
     */
    public function getMotifpi()
    {
        return $this->motifpi;
    }

    /**
     * Set tailletshirt
     *
     * @param string $tailletshirt
     */
    public function setTailletshirt($tailletshirt)
    {
        $this->tailletshirt = $tailletshirt;
    }

    /**
     * Get tailletshirt
     *
     * @return string 
     */
    public function getTailletshirt()
    {
        return $this->tailletshirt;
    }

    /**
     * Set commentaires
     *
     * @param text $commentaires
     */
    public function setCommentaires($commentaires)
    {
        $this->commentaires = $commentaires;
    }

    /**
     * Get commentaires
     *
     * @return text 
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }
    public function __construct()
    {
        $this->departements = new \Doctrine\Common\Collections\ArrayCollection();
    }
	
	/**
     * Set societeuser
     *
     * @param string $societeuser
     */
    public function setSocieteuser($societeuser)
    {
        $this->societeuser = $societeuser;
    }

    /**
     * Get societeuser
     *
     * @return string 
     */
    public function getSocieteuser()
    {
        return $this->societeuser;
    }
    
    /**
     * Add departements
     *
     * @param Phareos\DeskNetServiceBundle\Entity\dept $departements
     */
    public function adddept(\Phareos\DeskNetServiceBundle\Entity\dept $departements)
    {
        $this->departements[] = $departements;
    }

    /**
     * Get departements
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getDepartements()
    {
        return $this->departements;
    }
	

    /**
     * Add collabcontrat
     *
     * @param Phareos\DeskNetServiceBundle\Entity\contrats $collabcontrat
     */
    public function addcontrats(\Phareos\DeskNetServiceBundle\Entity\contrats $collabcontrat)
    {
        $this->collabcontrat[] = $collabcontrat;
    }

    /**
     * Get collabcontrat
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getCollabcontrat()
    {
        return $this->collabcontrat;
    }

    /**
     * Add collabavenant
     *
     * @param Phareos\DeskNetServiceBundle\Entity\avenant $collabavenant
     */
    public function addavenant(\Phareos\DeskNetServiceBundle\Entity\avenant $collabavenant)
    {
        $this->collabavenant[] = $collabavenant;
    }

    /**
     * Get collabavenant
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getCollabavenant()
    {
        return $this->collabavenant;
    }
}