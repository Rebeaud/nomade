<?php

namespace Phareos\DeskNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\DeskNetServiceBundle\Entity\client
 *
 * @ORM\Table(name="desk_client")
 * @ORM\Entity(repositoryClass="Phareos\DeskNetServiceBundle\Entity\clientRepository")
 */
class client
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="grdcpte", inversedBy="grdcpteclient", cascade={"remove"})
	 * @ORM\JoinColumn(name="grdcpte_id", referencedColumnName="id")
	 */
	protected $grdcpte;
	
	/**
	 * @ORM\ManyToMany(targetEntity="dept", cascade={"persist"})
	 */
	private $departements;
	
    /**
     * @var string $nom
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string $adres1
     *
     * @ORM\Column(name="adres1", type="string", length=255, nullable=true)
     */
    private $adres1;

    /**
     * @var string $adres2
     *
     * @ORM\Column(name="adres2", type="string", length=255, nullable=true)
     */
    private $adres2;

    /**
     * @var string $cp
     *
     * @ORM\Column(name="cp", type="string", length=255, nullable=true)
     */
    private $cp;

    /**
     * @var string $ville
     *
     * @ORM\Column(name="ville", type="string", length=255, nullable=true)
     */
    private $ville;

    /**
     * @var string $tel
     *
     * @ORM\Column(name="tel", type="string", length=255, nullable=true)
     */
    private $tel;

    /**
     * @var string $tel2
     *
     * @ORM\Column(name="tel2", type="string", length=255, nullable=true)
     */
    private $tel2;

    /**
     * @var string $tel3
     *
     * @ORM\Column(name="tel3", type="string", length=255, nullable=true)
     */
    private $tel3;

    /**
     * @var string $fax
     *
     * @ORM\Column(name="fax", type="string", length=255, nullable=true)
     */
    private $fax;

    /**
     * @var string $mail
     *
     * @ORM\Column(name="mail", type="string", length=255, nullable=true)
     */
    private $mail;

    /**
     * @var string $nummag
     *
     * @ORM\Column(name="nummag", type="string", length=255, nullable=true)
     */
    private $nummag;

    /**
     * @var string $civilrespsite
     *
     * @ORM\Column(name="civilrespsite", type="string", length=255, nullable=true)
     */
    private $civilrespsite;

    /**
     * @var string $nomresposite
     *
     * @ORM\Column(name="nomresposite", type="string", length=255, nullable=true)
     */
    private $nomresposite;

    /**
     * @var string $prenomrespsite
     *
     * @ORM\Column(name="prenomrespsite", type="string", length=255, nullable=true)
     */
    private $prenomrespsite;

    /**
     * @var string $telrespsite
     *
     * @ORM\Column(name="telrespsite", type="string", length=255, nullable=true)
     */
    private $telrespsite;

    /**
     * @var string $mailrespsite
     *
     * @ORM\Column(name="mailrespsite", type="string", length=255, nullable=true)
     */
    private $mailrespsite;

    /**
     * @var integer $nbrcolab
     *
     * @ORM\Column(name="nbrcolab", type="integer", nullable=true)
     */
    private $nbrcolab;

    /**
     * @var string $nomcolab
     *
     * @ORM\Column(name="nomcolab", type="string", length=255, nullable=true)
     */
    private $nomcolab;

    /**
     * @var string $typeinter
     *
     * @ORM\Column(name="typeinter", type="string", length=255, nullable=true)
     */
    private $typeinter;

    /**
     * @var string $intervautre
     *
     * @ORM\Column(name="intervautre", type="string", length=255, nullable=true)
     */
    private $intervautre;

    /**
     * @var string $nominterv
     *
     * @ORM\Column(name="nominterv", type="string", length=255, nullable=true)
     */
    private $nominterv;

    /**
     * @var string $frequenceinterv
     *
     * @ORM\Column(name="frequenceinterv", type="string", length=255, nullable=true)
     */
    private $frequenceinterv;

    /**
     * @var integer $surfacevit
     *
     * @ORM\Column(name="surfacevit", type="integer", nullable=true)
     */
    private $surfacevit;

    /**
     * @var integer $surfacebo
     *
     * @ORM\Column(name="surfacebo", type="integer", nullable=true)
     */
    private $surfacebo;

    /**
     * @var integer $surfacetapis
     *
     * @ORM\Column(name="surfacetapis", type="integer", nullable=true)
     */
    private $surfacetapis;

    /**
     * @var integer $surfacemag
     *
     * @ORM\Column(name="surfacemag", type="integer", nullable=true)
     */
    private $surfacemag;

    /**
     * @var integer $nbrenseignes
     *
     * @ORM\Column(name="nbrenseignes", type="integer", nullable=true)
     */
    private $nbrenseignes;

    /**
     * @var decimal $longenseigne
     *
     * @ORM\Column(name="longenseigne", type="decimal", nullable=true)
     */
    private $longenseigne;

    /**
     * @var date $datemisenplace
     *
     * @ORM\Column(name="datemisenplace", type="date", nullable=true)
     */
    private $datemisenplace;

    /**
     * @var string $installdistri
     *
     * @ORM\Column(name="installdistri", type="string", length=255, nullable=true)
     */
    private $installdistri;

    /**
     * @var string $cdb
     *
     * @ORM\Column(name="cdb", type="string", length=255, nullable=true)
     */
    private $cdb;

    /**
     * @var string $planprev
     *
     * @ORM\Column(name="planprev", type="string", length=255, nullable=true)
     */
    private $planprev;

    /**
     * @var string $dept
     *
     * @ORM\Column(name="dept", type="string", length=255, nullable=true)
     */
    private $dept;

    /**
     * @var string $societeuser
     *
     * @ORM\Column(name="societeuser", type="string", length=255, nullable=true)
     */
    private $societeuser;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set adres1
     *
     * @param string $adres1
     */
    public function setAdres1($adres1)
    {
        $this->adres1 = $adres1;
    }

    /**
     * Get adres1
     *
     * @return string 
     */
    public function getAdres1()
    {
        return $this->adres1;
    }

    /**
     * Set adres2
     *
     * @param string $adres2
     */
    public function setAdres2($adres2)
    {
        $this->adres2 = $adres2;
    }

    /**
     * Get adres2
     *
     * @return string 
     */
    public function getAdres2()
    {
        return $this->adres2;
    }

    /**
     * Set cp
     *
     * @param string $cp
     */
    public function setCp($cp)
    {
        $this->cp = $cp;
    }

    /**
     * Get cp
     *
     * @return string 
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set ville
     *
     * @param string $ville
     */
    public function setVille($ville)
    {
        $this->ville = $ville;
    }

    /**
     * Get ville
     *
     * @return string 
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set tel
     *
     * @param string $tel
     */
    public function setTel($tel)
    {
        $this->tel = $tel;
    }

    /**
     * Get tel
     *
     * @return string 
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set tel2
     *
     * @param string $tel2
     */
    public function setTel2($tel2)
    {
        $this->tel2 = $tel2;
    }

    /**
     * Get tel2
     *
     * @return string 
     */
    public function getTel2()
    {
        return $this->tel2;
    }

    /**
     * Set tel3
     *
     * @param string $tel3
     */
    public function setTel3($tel3)
    {
        $this->tel3 = $tel3;
    }

    /**
     * Get tel3
     *
     * @return string 
     */
    public function getTel3()
    {
        return $this->tel3;
    }

    /**
     * Set fax
     *
     * @param string $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * Get fax
     *
     * @return string 
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set mail
     *
     * @param string $mail
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    /**
     * Get mail
     *
     * @return string 
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set nummag
     *
     * @param string $nummag
     */
    public function setNummag($nummag)
    {
        $this->nummag = $nummag;
    }

    /**
     * Get nummag
     *
     * @return string 
     */
    public function getNummag()
    {
        return $this->nummag;
    }

    /**
     * Set civilrespsite
     *
     * @param string $civilrespsite
     */
    public function setCivilrespsite($civilrespsite)
    {
        $this->civilrespsite = $civilrespsite;
    }

    /**
     * Get civilrespsite
     *
     * @return string 
     */
    public function getCivilrespsite()
    {
        return $this->civilrespsite;
    }

    /**
     * Set nomresposite
     *
     * @param string $nomresposite
     */
    public function setNomresposite($nomresposite)
    {
        $this->nomresposite = $nomresposite;
    }

    /**
     * Get nomresposite
     *
     * @return string 
     */
    public function getNomresposite()
    {
        return $this->nomresposite;
    }

    /**
     * Set prenomrespsite
     *
     * @param string $prenomrespsite
     */
    public function setPrenomrespsite($prenomrespsite)
    {
        $this->prenomrespsite = $prenomrespsite;
    }

    /**
     * Get prenomrespsite
     *
     * @return string 
     */
    public function getPrenomrespsite()
    {
        return $this->prenomrespsite;
    }

    /**
     * Set telrespsite
     *
     * @param string $telrespsite
     */
    public function setTelrespsite($telrespsite)
    {
        $this->telrespsite = $telrespsite;
    }

    /**
     * Get telrespsite
     *
     * @return string 
     */
    public function getTelrespsite()
    {
        return $this->telrespsite;
    }

    /**
     * Set mailrespsite
     *
     * @param string $mailrespsite
     */
    public function setMailrespsite($mailrespsite)
    {
        $this->mailrespsite = $mailrespsite;
    }

    /**
     * Get mailrespsite
     *
     * @return string 
     */
    public function getMailrespsite()
    {
        return $this->mailrespsite;
    }

    /**
     * Set nbrcolab
     *
     * @param integer $nbrcolab
     */
    public function setNbrcolab($nbrcolab)
    {
        $this->nbrcolab = $nbrcolab;
    }

    /**
     * Get nbrcolab
     *
     * @return integer 
     */
    public function getNbrcolab()
    {
        return $this->nbrcolab;
    }

    /**
     * Set nomcolab
     *
     * @param string $nomcolab
     */
    public function setNomcolab($nomcolab)
    {
        $this->nomcolab = $nomcolab;
    }

    /**
     * Get nomcolab
     *
     * @return string 
     */
    public function getNomcolab()
    {
        return $this->nomcolab;
    }

    /**
     * Set typeinter
     *
     * @param string $typeinter
     */
    public function setTypeinter($typeinter)
    {
        $this->typeinter = $typeinter;
    }

    /**
     * Get typeinter
     *
     * @return string 
     */
    public function getTypeinter()
    {
        return $this->typeinter;
    }

    /**
     * Set intervautre
     *
     * @param string $intervautre
     */
    public function setIntervautre($intervautre)
    {
        $this->intervautre = $intervautre;
    }

    /**
     * Get intervautre
     *
     * @return string 
     */
    public function getIntervautre()
    {
        return $this->intervautre;
    }

    /**
     * Set nominterv
     *
     * @param string $nominterv
     */
    public function setNominterv($nominterv)
    {
        $this->nominterv = $nominterv;
    }

    /**
     * Get nominterv
     *
     * @return string 
     */
    public function getNominterv()
    {
        return $this->nominterv;
    }

    /**
     * Set frequenceinterv
     *
     * @param string $frequenceinterv
     */
    public function setFrequenceinterv($frequenceinterv)
    {
        $this->frequenceinterv = $frequenceinterv;
    }

    /**
     * Get frequenceinterv
     *
     * @return string 
     */
    public function getFrequenceinterv()
    {
        return $this->frequenceinterv;
    }

    /**
     * Set surfacevit
     *
     * @param integer $surfacevit
     */
    public function setSurfacevit($surfacevit)
    {
        $this->surfacevit = $surfacevit;
    }

    /**
     * Get surfacevit
     *
     * @return integer 
     */
    public function getSurfacevit()
    {
        return $this->surfacevit;
    }

    /**
     * Set surfacebo
     *
     * @param integer $surfacebo
     */
    public function setSurfacebo($surfacebo)
    {
        $this->surfacebo = $surfacebo;
    }

    /**
     * Get surfacebo
     *
     * @return integer 
     */
    public function getSurfacebo()
    {
        return $this->surfacebo;
    }

    /**
     * Set surfacetapis
     *
     * @param integer $surfacetapis
     */
    public function setSurfacetapis($surfacetapis)
    {
        $this->surfacetapis = $surfacetapis;
    }

    /**
     * Get surfacetapis
     *
     * @return integer 
     */
    public function getSurfacetapis()
    {
        return $this->surfacetapis;
    }

    /**
     * Set surfacemag
     *
     * @param integer $surfacemag
     */
    public function setSurfacemag($surfacemag)
    {
        $this->surfacemag = $surfacemag;
    }

    /**
     * Get surfacemag
     *
     * @return integer 
     */
    public function getSurfacemag()
    {
        return $this->surfacemag;
    }

    /**
     * Set nbrenseignes
     *
     * @param integer $nbrenseignes
     */
    public function setNbrenseignes($nbrenseignes)
    {
        $this->nbrenseignes = $nbrenseignes;
    }

    /**
     * Get nbrenseignes
     *
     * @return integer 
     */
    public function getNbrenseignes()
    {
        return $this->nbrenseignes;
    }

    /**
     * Set longenseigne
     *
     * @param decimal $longenseigne
     */
    public function setLongenseigne($longenseigne)
    {
        $this->longenseigne = $longenseigne;
    }

    /**
     * Get longenseigne
     *
     * @return decimal 
     */
    public function getLongenseigne()
    {
        return $this->longenseigne;
    }

    /**
     * Set datemisenplace
     *
     * @param date $datemisenplace
     */
    public function setDatemisenplace($datemisenplace)
    {
        $this->datemisenplace = $datemisenplace;
    }

    /**
     * Get datemisenplace
     *
     * @return date 
     */
    public function getDatemisenplace()
    {
        return $this->datemisenplace;
    }

    /**
     * Set installdistri
     *
     * @param string $installdistri
     */
    public function setInstalldistri($installdistri)
    {
        $this->installdistri = $installdistri;
    }

    /**
     * Get installdistri
     *
     * @return string 
     */
    public function getInstalldistri()
    {
        return $this->installdistri;
    }

    /**
     * Set cdb
     *
     * @param string $cdb
     */
    public function setCdb($cdb)
    {
        $this->cdb = $cdb;
    }

    /**
     * Get cdb
     *
     * @return string 
     */
    public function getCdb()
    {
        return $this->cdb;
    }

    /**
     * Set planprev
     *
     * @param string $planprev
     */
    public function setPlanprev($planprev)
    {
        $this->planprev = $planprev;
    }

    /**
     * Get planprev
     *
     * @return string 
     */
    public function getPlanprev()
    {
        return $this->planprev;
    }

    /**
     * Set dept
     *
     * @param string $dept
     */
    public function setDept($dept)
    {
        $this->dept = $dept;
    }

    /**
     * Get dept
     *
     * @return string 
     */
    public function getDept()
    {
        return $this->dept;
    }

    /**
     * Set societeuser
     *
     * @param string $societeuser
     */
    public function setSocieteuser($societeuser)
    {
        $this->societeuser = $societeuser;
    }

    /**
     * Get societeuser
     *
     * @return string 
     */
    public function getSocieteuser()
    {
        return $this->societeuser;
    }

    /**
     * Set grdcpte
     *
     * @param Phareos\DeskNetServiceBundle\Entity\grdcpte $grdcpte
     */
    public function setGrdcpte(\Phareos\DeskNetServiceBundle\Entity\grdcpte $grdcpte)
    {
        $this->grdcpte = $grdcpte;
    }

    /**
     * Get grdcpte
     *
     * @return Phareos\DeskNetServiceBundle\Entity\grdcpte 
     */
    public function getGrdcpte()
    {
        return $this->grdcpte;
    }
	
	public function __construct()
    {
        $this->departements = new \Doctrine\Common\Collections\ArrayCollection();
    }
	
	/**
     * Add departements
     *
     * @param Phareos\DeskNetServiceBundle\Entity\dept $departements
     */
    public function adddept(\Phareos\DeskNetServiceBundle\Entity\dept $departements)
    {
        $this->departements[] = $departements;
    }

    /**
     * Get departements
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getDepartements()
    {
        return $this->departements;
    }
	
		
}