<?php

namespace Phareos\DeskNetServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\DeskNetServiceBundle\Entity\avenant;
use Phareos\DeskNetServiceBundle\Form\avenantType;

/**
 * avenant controller.
 *
 */
class avenantController extends Controller
{
    /**
     * Lists all avenant entities.
     *
     */
    public function indexAction()
    {
        $session = $this->get('session');
		$Dept = $session->get('nomDEPT');
		$societeUSER = $session->get('societeUSER');
		
		$em = $this->getDoctrine()->getEntityManager();

        
		
		$repository_dept = $em->getRepository('PhareosDeskNetServiceBundle:dept');
		
		$depts = $repository_dept->findBy(array('client' => $societeUSER));
		
		

        $entities = $em->getRepository('PhareosDeskNetServiceBundle:avenant')->findAll();

        return $this->render('PhareosDeskNetServiceBundle:avenant:index.html.twig', array(
            'depts' => $depts,
			'defaultDept' => $Dept,
			'entities' => $entities
        ));
    }

    /**
     * Finds and displays a avenant entity.
     *
     */
    public function showAction($id)
    {
        $session = $this->get('session');
		$Dept = $session->get('nomDEPT');
		$societeUSER = $session->get('societeUSER');
		
		$em = $this->getDoctrine()->getEntityManager();
		
		$repository_dept = $em->getRepository('PhareosDeskNetServiceBundle:dept');

        $entity = $em->getRepository('PhareosDeskNetServiceBundle:avenant')->find($id);
		
		$depts = $repository_dept->findBy(array('client' => $societeUSER));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find avenant entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosDeskNetServiceBundle:avenant:show.html.twig', array(
            'depts' => $depts,
			'defaultDept' => $Dept,
			'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new avenant entity.
     *
     */
    public function newAction()
    {
        $session = $this->get('session');
		$Dept = $session->get('nomDEPT');
		$societeUSER = $session->get('societeUSER');
		
		$em = $this->getDoctrine()->getEntityManager();
		
		$repository_dept = $em->getRepository('PhareosDeskNetServiceBundle:dept');
		
		$depts = $repository_dept->findBy(array('client' => $societeUSER));
		
		$entity = new avenant();
        $form   = $this->createForm(new avenantType(), $entity);

        return $this->render('PhareosDeskNetServiceBundle:avenant:new.html.twig', array(
            'depts' => $depts,
			'defaultDept' => $Dept,
			'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new avenant entity.
     *
     */
    public function createAction()
    {
        $entity  = new avenant();
        $request = $this->getRequest();
        $form    = $this->createForm(new avenantType(), $entity);
        $form->bindRequest($request);
		
		$datedemande = $request->request->get('datedemande');
		$datedemande2 = new \DateTime($datedemande);
		
		$dateok = $request->request->get('dateok');
		$dateok2 = new \DateTime($dateok);
		
		$datecrea = $request->request->get('datecrea');
		$datecrea2 = new \DateTime($datecrea);
		
		$datemaj = $request->request->get('datemaj');
		$datemaj2 = new \DateTime($datemaj);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
			
			$entity->setDatedemande($datedemande2);
			$entity->setDateok($dateok2);
			$entity->setDatecrea($datecrea2);
			$entity->setDatemaj($datemaj2);
			
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('avenant_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosDeskNetServiceBundle:avenant:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing avenant entity.
     *
     */
    public function editAction($id)
    {
        $session = $this->get('session');
		$Dept = $session->get('nomDEPT');
		$societeUSER = $session->get('societeUSER');
		
		$em = $this->getDoctrine()->getEntityManager();
		
		$repository_dept = $em->getRepository('PhareosDeskNetServiceBundle:dept');

        $entity = $em->getRepository('PhareosDeskNetServiceBundle:avenant')->find($id);
		
		$depts = $repository_dept->findBy(array('client' => $societeUSER));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find avenant entity.');
        }

        $editForm = $this->createForm(new avenantType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosDeskNetServiceBundle:avenant:edit.html.twig', array(
            'depts' => $depts,
			'defaultDept' => $Dept,
			'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing avenant entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosDeskNetServiceBundle:avenant')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find avenant entity.');
        }

        $editForm   = $this->createForm(new avenantType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('avenant_edit', array('id' => $id)));
        }

        return $this->render('PhareosDeskNetServiceBundle:avenant:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a avenant entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosDeskNetServiceBundle:avenant')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find avenant entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('avenant'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
