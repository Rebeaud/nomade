<?php

namespace Phareos\DeskNetServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\DeskNetServiceBundle\Entity\contrats;
use Phareos\DeskNetServiceBundle\Form\contratsType;


/**
 * contrats controller.
 *
 */
class contratsController extends Controller
{
    /**
     * Lists all contrats entities.
     *
     */
    public function indexAction()
    {
        $session = $this->get('session');
		$Dept = $session->get('nomDEPT');
		$societeUSER = $session->get('societeUSER');
		
		$em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosDeskNetServiceBundle:contrats')->findAll();
		
		$repository_dept = $em->getRepository('PhareosDeskNetServiceBundle:dept');
		
		$depts = $repository_dept->findBy(array('client' => $societeUSER));

        return $this->render('PhareosDeskNetServiceBundle:contrats:index.html.twig', array(
            'depts' => $depts,
			'defaultDept' => $Dept,
			'entities' => $entities
        ));
    }

    /**
     * Finds and displays a contrats entity.
     *
     */
    public function showAction($id)
    {
        $session = $this->get('session');
		$Dept = $session->get('nomDEPT');
		$societeUSER = $session->get('societeUSER');
		
		$em = $this->getDoctrine()->getEntityManager();
		
		$repository_dept = $em->getRepository('PhareosDeskNetServiceBundle:dept');

        $entity = $em->getRepository('PhareosDeskNetServiceBundle:contrats')->find($id);
		
		$depts = $repository_dept->findBy(array('client' => $societeUSER));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find contrats entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosDeskNetServiceBundle:contrats:show.html.twig', array(
            'depts' => $depts,
			'defaultDept' => $Dept,
			'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new contrats entity.
     *
     */
    public function newAction()
    {
        $session = $this->get('session');
		$Dept = $session->get('nomDEPT');
		$societeUSER = $session->get('societeUSER');
		
		$em = $this->getDoctrine()->getEntityManager();
		
		$repository_dept = $em->getRepository('PhareosDeskNetServiceBundle:dept');
		
		$depts = $repository_dept->findBy(array('client' => $societeUSER));
		
		$entity = new contrats();
        $form   = $this->createForm(new contratsType(), $entity);

        return $this->render('PhareosDeskNetServiceBundle:contrats:new.html.twig', array(
            'depts' => $depts,
			'defaultDept' => $Dept,
			'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new contrats entity.
     *
     */
    public function createAction()
    {
        $entity  = new contrats();
        $request = $this->getRequest();
        $form    = $this->createForm(new contratsType(), $entity);
        $form->bindRequest($request);
		
		$dateemb = $request->request->get('dateemb');
		$dateemb2 = new \DateTime($dateemb);
		
		$datecont = $request->request->get('datecont');
		$datecont2 = new \DateTime($datecont);
		
		$dateenvoi = $request->request->get('dateenvoi');
		if ($dateenvoi == '00-00-0000')
		{
			$dateenvoi2 = null;
		}
		else
		{
			$dateenvoi2 = new \DateTime($dateenvoi);
		}
		
		$daterenv = $request->request->get('daterenv');
		if ($daterenv == '00-00-0000')
		{
			$daterenv2 = null;
		}
		else
		{
			$daterenv2 = new \DateTime($daterenv);
		}
		
		
		$datefin = $request->request->get('datefin');
		if ($datefin == '00-00-0000')
		{
			$datefin2 = null;
		}
		else
		{
			$datefin2 = new \DateTime($datefin);
		}
		

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
			
			$entity->setDateemb($dateemb2);
			$entity->setDatecont($datecont2);
			$entity->setDateenvoi($dateenvoi2);
			$entity->setDaterenv($daterenv2);
			$entity->setDatefin($datefin2);
			
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('contrats_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosDeskNetServiceBundle:contrats:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing contrats entity.
     *
     */
    public function editAction($id)
    {
        $session = $this->get('session');
		$Dept = $session->get('nomDEPT');
		$societeUSER = $session->get('societeUSER');
		
		$em = $this->getDoctrine()->getEntityManager();
		
		$repository_dept = $em->getRepository('PhareosDeskNetServiceBundle:dept');

        $entity = $em->getRepository('PhareosDeskNetServiceBundle:contrats')->find($id);
		
		$depts = $repository_dept->findBy(array('client' => $societeUSER));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find contrats entity.');
        }

        $editForm = $this->createForm(new contratsType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosDeskNetServiceBundle:contrats:edit.html.twig', array(
            'depts' => $depts,
			'defaultDept' => $Dept,
			'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing contrats entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosDeskNetServiceBundle:contrats')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find contrats entity.');
        }

        $editForm   = $this->createForm(new contratsType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('contrats_edit', array('id' => $id)));
        }

        return $this->render('PhareosDeskNetServiceBundle:contrats:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a contrats entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosDeskNetServiceBundle:contrats')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find contrats entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('contrats'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
