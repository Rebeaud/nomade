<?php

namespace Phareos\DeskNetServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    
    public function indexAction()
    {	
		$session = $this->get('session');
		$societeUSER = $session->get('societeUSER');
		
		$em = $this->getDoctrine()->getEntityManager();
		
		$repository_dept = $em->getRepository('PhareosDeskNetServiceBundle:dept');
		
		$depts = $repository_dept->findBy(array('client' => $societeUSER));
		//$depts = $repository_dept->findAll();
		
		return $this->render('PhareosDeskNetServiceBundle:Default:index.html.twig', array(
			'depts' => $depts,
			'defaultDept' => 'Tous'
        ));
    }
}
