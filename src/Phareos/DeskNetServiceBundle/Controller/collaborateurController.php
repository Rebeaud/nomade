<?php

namespace Phareos\DeskNetServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Phareos\DeskNetServiceBundle\Entity\collaborateur;
use Phareos\DeskNetServiceBundle\Form\collaborateurType;

/**
 * collaborateur controller.
 *
 */
class collaborateurController extends Controller
{
    /**
     * Lists all collaborateur entities.
     *
     */
    public function indexAction()
    {
        $session = $this->get('session');
		$Dept = $session->get('nomDEPT');
		$societeUSER = $session->get('societeUSER');
		
		$em = $this->getDoctrine()->getEntityManager();
		
		if($Dept == 'Tous')
		{
			$entities = $em->getRepository('PhareosDeskNetServiceBundle:collaborateur')->findBy(array('societeuser' => $societeUSER));
		}
		else
		{
			$entities = $em -> getRepository('PhareosDeskNetServiceBundle:collaborateur')
							-> getAvecDept(array ($Dept), $societeUSER);;
		}

		
		$repository_dept = $em->getRepository('PhareosDeskNetServiceBundle:dept');
		
		$depts = $repository_dept->findBy(array('client' => $societeUSER));

        return $this->render('PhareosDeskNetServiceBundle:collaborateur:index.html.twig', array(
            'depts' => $depts,
			'defaultDept' => $Dept,
			'entities' => $entities
        ));
    }

    /**
     * Finds and displays a collaborateur entity.
     *
     */
    public function showAction($id)
    {
        $session = $this->get('session');
		$Dept = $session->get('nomDEPT');
		$societeUSER = $session->get('societeUSER');
		
		$em = $this->getDoctrine()->getEntityManager();
		
		$repository_dept = $em->getRepository('PhareosDeskNetServiceBundle:dept');
		
		$depts = $repository_dept->findBy(array('client' => $societeUSER));

        $entity = $em->getRepository('PhareosDeskNetServiceBundle:collaborateur')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find collaborateur entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosDeskNetServiceBundle:collaborateur:show.html.twig', array(
            'depts' => $depts,
			'defaultDept' => $Dept,
			'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new collaborateur entity.
     *
     */
    public function newAction()
    {
        
		$session = $this->get('session');
		$Dept = $session->get('nomDEPT');
		$societeUSER = $session->get('societeUSER');
		
		$em = $this->getDoctrine()->getEntityManager();
		
		$repository_dept = $em->getRepository('PhareosDeskNetServiceBundle:dept');
		
		$depts = $repository_dept->findBy(array('client' => $societeUSER));
		
		$entity = new collaborateur();
		$_SESSION['societe'] = $societeUSER;
        $form   = $this->createForm(new collaborateurType(), $entity);

        return $this->render('PhareosDeskNetServiceBundle:collaborateur:new.html.twig', array(
            'depts' => $depts,
			'defaultDept' => $Dept,
			'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new collaborateur entity.
     *
     */
    public function createAction()
    {
        $entity  = new collaborateur();
        $request = $this->getRequest();
        $form    = $this->createForm(new collaborateurType(), $entity);
        $form->bindRequest($request);
		
		$session = $this->get('session');
		$Dept = $session->get('nomDEPT');
		$societeUSER = $session->get('societeUSER');

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
			$entity->setSocieteuser($societeUSER);
			
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('collaborateur_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosDeskNetServiceBundle:collaborateur:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing collaborateur entity.
     *
     */
    public function editAction($id)
    {
        $session = $this->get('session');
		$Dept = $session->get('nomDEPT');
		$societeUSER = $session->get('societeUSER');
		
		$em = $this->getDoctrine()->getEntityManager();
		
		$repository_dept = $em->getRepository('PhareosDeskNetServiceBundle:dept');
		
		$depts = $repository_dept->findBy(array('client' => $societeUSER));

        $entity = $em->getRepository('PhareosDeskNetServiceBundle:collaborateur')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find collaborateur entity.');
        }

        $_SESSION['societe'] = $societeUSER;
		$editForm = $this->createForm(new collaborateurType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosDeskNetServiceBundle:collaborateur:edit.html.twig', array(
            'depts' => $depts,
			'defaultDept' => $Dept,
			'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing collaborateur entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosDeskNetServiceBundle:collaborateur')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find collaborateur entity.');
        }

        $editForm   = $this->createForm(new collaborateurType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('collaborateur_show', array('id' => $id)));
        }

        return $this->render('PhareosDeskNetServiceBundle:collaborateur:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a collaborateur entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosDeskNetServiceBundle:collaborateur')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find collaborateur entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('collaborateur'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
