<?php

namespace Phareos\DeskNetServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class defaultdeptController extends Controller
{
    
    public function indexAction()
    {	
		$session = $this->get('session');
		$request = $this->get('request');
		$Dept = $request->query->get('Dept');
		
		$session->set('nomDEPT', $Dept);
		
		$societeUSER = $session->get('societeUSER');
		
		$em = $this->getDoctrine()->getEntityManager();
		
		$repository_dept = $em->getRepository('PhareosDeskNetServiceBundle:dept');
		
		$depts = $repository_dept->findBy(array('client' => $societeUSER));
		
		
		return $this->render('PhareosDeskNetServiceBundle:defaultdept:index.html.twig', array(
			'depts' => $depts,
			'defaultDept' => $Dept
        ));
    }
}
