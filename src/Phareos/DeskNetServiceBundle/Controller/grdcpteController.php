<?php

namespace Phareos\DeskNetServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Phareos\DeskNetServiceBundle\Entity\grdcpte;
use Phareos\DeskNetServiceBundle\Form\grdcpteType;
use Phareos\DeskNetServiceBundle\Form\grdcpteType2;

/**
 * grdcpte controller.
 *
 */
class grdcpteController extends Controller
{
    /**
     * Lists all grdcpte entities.
     *
     */
    public function indexAction()
    {
        $session = $this->get('session');
		$Dept = $session->get('nomDEPT');
		$societeUSER = $session->get('societeUSER');
		
		$em = $this->getDoctrine()->getEntityManager();
		
		$repository_dept = $em->getRepository('PhareosDeskNetServiceBundle:dept');
		
		if($Dept == 'Tous')
		{
			$entities = $em->getRepository('PhareosDeskNetServiceBundle:grdcpte')->findBy(array('societeuser' => $societeUSER));
		}
		else
		{
			$entities = $em -> getRepository('PhareosDeskNetServiceBundle:grdcpte')
							-> getAvecDept(array ($Dept), $societeUSER);
		}
		
		$depts = $repository_dept->findBy(array('client' => $societeUSER));

        return $this->render('PhareosDeskNetServiceBundle:grdcpte:index.html.twig', array(
            'depts' => $depts,
			'defaultDept' => $Dept,
			'entities' => $entities
        ));
    }

    /**
     * Finds and displays a grdcpte entity.
     *
     */
    public function showAction($id)
    {
        $session = $this->get('session');
		$Dept = $session->get('nomDEPT');
		$societeUSER = $session->get('societeUSER');
		
		$em = $this->getDoctrine()->getEntityManager();
		
		$repository_dept = $em->getRepository('PhareosDeskNetServiceBundle:dept');
        $entity = $em->getRepository('PhareosDeskNetServiceBundle:grdcpte')->find($id);
		
		$depts = $repository_dept->findBy(array('client' => $societeUSER));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find grdcpte entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosDeskNetServiceBundle:grdcpte:show.html.twig', array(
            'depts' => $depts,
			'defaultDept' => $Dept,
			'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new grdcpte entity.
     *
     */
    public function newAction()
    {
        $session = $this->get('session');
		$Dept = $session->get('nomDEPT');
		$societeUSER = $session->get('societeUSER');
		
		$em = $this->getDoctrine()->getEntityManager();
		
		$repository_dept = $em->getRepository('PhareosDeskNetServiceBundle:dept');
		
		$depts = $repository_dept->findBy(array('client' => $societeUSER));
		
		
		$entity = new grdcpte();
		$_SESSION['societe'] = $societeUSER;
        $form   = $this->createForm(new grdcpteType(), $entity);

        return $this->render('PhareosDeskNetServiceBundle:grdcpte:new.html.twig', array(
            'depts' => $depts,
			'defaultDept' => $Dept,
			'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new grdcpte entity.
     *
     */
    public function createAction()
    {
        $entity  = new grdcpte();
        $request = $this->getRequest();
        $form    = $this->createForm(new grdcpteType(), $entity);
        $form->bindRequest($request);
		
		$session = $this->get('session');
		$Dept = $session->get('nomDEPT');
		$societeUSER = $session->get('societeUSER');

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
			
			$entity->setSocieteuser($societeUSER);
			
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('grdcpte_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosDeskNetServiceBundle:grdcpte:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing grdcpte entity.
     *
     */
    public function editAction($id)
    {
        $session = $this->get('session');
		$Dept = $session->get('nomDEPT');
		$societeUSER = $session->get('societeUSER');
		
		$em = $this->getDoctrine()->getEntityManager();
		
		$repository_dept = $em->getRepository('PhareosDeskNetServiceBundle:dept');

        $entity = $em->getRepository('PhareosDeskNetServiceBundle:grdcpte')->find($id);
		
		$depts = $repository_dept->findBy(array('client' => $societeUSER));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find grdcpte entity.');
        }

        $_SESSION['societe'] = $societeUSER;
		$editForm = $this->createForm(new grdcpteType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosDeskNetServiceBundle:grdcpte:edit.html.twig', array(
            'depts' => $depts,
			'defaultDept' => $Dept,
			'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing grdcpte entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosDeskNetServiceBundle:grdcpte')->find($id);
		
		$session = $this->get('session');
		$Dept = $session->get('nomDEPT');
		$societeUSER = $session->get('societeUSER');

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find grdcpte entity.');
        }

        $editForm   = $this->createForm(new grdcpteType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('grdcpte_show', array('id' => $id)));
        }

        return $this->render('PhareosDeskNetServiceBundle:grdcpte:edit.html.twig', array(
			'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a grdcpte entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosDeskNetServiceBundle:grdcpte')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find grdcpte entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('grdcpte'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
