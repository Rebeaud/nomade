<?php

namespace Phareos\DeskNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

use Phareos\DeskNetServiceBundle\Entity\dept;
use Phareos\DeskNetServiceBundle\Entity\deptRepository;

class grdcpteType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $societeUSER = $_SESSION['societe'];
		
		$builder
            ->add('Nom')
            ->add('adres1')
            ->add('adres2')
			->add('cp')
			->add('ville')
            ->add('tel')
            ->add('fax')
            ->add('mail')
            ->add('acheteur')
			->add('prenach')
			->add('achcivil', 'choice', array('choices' => array('Mr' => "Mr", 'Mme' => "Mme", 'Mlle' => "Mlle"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Civilité -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('achtel')
            ->add('achmail')
            ->add('nbragences')
            ->add('cdcpdf')
            ->add('cdbpdf')
            ->add('contratpdf')
            ->add('planprevpdf')
			->add('departements', 'entity', array('class' => 'PhareosDeskNetServiceBundle:dept',
												'property' => 'nom',
												'query_builder' => function(deptRepository $er) {
												return $er->createQueryBuilder ('u')
												->where ('u.client = :societeUSER')
												->setParameter('societeUSER', $_SESSION['societe'])
												->orderBY ('u.nom', 'ASC');
												},
												'multiple' => true,
												'expanded' => false
												))
        ;
    }

    public function getName()
    {
        return 'phareos_desknetservicebundle_grdcptetype';
    }
}
