<?php

namespace Phareos\DeskNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

use Phareos\DeskNetServiceBundle\Entity\dept;
use Phareos\DeskNetServiceBundle\Entity\deptRepository;

class collaborateurType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $societeUSER = $_SESSION['societe'];
		
		$builder
            ->add('nom')
            ->add('prenom')
            ->add('typeposte', 'choice', array('choices' => array('Administratif' => "Administratif", 'Agent' => "Agent", 'Responsable Secteur' => "Responsable Secteur", 'Autre..' => "Autre.."), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisir -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('adres1')
            ->add('adres2')
            ->add('cp')
            ->add('ville')
            ->add('tel')
            ->add('mobile')
            ->add('mail')
            ->add('datenaissance')
            ->add('lieunaissance')
            ->add('situatfam', 'choice', array('choices' => array('Célibataire' => "Célibataire", 'Divorcé(e)' => "Divorcé(e)", 'Marié(e)' => "Marié(e)"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisir -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('nomjeunefille')
            ->add('numsecu')
            ->add('datevismed')
            ->add('handicap')
            ->add('annexesept', 'choice', array('choices' => array('Oui' => "Oui", 'Non' => "Non"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisir -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('txhoraire')
            ->add('echelon')
            ->add('nivechelon')
            ->add('anciennete')
            ->add('cartesejour')
            ->add('autortravail')
            ->add('carteident')
            ->add('sexe', 'choice', array('choices' => array('M' => "M", 'F' => "F"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisir -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('perimetre')
            ->add('nationalite')
            ->add('permis', 'choice', array('choices' => array('Oui' => "Oui", 'Non' => "Non"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisir -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('vehicule', 'choice', array('choices' => array('Oui' => "Oui", 'Non' => "Non"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisir -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('complement')
            ->add('attestation')
            ->add('accompte')
            ->add('datefincsj')
            ->add('motifpi', 'choice', array('choices' => array('Annulé DUE' => "Annulé DUE", 'Mauvais élément' => "Mauvais élément"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisir -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('tailletshirt')
            ->add('commentaires')
            ->add('departements', 'entity', array('class' => 'PhareosDeskNetServiceBundle:dept',
												'property' => 'nom',
												'query_builder' => function(deptRepository $er) {
												return $er->createQueryBuilder ('u')
												->where ('u.client = :societeUSER')
												->setParameter('societeUSER', $_SESSION['societe'])
												->orderBY ('u.nom', 'ASC');
												},
												'multiple' => true,
												'expanded' => false
												))
        ;
    }

    public function getName()
    {
        return 'phareos_desknetservicebundle_collaborateurtype';
    }
}
