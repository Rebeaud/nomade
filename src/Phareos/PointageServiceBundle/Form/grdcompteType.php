<?php

namespace Phareos\PointageServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class grdcompteType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('matricule_gc')
            ->add('nom_gc')
            ->add('email_gc')
            ->add('adr1ss')
            ->add('adr2ss')
            ->add('cpss')
            ->add('villess')
            ->add('tel')
            ->add('fax')
            ->add('contactss')
            ->add('commss')
        ;
    }

    public function getName()
    {
        return 'phareos_pointageservicebundle_grdcomptetype';
    }
}
