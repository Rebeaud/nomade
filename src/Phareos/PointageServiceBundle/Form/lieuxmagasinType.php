<?php

namespace Phareos\PointageServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class lieuxmagasinType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('id_gc')
            ->add('matricule_gc')
            ->add('codag')
            ->add('adres1mag')
            ->add('adres2mag')
            ->add('cpmag')
            ->add('villemag')
            ->add('emailmag')
            ->add('faxmag')
            ->add('telmag')
            ->add('tel2mag')
            ->add('tel3mag')
            ->add('tel4')
            ->add('commentaires')
            ->add('agent_1_id')
            ->add('agent_2_id')
            ->add('agent_3_id')
            ->add('agent_4_id')
            ->add('agent_5_id')
            ->add('agent_6_id')
            ->add('agent_7_id')
            ->add('agent_8_id')
            ->add('agent_9_id')
            ->add('agent_10_id')
        ;
    }

    public function getName()
    {
        return 'phareos_pointageservicebundle_lieuxmagasintype';
    }
}
