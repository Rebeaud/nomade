<?php

namespace Phareos\PointageServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class progaffectType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('id_agent')
            ->add('id_lieux')
            ->add('joursem')
            ->add('heurearrive')
            ->add('heuresortie')
        ;
    }

    public function getName()
    {
        return 'phareos_pointageservicebundle_progaffecttype';
    }
}
