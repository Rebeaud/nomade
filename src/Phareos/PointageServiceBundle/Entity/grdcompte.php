<?php

namespace Phareos\PointageServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\PointageServiceBundle\Entity\grdcompte
 *
 * @ORM\Table(name="poi_grdcompte")
 * @ORM\Entity(repositoryClass="Phareos\PointageServiceBundle\Entity\grdcompteRepository")
 */
class grdcompte
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $matricule_gc
     *
     * @ORM\Column(name="matricule_gc", type="string", length=255, nullable=true)
     */
    private $matricule_gc;

    /**
     * @var string $nom_gc
     *
     * @ORM\Column(name="nom_gc", type="string", length=255, nullable=true)
     */
    private $nom_gc;

    /**
     * @var string $email_gc
     *
     * @ORM\Column(name="email_gc", type="string", length=255, nullable=true)
     */
    private $email_gc;

    /**
     * @var string $adr1ss
     *
     * @ORM\Column(name="adr1ss", type="string", length=255, nullable=true)
     */
    private $adr1ss;

    /**
     * @var string $adr2ss
     *
     * @ORM\Column(name="adr2ss", type="string", length=255, nullable=true)
     */
    private $adr2ss;

    /**
     * @var string $cpss
     *
     * @ORM\Column(name="cpss", type="string", length=255, nullable=true)
     */
    private $cpss;

    /**
     * @var string $villess
     *
     * @ORM\Column(name="villess", type="string", length=255, nullable=true)
     */
    private $villess;

    /**
     * @var string $tel
     *
     * @ORM\Column(name="tel", type="string", length=255, nullable=true)
     */
    private $tel;

    /**
     * @var string $fax
     *
     * @ORM\Column(name="fax", type="string", length=255, nullable=true)
     */
    private $fax;

    /**
     * @var string $contactss
     *
     * @ORM\Column(name="contactss", type="string", length=255, nullable=true)
     */
    private $contactss;

    /**
     * @var text $commss
     *
     * @ORM\Column(name="commss", type="text", nullable=true)
     */
    private $commss;

    /**
     * @var string $societeuser
     *
     * @ORM\Column(name="societeuser", type="string", length=255, nullable=true)
     */
    private $societeuser;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set matricule_gc
     *
     * @param string $matriculeGc
     */
    public function setMatriculeGc($matriculeGc)
    {
        $this->matricule_gc = $matriculeGc;
    }

    /**
     * Get matricule_gc
     *
     * @return string 
     */
    public function getMatriculeGc()
    {
        return $this->matricule_gc;
    }

    /**
     * Set nom_gc
     *
     * @param string $nomGc
     */
    public function setNomGc($nomGc)
    {
        $this->nom_gc = $nomGc;
    }

    /**
     * Get nom_gc
     *
     * @return string 
     */
    public function getNomGc()
    {
        return $this->nom_gc;
    }

    /**
     * Set email_gc
     *
     * @param string $emailGc
     */
    public function setEmailGc($emailGc)
    {
        $this->email_gc = $emailGc;
    }

    /**
     * Get email_gc
     *
     * @return string 
     */
    public function getEmailGc()
    {
        return $this->email_gc;
    }

    /**
     * Set adr1ss
     *
     * @param string $adr1ss
     */
    public function setAdr1ss($adr1ss)
    {
        $this->adr1ss = $adr1ss;
    }

    /**
     * Get adr1ss
     *
     * @return string 
     */
    public function getAdr1ss()
    {
        return $this->adr1ss;
    }

    /**
     * Set adr2ss
     *
     * @param string $adr2ss
     */
    public function setAdr2ss($adr2ss)
    {
        $this->adr2ss = $adr2ss;
    }

    /**
     * Get adr2ss
     *
     * @return string 
     */
    public function getAdr2ss()
    {
        return $this->adr2ss;
    }

    /**
     * Set cpss
     *
     * @param string $cpss
     */
    public function setCpss($cpss)
    {
        $this->cpss = $cpss;
    }

    /**
     * Get cpss
     *
     * @return string 
     */
    public function getCpss()
    {
        return $this->cpss;
    }

    /**
     * Set villess
     *
     * @param string $villess
     */
    public function setVilless($villess)
    {
        $this->villess = $villess;
    }

    /**
     * Get villess
     *
     * @return string 
     */
    public function getVilless()
    {
        return $this->villess;
    }

    /**
     * Set tel
     *
     * @param string $tel
     */
    public function setTel($tel)
    {
        $this->tel = $tel;
    }

    /**
     * Get tel
     *
     * @return string 
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set fax
     *
     * @param string $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * Get fax
     *
     * @return string 
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set contactss
     *
     * @param string $contactss
     */
    public function setContactss($contactss)
    {
        $this->contactss = $contactss;
    }

    /**
     * Get contactss
     *
     * @return string 
     */
    public function getContactss()
    {
        return $this->contactss;
    }

    /**
     * Set commss
     *
     * @param text $commss
     */
    public function setCommss($commss)
    {
        $this->commss = $commss;
    }

    /**
     * Get commss
     *
     * @return text 
     */
    public function getCommss()
    {
        return $this->commss;
    }

    /**
     * Set societeuser
     *
     * @param string $societeuser
     */
    public function setSocieteuser($societeuser)
    {
        $this->societeuser = $societeuser;
    }

    /**
     * Get societeuser
     *
     * @return string 
     */
    public function getSocieteuser()
    {
        return $this->societeuser;
    }
}