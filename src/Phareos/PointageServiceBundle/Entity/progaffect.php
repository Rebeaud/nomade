<?php

namespace Phareos\PointageServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\PointageServiceBundle\Entity\progaffect
 *
 * @ORM\Table(name="poi_progaffect")
 * @ORM\Entity(repositoryClass="Phareos\PointageServiceBundle\Entity\progaffectRepository")
 */
class progaffect
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer $id_agent
     *
     * @ORM\Column(name="id_agent", type="integer")
     */
    private $id_agent;

    /**
     * @var integer $id_lieux
     *
     * @ORM\Column(name="id_lieux", type="integer")
     */
    private $id_lieux;

    /**
     * @var string $joursem
     *
     * @ORM\Column(name="joursem", type="string", length=255)
     */
    private $joursem;

    /**
     * @var time $heurearrive
     *
     * @ORM\Column(name="heurearrive", type="time")
     */
    private $heurearrive;

    /**
     * @var time $heuresortie
     *
     * @ORM\Column(name="heuresortie", type="time")
     */
    private $heuresortie;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id_agent
     *
     * @param integer $idAgent
     */
    public function setIdAgent($idAgent)
    {
        $this->id_agent = $idAgent;
    }

    /**
     * Get id_agent
     *
     * @return integer 
     */
    public function getIdAgent()
    {
        return $this->id_agent;
    }

    /**
     * Set id_lieux
     *
     * @param integer $idLieux
     */
    public function setIdLieux($idLieux)
    {
        $this->id_lieux = $idLieux;
    }

    /**
     * Get id_lieux
     *
     * @return integer 
     */
    public function getIdLieux()
    {
        return $this->id_lieux;
    }

    /**
     * Set joursem
     *
     * @param string $joursem
     */
    public function setJoursem($joursem)
    {
        $this->joursem = $joursem;
    }

    /**
     * Get joursem
     *
     * @return string 
     */
    public function getJoursem()
    {
        return $this->joursem;
    }

    /**
     * Set heurearrive
     *
     * @param time $heurearrive
     */
    public function setHeurearrive($heurearrive)
    {
        $this->heurearrive = $heurearrive;
    }

    /**
     * Get heurearrive
     *
     * @return time 
     */
    public function getHeurearrive()
    {
        return $this->heurearrive;
    }

    /**
     * Set heuresortie
     *
     * @param time $heuresortie
     */
    public function setHeuresortie($heuresortie)
    {
        $this->heuresortie = $heuresortie;
    }

    /**
     * Get heuresortie
     *
     * @return time 
     */
    public function getHeuresortie()
    {
        return $this->heuresortie;
    }
}