<?php

namespace Phareos\PointageServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class DefaultController extends Controller
{
    
    public function indexAction()
    {
        return $this->render('PhareosPointageServiceBundle:Default:index.html.twig');
    }
}
