<?php

namespace Phareos\PointageServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Phareos\PointageServiceBundle\Entity\agents;
use Phareos\PointageServiceBundle\Form\agentsType;

/**
 * agents controller.
 *
 */
class agentsController extends Controller
{
    /**
     * Lists all agents entities.
     *
     */
    public function indexAction()
    {
        $session = $this->get('session');
		$societeUSER = $session->get('societeUSER');
		
		$em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosPointageServiceBundle:agents')->findBy(array('societeuser' => $societeUSER));

        return $this->render('PhareosPointageServiceBundle:agents:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a agents entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosPointageServiceBundle:agents')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find agents entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosPointageServiceBundle:agents:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new agents entity.
     *
     */
    public function newAction()
    {
        $entity = new agents();
        $form   = $this->createForm(new agentsType(), $entity);

        return $this->render('PhareosPointageServiceBundle:agents:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new agents entity.
     *
     */
    public function createAction()
    {
        $entity  = new agents();
        $request = $this->getRequest();
        $form    = $this->createForm(new agentsType(), $entity);
        $form->bindRequest($request);
		
		$session = $this->get('session');
		$societeUSER = $session->get('societeUSER');

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
			$entity->setSocieteuser($societeUSER);
			
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('agents_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosPointageServiceBundle:agents:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing agents entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosPointageServiceBundle:agents')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find agents entity.');
        }

        $editForm = $this->createForm(new agentsType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosPointageServiceBundle:agents:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing agents entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosPointageServiceBundle:agents')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find agents entity.');
        }

        $editForm   = $this->createForm(new agentsType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('agents_show', array('id' => $id)));
        }

        return $this->render('PhareosPointageServiceBundle:agents:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a agents entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosPointageServiceBundle:agents')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find agents entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('agents'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
