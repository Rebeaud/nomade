<?php

namespace Phareos\LogisToolBoxBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class emplacementType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('emplacement')
			->add('alletrav')
			->add('actif')
        ;
    }

    public function getName()
    {
        return 'phareos_logistoolboxbundle_emplacementtype';
    }
}
