<?php

namespace Phareos\LogisToolBoxBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Phareos\LogisToolBoxBundle\Entity\clienttool;
use Phareos\LogisToolBoxBundle\Entity\clienttoolRepository;

class articleseditclientType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('nom', 'hidden')
            ->add('designat', 'hidden')
            ->add('reference')
			->add('fourniss', 'hidden')
			->add('categorie', 'hidden')
			->add('client', 'hidden')
			//->add('client', 'choice', array('choices' => array('Travo Services +' => "Travo Services +"), 
                                            //'multiple' => false, 
                                            //'expanded' => false, 
                                            //'preferred_choices' => array(0),
                                            //'empty_value' => '- Choisissez une option -',
                                            //'empty_data'  => null,
											//'required' => false
                                            //))
            //->add('typeunit', 'choice', array('choices' => array('Palettes' => "Palettes", 'Cartons' => "Cartons", 'Boites' => "Boites"), 
                                            //'multiple' => false, 
                                            //'expanded' => false, 
                                            //'preferred_choices' => array(2),
                                            //'empty_value' => '- Choisissez une option -',
                                            //'empty_data'  => null,
											//'required' => false
                                            //))
            //->add('poids')
            //->add('haut')
            //->add('large')
            //->add('profond')
            //->add('qtetot', 'hidden')
            //->add('qteencours', 'hidden')
            //->add('qtequarant', 'hidden')
            //->add('daterecep')
            //->add('dateprepa')
        ;
    }

    public function getName()
    {
        return 'phareos_logistoolboxbundle_articleseditclienttype';
    }
}
