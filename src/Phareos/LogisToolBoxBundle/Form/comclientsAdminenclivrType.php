<?php

namespace Phareos\LogisToolBoxBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class comclientsAdminenclivrType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            //->add('install')
            //->add('comm')
            //->add('compdf')
			->add('etat', 'choice', array('choices' => array('En cours de livraison' => "En cours de livraison", 'Livré' => "Livré"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array('En cours de livraison'),
                                            'empty_value' => false,
											'required' => false
                                            ))
        ;
    }

    public function getName()
    {
        return 'phareos_logistoolboxbundle_comclientsAdminenclivrType';
    }
}
