<?php

namespace Phareos\LogisToolBoxBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class clienttoolType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('contact')
            ->add('adress1')
            ->add('adress2')
            ->add('cp')
            ->add('ville')
            ->add('tel')
            ->add('fax')
            ->add('email')
        ;
    }

    public function getName()
    {
        return 'phareos_logistoolboxbundle_clienttooltype';
    }
}
