<?php

namespace Phareos\LogisToolBoxBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class com_articlesType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('idcomclients')
            ->add('idarticles')
            ->add('qtte')
        ;
    }

    public function getName()
    {
        return 'phareos_logistoolboxbundle_com_articlestype';
    }
}
