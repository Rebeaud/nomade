<?php

namespace Phareos\LogisToolBoxBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class comclientsshoweditType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('comm')
			->add('livrimp')
			->add('numcommaint')
			->add('datelivrend')
        ;
    }

    public function getName()
    {
        return 'phareos_logistoolboxbundle_comclientsshowedittype';
    }
}
