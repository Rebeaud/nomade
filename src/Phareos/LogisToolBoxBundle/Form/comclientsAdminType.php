<?php

namespace Phareos\LogisToolBoxBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class comclientsAdminType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            //->add('install')
            //->add('comm')
            //->add('compdf')
			->add('etat', 'choice', array('choices' => array('En cours de préparation' => "En cours de préparation", 'Préparé en attente expédition' => "Préparé en attente expédition", 'En cours de livraison' => "En cours de livraison", 'Livré' => "Livré"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array('En cours de préparation'),
                                            'empty_value' => false,
											'required' => false
                                            ))
        ;
    }

    public function getName()
    {
        return 'phareos_logistoolboxbundle_comclientsAdminType';
    }
}
