<?php

namespace Phareos\LogisToolBoxBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class commanderecipType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('comm')
            ->add('compdf')
			//->add('datelivrprev')
        ;
    }

    public function getName()
    {
        return 'phareos_logistoolboxbundle_commandereciptype';
    }
}
