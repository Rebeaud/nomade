<?php

namespace Phareos\LogisToolBoxBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class destinationType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('typecpte', 'choice', array('choices' => array('Inditex' => "Inditex", 'Autres' => "Autres"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => 'Autres',
											'required' => true
                                            ))
			->add('compte')
			->add('nummag')
            ->add('site')
			->add('contact')
            ->add('adresse1')
            ->add('adresse2')
            ->add('cp')
            ->add('ville')
            ->add('tel')
            ->add('fax')
			->add('tel2')
			->add('mail')
			->add('sitenet')
			->add('pays')
        ;
    }

    public function getName()
    {
        return 'phareos_logistoolboxbundle_destinationtype';
    }
}
