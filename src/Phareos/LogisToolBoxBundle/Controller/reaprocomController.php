<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\LogisToolBoxBundle\Entity\reaprocom;
use Phareos\LogisToolBoxBundle\Form\reaprocomType;

/**
 * reaprocom controller.
 *
 */
class reaprocomController extends Controller
{
    /**
     * Lists all reaprocom entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosLogisToolBoxBundle:reaprocom')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:reaprocom:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a reaprocom entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:reaprocom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find reaprocom entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:reaprocom:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new reaprocom entity.
     *
     */
    public function newAction()
    {
        $entity = new reaprocom();
        $form   = $this->createForm(new reaprocomType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:reaprocom:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new reaprocom entity.
     *
     */
    public function createAction()
    {
        $entity  = new reaprocom();
        $request = $this->getRequest();
        $form    = $this->createForm(new reaprocomType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('reaprocom_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosLogisToolBoxBundle:reaprocom:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing reaprocom entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:reaprocom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find reaprocom entity.');
        }

        $editForm = $this->createForm(new reaprocomType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:reaprocom:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing reaprocom entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:reaprocom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find reaprocom entity.');
        }

        $editForm   = $this->createForm(new reaprocomType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('reaprocom_edit', array('id' => $id)));
        }

        return $this->render('PhareosLogisToolBoxBundle:reaprocom:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a reaprocom entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosLogisToolBoxBundle:reaprocom')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find reaprocom entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('reaprocom'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
