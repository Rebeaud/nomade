<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\LogisToolBoxBundle\Entity\destination;
use Phareos\LogisToolBoxBundle\Form\destinationType;

/**
 * destination controller.
 *
 */
class destinationController extends Controller
{
    /**
     * Lists all destination entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
		
		$session = $this->get('session');
		$applicationUSER = $session->get('applicationUSER');
		$societeUSER = $session->get('societeUSER');
		
		if ($applicationUSER == 'Toolbox')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:destination')->findAll();
		}
		else
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:destination')->findBy(array('client' => $societeUSER));
		}

        //$entities = $em->getRepository('PhareosLogisToolBoxBundle:destination')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:destination:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a destination entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:destination')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find destination entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:destination:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new destination entity.
     *
     */
    public function newAction()
    {
        $entity = new destination();
        $form   = $this->createForm(new destinationType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:destination:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new destination entity.
     *
     */
    public function createAction()
    {
        
		$session = $this->get('session');
		
		$societeUSER = $session->get('societeUSER');
		
		$entity  = new destination();
        $request = $this->getRequest();
        $form    = $this->createForm(new destinationType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
			$entity->setClient($societeUSER);
			
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('destination_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosLogisToolBoxBundle:destination:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing destination entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:destination')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find destination entity.');
        }

        $editForm = $this->createForm(new destinationType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:destination:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing destination entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:destination')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find destination entity.');
        }

        $editForm   = $this->createForm(new destinationType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('destination_show', array('id' => $entity->getId())));
        }

        return $this->render('PhareosLogisToolBoxBundle:destination:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a destination entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosLogisToolBoxBundle:destination')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find destination entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('destination'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
