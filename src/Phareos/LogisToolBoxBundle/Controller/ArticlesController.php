<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Phareos\LogisToolBoxBundle\Entity\articlescom;
use Phareos\LogisToolBoxBundle\Form\articlescomType;

class ArticlesController extends Controller
{
    
    public function indexAction()
    {
		$em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosLogisToolBoxBundle:articlescom')->findAll();
		
		$entity = new articlescom();
        $form   = $this->createForm(new articlescomType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:Articles:index.html.twig', array(
            'entities' => $entities,
			'entity' => $entity,
            'form'   => $form->createView()
        ));
		
		//return $this->render('PhareosLogisToolBoxBundle:Articles:index.html.twig');
    }
	
	public function createAction()
    {
		$entity  = new articlescom();
        $request = $this->getRequest();
        $form    = $this->createForm(new articlescomType(), $entity);
        $form->bindRequest($request);
		//$date = new \DateTime('now');
		$auteur = $this->container->get('security.context')->getToken()->getUser();
		
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
			//$entity->setDate($date);
			$entity->setAuteur($auteur);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('Articles'));
            
        }

        return $this->render('Articles', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }
}