<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Phareos\LogisToolBoxBundle\Entity\stockcom;
use Phareos\LogisToolBoxBundle\Form\stockcomType;

class StockController extends Controller
{
    
    public function indexAction()
    {
		$em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosLogisToolBoxBundle:stockcom')->findAll();
		
		$entity = new stockcom();
        $form   = $this->createForm(new stockcomType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:Stock:index.html.twig', array(
            'entities' => $entities,
			'entity' => $entity,
            'form'   => $form->createView()
        ));
		//return $this->render('PhareosLogisToolBoxBundle:Stock:index.html.twig');
    }
	
	public function createAction()
    {
		$entity  = new stockcom();
        $request = $this->getRequest();
        $form    = $this->createForm(new stockcomType(), $entity);
        $form->bindRequest($request);
		//$date = new \DateTime('now');
		$auteur = $this->container->get('security.context')->getToken()->getUser();
		
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
			//$entity->setDate($date);
			$entity->setAuteur($auteur);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('Stock'));
            
        }

        return $this->render('Stock', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }
}