<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\LogisToolBoxBundle\Entity\categorie;
use Phareos\LogisToolBoxBundle\Form\categorieType;
use Phareos\LogisToolBoxBundle\Form\categorieclientType;

/**
 * categorie controller.
 *
 */
class categorieController extends Controller
{
    /**
     * Lists all categorie entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
		
		$session = $this->get('session');
		$applicationUSER = $session->get('applicationUSER');
		$societeUSER = $session->get('societeUSER');
		
		
		if ($applicationUSER == 'Toolbox')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:categorie')->findAll();
		}
		else
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:categorie')->findBy(array('client' => $societeUSER));
		}

        //$entities = $em->getRepository('PhareosLogisToolBoxBundle:categorie')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:categorie:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a categorie entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:categorie')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find categorie entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:categorie:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new categorie entity.
     *
     */
    public function newAction()
    {
        $entity = new categorie();
		
		$session = $this->get('session');
		$applicationUSER = $session->get('applicationUSER');
		$societeUSER = $session->get('societeUSER');
		
		if ($applicationUSER == 'Toolbox')
		{
			$form   = $this->createForm(new categorieType(), $entity);
		}
		else
		{
			$form   = $this->createForm(new categorieclientType(), $entity);
		}
		
        //$form   = $this->createForm(new categorieType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:categorie:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new categorie entity.
     *
     */
    public function createAction()
    {
        $entity  = new categorie();
        $request = $this->getRequest();
		
		$session = $this->get('session');
		$applicationUSER = $session->get('applicationUSER');
		$societeUSER = $session->get('societeUSER');
		
		if ($applicationUSER == 'Toolbox')
		{
			$form   = $this->createForm(new categorieType(), $entity);
		}
		else
		{
			$form   = $this->createForm(new categorieclientType(), $entity);
		}
		
        //$form    = $this->createForm(new categorieType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
			
			if ($applicationUSER == 'Toolbox')
			{
			}
			else
			{
				$entity->setClient($societeUSER);
			}
			
			
			
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('categorie'));
            
        }

        return $this->render('PhareosLogisToolBoxBundle:categorie:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing categorie entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:categorie')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find categorie entity.');
        }

        $editForm = $this->createForm(new categorieType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:categorie:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing categorie entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:categorie')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find categorie entity.');
        }

        $editForm   = $this->createForm(new categorieType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('categorie_edit', array('id' => $id)));
        }

        return $this->render('PhareosLogisToolBoxBundle:categorie:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a categorie entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosLogisToolBoxBundle:categorie')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find categorie entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('categorie'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
