<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\LogisToolBoxBundle\Entity\recepcom;
use Phareos\LogisToolBoxBundle\Form\recepcomType;

/**
 * recepcom controller.
 *
 */
class recepcomController extends Controller
{
    /**
     * Lists all recepcom entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosLogisToolBoxBundle:recepcom')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:recepcom:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a recepcom entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:recepcom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find recepcom entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:recepcom:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new recepcom entity.
     *
     */
    public function newAction()
    {
        $entity = new recepcom();
        $form   = $this->createForm(new recepcomType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:recepcom:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new recepcom entity.
     *
     */
    public function createAction()
    {
        $entity  = new recepcom();
        $request = $this->getRequest();
        $form    = $this->createForm(new recepcomType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('recepcom_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosLogisToolBoxBundle:recepcom:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing recepcom entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:recepcom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find recepcom entity.');
        }

        $editForm = $this->createForm(new recepcomType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:recepcom:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing recepcom entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:recepcom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find recepcom entity.');
        }

        $editForm   = $this->createForm(new recepcomType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('recepcom_edit', array('id' => $id)));
        }

        return $this->render('PhareosLogisToolBoxBundle:recepcom:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a recepcom entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosLogisToolBoxBundle:recepcom')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find recepcom entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('recepcom'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
