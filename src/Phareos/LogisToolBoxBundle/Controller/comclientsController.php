<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Phareos\LogisToolBoxBundle\Entity\comclients;
use Phareos\LogisToolBoxBundle\Form\comclientsType;
use Phareos\LogisToolBoxBundle\Form\comclientspanType;
use Phareos\LogisToolBoxBundle\Form\comclientsshoweditType;
use Phareos\LogisToolBoxBundle\Entity\com_articles;
use Phareos\LogisToolBoxBundle\Entity\destination;
use Phareos\LogisToolBoxBundle\Entity\articles;
use Phareos\LogisToolBoxBundle\Form\comclientsAdminType;
use Phareos\LogisToolBoxBundle\Form\comclientsAdminprepaType;
use Phareos\LogisToolBoxBundle\Form\comclientsAdminencprepaType;
use Phareos\LogisToolBoxBundle\Form\comclientsAdminatexpType;
use Phareos\LogisToolBoxBundle\Form\comclientsAdminenclivrType;
use Phareos\LogisToolBoxBundle\Form\comclientseditpanType;
use Phareos\LogisToolBoxBundle\Form\comclientsprepafactType;
use Phareos\LogisToolBoxBundle\Entity\commanderecip;
use Phareos\LogisToolBoxBundle\Entity\com_recip;
use Phareos\LogisToolBoxBundle\Entity\com_article_tmp;
use Phareos\LogisToolBoxBundle\Form\com_article_tmpType;
use Phareos\NomadeNetServiceBundle\Entity\mailswift;

/**
 * comclients controller.
 *
 */
class comclientsController extends Controller
{
    /**
     * Lists all comclients entities.
     *
     */
    public function indexAction()
    {
        
		$session = $this->get('session');
		$societeUSER = $session->get('societeUSER');
		$applicationUSER = $session->get('applicationUSER');
		
		$em = $this->getDoctrine()->getEntityManager();

        if ($applicationUSER == 'Toolbox')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('idclient' => null, 'archive' => 0), array('numop' => 'desc'));
			
		}
		elseif ($applicationUSER == 'ToolboxInterv')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('install' => 1, 'archive' => 0), array('numop' => 'desc'));
			
		}
		else
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('client' => $societeUSER, 'archive' => 0), array('numop' => 'desc'));
			$userid = $this->container->get('security.context')->getToken()->getUser()->getid();
			$entitiescomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->findBy(array('iduser' => $userid));
			if ($entitiescomarticlestmp) {

        
			foreach ($entitiescomarticlestmp as $articlestmp)
				{
					$idArticletmp = $articlestmp->getID();
					$entitycomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->find($idArticletmp);
			
					$em->remove($entitycomarticlestmp);
					$em->flush();
				}
			}
		}
		
		//$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:comclients:index.html.twig', array(
            'entities' => $entities
        ));
    }
	
	public function indexprepavirtAction()
    {
        
		$session = $this->get('session');
		$societeUSER = $session->get('societeUSER');
		$applicationUSER = $session->get('applicationUSER');
		
		$em = $this->getDoctrine()->getEntityManager();

        if ($applicationUSER == 'Toolbox')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('idclient' => null, 'etat' => 'En préparation virtuelle'), array('numop' => 'desc'));
			
			foreach ($entities as $entity)
				{
					$entityId = $entity->getId();
					$entitiescomarticles = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findBy(array('idcomclients' => $entityId));
					
					$poidsArticleTot = 0;
					$volumeArticleTot = 0;
					$entity->setValid(1);
					$entity->setValidor(1);
					$em->persist($entity);
					$em->flush();
					
					foreach ($entitiescomarticles as $entitycomarticle){
						$qTteArticletmp = $entitycomarticle->getQtte();//qtte articles commandé
						$idArticlecom = $entitycomarticle->getIdarticles();//id article commandé
						//recherche de l'article concerné
						$articlesentities = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticlecom);
						
						//calcul des validations
						$stockReel = $articlesentities->getQtetot();//quantité totale en stock (visu client)
						$stockReelphys = $articlesentities->getQtephys();//quantité totale en stock (stock physique)
						$stockAtenteRecip = $articlesentities->getQteencours();//quantité en attente de réception
						$stockvalidor = $stockReel + $stockAtenteRecip;
						
						if ($qTteArticletmp > $stockReelphys){
							$entity->setValid(0);
							$em->persist($entity);
							$em->flush();
						}
						
						if ($qTteArticletmp > $stockvalidor){
							$entity->setValidor(0);
							$em->persist($entity);
							$em->flush();
						}
						
						//on récupère le poids de l'artcile concerné
						$poidsArticle = $articlesentities->getPoids();
				
						//on calcul le poids total pour chaque référence d'articles (articles de même référence * qantité)
						$poidsArticleRef = $poidsArticle * $qTteArticletmp;
				
						//on calcul le poids total de tous les articles
						$poidsArticleTot += $poidsArticleRef;
						
						$entitycomarticle->setTest($poidsArticleRef);
						$em->persist($entitycomarticle);
						$em->flush();
				
						//on récupère la largeur de l'article concerné
						$largeurArticle = $articlesentities->getLarge();
				
						//on récupère la hauteur de l'article concerné
						$hautArticle = $articlesentities->getHaut();
				
						//on récupère la profondeur de l'article concerné
						$profondArticle = $articlesentities->getProfond();
				
						//on calcul le volume de l'article concerné
						$volumeArticle = $largeurArticle * $hautArticle * $profondArticle;
				
						//on calcul le volume total de tous les articles de la même référence (articles de même référence * qantité)
						$volumeArticleRef = $volumeArticle * $qTteArticletmp;
				
						//on calcul le volume total de tous les articles de la commande
						$volumeArticleTot += $volumeArticleRef;
						
						
						
					}
					
					$entity->setPoids($poidsArticleTot);
					$entity->setVolume($volumeArticleTot);
					$em->persist($entity);
					$em->flush();
				}
			
		}
		elseif ($applicationUSER == 'ToolboxInterv')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('install' => 1), array('numop' => 'desc'));
			
			
			
		}
		else
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('client' => $societeUSER, 'etat' => 'En préparation virtuelle'), array('numop' => 'desc'));
			$userid = $this->container->get('security.context')->getToken()->getUser()->getid();
			
			foreach ($entities as $entity)
				{
					$entityId = $entity->getId();
					$entitiescomarticles = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findBy(array('idcomclients' => $entityId));
					$entity->setValid(1);
					$entity->setValidor(1);
					$em->persist($entity);
					$em->flush();
					
					
					foreach ($entitiescomarticles as $entitycomarticle){
						$qtteArtcilecom = $entitycomarticle->getQtte();//qtte articles commandé
						$idArticlecom = $entitycomarticle->getIdarticles();//id article commandé
						//recherche de l'article concerné
						$entityArticle = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticlecom);
						$stockReel = $entityArticle->getQtetot();//quantité totale en stock
						$stockReelphys = $entityArticle->getQtephys();//quantité totale en stock (stock physique)
						$stockAtenteRecip = $entityArticle->getQteencours();//quantité en attente de réception
						$stockvalidor = $stockReel + $stockAtenteRecip;
						if ($qtteArtcilecom > $stockReelphys){
							$entity->setValid(0);
							$em->persist($entity);
							$em->flush();
						}
						
						if ($qtteArtcilecom > $stockvalidor){
							$entity->setValidor(0);
							$em->persist($entity);
							$em->flush();
						}
						
						
						
					}
					
				}
			
			$entitiescomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->findBy(array('iduser' => $userid));
			if ($entitiescomarticlestmp) {

        
			foreach ($entitiescomarticlestmp as $articlestmp)
				{
					$idArticletmp = $articlestmp->getID();
					$entitycomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->find($idArticletmp);
			
					$em->remove($entitycomarticlestmp);
					$em->flush();
				}
			}
		}
		
		//$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:comclients:indexprepavirt.html.twig', array(
            'entities' => $entities
        ));
    }
	
	public function indexatprepaAction()
    {
        
		$session = $this->get('session');
		$societeUSER = $session->get('societeUSER');
		$applicationUSER = $session->get('applicationUSER');
		
		$em = $this->getDoctrine()->getEntityManager();

        if ($applicationUSER == 'Toolbox')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('idclient' => null, 'etat' => 'En attente de préparation'), array('numop' => 'desc'));
			foreach ($entities as $entity)
				{
					$entityId = $entity->getId();
					$entitiescomarticles = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findBy(array('idcomclients' => $entityId));
					$entity->setValid(1);
					
					$em->persist($entity);
					$em->flush();
					
					
					foreach ($entitiescomarticles as $entitycomarticle){
						$qtteArtcilecom = $entitycomarticle->getQtte();//qtte articles commandé
						$idArticlecom = $entitycomarticle->getIdarticles();//id article commandé
						//recherche de l'article concerné
						$entityArticle = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticlecom);
						$stockReel = $entityArticle->getQtetot();//quantité totale en stock (stock visu client)
						$stockReelphys = $entityArticle->getQtephys();//quantité totale en stock (stock physique)
						$stockAtenteRecip = $entityArticle->getQteencours();//quantité en attente de réception
						//$stockvalidor = $stockReel + $stockAtenteRecip;
						if ($qtteArtcilecom > $stockReelphys){
							$entity->setValid(0);
							$em->persist($entity);
							$em->flush();
						}
						
						
						
						
						
					}
					
				}
			
		}
		elseif ($applicationUSER == 'ToolboxInterv')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('install' => 1), array('numop' => 'desc'));
			
		}
		else
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('client' => $societeUSER, 'etat' => 'En attente de préparation'), array('numop' => 'desc'));
			$userid = $this->container->get('security.context')->getToken()->getUser()->getid();
			$entitiescomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->findBy(array('iduser' => $userid));
			if ($entitiescomarticlestmp) {

        
			foreach ($entitiescomarticlestmp as $articlestmp)
				{
					$idArticletmp = $articlestmp->getID();
					$entitycomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->find($idArticletmp);
			
					$em->remove($entitycomarticlestmp);
					$em->flush();
				}
			}
			
			foreach ($entities as $entity)
				{
					$entityId = $entity->getId();
					$entitiescomarticles = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findBy(array('idcomclients' => $entityId));
					$entity->setValid(1);
					
					$em->persist($entity);
					$em->flush();
					
					
					foreach ($entitiescomarticles as $entitycomarticle){
						$qtteArtcilecom = $entitycomarticle->getQtte();//qtte articles commandé
						$idArticlecom = $entitycomarticle->getIdarticles();//id article commandé
						//recherche de l'article concerné
						$entityArticle = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticlecom);
						$stockReel = $entityArticle->getQtetot();//quantité totale en stock (stock visu client)
						$stockReelphys = $entityArticle->getQtephys();//quantité totale en stock (stock physique)
						$stockAtenteRecip = $entityArticle->getQteencours();//quantité en attente de réception
						//$stockvalidor = $stockReel + $stockAtenteRecip;
						if ($qtteArtcilecom > $stockReelphys){
							$entity->setValid(0);
							$em->persist($entity);
							$em->flush();
						}
						
						
						
						
						
					}
					
				}
		}
		
		//$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:comclients:index.html.twig', array(
            'entities' => $entities
        ));
    }
	
	public function indexencprepaAction()
    {
        
		$session = $this->get('session');
		$societeUSER = $session->get('societeUSER');
		$applicationUSER = $session->get('applicationUSER');
		
		$em = $this->getDoctrine()->getEntityManager();

        if ($applicationUSER == 'Toolbox')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('idclient' => null, 'etat' => 'En cours de préparation'), array('numop' => 'desc'));
			
		}
		elseif ($applicationUSER == 'ToolboxInterv')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('install' => 1), array('numop' => 'desc'));
			
		}
		else
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('client' => $societeUSER, 'etat' => 'En cours de préparation'), array('numop' => 'desc'));
			$userid = $this->container->get('security.context')->getToken()->getUser()->getid();
			$entitiescomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->findBy(array('iduser' => $userid));
			if ($entitiescomarticlestmp) {

        
			foreach ($entitiescomarticlestmp as $articlestmp)
				{
					$idArticletmp = $articlestmp->getID();
					$entitycomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->find($idArticletmp);
			
					$em->remove($entitycomarticlestmp);
					$em->flush();
				}
			}
		}
		
		//$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:comclients:index.html.twig', array(
            'entities' => $entities
        ));
    }
	
	public function indexatexpAction()
    {
        
		$session = $this->get('session');
		$societeUSER = $session->get('societeUSER');
		$applicationUSER = $session->get('applicationUSER');
		
		$em = $this->getDoctrine()->getEntityManager();

        if ($applicationUSER == 'Toolbox')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('idclient' => null, 'etat' => 'Préparé en attente expédition'), array('numop' => 'desc'));
			
		}
		elseif ($applicationUSER == 'ToolboxInterv')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('install' => 1), array('numop' => 'desc'));
			
		}
		else
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('client' => $societeUSER, 'etat' => 'Préparé en attente expédition'), array('numop' => 'desc'));
			$userid = $this->container->get('security.context')->getToken()->getUser()->getid();
			$entitiescomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->findBy(array('iduser' => $userid));
			if ($entitiescomarticlestmp) {

        
			foreach ($entitiescomarticlestmp as $articlestmp)
				{
					$idArticletmp = $articlestmp->getID();
					$entitycomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->find($idArticletmp);
			
					$em->remove($entitycomarticlestmp);
					$em->flush();
				}
			}
		}
		
		//$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:comclients:index.html.twig', array(
            'entities' => $entities
        ));
    }
	
	public function indexenclivAction()
    {
        
		$session = $this->get('session');
		$societeUSER = $session->get('societeUSER');
		$applicationUSER = $session->get('applicationUSER');
		
		$em = $this->getDoctrine()->getEntityManager();

        if ($applicationUSER == 'Toolbox')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('idclient' => null, 'etat' => 'En cours de livraison'), array('numop' => 'desc'));
			
		}
		elseif ($applicationUSER == 'ToolboxInterv')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('install' => 1), array('numop' => 'desc'));
			
		}
		else
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('client' => $societeUSER, 'etat' => 'En cours de livraison'), array('numop' => 'desc'));
			$userid = $this->container->get('security.context')->getToken()->getUser()->getid();
			$entitiescomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->findBy(array('iduser' => $userid));
			if ($entitiescomarticlestmp) {

        
			foreach ($entitiescomarticlestmp as $articlestmp)
				{
					$idArticletmp = $articlestmp->getID();
					$entitycomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->find($idArticletmp);
			
					$em->remove($entitycomarticlestmp);
					$em->flush();
				}
			}
		}
		
		//$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:comclients:index.html.twig', array(
            'entities' => $entities
        ));
    }
	
	public function indexlivreAction()
    {
        
		$session = $this->get('session');
		$societeUSER = $session->get('societeUSER');
		$applicationUSER = $session->get('applicationUSER');
		
		$em = $this->getDoctrine()->getEntityManager();

        if ($applicationUSER == 'Toolbox')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('idclient' => null, 'etat' => 'Livré', 'archive' => 0), array('numop' => 'desc'));
			
		}
		elseif ($applicationUSER == 'ToolboxInterv')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('install' => 1), array('numop' => 'desc'));
			
		}
		else
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('client' => $societeUSER, 'etat' => 'Livré', 'archive' => 0), array('numop' => 'desc'));
			$userid = $this->container->get('security.context')->getToken()->getUser()->getid();
			$entitiescomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->findBy(array('iduser' => $userid));
			if ($entitiescomarticlestmp) {

        
			foreach ($entitiescomarticlestmp as $articlestmp)
				{
					$idArticletmp = $articlestmp->getID();
					$entitycomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->find($idArticletmp);
			
					$em->remove($entitycomarticlestmp);
					$em->flush();
				}
			}
		}
		
		//$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:comclients:indexlivre.html.twig', array(
            'entities' => $entities
        ));
    }
	
	public function indexprepafactAction()
    {
        
		$session = $this->get('session');
		$societeUSER = $session->get('societeUSER');
		$applicationUSER = $session->get('applicationUSER');
		
		$em = $this->getDoctrine()->getEntityManager();

        if ($applicationUSER == 'Toolbox')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('idclient' => null, 'etat' => 'Livré', 'archive' => 0, 'prefact'  => 0), array('numop' => 'desc'));
			
		}
		elseif ($applicationUSER == 'ToolboxInterv')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('install' => 1), array('numop' => 'desc'));
			
		}
		else
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('client' => $societeUSER, 'etat' => 'Livré', 'archive' => 0), array('numop' => 'desc'));
			
		}
		
		$entity = new comclients();
		$editForm = $this->createForm(new comclientsprepafactType(), $entity);
		
		//$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:comclients:indexprepafact.html.twig', array(
            'entities' => $entities,
			'edit_form'   => $editForm->createView()
        ));
    }
	
	public function updateprepafactAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('etat' => 'Livré', 'archive' => 0));

        $request = $this->getRequest();

        foreach ($entities as $entitycomclient)
				{
			
					$entityId = $entitycomclient->getId();
					
					$entity = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->find($entityId);
					
					
					
					$checksel = $request->request->get('checksel'.$entityId);
					
					if ($checksel == 'OK'){
					
						$prepa = $request->request->get('prepa'.$entityId);
						$admin = $request->request->get('admin'.$entityId);
						$pal = $request->request->get('pal'.$entityId);
						$fourn = $request->request->get('fourn'.$entityId);
						$transp = $request->request->get('transp'.$entityId);
						$recbon = $request->request->get('recbon'.$entityId);

						$entity->setPrefact(1);
						$entity->setPrepa($prepa);
						$entity->setAdmin($admin);
						$entity->setPale($pal);
						$entity->setFourniture($fourn);
						$entity->setTransp($transp);
						$entity->setRecupbon($recbon);
						$em->persist($entity);
						$em->flush();
					}
					
				}
        return $this->redirect($this->generateUrl('comclientsviewfact'));
    }
	
	public function indexviewfactAction()
    {
        
		$session = $this->get('session');
		$societeUSER = $session->get('societeUSER');
		$applicationUSER = $session->get('applicationUSER');
		
		$em = $this->getDoctrine()->getEntityManager();

        if ($applicationUSER == 'Toolbox')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('idclient' => null, 'etat' => 'Livré', 'archive' => 0, 'prefact'  => 1), array('numop' => 'desc'));
			
		}
		elseif ($applicationUSER == 'ToolboxInterv')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('install' => 1), array('numop' => 'desc'));
			
		}
		else
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('client' => $societeUSER, 'etat' => 'Livré', 'archive' => 0, 'prefact'  => 1), array('numop' => 'desc'));
			
		}
		
		
		
		//$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:comclients:indexviewfact.html.twig', array(
            'entities' => $entities
        ));
    }
	
	public function indexarchiveAction()
    {
        
		$session = $this->get('session');
		$societeUSER = $session->get('societeUSER');
		$applicationUSER = $session->get('applicationUSER');
		
		$em = $this->getDoctrine()->getEntityManager();

        if ($applicationUSER == 'Toolbox')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('idclient' => null, 'etat' => 'Livré', 'archive' => 1), array('numop' => 'desc'));
		}
		elseif ($applicationUSER == 'ToolboxInterv')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('install' => 1), array('numop' => 'desc'));
			
		}
		else
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('client' => $societeUSER, 'etat' => 'Livré', 'archive' => 1), array('numop' => 'desc'));
			$userid = $this->container->get('security.context')->getToken()->getUser()->getid();
			$entitiescomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->findBy(array('iduser' => $userid));
			if ($entitiescomarticlestmp) {

        
			foreach ($entitiescomarticlestmp as $articlestmp)
				{
					$idArticletmp = $articlestmp->getID();
					$entitycomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->find($idArticletmp);
			
					$em->remove($entitycomarticlestmp);
					$em->flush();
				}
			}
		}
		
		//$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:comclients:index.html.twig', array(
            'entities' => $entities
        ));
    }
	
	public function indexpdfAction()
    {
        
		$session = $this->get('session');
		$societeUSER = $session->get('societeUSER');
		$applicationUSER = $session->get('applicationUSER');
		
		$em = $this->getDoctrine()->getEntityManager();

        if ($applicationUSER == 'Toolbox')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('idclient' => null), array('numop' => 'desc'));
		}
		elseif ($applicationUSER == 'ToolboxInterv')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('install' => 1), array('numop' => 'desc'));
		}
		else
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('client' => $societeUSER), array('numop' => 'desc'));
			$userid = $this->container->get('security.context')->getToken()->getUser()->getid();
			$entitiescomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->findBy(array('iduser' => $userid));
			if ($entitiescomarticlestmp) {

        
			foreach ($entitiescomarticlestmp as $articlestmp)
				{
					$idArticletmp = $articlestmp->getID();
					$entitycomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->find($idArticletmp);
			
					$em->remove($entitycomarticlestmp);
					$em->flush();
				}
			}
		}
		
		//$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findAll();

        $html = $this->render('PhareosLogisToolBoxBundle:comclients:index2pdf.html.twig', array(
            'entities' => $entities
			));
		
		$html2pdf = new \Html2Pdf_Html2Pdf('L','A4','en', false, 'ISO-8859-1');
		$html2pdf->pdf->SetDisplayMode('real');
		$html2pdf->setDefaultFont('Arial');
		$html2pdf->writeHTML(utf8_decode($html), isset($_GET['vuehtml']));
		$fichier = $html2pdf->Output('Listing_op.pdf', 'D');

		$response = new Response();
		$response->clearHttpHeaders();
		$response->setContent(file_get_contents($fichier));
		$response->headers->set('Content-Type', 'application/force-download');
		$response->headers->set('Content-disposition', 'filename='. $fichier);

		return $response;
		
		//return $this->render('PhareosLogisToolBoxBundle:comclients:index.html.twig', array(
          //  'entities' => $entities
        //));
    }
	
	public function choixdestinationAction()
    {
        
		$session = $this->get('session');
		$session->set('liste_erreurs_session', '');
		
		$userid = $this->container->get('security.context')->getToken()->getUser()->getid();
		$societeUSER = $session->get('societeUSER');
		
		
		$em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosLogisToolBoxBundle:destination')->findBy(array('client' => $societeUSER));
		$entitiescomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_article_tmp')->findBy(array('iduser' => $userid));

		foreach ($entitiescomarticlestmp as $articlestmp)
		{
			$idArticletmp = $articlestmp->getID();
			$entitycomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_article_tmp')->find($idArticletmp);
			
			$em->remove($entitycomarticlestmp);
            $em->flush();
		}
		
        return $this->render('PhareosLogisToolBoxBundle:destination:choix.destination.html.twig', array(
            'entities' => $entities
        ));
    }
	
	public function visualisationstockAction()
    {
        
		$session = $this->get('session');
		$session->set('liste_erreurs_session', '');
		
		$societeUSER = $session->get('societeUSER');
		$applicationUSER = $session->get('applicationUSER');
		
		$em = $this->getDoctrine()->getEntityManager();

		if ($applicationUSER == 'Toolbox')
		{
			$entitiesarticles = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findAll();
		}
		else
		{
			$entitiesarticles = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findBy(array('client' => $societeUSER));
		}
		
		
		//$entitiesarticles = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findAll();
        

		
		
        return $this->render('PhareosLogisToolBoxBundle:comclients:visualisationstock.html.twig', array(
            'entities' => $entitiesarticles
        ));
    }
	
	

    /**
     * Finds and displays a comclients entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->find($id);
		
		//on recup l'id de livraison
		$entityIdLivr = $entity->getAdlivrid();
		
		//on recup l'entité livraison destination
		$entitylivraison = $em->getRepository('PhareosLogisToolBoxBundle:destination')->find($entityIdLivr);
		
		//on recup les articles commandés (tous les enregistrement ayant l'id de la commande
		$entitiesarticlescom = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findBy(array('idcomclients' => $id));
		
		//on recup les articles commandés
		$cpt=0;
		//$qTTarAY=array(NULL);
		$idArticlearAY=array(NULL,NULL);
		//$designatArticlarAY=array(NULL);
		//$comptId=array(NULL);
		$resultCount = count($entitiesarticlescom);

		//echo $resultCount;
		if ($resultCount > 1) //recherche si plusieurs articles
		{
		
		foreach ($entitiesarticlescom as $articlescom) //pour tous les articles de la commande
			{
			
			
			//qtte commandé
			$qtteArticleCom = $articlescom->getQtte();
			
			//id de l'article
			$idArticle = $articlescom->getIdarticles();
			
			//on recup l'enregistrement de l'artcile
			$Article = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticle);
			
			
			
			//designation de l'article
			$designatArticle = $Article->getDesignat();
			
			$allee = $Article->getAllee();
			$travee = $Article->getTravee();
			$niveau = $Article->getNiveau();
			$stockReel = $Article->getQtetot();
			$stockEnAttente = $Article->getQteencours();
			$stockReserve = $Article->getQtereserve();
			$emplacement = $Article->getEmplacement();
			$stockReelphys = $Article->getQtephys();
			
			
			
			//$qTTarAY[$cpt] = $qtteArticleCom;
			//$qTTarAY[$cpt] = $qtteArticleCom;
			//$designatArticlarAY[$cpt] = $designatArticle;
			//$comptId[$cpt] = $cpt;
			
			$idArticlearAY[$cpt] = array(
				'qtte' => $qtteArticleCom,
				'designat' => $designatArticle,
				'allee' => $allee,
				'travee' => $travee,
				'niveau' => $niveau,
				'stockreel' => $stockReel,
				'stockenattente' => $stockEnAttente,
				'stockreserve' => $stockReserve,
				'emplacement' => $emplacement,
				'stockreelphys' => $stockReelphys
			);
			
			//$cpttot = $cpt;
			
			$cpt = $cpt + 1;
			}
			
			return $this->render('PhareosLogisToolBoxBundle:comclients:show.html.twig', array(
			'resultcount' => $resultCount,
			'articles' => $Article,
			'idarticle'=> $idArticlearAY,
			'destination' => $entitylivraison,
			'entity'      => $entity,
			));
			$deleteForm = $this->createDeleteForm($id);
		}
		elseif ($resultCount == 1) //si un seul article //si un seul article
		{
			$entityarticlecom = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findOneBy(array('idcomclients' => $id));
			$qtteArticleCom = $entityarticlecom->getQtte();
			
			$idArticle = $entityarticlecom->getIdarticles();
			$Article = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticle);
			$designatArticle = $Article->getDesignat();
			
			$allee = $Article->getAllee();
			$travee = $Article->getTravee();
			$niveau = $Article->getNiveau();
			$stockReel = $Article->getQtetot();
			$stockEnAttente = $Article->getQteencours();
			$stockReserve = $Article->getQtereserve();
			$emplacement = $Article->getEmplacement();
			$stockReelphys = $Article->getQtephys();
			
			$idArticlearAY = array(
				'qtte' => $qtteArticleCom,
				'designat' => $designatArticle,
				'allee' => $allee,
				'travee' => $travee,
				'niveau' => $niveau,
				'stockreel' => $stockReel,
				'stockenattente' => $stockEnAttente,
				'stockreserve' => $stockReserve,
				'emplacement' => $emplacement,
				'stockreelphys' => $stockReelphys
			);
			
			return $this->render('PhareosLogisToolBoxBundle:comclients:show.html.twig', array(
			'resultcount' => $resultCount,
			'articles' => $Article,
			'qtte'		  => $qtteArticleCom,
			'designat'	  => $designatArticle,
			'destination' => $entitylivraison,
			'entity'      => $entity,
			'allee' => $allee,
			'travee' => $travee,
			'niveau' => $niveau,
			'stockreel' => $stockReel,
			'stockenattente' => $stockEnAttente,
			'stockreserve' => $stockReserve,
			'emplacement' => $emplacement,
			'stockreelphys' => $stockReelphys,
			));
			
			$deleteForm = $this->createDeleteForm($id);
		}
		else
		{
			
			
			return $this->render('PhareosLogisToolBoxBundle:comclients:show.html.twig', array(
			'resultcount' => $resultCount,
			'destination' => $entitylivraison,
			'entity'      => $entity,
			));
			
			$deleteForm = $this->createDeleteForm($id);
		}
		
		

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find comclients entity.');
        }

        
    }
	
	public function showlivreAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->find($id);
		
		//on recup l'id de livraison
		$entityIdLivr = $entity->getAdlivrid();
		
		//on recup l'entité livraison destination
		$entitylivraison = $em->getRepository('PhareosLogisToolBoxBundle:destination')->find($entityIdLivr);
		
		//on recup les articles commandés (tous les enregistrement ayant l'id de la commande
		$entitiesarticlescom = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findBy(array('idcomclients' => $id));
		
		//on recup les articles commandés
		$cpt=0;
		//$qTTarAY=array(NULL);
		$idArticlearAY=array(NULL,NULL);
		//$designatArticlarAY=array(NULL);
		//$comptId=array(NULL);
		$resultCount = count($entitiesarticlescom);

		//echo $resultCount;
		if ($resultCount > 1) //recherche si plusieurs articles
		{
		
		foreach ($entitiesarticlescom as $articlescom) //pour tous les articles de la commande
			{
			
			
			//qtte commandé
			$qtteArticleCom = $articlescom->getQtte();
			
			//id de l'article
			$idArticle = $articlescom->getIdarticles();
			
			//on recup l'enregistrement de l'artcile
			$Article = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticle);
			
			
			
			//designation de l'article
			$designatArticle = $Article->getDesignat();
			
			$allee = $Article->getAllee();
			$travee = $Article->getTravee();
			$niveau = $Article->getNiveau();
			$stockReel = $Article->getQtetot();
			$stockEnAttente = $Article->getQteencours();
			$stockReserve = $Article->getQtereserve();
			$emplacement = $Article->getEmplacement();
			$poids = $Article->getPoids();
			
			
			
			//$qTTarAY[$cpt] = $qtteArticleCom;
			//$qTTarAY[$cpt] = $qtteArticleCom;
			//$designatArticlarAY[$cpt] = $designatArticle;
			//$comptId[$cpt] = $cpt;
			
			$idArticlearAY[$cpt] = array(
				'qtte' => $qtteArticleCom,
				'designat' => $designatArticle,
				'allee' => $allee,
				'travee' => $travee,
				'niveau' => $niveau,
				'stockreel' => $stockReel,
				'stockenattente' => $stockEnAttente,
				'stockreserve' => $stockReserve,
				'emplacement' => $emplacement,
				'poids' => $poids
			);
			
			//$cpttot = $cpt;
			
			$cpt = $cpt + 1;
			}
			
			return $this->render('PhareosLogisToolBoxBundle:comclients:showlivre.html.twig', array(
			'resultcount' => $resultCount,
			'articles' => $Article,
			'idarticle'=> $idArticlearAY,
			'destination' => $entitylivraison,
			'entity'      => $entity,
			));
			$deleteForm = $this->createDeleteForm($id);
		}
		elseif ($resultCount == 1) //si un seul article //si un seul article
		{
			$entityarticlecom = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findOneBy(array('idcomclients' => $id));
			$qtteArticleCom = $entityarticlecom->getQtte();
			
			$idArticle = $entityarticlecom->getIdarticles();
			$Article = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticle);
			$designatArticle = $Article->getDesignat();
			
			$allee = $Article->getAllee();
			$travee = $Article->getTravee();
			$niveau = $Article->getNiveau();
			$stockReel = $Article->getQtetot();
			$stockEnAttente = $Article->getQteencours();
			$stockReserve = $Article->getQtereserve();
			$emplacement = $Article->getEmplacement();
			$poids = $Article->getPoids();
			
			$idArticlearAY = array(
				'qtte' => $qtteArticleCom,
				'designat' => $designatArticle,
				'allee' => $allee,
				'travee' => $travee,
				'niveau' => $niveau,
				'stockreel' => $stockReel,
				'stockenattente' => $stockEnAttente,
				'stockreserve' => $stockReserve,
				'emplacement' => $emplacement,
				'poids' => $poids
			);
			
			return $this->render('PhareosLogisToolBoxBundle:comclients:showlivre.html.twig', array(
			'resultcount' => $resultCount,
			'articles' => $Article,
			'qtte'		  => $qtteArticleCom,
			'designat'	  => $designatArticle,
			'destination' => $entitylivraison,
			'entity'      => $entity,
			'allee' => $allee,
			'travee' => $travee,
			'niveau' => $niveau,
			'stockreel' => $stockReel,
			'stockenattente' => $stockEnAttente,
			'stockreserve' => $stockReserve,
			'emplacement' => $emplacement,
			'poids' => $poids
			));
			
			$deleteForm = $this->createDeleteForm($id);
		}
		else
		{
			
			
			return $this->render('PhareosLogisToolBoxBundle:comclients:showlivre.html.twig', array(
			'resultcount' => $resultCount,
			'destination' => $entitylivraison,
			'entity'      => $entity,
			));
			
			$deleteForm = $this->createDeleteForm($id);
		}
		
		

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find comclients entity.');
        }

        
    }
	
	public function showviewAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->find($id);
		
		//on recup l'id de livraison
		$entityIdLivr = $entity->getAdlivrid();
		
		//on recup l'entité livraison destination
		$entitylivraison = $em->getRepository('PhareosLogisToolBoxBundle:destination')->find($entityIdLivr);
		
		//on recup les articles commandés (tous les enregistrement ayant l'id de la commande
		$entitiesarticlescom = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findBy(array('idcomclients' => $id));
		
		//on recup les articles commandés
		$cpt=0;
		//$qTTarAY=array(NULL);
		$idArticlearAY=array(NULL,NULL);
		//$designatArticlarAY=array(NULL);
		//$comptId=array(NULL);
		$resultCount = count($entitiesarticlescom);

		//echo $resultCount;
		if ($resultCount > 1) //recherche si plusieurs articles
		{
		
		foreach ($entitiesarticlescom as $articlescom) //pour tous les articles de la commande
			{
			
			
			//qtte commandé
			$qtteArticleCom = $articlescom->getQtte();
			
			//id de l'article
			$idArticle = $articlescom->getIdarticles();
			
			//on recup l'enregistrement de l'artcile
			$Article = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticle);
			
			
			
			//designation de l'article
			$designatArticle = $Article->getDesignat();
			
			$allee = $Article->getAllee();
			$travee = $Article->getTravee();
			$niveau = $Article->getNiveau();
			$stockReel = $Article->getQtetot();
			$stockEnAttente = $Article->getQteencours();
			$stockReserve = $Article->getQtereserve();
			$emplacement = $Article->getEmplacement();
			$poids = $Article->getPoids();
			$idart = $Article->getId();
			
			
			
			//$qTTarAY[$cpt] = $qtteArticleCom;
			//$qTTarAY[$cpt] = $qtteArticleCom;
			//$designatArticlarAY[$cpt] = $designatArticle;
			//$comptId[$cpt] = $cpt;
			
			$idArticlearAY[$cpt] = array(
				'qtte' => $qtteArticleCom,
				'designat' => $designatArticle,
				'allee' => $allee,
				'travee' => $travee,
				'niveau' => $niveau,
				'stockreel' => $stockReel,
				'stockenattente' => $stockEnAttente,
				'stockreserve' => $stockReserve,
				'emplacement' => $emplacement,
				'poids' => $poids,
				'idart' => $idart
			);
			
			//$cpttot = $cpt;
			
			$cpt = $cpt + 1;
			}
			
			return $this->render('PhareosLogisToolBoxBundle:comclients:showview.html.twig', array(
			'resultcount' => $resultCount,
			'articles' => $Article,
			'idarticle'=> $idArticlearAY,
			'destination' => $entitylivraison,
			'entity'      => $entity,
			));
			$deleteForm = $this->createDeleteForm($id);
		}
		elseif ($resultCount == 1) //si un seul article //si un seul article
		{
			$entityarticlecom = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findOneBy(array('idcomclients' => $id));
			$qtteArticleCom = $entityarticlecom->getQtte();
			
			$idArticle = $entityarticlecom->getIdarticles();
			$Article = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticle);
			$designatArticle = $Article->getDesignat();
			
			$allee = $Article->getAllee();
			$travee = $Article->getTravee();
			$niveau = $Article->getNiveau();
			$stockReel = $Article->getQtetot();
			$stockEnAttente = $Article->getQteencours();
			$stockReserve = $Article->getQtereserve();
			$emplacement = $Article->getEmplacement();
			$poids = $Article->getPoids();
			$idart = $Article->getId();
			
			$idArticlearAY = array(
				'qtte' => $qtteArticleCom,
				'designat' => $designatArticle,
				'allee' => $allee,
				'travee' => $travee,
				'niveau' => $niveau,
				'stockreel' => $stockReel,
				'stockenattente' => $stockEnAttente,
				'stockreserve' => $stockReserve,
				'emplacement' => $emplacement,
				'poids' => $poids,
				'idart' => $idart
			);
			
			return $this->render('PhareosLogisToolBoxBundle:comclients:showview.html.twig', array(
			'resultcount' => $resultCount,
			'articles' => $Article,
			'qtte'		  => $qtteArticleCom,
			'designat'	  => $designatArticle,
			'destination' => $entitylivraison,
			'entity'      => $entity,
			'allee' => $allee,
			'travee' => $travee,
			'niveau' => $niveau,
			'stockreel' => $stockReel,
			'stockenattente' => $stockEnAttente,
			'stockreserve' => $stockReserve,
			'emplacement' => $emplacement,
			'poids' => $poids,
			'idart' => $idart
			));
			
			$deleteForm = $this->createDeleteForm($id);
		}
		else
		{
			
			
			return $this->render('PhareosLogisToolBoxBundle:comclients:showview.html.twig', array(
			'resultcount' => $resultCount,
			'destination' => $entitylivraison,
			'entity'      => $entity,
			));
			
			$deleteForm = $this->createDeleteForm($id);
		}
		
		

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find comclients entity.');
        }

        
    }
	
	public function showpanAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->find($id);
		
		//on recup l'id de livraison
		$entityIdLivr = $entity->getAdlivrid();
		
		//on recup l'entité livraison destination
		$entitylivraison = $em->getRepository('PhareosLogisToolBoxBundle:destination')->find($entityIdLivr);
		
		//on recup les articles commandés (tous les enregistrement ayant l'id de la commande
		$entitiesarticlescom = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findBy(array('idcomclients' => $id));
		
		//on recup les articles commandés
		$cpt=0;
		//$qTTarAY=array(NULL);
		$idArticlearAY=array(NULL,NULL);
		//$designatArticlarAY=array(NULL);
		//$comptId=array(NULL);
		$resultCount = count($entitiesarticlescom);

		//echo $resultCount;
		if ($resultCount > 1) //recherche si plusieurs articles
		{
		
		foreach ($entitiesarticlescom as $articlescom) //pour tous les articles de la commande
			{
			
			
			//qtte commandé
			$qtteArticleCom = $articlescom->getQtte();
			
			//id de l'article
			$idArticle = $articlescom->getIdarticles();
			
			//on recup l'enregistrement de l'artcile
			$Article = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticle);
			
			
			
			//designation de l'article
			$designatArticle = $Article->getDesignat();
			
			$allee = $Article->getAllee();
			$travee = $Article->getTravee();
			$niveau = $Article->getNiveau();
			$stockReel = $Article->getQtetot();
			$stockEnAttente = $Article->getQteencours();
			$stockReserve = $Article->getQtereserve();
			$emplacement = $Article->getEmplacement();
			
			
			
			//$qTTarAY[$cpt] = $qtteArticleCom;
			//$qTTarAY[$cpt] = $qtteArticleCom;
			//$designatArticlarAY[$cpt] = $designatArticle;
			//$comptId[$cpt] = $cpt;
			
			$idArticlearAY[$cpt] = array(
				'qtte' => $qtteArticleCom,
				'designat' => $designatArticle,
				'allee' => $allee,
				'travee' => $travee,
				'niveau' => $niveau,
				'stockreel' => $stockReel,
				'stockenattente' => $stockEnAttente,
				'stockreserve' => $stockReserve,
				'emplacement' => $emplacement
			);
			
			//$cpttot = $cpt;
			
			$cpt = $cpt + 1;
			}
			
			return $this->render('PhareosLogisToolBoxBundle:comclients:showpan.html.twig', array(
			'resultcount' => $resultCount,
			'articles' => $Article,
			'idarticle'=> $idArticlearAY,
			'destination' => $entitylivraison,
			'entity'      => $entity,
			));
			$deleteForm = $this->createDeleteForm($id);
		}
		elseif ($resultCount == 1) //si un seul article //si un seul article
		{
			$entityarticlecom = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findOneBy(array('idcomclients' => $id));
			$qtteArticleCom = $entityarticlecom->getQtte();
			
			$idArticle = $entityarticlecom->getIdarticles();
			$Article = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticle);
			$designatArticle = $Article->getDesignat();
			
			$allee = $Article->getAllee();
			$travee = $Article->getTravee();
			$niveau = $Article->getNiveau();
			$stockReel = $Article->getQtetot();
			$stockEnAttente = $Article->getQteencours();
			$stockReserve = $Article->getQtereserve();
			$emplacement = $Article->getEmplacement();
			
			$idArticlearAY = array(
				'qtte' => $qtteArticleCom,
				'designat' => $designatArticle,
				'allee' => $allee,
				'travee' => $travee,
				'niveau' => $niveau,
				'stockreel' => $stockReel,
				'stockenattente' => $stockEnAttente,
				'stockreserve' => $stockReserve,
				'emplacement' => $emplacement
			);
			
			return $this->render('PhareosLogisToolBoxBundle:comclients:showpan.html.twig', array(
			'resultcount' => $resultCount,
			'articles' => $Article,
			'qtte'		  => $qtteArticleCom,
			'designat'	  => $designatArticle,
			'destination' => $entitylivraison,
			'entity'      => $entity,
			'allee' => $allee,
			'travee' => $travee,
			'niveau' => $niveau,
			'stockreel' => $stockReel,
			'stockenattente' => $stockEnAttente,
			'stockreserve' => $stockReserve,
			'emplacement' => $emplacement
			));
			
			$deleteForm = $this->createDeleteForm($id);
		}
		else
		{
			
			
			return $this->render('PhareosLogisToolBoxBundle:comclients:showpan.html.twig', array(
			'resultcount' => $resultCount,
			'destination' => $entitylivraison,
			'entity'      => $entity,
			));
			
			$deleteForm = $this->createDeleteForm($id);
		}
		
		

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find comclients entity.');
        }

        
    }
	
	public function validpanAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->find($id);
		
		$entitiescomarticles = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findBy(array('idcomclients' => $id));
		
		$poidsArticleTot = 0;
		$volumeArticleTot = 0;
					
					foreach ($entitiescomarticles as $entitycomarticle){
						$qTteArticletmp = $entitycomarticle->getQtte();//qtte articles commandé
						$idArticlecom = $entitycomarticle->getIdarticles();//id article commandé
						//recherche de l'article concerné
						$articlesentities = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticlecom);
						
						//on récupère le poids de l'artcile concerné
						$poidsArticle = $articlesentities->getPoids();
				
						//on calcul le poids total pour chaque référence d'articles (articles de même référence * qantité)
						$poidsArticleRef = $poidsArticle * $qTteArticletmp;
				
						//on calcul le poids total de tous les articles
						$poidsArticleTot += $poidsArticleRef;
				
						//on récupère la largeur de l'article concerné
						$largeurArticle = $articlesentities->getLarge();
				
						//on récupère la hauteur de l'article concerné
						$hautArticle = $articlesentities->getHaut();
				
						//on récupère la profondeur de l'article concerné
						$profondArticle = $articlesentities->getProfond();
				
						//on calcul le volume de l'article concerné
						$volumeArticle = $largeurArticle * $hautArticle * $profondArticle;
				
						//on calcul le volume total de tous les articles de la même référence (articles de même référence * qantité)
						$volumeArticleRef = $volumeArticle * $qTteArticletmp;
				
						//on calcul le volume total de tous les articles de la commande
						$volumeArticleTot += $volumeArticleRef;
						
						//on recalcul la quantité réservé
						$qteancreserve = $articlesentities->getQtereserve(); //ancienne quantitée
						$qtenewreserve = $qteancreserve - $qTteArticletmp;
						//on met a jour la nouvelle qtte réservé
						//$articlesentities->setQtereserve($qtenewreserve);
						
						//on recalcul le nouveau stock réel (stock visu client)
						$qteancstock = $articlesentities->getQtetot(); //ancien stock réel (stock visu client)
						$qtenewstock = $qteancstock - $qTteArticletmp;
						//on met a jour le nouveau stock réel (stock visu client)
						$articlesentities->setQtetot($qtenewstock);
						
						//$em->persist($articlesentities);
						//$em->flush();
						
						
						
					}
					
					$entity->setEtat('En attente de préparation');
					$entity->setPoids($poidsArticleTot);
					$entity->setVolume($volumeArticleTot);
					$em->persist($entity);
					$em->flush();
		
		
		
		

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find comclients entity.');
        }
		
		return $this->redirect($this->generateUrl('comclientsprepavirt'));

        
    }
	
	public function showblAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->find($id);
		
		$societedestin = $entity->getClient();
		
		$logodestin = $em->getRepository('PhareosLogisToolBoxBundle:clienttool')->findOneBy(array('nom' => $societedestin));
		
		$logodestin2 = $logodestin->getLogo();
		
		//on recup l'id de livraison
		$entityIdLivr = $entity->getAdlivrid();
		
		//on recup l'entité livraison destination
		$entitylivraison = $em->getRepository('PhareosLogisToolBoxBundle:destination')->find($entityIdLivr);
		
		//on recup les articles commandés (tous les enregistrement ayant l'id de la commande
		$entitiesarticlescom = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findBy(array('idcomclients' => $id));
		
		//on recup les articles commandés
		$cpt=0;
		//$qTTarAY=array(NULL);
		$idArticlearAY=array(NULL,NULL);
		//$designatArticlarAY=array(NULL);
		//$comptId=array(NULL);
		$resultCount = count($entitiesarticlescom);

		//echo $resultCount;
		if ($resultCount > 1) //recherche si plusieurs articles
		{
		
		foreach ($entitiesarticlescom as $articlescom) //pour tous les articles de la commande
			{
			
			
			//qtte commandé
			$qtteArticleCom = $articlescom->getQtte();
			
			//id de l'article
			$idArticle = $articlescom->getIdarticles();
			
			//on recup l'enregistrement de l'artcile
			$Article = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticle);
			
			
			
			//designation de l'article
			$designatArticle = $Article->getDesignat();
			$reference = $Article->getReference();
			
			$allee = $Article->getAllee();
			$travee = $Article->getTravee();
			$niveau = $Article->getNiveau();
			$emplacement = $Article->getEmplacement();
			
			
			
			//$qTTarAY[$cpt] = $qtteArticleCom;
			//$qTTarAY[$cpt] = $qtteArticleCom;
			//$designatArticlarAY[$cpt] = $designatArticle;
			//$comptId[$cpt] = $cpt;
			
			$idArticlearAY[$cpt] = array(
				'qtte' => $qtteArticleCom,
				'designat' => $designatArticle,
				'reference' => $reference,
				'allee' => $allee,
				'travee' => $travee,
				'niveau' => $niveau,
				'emplacement' => $emplacement
			);
			
			//$cpttot = $cpt;
			
			$cpt = $cpt + 1;
			}
			
			return $this->render('PhareosLogisToolBoxBundle:comclients:showbl.html.twig', array(
			'logodestin2' => $logodestin2,
			'resultcount' => $resultCount,
			'articles' => $Article,
			'idarticle'=> $idArticlearAY,
			'destination' => $entitylivraison,
			'entity'      => $entity,
			));
			$deleteForm = $this->createDeleteForm($id);
		}
		elseif ($resultCount == 1) //si un seul article //si un seul article
		{
			$entityarticlecom = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findOneBy(array('idcomclients' => $id));
			$qtteArticleCom = $entityarticlecom->getQtte();
			
			$idArticle = $entityarticlecom->getIdarticles();
			$Article = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticle);
			$designatArticle = $Article->getDesignat();
			$reference = $Article->getReference();
			
			$allee = $Article->getAllee();
			$travee = $Article->getTravee();
			$niveau = $Article->getNiveau();
			$emplacement = $Article->getEmplacement();
			
			$idArticlearAY = array(
				'qtte' => $qtteArticleCom,
				'designat' => $designatArticle,
				'reference' => $reference,
				'allee' => $allee,
				'travee' => $travee,
				'niveau' => $niveau,
				'emplacement' => $emplacement
			);
			
			return $this->render('PhareosLogisToolBoxBundle:comclients:showbl.html.twig', array(
			'logodestin2' => $logodestin2,
			'resultcount' => $resultCount,
			'articles' => $Article,
			'qtte'		  => $qtteArticleCom,
			'designat'	  => $designatArticle,
			'reference' => $reference,
			'destination' => $entitylivraison,
			'entity'      => $entity,
			'allee' => $allee,
			'travee' => $travee,
			'niveau' => $niveau,
			'emplacement' => $emplacement
			));
			
			$deleteForm = $this->createDeleteForm($id);
		}
		else
		{
			
			
			return $this->render('PhareosLogisToolBoxBundle:comclients:showbl.html.twig', array(
			'logodestin2' => $logodestin2,
			'resultcount' => $resultCount,
			'destination' => $entitylivraison,
			'entity'      => $entity,
			));
			
			$deleteForm = $this->createDeleteForm($id);
		}
		
		

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find comclients entity.');
        }

        
    }
	
	public function showprepaAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->find($id);
		
		$societedestin = $entity->getClient();
		
		$logodestin = $em->getRepository('PhareosLogisToolBoxBundle:clienttool')->findOneBy(array('nom' => $societedestin));
		
		$logodestin2 = $logodestin->getLogo();
		
		//on recup l'id de livraison
		$entityIdLivr = $entity->getAdlivrid();
		
		//on recup l'entité livraison destination
		$entitylivraison = $em->getRepository('PhareosLogisToolBoxBundle:destination')->find($entityIdLivr);
		
		//on recup les articles commandés (tous les enregistrement ayant l'id de la commande
		$entitiesarticlescom = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findBy(array('idcomclients' => $id));
		
		//on recup les articles commandés
		$cpt=0;
		//$qTTarAY=array(NULL);
		$idArticlearAY=array(NULL,NULL);
		//$designatArticlarAY=array(NULL);
		//$comptId=array(NULL);
		$resultCount = count($entitiesarticlescom);

		//echo $resultCount;
		if ($resultCount > 1) //recherche si plusieurs articles
		{
		
		foreach ($entitiesarticlescom as $articlescom) //pour tous les articles de la commande
			{
			
			
			//qtte commandé
			$qtteArticleCom = $articlescom->getQtte();
			
			//id de l'article
			$idArticle = $articlescom->getIdarticles();
			
			//on recup l'enregistrement de l'artcile
			$Article = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticle);
			
			
			
			//designation de l'article
			$designatArticle = $Article->getDesignat();
			
			$allee = $Article->getAllee();
			$travee = $Article->getTravee();
			$niveau = $Article->getNiveau();
			$stockReel = $Article->getQtetot();
			$stockEnAttente = $Article->getQteencours();
			$stockReserve = $Article->getQtereserve();
			$emplacement = $Article->getEmplacement();
			$stockReelphys = $Article->getQtephys();
			
			
			
			//$qTTarAY[$cpt] = $qtteArticleCom;
			//$qTTarAY[$cpt] = $qtteArticleCom;
			//$designatArticlarAY[$cpt] = $designatArticle;
			//$comptId[$cpt] = $cpt;
			
			$idArticlearAY[$cpt] = array(
				'qtte' => $qtteArticleCom,
				'designat' => $designatArticle,
				'allee' => $allee,
				'travee' => $travee,
				'niveau' => $niveau,
				'stockreel' => $stockReel,
				'stockenattente' => $stockEnAttente,
				'stockreserve' => $stockReserve,
				'emplacement' => $emplacement,
				'stockreelphys' => $stockReelphys
			);
			
			//$cpttot = $cpt;
			
			$cpt = $cpt + 1;
			}
			
			return $this->render('PhareosLogisToolBoxBundle:comclients:showprepa.html.twig', array(
			'logodestin2' => $logodestin2,
			'resultcount' => $resultCount,
			'articles' => $Article,
			'idarticle'=> $idArticlearAY,
			'destination' => $entitylivraison,
			'entity'      => $entity,
			));
			$deleteForm = $this->createDeleteForm($id);
		}
		elseif ($resultCount == 1) //si un seul article //si un seul article
		{
			$entityarticlecom = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findOneBy(array('idcomclients' => $id));
			$qtteArticleCom = $entityarticlecom->getQtte();
			
			$idArticle = $entityarticlecom->getIdarticles();
			$Article = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticle);
			$designatArticle = $Article->getDesignat();
			
			$allee = $Article->getAllee();
			$travee = $Article->getTravee();
			$niveau = $Article->getNiveau();
			$stockReel = $Article->getQtetot();
			$stockEnAttente = $Article->getQteencours();
			$stockReserve = $Article->getQtereserve();
			$emplacement = $Article->getEmplacement();
			$stockReelphys = $Article->getQtephys();
			
			$idArticlearAY = array(
				'qtte' => $qtteArticleCom,
				'designat' => $designatArticle,
				'allee' => $allee,
				'travee' => $travee,
				'niveau' => $niveau,
				'stockreel' => $stockReel,
				'stockenattente' => $stockEnAttente,
				'stockreserve' => $stockReserve,
				'emplacement' => $emplacement,
				'stockreelphys' => $stockReelphys
			);
			
			return $this->render('PhareosLogisToolBoxBundle:comclients:showprepa.html.twig', array(
			'logodestin2' => $logodestin2,
			'resultcount' => $resultCount,
			'articles' => $Article,
			'qtte'		  => $qtteArticleCom,
			'designat'	  => $designatArticle,
			'destination' => $entitylivraison,
			'entity'      => $entity,
			'allee' => $allee,
			'travee' => $travee,
			'niveau' => $niveau,
			'stockreel' => $stockReel,
			'stockenattente' => $stockEnAttente,
			'stockreserve' => $stockReserve,
			'emplacement' => $emplacement,
			'stockreelphys' => $stockReelphys
			));
			
			$deleteForm = $this->createDeleteForm($id);
		}
		else
		{
			
			
			return $this->render('PhareosLogisToolBoxBundle:comclients:showprepa.html.twig', array(
			'logodestin2' => $logodestin2,
			'resultcount' => $resultCount,
			'destination' => $entitylivraison,
			'entity'      => $entity,
			));
			
			$deleteForm = $this->createDeleteForm($id);
		}
		
		

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find comclients entity.');
        }

        
    }
	
	    /**
     * Finds and displays a comclients entity.
     *
     */
    public function editclientAction($id)
    {
        $session = $this->get('session');
		
		$societeUSER = $session->get('societeUSER');
		
		$session->set('toolcomClient', $id);
		
		
		$em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->find($id);
		
		$editForm = $this->createForm(new comclientsshoweditType(), $entity);
        $deleteForm = $this->createDeleteForm($id);
		
		//on recup l'id de livraison
		$entityIdLivr = $entity->getAdlivrid();
		
		//on recup l'entité livraison destination
		$entitylivraison = $em->getRepository('PhareosLogisToolBoxBundle:destination')->find($entityIdLivr);
		
		$entitylivraisonchoix = $em->getRepository('PhareosLogisToolBoxBundle:destination')->findBy(array('client' => $societeUSER));
		
		//on recup les articles commandés (tous les enregistrement ayant l'id de la commande
		$entitiesarticlescom = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findBy(array('idcomclients' => $id));
		$entitiesarticlestot = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findBy(array('client' => $societeUSER),
                                      array('designat' => 'ASC'));
		
		//on recup les articles commandés
		$cpt=0;
		//$qTTarAY=array(NULL);
		$idArticlearAY=array(NULL,NULL);
		//$designatArticlarAY=array(NULL);
		//$comptId=array(NULL);
		$resultCount = count($entitiesarticlescom);

		//echo $resultCount;
		if ($resultCount > 1) //recherche si plusieurs articles
		{
		
		foreach ($entitiesarticlescom as $articlescom) //pour tous les articles de la commande
			{
			
			
			//id table com_article (articlescom)
			$idArticlecom = $articlescom->getId();
			
			//qtte commandé
			$qtteArticleCom = $articlescom->getQtte();
			
			//id de l'article
			$idArticle = $articlescom->getIdarticles();
			
			//on recup l'enregistrement de l'artcile
			$Article = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticle);
			
			
			
			//designation de l'article
			$designatArticle = $Article->getDesignat();
			
			
			
			//$qTTarAY[$cpt] = $qtteArticleCom;
			//$qTTarAY[$cpt] = $qtteArticleCom;
			//$designatArticlarAY[$cpt] = $designatArticle;
			//$comptId[$cpt] = $cpt;
			
			$idArticlearAY[$cpt] = array(
				'qtte' => $qtteArticleCom,
				'designat' => $designatArticle,
				'idarticle' => $idArticle,
				'idarticlecom' => $idArticlecom,
			);
			
			//$cpttot = $cpt;
			
			$cpt = $cpt + 1;
			}
			
			return $this->render('PhareosLogisToolBoxBundle:comclients:showedit.html.twig', array(
			'resultcount' => $resultCount,
			'articles' => $Article,
			'idarticle'=> $idArticlearAY,
			'destination' => $entitylivraison,
			'entity'      => $entity,
			'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
			'destinationchoix' => $entitylivraisonchoix,
			'entitiesarticlestot' => $entitiesarticlestot,
			));
			$deleteForm = $this->createDeleteForm($id);
		}
		elseif ($resultCount == 1) //si un seul article
		{
			$entityarticlecom = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findOneBy(array('idcomclients' => $id));
			$qtteArticleCom = $entityarticlecom->getQtte();
			
			$idArticle = $entityarticlecom->getIdarticles();
			$Article = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticle);
			$designatArticle = $Article->getDesignat();
			$idArticlecomsingle = $entityarticlecom->getId();
			
			$idArticlearAY = array(
				'qtte' => $qtteArticleCom,
				'designat' => $designatArticle
			);
			
			return $this->render('PhareosLogisToolBoxBundle:comclients:showedit.html.twig', array(
			'resultcount' => $resultCount,
			'articles' => $Article,
			'qtte'		  => $qtteArticleCom,
			'idarticle'	  => $idArticle,
			'designat'	  => $designatArticle,
			'destination' => $entitylivraison,
			'entity'      => $entity,
			'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
			'idarticlecom' => $idArticlecomsingle,
			'destinationchoix' => $entitylivraisonchoix,
			'entitiesarticlestot' => $entitiesarticlestot,
			));
			
			$deleteForm = $this->createDeleteForm($id);
		}
		else
		{
			
			
			return $this->render('PhareosLogisToolBoxBundle:comclients:showedit.html.twig', array(
			'resultcount' => $resultCount,
			'destination' => $entitylivraison,
			'entity'      => $entity,
			'destinationchoix' => $entitylivraisonchoix,
			'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
			));
			
			$deleteForm = $this->createDeleteForm($id);
		}
		

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find comclients entity.');
        }

        
    }
	
	public function updateclientAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find comclients entity.');
        }

        $editForm   = $this->createForm(new comclientsshoweditType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);
		
		$datelivrsouh = $request->request->get('datelivrsouh');
		$datelivrsouh2 = new \DateTime($datelivrsouh);
		
		$entity->setDatelivrsouh($datelivrsouh2);
		

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('comclients'));
        }
		
		return $this->redirect($this->generateUrl('comclients'));

        //return $this->render('PhareosLogisToolBoxBundle:comclients:edit.html.twig', array(
          //  'entity'      => $entity,
           // 'edit_form'   => $editForm->createView(),
           // 'delete_form' => $deleteForm->createView(),
        //));
    }
	
	public function editpanAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
		
		$session = $this->get('session');
		$societeUSER = $session->get('societeUSER');

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->find($id);
		
		
		$editForm   = $this->createForm(new comclientseditpanType(), $entity);
		
		$deleteForm = $this->createDeleteForm($id);
		
		//on recup l'id de livraison
		$entityIdLivr = $entity->getAdlivrid();
		
		//on recup l'entité livraison destination
		$entitylivraison = $em->getRepository('PhareosLogisToolBoxBundle:destination')->find($entityIdLivr);
		
		//on recup les articles commandés (tous les enregistrement ayant l'id de la commande
		$entitiesarticlescom = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findBy(array('idcomclients' => $id));
		
		//on recup les articles commandés
		$cpt=0;
		//$qTTarAY=array(NULL);
		$idArticlearAY=array(NULL,NULL);
		//$designatArticlarAY=array(NULL);
		//$comptId=array(NULL);
		$resultCount = count($entitiesarticlescom);
		
		$Articlepan = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findBy(array('client' => $societeUSER),
                                      array('designat' => 'ASC'));

		//echo $resultCount;
		if ($resultCount > 1) //recherche si plusieurs articles
		{
		
		foreach ($entitiesarticlescom as $articlescom) //pour tous les articles de la commande
			{
			
			
			//qtte commandé
			$qtteArticleCom = $articlescom->getQtte();
			
			//id de l'article
			$idArticle = $articlescom->getIdarticles();
			
			$product_list = $idArticle . '|'. $qtteArticleCom;
			
			
			 
			
			//on recup l'enregistrement de l'article
			$Article = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticle);
			
			
			
			
			
			//designation de l'article
			$designatArticle = $Article->getDesignat();
			
			$allee = $Article->getAllee();
			$travee = $Article->getTravee();
			$niveau = $Article->getNiveau();
			$categorie = $Article->getCategorie();
			$fournisseur = $Article->getFourniss();
			$stockReel = $Article->getQtetot();
			$stockEnAttente = $Article->getQteencours();
			$stockReserve = $Article->getQtereserve();
			$emplacement = $Article->getEmplacement();
			
			
			
			//$qTTarAY[$cpt] = $qtteArticleCom;
			//$qTTarAY[$cpt] = $qtteArticleCom;
			//$designatArticlarAY[$cpt] = $designatArticle;
			//$comptId[$cpt] = $cpt;
			
			$idArticlearAY[$cpt] = array(
				'idart' => $idArticle,
				'qtte' => $qtteArticleCom,
				'designat' => $designatArticle,
				'allee' => $allee,
				'travee' => $travee,
				'niveau' => $niveau,
				'categorie' => $categorie,
				'fournisseur' => $fournisseur,
				'stockreel' => $stockReel,
				'stockenattente' => $stockEnAttente,
				'stockreserve' => $stockReserve,
				'emplacement' => $emplacement,
				'products_selected' => $product_list
			);
			
			//$cpttot = $cpt;
			
			$cpt = $cpt + 1;
			}
			
			
			
			return $this->render('PhareosLogisToolBoxBundle:comclients:editpan.html.twig', array(
			'resultcount' => $resultCount,
			'articles' => $Article,
			'idarticle'=> $idArticlearAY,
			'destination' => $entitylivraison,
			'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
			'delete_form' => $deleteForm->createView(),
			'entitiesarticles' => $Articlepan,
			'products_selected' => $product_list
			));
			
		}
		elseif ($resultCount == 1) //si un seul article //si un seul article
		{
			$entityarticlecom = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findOneBy(array('idcomclients' => $id));
			$qtteArticleCom = $entityarticlecom->getQtte();
			
			$idArticle = $entityarticlecom->getIdarticles();
			$Article = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticle);
			$designatArticle = $Article->getDesignat();
			
			$allee = $Article->getAllee();
			$travee = $Article->getTravee();
			$niveau = $Article->getNiveau();
			$categorie = $Article->getCategorie();
			$fournisseur = $Article->getFourniss();
			$stockReel = $Article->getQtetot();
			$stockEnAttente = $Article->getQteencours();
			$stockReserve = $Article->getQtereserve();
			$emplacement = $Article->getEmplacement();
			
			$product_list = $idArticle . '|'. $qtteArticleCom;
			
			$idArticlearAY = array(
				'idart' => $idArticle,
				'qtte' => $qtteArticleCom,
				'designat' => $designatArticle,
				'allee' => $allee,
				'travee' => $travee,
				'niveau' => $niveau,
				'categorie' => $categorie,
				'fournisseur' => $fournisseur,
				'stockreel' => $stockReel,
				'stockenattente' => $stockEnAttente,
				'stockreserve' => $stockReserve,
				'emplacement' => $emplacement,
				'products_selected' => $product_list
			);
			
			
			
			return $this->render('PhareosLogisToolBoxBundle:comclients:editpan.html.twig', array(
			'resultcount' => $resultCount,
			'idart' => $idArticle,
			'articles' => $Article,
			'qtte'		  => $qtteArticleCom,
			'designat'	  => $designatArticle,
			'destination' => $entitylivraison,
			'entity'      => $entity,
			'allee' => $allee,
			'travee' => $travee,
			'niveau' => $niveau,
			'categorie' => $categorie,
			'fournisseur' => $fournisseur,
			'stockreel' => $stockReel,
			'stockenattente' => $stockEnAttente,
			'stockreserve' => $stockReserve,
			'emplacement' => $emplacement,
            'edit_form'   => $editForm->createView(),
			'delete_form' => $deleteForm->createView(),
			'products_selected' => $product_list,
			'entitiesarticles' => $Articlepan,
			'idarticle'=> $idArticlearAY
			));
			
			
		}
		else
		{
			
			
			return $this->render('PhareosLogisToolBoxBundle:comclients:editpan.html.twig', array(
			'resultcount' => $resultCount,
			'destination' => $entitylivraison,
			'entity'      => $entity,
			'entitiesarticles' => $Articlepan
			));
			
			
		}
		
		

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find comclients entity.');
        }

        
    }
	
	public function showpdfAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->find($id);
		
		//on recup l'id de livraison
		$entityIdLivr = $entity->getAdlivrid();
		
		//on recup l'entité livraison destination
		$entitylivraison = $em->getRepository('PhareosLogisToolBoxBundle:destination')->find($entityIdLivr);
		
		//on recup les articles commandés (tous les enregistrement ayant l'id de la commande
		$entitiesarticlescom = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findBy(array('idcomclients' => $id));
		
		//on recup les articles commandés
		$cpt=0;
		//$qTTarAY=array(NULL);
		$idArticlearAY=array(NULL,NULL);
		//$designatArticlarAY=array(NULL);
		//$comptId=array(NULL);
		$resultCount = count($entitiesarticlescom);

		//echo $resultCount;
		if ($resultCount > 1) //recherche si plusieurs articles
		{
		
		foreach ($entitiesarticlescom as $articlescom) //pour tous les articles de la commande
			{
			
			
			//qtte commandé
			$qtteArticleCom = $articlescom->getQtte();
			
			//id de l'article
			$idArticle = $articlescom->getIdarticles();
			
			//on recup l'enregistrement de l'artcile
			$Article = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticle);
			
			
			
			//designation de l'article
			$designatArticle = $Article->getDesignat();
			
			$allee = $Article->getAllee();
			$travee = $Article->getTravee();
			$niveau = $Article->getNiveau();
			$emplacement = $Article->getEmplacement();
			
			
			
			//$qTTarAY[$cpt] = $qtteArticleCom;
			//$qTTarAY[$cpt] = $qtteArticleCom;
			//$designatArticlarAY[$cpt] = $designatArticle;
			//$comptId[$cpt] = $cpt;
			
			$idArticlearAY[$cpt] = array(
				'qtte' => $qtteArticleCom,
				'designat' => $designatArticle,
				'allee' => $allee,
				'travee' => $travee,
				'niveau' => $niveau,
				'emplacement' => $emplacement
			);
			
			//$cpttot = $cpt;
			
			$cpt = $cpt + 1;
			}
			
			
			$html = $this->render('PhareosLogisToolBoxBundle:comclients:show2pdf.html.twig', array(
            'resultcount' => $resultCount,
			'articles' => $Article,
			'idarticle'=> $idArticlearAY,
			'destination' => $entitylivraison,
			'entity'      => $entity,
			));
		
			$html2pdf = new \Html2Pdf_Html2Pdf('L','A4','en', false, 'ISO-8859-1');
			$html2pdf->pdf->SetDisplayMode('real');
			$html2pdf->setDefaultFont('Arial');
			$html2pdf->writeHTML(utf8_decode($html), isset($_GET['vuehtml']));
			$fichier = $html2pdf->Output('Listing_op.pdf', 'D');

			$response = new Response();
			$response->clearHttpHeaders();
			$response->setContent(file_get_contents($fichier));
			$response->headers->set('Content-Type', 'application/force-download');
			$response->headers->set('Content-disposition', 'filename='. $fichier);

			return $response;
			
			//return $this->render('PhareosLogisToolBoxBundle:comclients:show.html.twig', array(
			//'resultcount' => $resultCount,
			//'articles' => $Article,
			//'idarticle'=> $idArticlearAY,
			//'destination' => $entitylivraison,
			//'entity'      => $entity,
			//));
			$deleteForm = $this->createDeleteForm($id);
		}
		else //si un seul article
		{
			$entityarticlecom = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findOneBy(array('idcomclients' => $id));
			$qtteArticleCom = $entityarticlecom->getQtte();
			
			$idArticle = $entityarticlecom->getIdarticles();
			$Article = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticle);
			$designatArticle = $Article->getDesignat();
			
			$allee = $Article->getAllee();
			$travee = $Article->getTravee();
			$niveau = $Article->getNiveau();
			$emplacement = $Article->getEmplacement();
			
			$idArticlearAY = array(
				'qtte' => $qtteArticleCom,
				'designat' => $designatArticle,
				'allee' => $allee,
				'travee' => $travee,
				'niveau' => $niveau,
				'emplacement' => $emplacement
			);
			
			$html = $this->render('PhareosLogisToolBoxBundle:comclients:show2pdf.html.twig', array(
            'resultcount' => $resultCount,
			'articles' => $Article,
			'qtte'		  => $qtteArticleCom,
			'designat'	  => $designatArticle,
			'destination' => $entitylivraison,
			'entity'      => $entity,
			'allee' => $allee,
			'travee' => $travee,
			'niveau' => $niveau,
			'emplacement' => $emplacement
			));
		
			$html2pdf = new \Html2Pdf_Html2Pdf('L','A4','en', false, 'ISO-8859-1');
			$html2pdf->pdf->SetDisplayMode('real');
			$html2pdf->setDefaultFont('Arial');
			$html2pdf->writeHTML(utf8_decode($html), isset($_GET['vuehtml']));
			$fichier = $html2pdf->Output('Listing_op.pdf', 'D');

			$response = new Response();
			$response->clearHttpHeaders();
			$response->setContent(file_get_contents($fichier));
			$response->headers->set('Content-Type', 'application/force-download');
			$response->headers->set('Content-disposition', 'filename='. $fichier);

			return $response;
			
			//return $this->render('PhareosLogisToolBoxBundle:comclients:show.html.twig', array(
			//'resultcount' => $resultCount,
			//'articles' => $Article,
			//'qtte'		  => $qtteArticleCom,
			//'designat'	  => $designatArticle,
			//'destination' => $entitylivraison,
			//'entity'      => $entity,
			//));
			
			$deleteForm = $this->createDeleteForm($id);
		}
		

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find comclients entity.');
        }

        
    }

    /**
     * Displays a form to create a new comclients entity.
     *
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
		$session = $this->get('session');
		
		$userid = $this->container->get('security.context')->getToken()->getUser()->getid();
		
		$idDestSelect = $session->get('idDestSelectSession');
		$entitydestselect = $em->getRepository('PhareosLogisToolBoxBundle:destination')->find($idDestSelect);
		$entitiesarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_article_tmp')->findBy(array('iduser' => $userid));
		
		$entity = new comclients();
        $form   = $this->createForm(new comclientsType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:comclients:new.html.twig', array(
            'entitiesarticlestmp' => $entitiesarticlestmp,
			'entitydestselect' => $entitydestselect,
			'entity' => $entity,
            'form'   => $form->createView()
        ));
    }
	
	public function newpanAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
		$session = $this->get('session');
		
		$userid = $this->container->get('security.context')->getToken()->getUser()->getid();
		
		$idDestSelect = $session->get('idDestSelectSession');
		$entitydestselect = $em->getRepository('PhareosLogisToolBoxBundle:destination')->find($idDestSelect);
		$entitiesarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_article_tmp')->findBy(array('iduser' => $userid));
		
		$entity = new comclients();
        $form   = $this->createForm(new comclientspanType(), $entity);
		
		$validor = 0;
		
		foreach ($entitiesarticlestmp as $articlescom) //pour tous les articles de la commande
			{
				$articleId = $articlescom->getIdarticle();
				$qteencomm = $articlescom->getQtte();
				
				$Article = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($articleId);//selection de l'article concerné
				$stockReel = $Article->getQtetot();//quantité totale en stock
				$stockAtenteRecip = $Article->getQteencours();//quantité en attente de réception
				$stockvalidor = $stockReel + $stockAtenteRecip;
				
				
				
				if ($qteencomm > $stockvalidor){
							$validor = 1;
						}
				
			}

        return $this->render('PhareosLogisToolBoxBundle:comclients:newpan.html.twig', array(
            'entitiesarticlestmp' => $entitiesarticlestmp,
			'entitydestselect' => $entitydestselect,
			'entity' => $entity,
			'validor' => $validor,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new comclients entity.
     *
     */
    public function createAction()
    {
        //on recupère l'ID de la destination
		$session = $this->get('session');
		$idDestSelect = $session->get('idDestSelectSession');
		
		//on recup le nom du site et le num du magasin
		$em = $this->getDoctrine()->getEntityManager();
		$entitysite = $em->getRepository('PhareosLogisToolBoxBundle:destination')->find($idDestSelect);
		$nomdusite = $entitysite->getSite();
		$nummag = $entitysite->getNummag();
		$comptemag = $entitysite->getCompte();
		$repository_mail = $em->getRepository('PhareosNomadeNetServiceBundle:mailswift');
			
			$mail1 = $repository_mail->find(1)->getAdresse();
			$mail2 = $repository_mail->find(2)->getAdresse();
			$mail3 = $repository_mail->find(3)->getAdresse();
			$mail4 = $repository_mail->find(4)->getAdresse();
			$mail5 = $repository_mail->find(5)->getAdresse();
			$mail6 = $repository_mail->find(6)->getAdresse();
			$mail7 = $repository_mail->find(7)->getAdresse();
		
		//on recup la societe qui passe la commande
		$societeUSER = $session->get('societeUSER');
		
		// on creer un nouvel enregistrement commandeclients
		$entity  = new comclients();
        $request = $this->getRequest();
        $form    = $this->createForm(new comclientsType(), $entity);
        $form->bindRequest($request);
		
		$datelivrsouh = $request->request->get('datelivrsouh');
		$heurelivrsouh = $request->request->get('heurelivrsouh');
		$dateheurelivrsouh = $datelivrsouh . ' ' . $heurelivrsouh;
		$datelivrsouh2 = new \DateTime($dateheurelivrsouh);
		
		$numOP = $entity->getDatecom();
		$numOP2 = $numOP->format('YmdHis');
		//on recup l'etat de la livraison
		$etatlivr = $entity->getInstall();
		
		$autoor = $entity->getAutoor();

        if ($form->isValid()) {
			$entity->setArchive(0);
            $entity->setNumop($numOP2);
			$entity->setDatelivrsouh($datelivrsouh2);
			$entity->setNomdusite($nomdusite);
			$entity->setAdlivrid($idDestSelect);
			$entity->setEtat('En attente de préparation');
			$entity->setClient($societeUSER);
			$entity->setNummag($nummag);
			$entity->setCompte($comptemag);
			
			//upload the file
			//$entity->uploadPdf();
			
            $em->persist($entity);
			$em->flush();
			

			
            //on recup l'utilisateur qui passe commande
			$userid = $this->container->get('security.context')->getToken()->getUser()->getid();
			
			//on récupère l'id de la commande commandeclients
			$idcommande = $entity->getId();
			$session->set('idcommande', $idcommande);
			//on récupère tous les enregistrement de la table temporaire com_article_tmp de l'utilisateur $userid
			$entitiesarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_article_tmp')->findBy(array('iduser' => $userid));
			foreach ($entitiesarticlestmp as $articlestmp) //pour chaque enregistrement de la table temporaire
			{
				//on creer un nouvelle enregistrement com_articles
				$entitycomarticles = new com_articles();
				
				//on récupère la qtte de l'article commandé en temporaire
				$qTteArticletmp = $articlestmp->getQtte();
				
				//on récuprère le nom de l'article de la table temporaire
				$nOMArticletmp = $articlestmp->getNomarticle();
				
				//on recherche dans la table article l'enregistrement correspondant au nom trouvé
				$articlesentities = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findOneBy(array ('designat' => $nOMArticletmp));
				
				//on récupère l'idArticle de cet enregistrement
				$articleIdfromarticlesentities = $articlesentities->getId();
				
				//on récupère la qtte de cet enregistrement: quantité total en stock
				$articleQtetotfromarticlesentities = $articlesentities->getQtetot();
				
				//on calcul le stock restant
				$articleQtetotnouveauStock = $articleQtetotfromarticlesentities - $qTteArticletmp;
				
				//on récupère l'article dont on doit modifier le stock
				$entityArticle = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($articleIdfromarticlesentities);
				
				//on modifie le stock de l'article concerné
				$entityArticle->setQtetot($articleQtetotnouveauStock);
				
				$entitycomarticles->setIdcomclients($idcommande);
				$entitycomarticles->setIdarticles($articleIdfromarticlesentities);
				$entitycomarticles->setQtte($qTteArticletmp);
				$em->persist($entitycomarticles);
				$em->persist($entityArticle);
				$em->flush();
				
				
				
			}
			
			if ($autoor == 1) {
				
				$i = 0;
				
				foreach ($entitiesarticlestmp as $articlestmp) //pour chaque enregistrement de la table temporaire
				{
					//code de création d'un or par article
					$entityOR  = new commanderecip();
					$datelivrprev2 = new \DateTime('now');
					
					$i = $i + 1;
		
					$numOR = $entityOR->getDatecom();
					$numOR2 = $numOR->format('YmdHi').'A'.$i;
					$entityOR->setArchive(0);
					$entityOR->setNumor($numOR2);
					$entityOR->setDatelivrprev($datelivrprev2);
					$entityOR->setEtat('En attente de réception');
					$entityOR->setClient($societeUSER);
					
					$em->persist($entityOR);
					$em->flush();
					
					//on récupère l'id de la commande commandeclients
					$idcommandeOR = $entityOR->getId();
					
					//on creer un nouvelle enregistrement com_recip
					$entitycomarticles = new com_recip();
					
					//on récupère la qtte de l'article commandé en temporaire
					$qTteArticletmp = $articlestmp->getQtte();
					
					//on récuprère le nom de l'article de la table temporaire
					$nOMArticletmp = $articlestmp->getNomarticle();
					
					//on recherche dans la table article l'enregistrement correspondant au nom trouvé
					$articlesentities = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findOneBy(array ('designat' => $nOMArticletmp));
					
					//on récupère l'idArticle de cet enregistrement
					$articleIdfromarticlesentities = $articlesentities->getId();
					
					//on récupère la qtte attente de réception de cet enregistrement: quantité encours
					$articleQteencoursfromarticlesentities = $articlesentities->getQteencours();
					
					//on calcul le stock en attente de réception
					$articleQteencoursnouveauStock = $articleQteencoursfromarticlesentities + $qTteArticletmp;
					
					//on récupère l'article dont on doit modifier le stock en attente de recep
					$entityArticle = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($articleIdfromarticlesentities);
					
					//on modifie le stock de l'article concerné
					$entityArticle->setQteencours($articleQteencoursnouveauStock);
					
					$entitycomarticles->setIdcomrecips($idcommandeOR);
					$entitycomarticles->setIdarticles($articleIdfromarticlesentities);
					$entitycomarticles->setQtte($qTteArticletmp);
					$entitycomarticles->setQttereception(0);
					$entitycomarticles->setQtteattente($qTteArticletmp);
					$em->persist($entitycomarticles);
					//$em->persist($entityArticle);
					$em->flush();
					
				}
			
			}
			
			if ($etatlivr == 1) {
			
				$message = \Swift_Message::newInstance()
					->setSubject('Demande Intervention Application ToolBox')
					//->setFrom('mail.application@phareoscloud.org')
					//->setTo('contact@altea-proprete.com')
					//code alexis setfrom et setto
					->setFrom($mail1)
					->setTo($mail5)
					->setBody($this->renderView('PhareosLogisToolBoxBundle:comclients:showmail.html.twig', array(
																		'id' => $entity->getId(),
																	 'numOP' => $numOP2,
																	 'client'=> $societeUSER
																	 )))
					;
				$this->get('mailer')->send($message);
				
				$message2 = \Swift_Message::newInstance()
					->setSubject('Demande Intervention Application ToolBox')
					//->setFrom('mail.application@phareoscloud.org')
					//->setTo('admin.dev.toolbox@phareoscloud.org')
					//code alexis setfrom et setto
					->setFrom($mail1)
					->setTo($mail2)
					->setBody($this->renderView('PhareosLogisToolBoxBundle:comclients:showmail.html.twig', array(
																		'id' => $entity->getId(),
																	 'numOP' => $numOP2,
																	 'client'=> $societeUSER
																	 )))
					;
					
				$this->get('mailer')->send($message2);
					
				$message3 = \Swift_Message::newInstance()
					->setSubject('Demande Intervention Application ToolBox copie Toolbox')
					//->setFrom('mail.application@phareoscloud.org')
					//->setTo('contact@toolbox-service.com')
					//code alexis setfrom et setto
					->setFrom($mail1)
					->setTo($mail6)
					->setBody($this->renderView('PhareosLogisToolBoxBundle:comclients:showmail.html.twig', array(
																		'id' => $entity->getId(),
																	 'numOP' => $numOP2,
																	 'client'=> $societeUSER
																	 )))
					;
				$this->get('mailer')->send($message3);
				
				$message4 = \Swift_Message::newInstance()
					->setSubject('Demande Intervention Application ToolBox copie Préparation Intervention')
					//->setFrom('mail.application@phareoscloud.org')
					//->setTo('thierrygallet.altea@gmail.com')
					//code alexis setfrom et setto
					->setFrom($mail1)
					->setTo($mail7)
					->setBody($this->renderView('PhareosLogisToolBoxBundle:comclients:showmail.html.twig', array(
																		'id' => $entity->getId(),
																	 'numOP' => $numOP2,
																	 'client'=> $societeUSER
																	 )))
					;
				$this->get('mailer')->send($message4);
			
			}
			

            return $this->redirect($this->generateUrl('comclients_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosLogisToolBoxBundle:comclients:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }
	
	    /**
     * Creates a new comclients entity.
     *
     */
    public function createpanAction()
    {
        //on recupère l'ID de la destination
		$session = $this->get('session');
		$idDestSelect = $session->get('idDestSelectSession');
		
		//on recup le nom du site et le num du magasin
		$em = $this->getDoctrine()->getEntityManager();
		$entitysite = $em->getRepository('PhareosLogisToolBoxBundle:destination')->find($idDestSelect);
		$nomdusite = $entitysite->getSite();
		$nummag = $entitysite->getNummag();
		$comptemag = $entitysite->getCompte();
		
		$repository_mail = $em->getRepository('PhareosNomadeNetServiceBundle:mailswift');
			
			$mail1 = $repository_mail->find(1)->getAdresse();
			$mail2 = $repository_mail->find(2)->getAdresse();
			$mail3 = $repository_mail->find(3)->getAdresse();
			$mail4 = $repository_mail->find(4)->getAdresse();
			$mail5 = $repository_mail->find(5)->getAdresse();
			$mail6 = $repository_mail->find(6)->getAdresse();
			$mail7 = $repository_mail->find(7)->getAdresse();
		
		//on recup la societe qui passe la commande
		$societeUSER = $session->get('societeUSER');
		
		// on creer un nouvel enregistrement commandeclients
		$entity  = new comclients();
        $request = $this->getRequest();
        $form    = $this->createForm(new comclientspanType(), $entity);
        $form->bindRequest($request);
		
		$datelivrsouh = $request->request->get('datelivrsouh');
		$heurelivrsouh = $request->request->get('heurelivrsouh');
		$dateheurelivrsouh = $datelivrsouh . ' ' . $heurelivrsouh;
		$datelivrsouh2 = new \DateTime($dateheurelivrsouh);
		
		$numOP = $entity->getDatecom();
		$numOP2 = $numOP->format('YmdHis');
		//on recup l'etat de la livraison
		$etatlivr = $entity->getInstall();
		
		//$autoor = $entity->getAutoor();

        if ($form->isValid()) {
			$entity->setArchive(0);
            $entity->setNumop($numOP2);
			$entity->setDatelivrsouh($datelivrsouh2);
			$entity->setNomdusite($nomdusite);
			$entity->setAdlivrid($idDestSelect);
			$entity->setEtat('En préparation virtuelle');
			$entity->setClient($societeUSER);
			$entity->setNummag($nummag);
			$entity->setCompte($comptemag);
			$entity->setPrefact(0);
			
			//upload the file
			//$entity->uploadPdf();
			
            
			$em->persist($entity);
			$em->flush();

			
            //on recup l'utilisateur qui passe commande
			$userid = $this->container->get('security.context')->getToken()->getUser()->getid();
			
			//on récupère l'id de la commande commandeclients
			$idcommande = $entity->getId();
			$session->set('idcommande', $idcommande);
			//on récupère tous les enregistrement de la table temporaire com_article_tmp de l'utilisateur $userid
			$entitiesarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_article_tmp')->findBy(array('iduser' => $userid));
			
			$poidsArticleTot = 0;
			$volumeArticleTot = 0;
			
			foreach ($entitiesarticlestmp as $articlestmp) //pour chaque enregistrement de la table temporaire
			{
				//on creer un nouvelle enregistrement com_articles
				$entitycomarticles = new com_articles();
				
				//on récupère la qtte de l'article commandé en temporaire
				$qTteArticletmp = $articlestmp->getQtte();
				
				//on récuprère l'ID de l'article de la table temporaire
				$IdArticletmp = $articlestmp->getIdarticle();
				
				//on recherche dans la table article l'enregistrement correspondant 
				$articlesentities = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($IdArticletmp);
				
				//on récupère l'idArticle de cet enregistrement
				$articleIdfromarticlesentities = $articlesentities->getId();
				
				//on récupère la qtte réservé de cet enregistrement: quantité réservé
				$articleQtetotfromarticlesentities = $articlesentities->getQtereserve();
				
				//on calcul le nouveau stock reservé
				$articleQtetotnouveauStock = $articleQtetotfromarticlesentities + $qTteArticletmp;
				
				//on récupère l'article dont on doit modifier le stock réservé
				$entityArticle = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($articleIdfromarticlesentities);
				
				//on modifie le stock de l'article concerné
				$entityArticle->setQtereserve($articleQtetotnouveauStock);
				
				//on récupère le poids de l'artcile concerné
				$poidsArticle = $articlesentities->getPoids();
				
				//on calcul le poids total pour chaque référence d'articles (articles de même référence * qantité)
				$poidsArticleRef = $poidsArticle * $qTteArticletmp;
				
				//on calcul le poids total de tous les articles
				$poidsArticleTot += $poidsArticleRef;
				
				//on récupère la largeur de l'article concerné
				$largeurArticle = $articlesentities->getLarge();
				
				//on récupère la hauteur de l'article concerné
				$hautArticle = $articlesentities->getHaut();
				
				//on récupère la profondeur de l'article concerné
				$profondArticle = $articlesentities->getProfond();
				
				//on calcul le volume de l'article concerné
				$volumeArticle = $largeurArticle * $hautArticle * $profondArticle;
				
				//on calcul le volume total de tous les articles de la même référence (articles de même référence * qantité)
				$volumeArticleRef = $volumeArticle * $qTteArticletmp;
				
				//on calcul le volume total de tous les articles de la commande
				$volumeArticleTot += $volumeArticleRef;
				
				$entitycomarticles->setIdcomclients($idcommande);
				$entitycomarticles->setIdarticles($articleIdfromarticlesentities);
				$entitycomarticles->setQtte($qTteArticletmp);
				$em->persist($entitycomarticles);
				$em->persist($entityArticle);
				$em->flush();
				
				
				
			}
			
			$entity->setPoids($poidsArticleTot);
			$entity->setVolume($volumeArticleTot);
			
			$em->persist($entity);
			$em->flush();
			

			
			if ($etatlivr == 1) {
			
				$message = \Swift_Message::newInstance()
					->setSubject('Demande Intervention Application ToolBox')
					//->setFrom('mail.application@phareoscloud.org')
					//->setTo('contact@altea-proprete.com')
					//code alexis setfrom et setto
					->setFrom($mail1)
					->setTo($mail5)
					->setBody($this->renderView('PhareosLogisToolBoxBundle:comclients:showmail.html.twig', array(
																		'id' => $entity->getId(),
																	 'numOP' => $numOP2,
																	 'client'=> $societeUSER
																	 )))
					;
				$this->get('mailer')->send($message);
				
				$message2 = \Swift_Message::newInstance()
					->setSubject('Demande Intervention Application ToolBox')
					//->setFrom('mail.application@phareoscloud.org')
					//->setTo('admin.dev.toolbox@phareoscloud.org')
					//code alexis setfrom et setto
					->setFrom($mail1)
					->setTo($mail2)
					->setBody($this->renderView('PhareosLogisToolBoxBundle:comclients:showmail.html.twig', array(
																		'id' => $entity->getId(),
																	 'numOP' => $numOP2,
																	 'client'=> $societeUSER
																	 )))
					;
					
				$this->get('mailer')->send($message2);
					
				$message3 = \Swift_Message::newInstance()
					->setSubject('Demande Intervention Application ToolBox copie Toolbox')
					//->setFrom('mail.application@phareoscloud.org')
					//->setTo('contact@toolbox-service.com')
					//code alexis setfrom et setto
					->setFrom($mail1)
					->setTo($mail6)
					->setBody($this->renderView('PhareosLogisToolBoxBundle:comclients:showmail.html.twig', array(
																		'id' => $entity->getId(),
																	 'numOP' => $numOP2,
																	 'client'=> $societeUSER
																	 )))
					;
				$this->get('mailer')->send($message3);
				
				$message4 = \Swift_Message::newInstance()
					->setSubject('Demande Intervention Application ToolBox copie Préparation Intervention')
					//->setFrom('mail.application@phareoscloud.org')
					//->setTo('thierrygallet.altea@gmail.com')
					//code alexis setfrom et setto
					->setFrom($mail1)
					->setTo($mail7)
					->setBody($this->renderView('PhareosLogisToolBoxBundle:comclients:showmail.html.twig', array(
																		'id' => $entity->getId(),
																	 'numOP' => $numOP2,
																	 'client'=> $societeUSER
																	 )))
					;
				$this->get('mailer')->send($message4);
			
			}
			

            return $this->redirect($this->generateUrl('comclients_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosLogisToolBoxBundle:comclients:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }
	
	public function createpanvalAction()
    {
        //on recupère l'ID de la destination
		$session = $this->get('session');
		$idDestSelect = $session->get('idDestSelectSession');
		
		//on recup le nom du site et le num du magasin
		$em = $this->getDoctrine()->getEntityManager();
		$entitysite = $em->getRepository('PhareosLogisToolBoxBundle:destination')->find($idDestSelect);
		$nomdusite = $entitysite->getSite();
		$nummag = $entitysite->getNummag();
		$comptemag = $entitysite->getCompte();
		
		$repository_mail = $em->getRepository('PhareosNomadeNetServiceBundle:mailswift');
			
			$mail1 = $repository_mail->find(1)->getAdresse();
			$mail2 = $repository_mail->find(2)->getAdresse();
			$mail3 = $repository_mail->find(3)->getAdresse();
			$mail4 = $repository_mail->find(4)->getAdresse();
			$mail5 = $repository_mail->find(5)->getAdresse();
			$mail6 = $repository_mail->find(6)->getAdresse();
			$mail7 = $repository_mail->find(7)->getAdresse();
		
		//on recup la societe qui passe la commande
		$societeUSER = $session->get('societeUSER');
		
		// on creer un nouvel enregistrement commandeclients
		$entity  = new comclients();
        $request = $this->getRequest();
        $form    = $this->createForm(new comclientspanType(), $entity);
        $form->bindRequest($request);
		
		$datelivrsouh = $request->request->get('datelivrsouh');
		$heurelivrsouh = $request->request->get('heurelivrsouh');
		$dateheurelivrsouh = $datelivrsouh . ' ' . $heurelivrsouh;
		$datelivrsouh2 = new \DateTime($dateheurelivrsouh);
		
		$numOP = $entity->getDatecom();
		$numOP2 = $numOP->format('YmdHis');
		//on recup l'etat de la livraison
		$etatlivr = $entity->getInstall();
		
		//$autoor = $entity->getAutoor();

        if ($form->isValid()) {
			$entity->setArchive(0);
            $entity->setNumop($numOP2);
			$entity->setDatelivrsouh($datelivrsouh2);
			$entity->setNomdusite($nomdusite);
			$entity->setAdlivrid($idDestSelect);
			$entity->setEtat('En attente de préparation');
			$entity->setClient($societeUSER);
			$entity->setNummag($nummag);
			$entity->setCompte($comptemag);
			$entity->setPrefact(0);
			
			//upload the file
			//$entity->uploadPdf();
			
            
			$em->persist($entity);
			$em->flush();

			
            //on recup l'utilisateur qui passe commande
			$userid = $this->container->get('security.context')->getToken()->getUser()->getid();
			
			//on récupère l'id de la commande commandeclients
			$idcommande = $entity->getId();
			$session->set('idcommande', $idcommande);
			//on récupère tous les enregistrement de la table temporaire com_article_tmp de l'utilisateur $userid
			$entitiesarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_article_tmp')->findBy(array('iduser' => $userid));
			
			$poidsArticleTot = 0;
			$volumeArticleTot = 0;
			
			foreach ($entitiesarticlestmp as $articlestmp) //pour chaque enregistrement de la table temporaire
			{
				//on creer un nouvelle enregistrement com_articles
				$entitycomarticles = new com_articles();
				
				//on récupère la qtte de l'article commandé en temporaire
				$qTteArticletmp = $articlestmp->getQtte();
				
				//on récuprère l'ID de l'article de la table temporaire
				$IdArticletmp = $articlestmp->getIdarticle();
				
				//on recherche dans la table article l'enregistrement correspondant 
				$articlesentities = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($IdArticletmp);
				
				//on récupère l'idArticle de cet enregistrement
				$articleIdfromarticlesentities = $articlesentities->getId();
				
				//on récupère la qtte de stock réel (stock visu client)
				$articleQtetotfromarticlesentities = $articlesentities->getQtetot();
				
				//on récupère la qtte de stock réservé 
				$articleQtereservefromarticlesentities = $articlesentities->getQtereserve();
				
				//on calcul le nouveau stock réel (stock visu client)
				$articleQtetotnouveauStock = $articleQtetotfromarticlesentities - $qTteArticletmp;
				
				//on calcul la nouvelle qtté réservé
				$articleQtereservenouveauStock = $articleQtereservefromarticlesentities + $qTteArticletmp;
				
				//on récupère l'article dont on doit modifier le stock réservé
				$entityArticle = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($articleIdfromarticlesentities);
				
				//on modifie le stock de l'article concerné (stock visu client)
				$entityArticle->setQtetot($articleQtetotnouveauStock);
				
				//on modifie le stock réservé de l'article concerné 
				$entityArticle->setQtereserve($articleQtereservenouveauStock);
				
				$em->persist($entityArticle);
				$em->flush();
				
				//on récupère le poids de l'artcile concerné
				$poidsArticle = $articlesentities->getPoids();
				
				//on calcul le poids total pour chaque référence d'articles (articles de même référence * qantité)
				$poidsArticleRef = $poidsArticle * $qTteArticletmp;
				
				//on calcul le poids total de tous les articles
				$poidsArticleTot += $poidsArticleRef;
				
				//on récupère la largeur de l'article concerné
				$largeurArticle = $articlesentities->getLarge();
				
				//on récupère la hauteur de l'article concerné
				$hautArticle = $articlesentities->getHaut();
				
				//on récupère la profondeur de l'article concerné
				$profondArticle = $articlesentities->getProfond();
				
				//on calcul le volume de l'article concerné
				$volumeArticle = $largeurArticle * $hautArticle * $profondArticle;
				
				//on calcul le volume total de tous les articles de la même référence (articles de même référence * qantité)
				$volumeArticleRef = $volumeArticle * $qTteArticletmp;
				
				//on calcul le volume total de tous les articles de la commande
				$volumeArticleTot += $volumeArticleRef;
				
				$entitycomarticles->setIdcomclients($idcommande);
				$entitycomarticles->setIdarticles($articleIdfromarticlesentities);
				$entitycomarticles->setQtte($qTteArticletmp);
				$em->persist($entitycomarticles);
				
				$em->flush();
				
				
				
			}
			
			$entity->setPoids($poidsArticleTot);
			$entity->setVolume($volumeArticleTot);
			
			$em->persist($entity);
			$em->flush();
			

			
			if ($etatlivr == 1) {
			
				$message = \Swift_Message::newInstance()
					->setSubject('Demande Intervention Application ToolBox')
					//->setFrom('mail.application@phareoscloud.org')
					//->setTo('contact@altea-proprete.com')
					//code alexis setfrom et setto
					->setFrom($mail1)
					->setTo($mail5)
					->setBody($this->renderView('PhareosLogisToolBoxBundle:comclients:showmail.html.twig', array(
																		'id' => $entity->getId(),
																	 'numOP' => $numOP2,
																	 'client'=> $societeUSER
																	 )))
					;
				$this->get('mailer')->send($message);
				
				$message2 = \Swift_Message::newInstance()
					->setSubject('Demande Intervention Application ToolBox')
					//->setFrom('mail.application@phareoscloud.org')
					//->setTo('admin.dev.toolbox@phareoscloud.org')
					//code alexis setfrom et setto
					->setFrom($mail1)
					->setTo($mail2)
					->setBody($this->renderView('PhareosLogisToolBoxBundle:comclients:showmail.html.twig', array(
																		'id' => $entity->getId(),
																	 'numOP' => $numOP2,
																	 'client'=> $societeUSER
																	 )))
					;
					
				$this->get('mailer')->send($message2);
					
				$message3 = \Swift_Message::newInstance()
					->setSubject('Demande Intervention Application ToolBox copie Toolbox')
					//->setFrom('mail.application@phareoscloud.org')
					//->setTo('contact@toolbox-service.com')
					//code alexis setfrom et setto
					->setFrom($mail1)
					->setTo($mail6)
					->setBody($this->renderView('PhareosLogisToolBoxBundle:comclients:showmail.html.twig', array(
																		'id' => $entity->getId(),
																	 'numOP' => $numOP2,
																	 'client'=> $societeUSER
																	 )))
					;
				$this->get('mailer')->send($message3);
				
				$message4 = \Swift_Message::newInstance()
					->setSubject('Demande Intervention Application ToolBox copie Préparation Intervention')
					//->setFrom('mail.application@phareoscloud.org')
					//->setTo('thierrygallet.altea@gmail.com')
					//code alexis setfrom et setto
					->setFrom($mail1)
					->setTo($mail7)
					->setBody($this->renderView('PhareosLogisToolBoxBundle:comclients:showmail.html.twig', array(
																		'id' => $entity->getId(),
																	 'numOP' => $numOP2,
																	 'client'=> $societeUSER
																	 )))
					;
				$this->get('mailer')->send($message4);
			
			}
			

            return $this->redirect($this->generateUrl('comclients_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosLogisToolBoxBundle:comclients:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing comclients entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find comclients entity.');
        }

        $editForm = $this->createForm(new comclientsAdminType(), $entity);
        $deleteForm = $this->createDeleteForm($id);
		
		//$entity->setModifsouhait(0);
		
		//$em->persist($entity);
        //$em->flush();

        return $this->render('PhareosLogisToolBoxBundle:comclients:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
	
	public function editprepaAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find comclients entity.');
        }

        $editForm = $this->createForm(new comclientsAdminprepaType(), $entity);
        $deleteForm = $this->createDeleteForm($id);
		
		//$entity->setModifsouhait(0);
		
		//$em->persist($entity);
        //$em->flush();

        return $this->render('PhareosLogisToolBoxBundle:comclients:editprepa.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
	
	public function editencprepaAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find comclients entity.');
        }

        $editForm = $this->createForm(new comclientsAdminencprepaType(), $entity);
        $deleteForm = $this->createDeleteForm($id);
		
		//$entity->setModifsouhait(0);
		
		//$em->persist($entity);
        //$em->flush();

        return $this->render('PhareosLogisToolBoxBundle:comclients:editencprepa.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
	
	public function editatexpAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find comclients entity.');
        }

        $editForm = $this->createForm(new comclientsAdminatexpType(), $entity);
        $deleteForm = $this->createDeleteForm($id);
		
		//$entity->setModifsouhait(0);
		
		//$em->persist($entity);
        //$em->flush();

        return $this->render('PhareosLogisToolBoxBundle:comclients:editatexp.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
	
	public function editenclivrAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find comclients entity.');
        }

        $editForm = $this->createForm(new comclientsAdminenclivrType(), $entity);
        $deleteForm = $this->createDeleteForm($id);
		
		//$entity->setModifsouhait(0);
		
		//$em->persist($entity);
        //$em->flush();

        return $this->render('PhareosLogisToolBoxBundle:comclients:editenclivr.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing comclients entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find comclients entity.');
        }

        $editForm   = $this->createForm(new comclientsAdminType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);
		
		$etat = $entity->getEtat();
		$modifsouhait = $request->request->get('modifdatesouhait');
		$modifdateencoursprepa = $request->request->get('modifdateencoursprepa');
		$modifdateprepare = $request->request->get('modifdateprepare');
		$modifdateenvoi = $request->request->get('modifdateenvoi');
		$modifdatelivre = $request->request->get('modifdatelivre');
		
		$datelivrsouh = $request->request->get('datelivrsouh');
		$datelivrsouh2 = new \DateTime($datelivrsouh);
		
		
		$dateencoursprepa = $request->request->get('dateencoursprepa');
		$dateencoursprepa2 = new \DateTime($dateencoursprepa);
		$dateencoursprepa3 = $entity->getDateencoursprepa();
		
		$dateprepare = $request->request->get('dateprepare');
		$dateprepare2 = new \DateTime($dateprepare);
		$dateprepare3 = $entity->getDateprepare();
		
		$dateenvoi = $request->request->get('dateenvoi');
		$dateenvoi2 = new \DateTime($dateenvoi);
		$dateenvoi3 = $entity->getDateenvoi();
		
		$datelivre = $request->request->get('datelivre');
		$datelivre2 = new \DateTime($datelivre);
		$datelivre3 = $entity->getDatelivre();
		
		if ($modifsouhait) {
			$entity->setDatelivrsouh($datelivrsouh2);
			$entity->setDatelivrend (0);
		}
		
		if ($etat == 'En cours de préparation' and $dateencoursprepa3 == null) {
			$entity->setDateencoursprepa($dateencoursprepa2);
						$entitiescomarticles = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findBy(array('idcomclients' => $id));
			
			foreach ($entitiescomarticles as $entitycomarticle){
						$qTteArticletmp = $entitycomarticle->getQtte();//qtte articles commandé
						$idArticlecom = $entitycomarticle->getIdarticles();//id article commandé
						//recherche de l'article concerné
						$articlesentities = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticlecom);
						

						
						//on recalcul la quantité réservé
						$qteancreserve = $articlesentities->getQtereserve(); //ancienne quantitée
						$qtenewreserve = $qteancreserve - $qTteArticletmp;
						//on met a jour la nouvelle qtte réservé
						$articlesentities->setQtereserve($qtenewreserve);
						
						//on recalcul le nouveau stock réel 
						$qteancstock = $articlesentities->getQtetot(); //ancien stock réel
						$qtenewstock = $qteancstock - $qTteArticletmp;
						//on met a jour le nouveau stock réel
						$articlesentities->setQtetot($qtenewstock);
						
						//$em->persist($articlesentities);
						//$em->flush();
						
						
						
					}
		}
		
		if ($modifdateencoursprepa) {
			$entity->setDateencoursprepa($dateencoursprepa2);
		}
		
		if ($etat == 'Préparé en attente expédition' and $dateprepare3 == null) {
			$entity->setDateprepare($dateprepare2);
		}
		
		if ($modifdateprepare) {
			$entity->setDateprepare($dateprepare2);
		}
		
		if ($etat == 'En cours de livraison' and $dateenvoi3 == null) {
			$entity->setDateenvoi($dateenvoi2);
		}
		
		if ($modifdateenvoi) {
			$entity->setDateenvoi($dateenvoi2);
		}
		
		
		if ($etat == 'Livré' and $datelivre3 == null) {
			$entity->setDatelivre($datelivre2);
		}
		
		if ($modifdatelivre) {
			$entity->setDatelivre($datelivre2);
		}
		

        //if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('comclients'));
        //}

        //return $this->render('PhareosLogisToolBoxBundle:comclients:edit.html.twig', array(
          //  'entity'      => $entity,
           // 'edit_form'   => $editForm->createView(),
           // 'delete_form' => $deleteForm->createView(),
        //));
    }
	
	/**
     * Edits an existing comclients entity.
     *
     */
    public function updateprepaAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find comclients entity.');
        }

        $editForm   = $this->createForm(new comclientsAdminprepaType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);
		
		$etat = $entity->getEtat();
		$modifsouhait = $request->request->get('modifdatesouhait');
		$modifdateencoursprepa = $request->request->get('modifdateencoursprepa');
		$modifdateprepare = $request->request->get('modifdateprepare');
		$modifdateenvoi = $request->request->get('modifdateenvoi');
		$modifdatelivre = $request->request->get('modifdatelivre');
		
		$datelivrsouh = $request->request->get('datelivrsouh');
		$datelivrsouh2 = new \DateTime($datelivrsouh);
		
		
		$dateencoursprepa = $request->request->get('dateencoursprepa');
		$dateencoursprepa2 = new \DateTime($dateencoursprepa);
		$dateencoursprepa3 = $entity->getDateencoursprepa();
		
		$dateprepare = $request->request->get('dateprepare');
		$dateprepare2 = new \DateTime($dateprepare);
		$dateprepare3 = $entity->getDateprepare();
		
		$dateenvoi = $request->request->get('dateenvoi');
		$dateenvoi2 = new \DateTime($dateenvoi);
		$dateenvoi3 = $entity->getDateenvoi();
		
		$datelivre = $request->request->get('datelivre');
		$datelivre2 = new \DateTime($datelivre);
		$datelivre3 = $entity->getDatelivre();
		
		if ($modifsouhait) {
			$entity->setDatelivrsouh($datelivrsouh2);
			$entity->setDatelivrend (0);
		}
		
		if ($etat == 'En cours de préparation' and $dateencoursprepa3 == null) {
			$entity->setDateencoursprepa($dateencoursprepa2);
						$entitiescomarticles = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findBy(array('idcomclients' => $id));
			
			foreach ($entitiescomarticles as $entitycomarticle){
						$qTteArticletmp = $entitycomarticle->getQtte();//qtte articles commandé
						$idArticlecom = $entitycomarticle->getIdarticles();//id article commandé
						//recherche de l'article concerné
						$articlesentities = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticlecom);
						

						
						//on recalcul la quantité réservé
						$qteancreserve = $articlesentities->getQtereserve(); //ancienne quantitée
						$qtenewreserve = $qteancreserve - $qTteArticletmp;
						//on met a jour la nouvelle qtte réservé
						$articlesentities->setQtereserve($qtenewreserve);
						
						//on recalcul le nouveau stock réel (stock phys)
						$qteancstockphys = $articlesentities->getQtephys(); //ancien stock réel (stock phys)
						$qtenewstockphys = $qteancstockphys - $qTteArticletmp;
						//on met a jour le nouveau stock réel (stock phys)
						$articlesentities->setQtephys($qtenewstockphys);
						
						$em->persist($articlesentities);
						$em->flush();
						
						
						
					}
		}
		
		if ($modifdateencoursprepa) {
			$entity->setDateencoursprepa($dateencoursprepa2);
		}
		
		if ($etat == 'Préparé en attente expédition' and $dateprepare3 == null) {
			$entity->setDateprepare($dateprepare2);
		}
		
		if ($modifdateprepare) {
			$entity->setDateprepare($dateprepare2);
		}
		
		if ($etat == 'En cours de livraison' and $dateenvoi3 == null) {
			$entity->setDateenvoi($dateenvoi2);
		}
		
		if ($modifdateenvoi) {
			$entity->setDateenvoi($dateenvoi2);
		}
		
		
		if ($etat == 'Livré' and $datelivre3 == null) {
			$entity->setDatelivre($datelivre2);
		}
		
		if ($modifdatelivre) {
			$entity->setDatelivre($datelivre2);
		}
		

        //if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('comclientsencprepa'));
        //}

        //return $this->render('PhareosLogisToolBoxBundle:comclients:edit.html.twig', array(
          //  'entity'      => $entity,
           // 'edit_form'   => $editForm->createView(),
           // 'delete_form' => $deleteForm->createView(),
        //));
    }
	
	public function updateencprepaAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find comclients entity.');
        }

        $editForm   = $this->createForm(new comclientsAdminencprepaType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);
		
		$etat = $entity->getEtat();
		//$modifsouhait = $request->request->get('modifdatesouhait');
		//$modifdateencoursprepa = $request->request->get('modifdateencoursprepa');
		//$modifdateprepare = $request->request->get('modifdateprepare');
		//$modifdateenvoi = $request->request->get('modifdateenvoi');
		//$modifdatelivre = $request->request->get('modifdatelivre');
		
		//$datelivrsouh = $request->request->get('datelivrsouh');
		//$datelivrsouh2 = new \DateTime($datelivrsouh);
		
		
		//$dateencoursprepa = $request->request->get('dateencoursprepa');
		//$dateencoursprepa2 = new \DateTime($dateencoursprepa);
		//$dateencoursprepa3 = $entity->getDateencoursprepa();
		
		//$dateprepare = $request->request->get('dateprepare');
		//$dateprepare2 = new \DateTime($dateprepare);
		//$dateprepare3 = $entity->getDateprepare();
		
		//$dateenvoi = $request->request->get('dateenvoi');
		//$dateenvoi2 = new \DateTime($dateenvoi);
		//$dateenvoi3 = $entity->getDateenvoi();
		
		//$datelivre = $request->request->get('datelivre');
		//$datelivre2 = new \DateTime($datelivre);
		//$datelivre3 = $entity->getDatelivre();
		
		//if ($modifsouhait) {
		//	$entity->setDatelivrsouh($datelivrsouh2);
		//	$entity->setDatelivrend (0);
		//}

		
		//if ($etat == 'En cours de préparation' and $dateencoursprepa3 == null) {
		//	$entity->setDateencoursprepa($dateencoursprepa2);
		//				$entitiescomarticles = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findBy(array('idcomclients' => $id));
			
		//	foreach ($entitiescomarticles as $entitycomarticle){
		//				$qTteArticletmp = $entitycomarticle->getQtte();//qtte articles commandé

		//				//recherche de l'article concerné
		//				$articlesentities = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticlecom);
						

						
						//on recalcul la quantité réservé
		//				$qteancreserve = $articlesentities->getQtereserve(); //ancienne quantitée
		//				$qtenewreserve = $qteancreserve - $qTteArticletmp;
						//on met a jour la nouvelle qtte réservé
		//				$articlesentities->setQtereserve($qtenewreserve);
						
						//on recalcul le nouveau stock réel
		//				$qteancstock = $articlesentities->getQtetot(); //ancien stock réel
		//				$qtenewstock = $qteancstock - $qTteArticletmp;
						//on met a jour le nouveau stock réel
		//				$articlesentities->setQtetot($qtenewstock);
						
						//$em->persist($articlesentities);
						//$em->flush();
						
						
						
		//			}
		//}


		
		//if ($modifdateencoursprepa) {
		//	$entity->setDateencoursprepa($dateencoursprepa2);
		//}

		
		//if ($etat == 'Préparé en attente expédition' and $dateprepare3 == null) {
		//	$entity->setDateprepare($dateprepare2);
		//}

		
		//if ($modifdateprepare) {
		//	$entity->setDateprepare($dateprepare2);
		//}

		
		//if ($etat == 'En cours de livraison' and $dateenvoi3 == null) {
		//	$entity->setDateenvoi($dateenvoi2);
		//}

		
		//if ($modifdateenvoi) {
		//	$entity->setDateenvoi($dateenvoi2);
		//}

		
		
		//if ($etat == 'Livré' and $datelivre3 == null) {
		//	$entity->setDatelivre($datelivre2);
		//}

		
		//if ($modifdatelivre) {
		//	$entity->setDatelivre($datelivre2);
		//}

		

        //if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('comclientsatexp'));
        //}

        //return $this->render('PhareosLogisToolBoxBundle:comclients:edit.html.twig', array(
          //  'entity'      => $entity,
           // 'edit_form'   => $editForm->createView(),
           // 'delete_form' => $deleteForm->createView(),
        //));
    }
	
	public function updateatexpAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find comclients entity.');
        }

        $editForm   = $this->createForm(new comclientsAdminatexpType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);
		
		$etat = $entity->getEtat();
		//$modifsouhait = $request->request->get('modifdatesouhait');
		//$modifdateencoursprepa = $request->request->get('modifdateencoursprepa');
		//$modifdateprepare = $request->request->get('modifdateprepare');
		//$modifdateenvoi = $request->request->get('modifdateenvoi');
		//$modifdatelivre = $request->request->get('modifdatelivre');
		
		//$datelivrsouh = $request->request->get('datelivrsouh');
		//$datelivrsouh2 = new \DateTime($datelivrsouh);
		
		
		//$dateencoursprepa = $request->request->get('dateencoursprepa');
		//$dateencoursprepa2 = new \DateTime($dateencoursprepa);
		//$dateencoursprepa3 = $entity->getDateencoursprepa();
		
		//$dateprepare = $request->request->get('dateprepare');
		//$dateprepare2 = new \DateTime($dateprepare);
		//$dateprepare3 = $entity->getDateprepare();
		
		//$dateenvoi = $request->request->get('dateenvoi');
		//$dateenvoi2 = new \DateTime($dateenvoi);
		//$dateenvoi3 = $entity->getDateenvoi();
		
		//$datelivre = $request->request->get('datelivre');
		//$datelivre2 = new \DateTime($datelivre);
		//$datelivre3 = $entity->getDatelivre();
		
		//if ($modifsouhait) {
		//	$entity->setDatelivrsouh($datelivrsouh2);
		//	$entity->setDatelivrend (0);
		//}

        //if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('comclientsencliv'));
        //}

        //return $this->render('PhareosLogisToolBoxBundle:comclients:edit.html.twig', array(
          //  'entity'      => $entity,
           // 'edit_form'   => $editForm->createView(),
           // 'delete_form' => $deleteForm->createView(),
        //));
    }
	
	public function updateenclivrAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find comclients entity.');
        }

        $editForm   = $this->createForm(new comclientsAdminenclivrType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);
		
		$etat = $entity->getEtat();
		$modifsouhait = $request->request->get('modifdatesouhait');
		$modifdateencoursprepa = $request->request->get('modifdateencoursprepa');
		$modifdateprepare = $request->request->get('modifdateprepare');
		$modifdateenvoi = $request->request->get('modifdateenvoi');
		$modifdatelivre = $request->request->get('modifdatelivre');
		
		$datelivrsouh = $request->request->get('datelivrsouh');
		$datelivrsouh2 = new \DateTime($datelivrsouh);
		
		
		$dateencoursprepa = $request->request->get('dateencoursprepa');
		$dateencoursprepa2 = new \DateTime($dateencoursprepa);
		$dateencoursprepa3 = $entity->getDateencoursprepa();
		
		$dateprepare = $request->request->get('dateprepare');
		$dateprepare2 = new \DateTime($dateprepare);
		$dateprepare3 = $entity->getDateprepare();
		
		$dateenvoi = $request->request->get('dateenvoi');
		$dateenvoi2 = new \DateTime($dateenvoi);
		$dateenvoi3 = $entity->getDateenvoi();
		
		$datelivre = $request->request->get('datelivre');
		$datelivre2 = new \DateTime($datelivre);
		$datelivre3 = $entity->getDatelivre();
		
		if ($modifsouhait) {
			$entity->setDatelivrsouh($datelivrsouh2);
			$entity->setDatelivrend (0);
		}
		
		if ($etat == 'En cours de préparation' and $dateencoursprepa3 == null) {
			$entity->setDateencoursprepa($dateencoursprepa2);
						$entitiescomarticles = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findBy(array('idcomclients' => $id));
			
			foreach ($entitiescomarticles as $entitycomarticle){
						$qTteArticletmp = $entitycomarticle->getQtte();//qtte articles commandé
						$idArticlecom = $entitycomarticle->getIdarticles();//id article commandé
						//recherche de l'article concerné
						$articlesentities = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticlecom);
						

						
						//on recalcul la quantité réservé
						$qteancreserve = $articlesentities->getQtereserve(); //ancienne quantitée
						$qtenewreserve = $qteancreserve - $qTteArticletmp;
						//on met a jour la nouvelle qtte réservé
						$articlesentities->setQtereserve($qtenewreserve);
						
						//on recalcul le nouveau stock réel
						$qteancstock = $articlesentities->getQtetot(); //ancien stock réel
						$qtenewstock = $qteancstock - $qTteArticletmp;
						//on met a jour le nouveau stock réel
						$articlesentities->setQtetot($qtenewstock);
						
						//$em->persist($articlesentities);
						//$em->flush();
						
						
						
					}
		}
		
		if ($modifdateencoursprepa) {
			$entity->setDateencoursprepa($dateencoursprepa2);
		}
		
		if ($etat == 'Préparé en attente expédition' and $dateprepare3 == null) {
			$entity->setDateprepare($dateprepare2);
		}
		
		if ($modifdateprepare) {
			$entity->setDateprepare($dateprepare2);
		}
		
		if ($etat == 'En cours de livraison' and $dateenvoi3 == null) {
			$entity->setDateenvoi($dateenvoi2);
		}
		
		if ($modifdateenvoi) {
			$entity->setDateenvoi($dateenvoi2);
		}
		
		
		if ($etat == 'Livré' and $datelivre3 == null) {
			$entity->setDatelivre($datelivre2);
		}
		
		if ($modifdatelivre) {
			$entity->setDatelivre($datelivre2);
		}
		

        //if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('comclientslivre'));
        //}

        //return $this->render('PhareosLogisToolBoxBundle:comclients:edit.html.twig', array(
          //  'entity'      => $entity,
           // 'edit_form'   => $editForm->createView(),
           // 'delete_form' => $deleteForm->createView(),
        //));
    }
	
	public function updatepanAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
		
		$session = $this->get('session');

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find comclients entity.');
        }

        $editForm   = $this->createForm(new comclientseditpanType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);
		
		//on recup la societe qui passe la commande
		$societeUSER = $session->get('societeUSER');
		
		$etat = $entity->getEtat();
		
		$datelivrsouh = $request->request->get('datelivrsouh');
		$heurelivrsouh = $request->request->get('heurelivrsouh');
		$dateheurelivrsouh = $datelivrsouh . ' ' . $heurelivrsouh;
		$datelivrsouh2 = new \DateTime($dateheurelivrsouh);

		$entity->setDatelivrsouh($datelivrsouh2);
		
		// on recup l'ancien panier
		$entitypananc = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findBy(array('idcomclients' => $id));
		
		//on recup le nouveau panier
		$product_listcontrole = $request->request->get('products_selected');
		
		
		//on traite les OR auto **************************************************************************
		$entitiesarticlescom = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findBy(array('idcomclients' => $id));
		$i = 0;
		foreach ($entitiesarticlescom as $articlescom) //pour tous les articles de la commande
			{
				$articlesid = $articlescom->getIdarticles();
				$articlestmp = $articlescom->getQtte();
				$autoor = $request->request->get('autoor' . $articlesid);
				
				if ($autoor == 1){
					$entity->setAutoor(1);
					
					//création de l'OR concerné
					$entityOR  = new commanderecip();
					$datelivrprev2 = new \DateTime('now');
					
					$i = $i + 1;
		
					$numOR = $entityOR->getDatecom();
					$numOR2 = $numOR->format('YmdHi').'A'.$i;
					$entityOR->setArchive(0);
					$entityOR->setNumor($numOR2);
					$entityOR->setDatelivrprev($datelivrprev2);
					$entityOR->setEtat('En attente de réception');
					$entityOR->setClient($societeUSER);
					
					$em->persist($entityOR);
					$em->flush();
					
					//on récupère l'id de la commande commandeclients
					$idcommandeOR = $entityOR->getId();
					
					//on creer un nouvelle enregistrement com_recip
					$entitycomarticles = new com_recip();
					
					//on récupère la qtte de l'article commandé
					$qTteArticletmp = $articlestmp;
					
					//on récuprère le nom de l'article de la table temporaire
					//$nOMArticletmp = $articlestmp->getNomarticle();
					
					//on recherche dans la table article l'enregistrement correspondant au nom trouvé
					$articlesentities = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($articlesid);
					
					//on récupère l'idArticle de cet enregistrement
					$articleIdfromarticlesentities = $articlesentities->getId();
					
					//on récupère la qtte attente de réception de cet enregistrement: quantité encours
					$articleQteencoursfromarticlesentities = $articlesentities->getQteencours();
					
					//on calcul le stock en attente de réception
					$articleQteencoursnouveauStock = $articleQteencoursfromarticlesentities + $qTteArticletmp;
					
					//on récupère l'article dont on doit modifier le stock en attente de recep
					$entityArticle = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($articleIdfromarticlesentities);
					
					//on modifie le stock de l'article concerné
					$entityArticle->setQteencours($articleQteencoursnouveauStock);
					
					$entitycomarticles->setIdcomrecips($idcommandeOR);
					$entitycomarticles->setIdarticles($articleIdfromarticlesentities);
					$entitycomarticles->setQtte($qTteArticletmp);
					$entitycomarticles->setQttereception(0);
					$entitycomarticles->setQtteattente($qTteArticletmp);
					$em->persist($entitycomarticles);
					//$em->persist($entityArticle);
					$em->flush();
				}
			
			}
		
		//fin du traitement des OR auto *******************************************************************
		
		//détection des modifs effectués sur le panier -- 1ère partie: vérification des rajouts ou modification des articles existants
		
		if(isset($product_listcontrole)){
			$product_list = $_POST['products_selected'];
			
			if(isset($product_list)){
				foreach($product_list as $p_list){
					$chunks = explode('|',$p_list);
					$plist_id = $chunks[0];
					$plist_qty = $chunks[1];
					
					//création du tableau de contrôle des articles
					$tabarticlecontrole[$p_list] = $plist_id;
					
					//rechercher si l'article nouvellement modifier existe dans l'ancien panier
					$entitypanancmod = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findOneBy(array('idcomclients' => $id, 'idarticles' => $plist_id )); //selection de l'article et la commanade
					
					if(!empty($entitypanancmod)){//article existant dans l'ancienne commande modif de la quantité dans l'enregistrement
					
						//on recup l'ancienne quantité de la commande
						$entityQteAnc = $entitypanancmod->getQtte();
						
						if ($entityQteAnc == $plist_qty){
						}
						else 
						{
							$entityQteReservecom = $plist_qty - $entityQteAnc;
							
							//on recup l'article concerné
							$entityArticleMod = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($plist_id);
						
							//on recup l'ancienne quantité réservé
							$entityQteReserveanc = $entityArticleMod->getQtereserve();
						
							//on met a jour cette quantité réservé
							$entityQteReservenew = $entityQteReserveanc + $entityQteReservecom;
							$entityArticleMod->setQtereserve($entityQteReservenew);
							$em->persist($entityArticleMod);
							$em->flush();
						
						
							//on met a jour la nouvelle quantité commandée
							$entitypanancmod->setQtte($plist_qty);
						
							$em->persist($entitypanancmod);
							$em->flush();
						}
						
						
					}
					else //article inexistant dans l'ancienne commande création de l'enregistrement
					{
						$entitypannewmod = new com_articles();
						$entitypannewmod->setIdarticles($plist_id);
						$entitypannewmod->setIdcomclients($id);
						$entitypannewmod->setQtte($plist_qty);
						
						//on recup l'article concerné
						$entityArticleMod = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($plist_id);
						
						//on recup l'ancienne quantité réservé
						$entityQteReserveanc = $entityArticleMod->getQtereserve();
						
						//on met a jour cette quantité réservé
						$entityQteReservenew = $entityQteReserveanc + $plist_qty;
						$entityArticleMod->setQtereserve($entityQteReservenew);
						$em->persist($entityArticleMod);
						$em->flush();
						
						$em->persist($entitypannewmod);
						$em->flush();
					}
					
					
				}
				
				//selectionner tous les articles de l'ancienne commande pour verif suppression
				$entitiespanansupmod = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findBy(array('idcomclients' => $id ));
				foreach($entitiespanansupmod as $entitypan){
				
					// test si article présent dans $tabarticlecontrole
					$idtestarticle = $entitypan->getIdarticles();
					if (!in_array($idtestarticle, $tabarticlecontrole)) {
							
							//on recup l'article concerné
							$entityArticleMod = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idtestarticle);
							
							//on recup l'ancienne quantité commandé avant supression
							$entitypanqtte = $entitypan->getQtte();
							
							//on modifie le stock réservé
							$entityArticleModqtreserve = $entityArticleMod->getQtereserve();
							$entityArticleModNewreserve = $entityArticleModqtreserve - $entitypanqtte;
							$entityArticleMod->setQtereserve($entityArticleModNewreserve);
							
							$em->persist($entityArticleMod);
							$em->remove($entitypan);
							$em->flush();;
					}
				}
			
			}
			
			//selectionner tous les articles de l'ancienne commande pour verif suppression
			$entitiespanansupmod = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findBy(array('idcomclients' => $id ));
			foreach($entitiespanansupmod as $entitypan){
				$idtestarticle = $entitypan->getIdarticles();
				
				if(isset($product_list)){
				foreach($product_list as $p_list){
					$chunks = explode('|',$p_list);
					$plist_id = $chunks[0];
					$plist_qty = $chunks[1];
				
					if ($idtestarticle != $plist_id){
							
						//$em->remove($entitypan);
						//$em->flush();
					}
				}
				}
						
						
					
			}
			
			
		}
		

        //enregistrement des modifications commclients
        $em->persist($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('comclientsprepavirt'));
      
		
		
    }

    /**
     * Deletes a comclients entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find comclients entity.');
            }
			
			//on récupère tous les enregistrement de la table com_article de l'op ($id à supprimer)
			$entitiesarticles = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findBy(array('idcomclients' => $id));
			if ($entitiesarticles) {
			foreach ($entitiesarticles as $articlescom) //pour chaque enregistrement de la table com_article
			{
		
				
				//on récupère la qtte de l'article commandé
				$qTteArticlecom = $articlescom->getQtte();
				
				//on récuprère le num de l'article de la table id
				$idArticlecom = $articlescom->getIdarticles();
				
				//on recherche et on pointe dans la table article l'enregistrement correspondant
				$articlesentities = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticlecom);
				
				//on récupère la qtte de cet enregistrement: quantité total en stock
				$articleQtetotfromarticlesentities = $articlesentities->getQtetot();
				
				//on recalcul le stock restant
				$articleQtetotnouveauStock = $articleQtetotfromarticlesentities + $qTteArticlecom;
				
				//on modifie le stock de l'article concerné
				$articlesentities->setQtetot($articleQtetotnouveauStock);
				
				
				//$em->persist($articlesentities);
				
				//$em->flush();
				
				
				
			}
			}

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('comclients'));
    }
	
	public function deletepanAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find comclients entity.');
            }
			
			$entitiescomarticles = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findBy(array('idcomclients' => $id));
			
			foreach ($entitiescomarticles as $entitycomarticle){
						$qTteArticletmp = $entitycomarticle->getQtte();//qtte articles commandé
						$idArticlecom = $entitycomarticle->getIdarticles();//id article commandé
						//recherche de l'article concerné
						$articlesentities = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticlecom);
						
						
						
						//on recalcul la quantité réservé
						$qteancreserve = $articlesentities->getQtereserve(); //ancienne quantitée
						$qtenewreserve = $qteancreserve - $qTteArticletmp;
						//on met a jour la nouvelle qtte réservé
						$articlesentities->setQtereserve($qtenewreserve);
						
						
						
						$em->persist($articlesentities);
						$em->flush();
						
						
						
					}

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('comclientsprepavirt'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
	
	public function deletearticleAction($id, $ida) //
    {
        
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->find($ida);

            

            //$em->remove($entity);
            //$em->flush();
        //$session = $this->get('session');
		//$session->set('id1', $id);
		//$session->set('id2', $ida);

        return $this->redirect($this->generateUrl('comclients_editclient', array('id' => $id)));
		
		//return $this->redirect($this->generateUrl('comclients'));
    }
	
	public function updateintervAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->find($id);
		
		$societedestin = $entity->getClient();
		
		
		
		$numOP2 = $entity->getNumop();
		
		$session = $this->get('session');
		
		$societeUSER = $session->get('societeUSER');
		
		$emaildestin = $em->getRepository('PhareosLogisToolBoxBundle:clienttool')->findOneBy(array('nom' => $societedestin));
		
		$emaildestin2 = $emaildestin->getEmail();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find comclients entity.');
        }

        

        $request = $this->getRequest();

        
		
		
		
		$dateintervprev = $request->request->get('dateintervprev');
		$dateintervprev2 = new \DateTime($dateintervprev);
		
		
		
			$entity->setDateintervprev($dateintervprev2);
				

        //if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();
			
			$message1 = \Swift_Message::newInstance()
					->setSubject('Date Intervention Application ToolBox')
					//->setFrom('mail.application@phareoscloud.org')
					//->setTo($emaildestin2)
					//code alexis setfrom et setto
					->setFrom('mail.application@faqslife.org')
					->setTo('phareos.test@gmail.com')
					->setBody($this->renderView('PhareosLogisToolBoxBundle:comclients:showmail2.html.twig', array(
																		'id' => $entity->getId(),
																	 'numOP' => $numOP2,
																	 'client'=> $societeUSER,
																	 'date' => $dateintervprev,
																	 )))
					;
					
				$this->get('mailer')->send($message1);
			
			$message2 = \Swift_Message::newInstance()
					->setSubject('Date Intervention Application ToolBox')
					//->setFrom('mail.application@phareoscloud.org')
					//->setTo('admin.dev.toolbox@phareoscloud.org')
					//code alexis setfrom et setto
					->setFrom('mail.application@faqslife.org')
					->setTo('phareos.test@gmail.com')
					->setBody($this->renderView('PhareosLogisToolBoxBundle:comclients:showmail2.html.twig', array(
																		'id' => $entity->getId(),
																	 'numOP' => $numOP2,
																	 'client'=> $societeUSER,
																	 'date' => $dateintervprev,
																	 )))
					;
					
				$this->get('mailer')->send($message2);

            return $this->redirect($this->generateUrl('comclients'));
        //}

        //return $this->render('PhareosLogisToolBoxBundle:comclients:edit.html.twig', array(
          //  'entity'      => $entity,
           // 'edit_form'   => $editForm->createView(),
           // 'delete_form' => $deleteForm->createView(),
        //));
    }
	
	public function updatecomclientAction()
    {
        $session = $this->get('session');
		
		$idCommClient = $session->get('toolcomClient');
		
		$request = $this->getRequest();
		
		$em = $this->getDoctrine()->getEntityManager();
		
		$idArticle = $request->request->get('idarticle');
		
		$qtearticle = $request->request->get('qte');

        $entitycomarticles = new com_articles();
		
		$entitycomarticles->setIdcomclients($idCommClient);
		$entitycomarticles->setIdarticles($idArticle);
		$entitycomarticles->setQtte($qtearticle);

        //if ($editForm->isValid()) {
            $em->persist($entitycomarticles);
            $em->flush();

            return $this->redirect($this->generateUrl('comclients_editclient', array(
																		'id' => $idCommClient)));
        //}

        //return $this->render('PhareosLogisToolBoxBundle:comclients:edit.html.twig', array(
          //  'entity'      => $entity,
           // 'edit_form'   => $editForm->createView(),
           // 'delete_form' => $deleteForm->createView(),
        //));
    }
}
