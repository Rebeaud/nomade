<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\LogisToolBoxBundle\Entity\com_articles;
use Phareos\LogisToolBoxBundle\Form\com_articlesType;

/**
 * com_articles controller.
 *
 */
class com_articlesController extends Controller
{
    /**
     * Lists all com_articles entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:com_articles:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a com_articles entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find com_articles entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:com_articles:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new com_articles entity.
     *
     */
    public function newAction()
    {
        $entity = new com_articles();
        $form   = $this->createForm(new com_articlesType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:com_articles:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new com_articles entity.
     *
     */
    public function createAction()
    {
        $entity  = new com_articles();
        $request = $this->getRequest();
        $form    = $this->createForm(new com_articlesType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('com_articles_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosLogisToolBoxBundle:com_articles:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing com_articles entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find com_articles entity.');
        }

        $editForm = $this->createForm(new com_articlesType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:com_articles:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing com_articles entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find com_articles entity.');
        }

        $editForm   = $this->createForm(new com_articlesType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('com_articles_edit', array('id' => $id)));
        }

        return $this->render('PhareosLogisToolBoxBundle:com_articles:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a com_articles entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find com_articles entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('com_articles'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
