<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\LogisToolBoxBundle\Entity\ordrecstock;
use Phareos\LogisToolBoxBundle\Form\ordrecstockType;

use Phareos\LogisToolBoxBundle\Entity\emplarticle;
use Phareos\LogisToolBoxBundle\Form\emplarticleType;

/**
 * ordrecstock controller.
 *
 */
class ordrecstockController extends Controller
{
    /**
     * Lists all ordrecstock entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosLogisToolBoxBundle:ordrecstock')->findBy(array('stock' => 0));

        return $this->render('PhareosLogisToolBoxBundle:ordrecstock:index.html.twig', array(
            'entities' => $entities
        ));
    }
	
	/**
     * Lists all ordrecstock entities.
     *
     */
    public function indexorsAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosLogisToolBoxBundle:ordrecstock')->findBy(array('stock' => 1));

        return $this->render('PhareosLogisToolBoxBundle:ordrecstock:indexors.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a ordrecstock entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:ordrecstock')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ordrecstock entity.');
        }
		
		$entityemplarticle = new emplarticle();
        $form   = $this->createForm(new emplarticleType(), $entityemplarticle);
		
		$entitiesemplacement = $em->getRepository('PhareosLogisToolBoxBundle:emplacement')->findBy(array('actif' => 1));

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:ordrecstock:show.html.twig', array(
            'entityemplarticle'		=> $entityemplarticle,
			'entitiesemplacement'	=> $entitiesemplacement,
			'entity'      			=> $entity,
			'form'   				=> $form->createView(),
            'delete_form' 			=> $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new ordrecstock entity.
     *
     */
    public function newAction()
    {
        $entity = new ordrecstock();
        $form   = $this->createForm(new ordrecstockType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:ordrecstock:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new ordrecstock entity.
     *
     */
    public function createAction()
    {
        $entityemplarticle = new emplarticle();
		//$entity  = new ordrecstock();
        $request = $this->getRequest();
        //$form    = $this->createForm(new ordrecstockType(), $entity);
        //$form->bindRequest($request);

        $product_listcontrole = $request->request->get('products_selected');
		
		$plist_qtytot = 0;
		
		if(isset($product_listcontrole)){
			$product_list = $_POST['products_selected'];
		
		
			if(isset($product_list)){
				foreach($product_list as $p_list){
					$chunks = explode('|',$p_list);
					$plist_id = $chunks[0];
					$plist_qty = $chunks[1];
					
					$plist_qtytot = $plist_qtytot + $plist_qty;
				}
			}
		}
		
		$em = $this->getDoctrine()->getEntityManager();
        //$em->persist($entityemplarticle);
        //$em->flush();

        return $this->redirect($this->generateUrl('ordrecstock'));

        
    }

    /**
     * Displays a form to edit an existing ordrecstock entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:ordrecstock')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ordrecstock entity.');
        }
		
		$entityemplarticle = new emplarticle();
        $form   = $this->createForm(new emplarticleType(), $entityemplarticle);
		
		$entitiesemplacement = $em->getRepository('PhareosLogisToolBoxBundle:emplacement')->findAll();

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:ordrecstock:show.html.twig', array(
            'entityemplarticle'		=> $entityemplarticle,
			'entitiesemplacement'	=> $entitiesemplacement,
			'entity'      			=> $entity,
			'form'   				=> $form->createView(),
            'delete_form' 			=> $deleteForm->createView(),

        ));
    }

    /**
     * Edits an existing ordrecstock entity.
     *
     */
    public function updateAction($id)
    {
        
		//$entity  = new ordrecstock();
        $request = $this->getRequest();
        //$form    = $this->createForm(new ordrecstockType(), $entity);
        //$form->bindRequest($request);
		
		$em = $this->getDoctrine()->getEntityManager();
		
		$entity = $em->getRepository('PhareosLogisToolBoxBundle:ordrecstock')->find($id);
		
		$article = $entity->getArticles();

        $product_listcontrole = $request->request->get('products_selected');
		
		$plist_qtytot = 0;
		
		if(isset($product_listcontrole)){
			$product_list = $_POST['products_selected'];
		
		
			if(isset($product_list)){
				foreach($product_list as $p_list){
					$chunks = explode('|',$p_list);
					$plist_id = $chunks[0];
					$plist_qty = $chunks[1];
					
					$plist_qtytot = $plist_qtytot + $plist_qty;
					
					$emplacement = $em->getRepository('PhareosLogisToolBoxBundle:emplacement')->find($plist_id);
					
					$entityemplarticle = new emplarticle();
					$entityemplarticle->setQtteemp($plist_qty);
					$entityemplarticle->setEmplacement($emplacement);
					$entityemplarticle->setArticles($article);
					$entityemplarticle->setOrdrecstock($entity);
					$em->persist($entityemplarticle);
					$em->flush();
				}
			}
		}
		$qteAstocker = $entity->getQteastocker() - $plist_qtytot;
		
		$dateStock = new \DateTime();
		
		if ($qteAstocker == 0){
			$entity->setStock(1);
			$entity->setDatestock($dateStock);
		}
		$entity->setQtestockee($plist_qtytot);
		$entity->setQteastocker($qteAstocker);
        $em->persist($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('ordrecstock'));
    }

    /**
     * Deletes a ordrecstock entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosLogisToolBoxBundle:ordrecstock')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ordrecstock entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('ordrecstock'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
