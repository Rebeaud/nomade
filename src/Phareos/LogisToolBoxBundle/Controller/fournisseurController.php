<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\LogisToolBoxBundle\Entity\fournisseur;
use Phareos\LogisToolBoxBundle\Form\fournisseurType;
use Phareos\LogisToolBoxBundle\Form\fournisseureditType;
use Phareos\LogisToolBoxBundle\Form\fournisseurclientType;

/**
 * fournisseur controller.
 *
 */
class fournisseurController extends Controller
{
    /**
     * Lists all fournisseur entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
		
		$session = $this->get('session');
		$applicationUSER = $session->get('applicationUSER');
		$societeUSER = $session->get('societeUSER');
		
		if ($applicationUSER == 'Toolbox')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:fournisseur')->findAll();
		}
		else
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:fournisseur')->findBy(array('idclient' => $societeUSER));
		}

        //$entities = $em->getRepository('PhareosLogisToolBoxBundle:fournisseur')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:fournisseur:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a fournisseur entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:fournisseur')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find fournisseur entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:fournisseur:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new fournisseur entity.
     *
     */
    public function newAction()
    {
        $entity = new fournisseur();
		
		$session = $this->get('session');
		$applicationUSER = $session->get('applicationUSER');
		$societeUSER = $session->get('societeUSER');
		
		if ($applicationUSER == 'Toolbox')
		{
			$form   = $this->createForm(new fournisseurType(), $entity);
		}
		else
		{
			$form   = $this->createForm(new fournisseurclientType(), $entity);
		}
		
        //$form   = $this->createForm(new fournisseurType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:fournisseur:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new fournisseur entity.
     *
     */
    public function createAction()
    {
        $entity  = new fournisseur();
        $request = $this->getRequest();
		
		$session = $this->get('session');
		$applicationUSER = $session->get('applicationUSER');
		$societeUSER = $session->get('societeUSER');
		
		if ($applicationUSER == 'Toolbox')
		{
			$form    = $this->createForm(new fournisseurType(), $entity);
		}
		else
		{
			$form    = $this->createForm(new fournisseurclientType(), $entity);
		}
		
        //$form    = $this->createForm(new fournisseurType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
			
			if ($applicationUSER == 'Toolbox')
			{
			}
			else
			{
				$entity->setIdclient($societeUSER);
			}
			
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('fournisseur_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosLogisToolBoxBundle:fournisseur:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing fournisseur entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:fournisseur')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find fournisseur entity.');
        }

        $editForm = $this->createForm(new fournisseureditType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:fournisseur:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing fournisseur entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:fournisseur')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find fournisseur entity.');
        }

        $editForm   = $this->createForm(new fournisseureditType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('fournisseur_show', array('id' => $entity->getId())));
        }

        return $this->render('PhareosLogisToolBoxBundle:fournisseur:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a fournisseur entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosLogisToolBoxBundle:fournisseur')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find fournisseur entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('fournisseur'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
