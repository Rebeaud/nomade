<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\LogisToolBoxBundle\Entity\emplacement;
use Phareos\LogisToolBoxBundle\Form\emplacementType;

/**
 * emplacement controller.
 *
 */
class emplacementController extends Controller
{
    /**
     * Lists all emplacement entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosLogisToolBoxBundle:emplacement')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:emplacement:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a emplacement entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:emplacement')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find emplacement entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:emplacement:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new emplacement entity.
     *
     */
    public function newAction()
    {
        $entity = new emplacement();
        $form   = $this->createForm(new emplacementType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:emplacement:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new emplacement entity.
     *
     */
    public function createAction()
    {
        $entity  = new emplacement();
        $request = $this->getRequest();
        $form    = $this->createForm(new emplacementType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('emplacement_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosLogisToolBoxBundle:emplacement:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing emplacement entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:emplacement')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find emplacement entity.');
        }

        $editForm = $this->createForm(new emplacementType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:emplacement:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing emplacement entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:emplacement')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find emplacement entity.');
        }

        $editForm   = $this->createForm(new emplacementType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('emplacement_edit', array('id' => $id)));
        }

        return $this->render('PhareosLogisToolBoxBundle:emplacement:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a emplacement entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosLogisToolBoxBundle:emplacement')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find emplacement entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('emplacement'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
