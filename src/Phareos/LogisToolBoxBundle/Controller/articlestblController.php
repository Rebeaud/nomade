<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\LogisToolBoxBundle\Entity\articles;
use Phareos\LogisToolBoxBundle\Form\articlesType;
use Phareos\LogisToolBoxBundle\Form\articleseditType;
use Phareos\LogisToolBoxBundle\Form\articlesclientType;
use Phareos\LogisToolBoxBundle\Form\articleseditclientType;

use Phareos\LogisToolBoxBundle\Entity\com_articles;
use Phareos\LogisToolBoxBundle\Entity\comclients;

/**
 * articlestbl controller.
 *
 */
class articlestblController extends Controller
{
    /**
     * Lists all articles entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
		
		$session = $this->get('session');
		$applicationUSER = $session->get('applicationUSER');
		$societeUSER = $session->get('societeUSER');
		
		if ($applicationUSER == 'Toolbox' or $applicationUSER == 'ToolboxInterv')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findAll();
		}
		else
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findBy(array('client' => $societeUSER));
		}
		
        //$entities = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:articlestbl:index.html.twig', array(
            'entities' => $entities
        ));
    }
	
	public function indexopAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
		
		$session = $this->get('session');
		$applicationUSER = $session->get('applicationUSER');
		$societeUSER = $session->get('societeUSER');
		
		if ($applicationUSER == 'Toolbox' or $applicationUSER == 'ToolboxInterv')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findAll();
		}
		else
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findBy(array('client' => $societeUSER));
		}
		
        //$entities = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findAll();
		
		foreach ($entities as $article) //pour chaque article
			{
				$idArticle = $article->getId();
				
				//liste des commandes par article (op)
				$articlecoms = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findBy(array('idarticles' => $idArticle));
				
				foreach ($articlecoms as $articlecom) //pour chaque op de l'article
					{
						//id de l'op
						$idcomclient = $articlecom->getIdcomclients();
				
						//filtrage des ops en cours (panier)
						$opsdelarticle = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('etat' => 'En préparation virtuelle', 'id' => $idcomclient ));
				
						$nbarticlecoms = count ($articlecoms);
					}
			}

        return $this->render('PhareosLogisToolBoxBundle:articlestbl:indexop.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a articles entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find articles entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:articlestbl:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new articles entity.
     *
     */
    public function newAction()
    {
        $session = $this->get('session');
		$applicationUSER = $session->get('applicationUSER');
		$societeUSER = $session->get('societeUSER');
		
		$entity = new articles();
		
		if ($applicationUSER == 'Toolbox')
		{
			$form   = $this->createForm(new articlesType(), $entity);
		}
		else
		{
			$_SESSION['societe'] = $societeUSER;
			$form   = $this->createForm(new articlesclientType(), $entity);
		}
        //$form   = $this->createForm(new articlesType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:articlestbl:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new articles entity.
     *
     */
    public function createAction()
    {
        $entity  = new articles();
        $request = $this->getRequest();
		
		$session = $this->get('session');
		$applicationUSER = $session->get('applicationUSER');
		$societeUSER = $session->get('societeUSER');
		
		if ($applicationUSER == 'Toolbox')
		{
			$form    = $this->createForm(new articlesType(), $entity);
		}
		else
		{
			$form    = $this->createForm(new articlesclientType(), $entity);
		}
		
        //$form    = $this->createForm(new articlesType(), $entity);
        $form->bindRequest($request);
		//$daterecep = $request->request->get('daterecep');
		//$dateprepa = $request->request->get('dateprepa');
		//$daterecep2 = new \DateTime($daterecep);
		//$dateprepa2 = new \DateTime($dateprepa);
		
		$data = $form->getData();
		
		
		$designat = $data->getCategorie()  . " - " . $data->getFourniss() . " - " . $data->getReference() ;
		

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
			if ($applicationUSER == 'Toolbox')
			{
				$poids = $entity->getPoids();
				if ($poids == null) {
					$entity->setPoids(0);
				}
				
				$haut = $entity->getHaut();
				if ($haut == null) {
					$entity->setHaut(0);
				}
				
				$large = $entity->getLarge();
				if ($large == null) {
					$entity->setLarge(0);
				}
				
				$profond = $entity->getProfond();
				if ($profond == null) {
					$entity->setProfond(0);
				}
				
				$qteencours = $entity->getQteencours();
				if ($qteencours == null) {
					$entity->setQteencours(0);
				}
			}
			else
			{
				$entity->setClient($societeUSER);
				$entity->setQtetot(0);
				$entity->setQteencours(0);
				$entity->setPoids(0);
				$entity->setHaut(0);
				$entity->setLarge(0);
				$entity->setProfond(0);
				
			}
			$entity->setDesignat($designat);
			$entity->setQtereserve(0);
			$entity->setQtephys(0);
			//$entity->setDaterecep($daterecep2);
			//$entity->setDateprepa($dateprepa2);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('articlestbl_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosLogisToolBoxBundle:articlestbl:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing articles entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find articles entity.');
        }
		
		$session = $this->get('session');
		$applicationUSER = $session->get('applicationUSER');
		$societeUSER = $session->get('societeUSER');
		
		if ($applicationUSER == 'Toolbox')
		{
			$editForm = $this->createForm(new articleseditType(), $entity);
		}
		else
		{
			$editForm = $this->createForm(new articleseditclientType(), $entity);
		}
		
        //$editForm = $this->createForm(new articleseditType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:articlestbl:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing articles entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find articles entity.');
        }
		
		$session = $this->get('session');
		$applicationUSER = $session->get('applicationUSER');
		$societeUSER = $session->get('societeUSER');
		
		if ($applicationUSER == 'Toolbox')
		{
			$editForm = $this->createForm(new articleseditType(), $entity);
		}
		else
		{
			$editForm = $this->createForm(new articleseditclientType(), $entity);
		}

        //$editForm   = $this->createForm(new articleseditType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);
		
		//$daterecep = $request->request->get('daterecep');
		//$dateprepa = $request->request->get('dateprepa');
		//$daterecep2 = new \DateTime($daterecep);
		//$dateprepa2 = new \DateTime($dateprepa);
		
		$data = $editForm->getData();
		
		$designat = $data->getCategorie()  . " - " . $data->getFourniss() . " - " . $data->getReference() ;

       // if ($editForm->isValid()) {
		
			if ($applicationUSER == 'Toolbox')
			{
			//$entity->setDaterecep($daterecep2);
			//$entity->setDateprepa($dateprepa2);
			}
			$entity->setDesignat($designat);
			$em->persist($entity);
            $em->flush();

           // return $this->redirect($this->generateUrl('articlestbl_show', array('id' => $id)));
       // }

        return $this->render('PhareosLogisToolBoxBundle:articlestbl:show.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a articles entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find articles entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('articlestbl'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
