<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\LogisToolBoxBundle\Entity\rangecom;
use Phareos\LogisToolBoxBundle\Form\rangecomType;

/**
 * rangecom controller.
 *
 */
class rangecomController extends Controller
{
    /**
     * Lists all rangecom entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosLogisToolBoxBundle:rangecom')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:rangecom:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a rangecom entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:rangecom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find rangecom entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:rangecom:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new rangecom entity.
     *
     */
    public function newAction()
    {
        $entity = new rangecom();
        $form   = $this->createForm(new rangecomType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:rangecom:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new rangecom entity.
     *
     */
    public function createAction()
    {
        $entity  = new rangecom();
        $request = $this->getRequest();
        $form    = $this->createForm(new rangecomType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('rangecom_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosLogisToolBoxBundle:rangecom:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing rangecom entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:rangecom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find rangecom entity.');
        }

        $editForm = $this->createForm(new rangecomType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:rangecom:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing rangecom entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:rangecom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find rangecom entity.');
        }

        $editForm   = $this->createForm(new rangecomType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('rangecom_edit', array('id' => $id)));
        }

        return $this->render('PhareosLogisToolBoxBundle:rangecom:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a rangecom entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosLogisToolBoxBundle:rangecom')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find rangecom entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('rangecom'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
