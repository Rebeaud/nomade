<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Phareos\LogisToolBoxBundle\Entity\rangecom;
use Phareos\LogisToolBoxBundle\Form\rangecomType;

class RangementController extends Controller
{
    
    public function indexAction()
    {
		$em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosLogisToolBoxBundle:rangecom')->findAll();
		
		$entity = new rangecom();
        $form   = $this->createForm(new rangecomType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:Rangement:index.html.twig', array(
            'entities' => $entities,
			'entity' => $entity,
            'form'   => $form->createView()
        ));
		//return $this->render('PhareosLogisToolBoxBundle:Rangement:index.html.twig');
    }
	
	public function createAction()
    {
		$entity  = new rangecom();
        $request = $this->getRequest();
        $form    = $this->createForm(new rangecomType(), $entity);
        $form->bindRequest($request);
		//$date = new \DateTime('now');
		$auteur = $this->container->get('security.context')->getToken()->getUser();
		
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
			//$entity->setDate($date);
			$entity->setAuteur($auteur);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('Rangement'));
            
        }

        return $this->render('Rangement', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }
	
}