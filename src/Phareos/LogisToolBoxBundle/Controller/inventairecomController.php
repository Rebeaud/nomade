<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\LogisToolBoxBundle\Entity\inventairecom;
use Phareos\LogisToolBoxBundle\Form\inventairecomType;

/**
 * inventairecom controller.
 *
 */
class inventairecomController extends Controller
{
    /**
     * Lists all inventairecom entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosLogisToolBoxBundle:inventairecom')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:inventairecom:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a inventairecom entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:inventairecom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find inventairecom entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:inventairecom:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new inventairecom entity.
     *
     */
    public function newAction()
    {
        $entity = new inventairecom();
        $form   = $this->createForm(new inventairecomType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:inventairecom:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new inventairecom entity.
     *
     */
    public function createAction()
    {
        $entity  = new inventairecom();
        $request = $this->getRequest();
        $form    = $this->createForm(new inventairecomType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('inventairecom_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosLogisToolBoxBundle:inventairecom:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing inventairecom entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:inventairecom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find inventairecom entity.');
        }

        $editForm = $this->createForm(new inventairecomType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:inventairecom:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing inventairecom entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:inventairecom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find inventairecom entity.');
        }

        $editForm   = $this->createForm(new inventairecomType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('inventairecom_edit', array('id' => $id)));
        }

        return $this->render('PhareosLogisToolBoxBundle:inventairecom:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a inventairecom entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosLogisToolBoxBundle:inventairecom')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find inventairecom entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('inventairecom'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
