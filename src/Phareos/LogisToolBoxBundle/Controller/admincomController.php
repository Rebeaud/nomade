<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\LogisToolBoxBundle\Entity\admincom;
use Phareos\LogisToolBoxBundle\Form\admincomType;

/**
 * admincom controller.
 *
 */
class admincomController extends Controller
{
    /**
     * Lists all admincom entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosLogisToolBoxBundle:admincom')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:admincom:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a admincom entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:admincom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find admincom entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:admincom:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new admincom entity.
     *
     */
    public function newAction()
    {
        $entity = new admincom();
        $form   = $this->createForm(new admincomType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:admincom:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new admincom entity.
     *
     */
    public function createAction()
    {
        $entity  = new admincom();
        $request = $this->getRequest();
        $form    = $this->createForm(new admincomType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admincom_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosLogisToolBoxBundle:admincom:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing admincom entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:admincom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find admincom entity.');
        }

        $editForm = $this->createForm(new admincomType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:admincom:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing admincom entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:admincom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find admincom entity.');
        }

        $editForm   = $this->createForm(new admincomType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admincom_edit', array('id' => $id)));
        }

        return $this->render('PhareosLogisToolBoxBundle:admincom:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a admincom entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosLogisToolBoxBundle:admincom')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find admincom entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admincom'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
