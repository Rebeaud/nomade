<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\LogisToolBoxBundle\Entity\prepacom;
use Phareos\LogisToolBoxBundle\Form\prepacomType;

/**
 * prepacom controller.
 *
 */
class prepacomController extends Controller
{
    /**
     * Lists all prepacom entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosLogisToolBoxBundle:prepacom')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:prepacom:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a prepacom entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:prepacom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find prepacom entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:prepacom:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new prepacom entity.
     *
     */
    public function newAction()
    {
        $entity = new prepacom();
        $form   = $this->createForm(new prepacomType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:prepacom:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new prepacom entity.
     *
     */
    public function createAction()
    {
        $entity  = new prepacom();
        $request = $this->getRequest();
        $form    = $this->createForm(new prepacomType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('prepacom_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosLogisToolBoxBundle:prepacom:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing prepacom entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:prepacom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find prepacom entity.');
        }

        $editForm = $this->createForm(new prepacomType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:prepacom:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing prepacom entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:prepacom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find prepacom entity.');
        }

        $editForm   = $this->createForm(new prepacomType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('prepacom_edit', array('id' => $id)));
        }

        return $this->render('PhareosLogisToolBoxBundle:prepacom:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a prepacom entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosLogisToolBoxBundle:prepacom')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find prepacom entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('prepacom'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
