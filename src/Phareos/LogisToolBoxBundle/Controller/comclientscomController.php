<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\LogisToolBoxBundle\Entity\comclientscom;
use Phareos\LogisToolBoxBundle\Form\comclientscomType;

/**
 * comclientscom controller.
 *
 */
class comclientscomController extends Controller
{
    /**
     * Lists all comclientscom entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosLogisToolBoxBundle:comclientscom')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:comclientscom:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a comclientscom entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:comclientscom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find comclientscom entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:comclientscom:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new comclientscom entity.
     *
     */
    public function newAction()
    {
        $entity = new comclientscom();
        $form   = $this->createForm(new comclientscomType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:comclientscom:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new comclientscom entity.
     *
     */
    public function createAction()
    {
        $entity  = new comclientscom();
        $request = $this->getRequest();
        $form    = $this->createForm(new comclientscomType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('comclientscom_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosLogisToolBoxBundle:comclientscom:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing comclientscom entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:comclientscom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find comclientscom entity.');
        }

        $editForm = $this->createForm(new comclientscomType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:comclientscom:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing comclientscom entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:comclientscom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find comclientscom entity.');
        }

        $editForm   = $this->createForm(new comclientscomType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('comclientscom_edit', array('id' => $id)));
        }

        return $this->render('PhareosLogisToolBoxBundle:comclientscom:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a comclientscom entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosLogisToolBoxBundle:comclientscom')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find comclientscom entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('comclientscom'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
