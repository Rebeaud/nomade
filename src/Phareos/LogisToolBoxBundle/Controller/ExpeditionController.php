<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Phareos\LogisToolBoxBundle\Entity\expeditioncom;
use Phareos\LogisToolBoxBundle\Form\expeditioncomType;

class ExpeditionController extends Controller
{
    
    public function indexAction()
    {
		$em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosLogisToolBoxBundle:expeditioncom')->findAll();
		
		$entity = new expeditioncom();
        $form   = $this->createForm(new expeditioncomType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:Expedition:index.html.twig', array(
            'entities' => $entities,
			'entity' => $entity,
            'form'   => $form->createView()
        ));
		
		//return $this->render('PhareosLogisToolBoxBundle:Expedition:index.html.twig');
    }
	
	public function createAction()
    {
		$entity  = new expeditioncom();
        $request = $this->getRequest();
        $form    = $this->createForm(new expeditioncomType(), $entity);
        $form->bindRequest($request);
		//$date = new \DateTime('now');
		$auteur = $this->container->get('security.context')->getToken()->getUser();
		
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
			//$entity->setDate($date);
			$entity->setAuteur($auteur);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('Expedition'));
            
        }

        return $this->render('Expedition', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }
}