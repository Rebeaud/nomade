<?php

namespace Phareos\LogisToolBoxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\LogisToolBoxBundle\Entity\destination
 *
 * @ORM\Table(name="too_destination")
 * @ORM\Entity(repositoryClass="Phareos\LogisToolBoxBundle\Entity\destinationRepository")
 */
class destination
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $client
     *
     * @ORM\Column(name="client", type="string", length=255, nullable=true)
     */
    private $client;
	
	
	/**
     * @var string $compte
     *
     * @ORM\Column(name="compte", type="string", length=255)
     */
    private $compte;
	
	/**
     * @var string $typecpte
     *
     * @ORM\Column(name="typecpte", type="string", length=255)
     */
    private $typecpte;
	
	/**
     * @var string $nummag
     *
     * @ORM\Column(name="nummag", type="string", length=255, nullable=true)
     */
    private $nummag;

    /**
     * @var string $site
     *
     * @ORM\Column(name="site", type="string", length=255)
     */
    private $site;
	
	/**
     * @var string $contact
     *
     * @ORM\Column(name="contact", type="string", length=255, nullable=true)
     */
    private $contact;

    /**
     * @var text $adresse1
     *
     * @ORM\Column(name="adresse1", type="text", nullable=true)
     */
    private $adresse1;

    /**
     * @var text $adresse2
     *
     * @ORM\Column(name="adresse2", type="text", nullable=true)
     */
    private $adresse2;

    /**
     * @var string $cp
     *
     * @ORM\Column(name="cp", type="string", length=255)
     */
    private $cp;

    /**
     * @var string $ville
     *
     * @ORM\Column(name="ville", type="string", length=255)
     */
    private $ville;

    /**
     * @var string $tel
     *
     * @ORM\Column(name="tel", type="string", length=255, nullable=true)
     */
    private $tel;
	
	/**
     * @var string $tel2
     *
     * @ORM\Column(name="tel2", type="string", length=255, nullable=true)
     */
    private $tel2;

    /**
     * @var string $fax
     *
     * @ORM\Column(name="fax", type="string", length=255, nullable=true)
     */
    private $fax;
	
	/**
     * @var string $mail
     *
     * @ORM\Column(name="mail", type="string", length=255, nullable=true)
     */
    private $mail;
	
	/**
     * @var string $sitenet
     *
     * @ORM\Column(name="sitenet", type="string", length=255, nullable=true)
     */
    private $sitenet;
	
	/**
     * @var string $pays
     *
     * @ORM\Column(name="pays", type="string", length=255, nullable=true)
     */
    private $pays;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
	
	/**
     * Set client
     *
     * @param string $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * Get client
     *
     * @return string 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set compte
     *
     * @param string $compte
     */
    public function setCompte($compte)
    {
        $this->compte = $compte;
    }

    /**
     * Get compte
     *
     * @return string 
     */
    public function getCompte()
    {
        return $this->compte;
    }
	
	/**
     * Set typecpte
     *
     * @param string $typecpte
     */
    public function setTypecpte($typecpte)
    {
        $this->typecpte = $typecpte;
    }

    /**
     * Get typecpte
     *
     * @return string 
     */
    public function getTypecpte()
    {
        return $this->typecpte;
    }
	
	/**
     * Set nummag
     *
     * @param string $nummag
     */
    public function setNummag($nummag)
    {
        $this->nummag = $nummag;
    }

    /**
     * Get nummag
     *
     * @return string 
     */
    public function getNummag()
    {
        return $this->nummag;
    }

    /**
     * Set site
     *
     * @param string $site
     */
    public function setSite($site)
    {
        $this->site = $site;
    }

    /**
     * Get site
     *
     * @return string 
     */
    public function getSite()
    {
        return $this->site;
    }
	
	/**
     * Set contact
     *
     * @param string $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * Get contact
     *
     * @return string 
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set adresse1
     *
     * @param text $adresse1
     */
    public function setAdresse1($adresse1)
    {
        $this->adresse1 = $adresse1;
    }

    /**
     * Get adresse1
     *
     * @return text 
     */
    public function getAdresse1()
    {
        return $this->adresse1;
    }

    /**
     * Set adresse2
     *
     * @param text $adresse2
     */
    public function setAdresse2($adresse2)
    {
        $this->adresse2 = $adresse2;
    }

    /**
     * Get adresse2
     *
     * @return text 
     */
    public function getAdresse2()
    {
        return $this->adresse2;
    }

    /**
     * Set cp
     *
     * @param string $cp
     */
    public function setCp($cp)
    {
        $this->cp = $cp;
    }

    /**
     * Get cp
     *
     * @return string 
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set ville
     *
     * @param string $ville
     */
    public function setVille($ville)
    {
        $this->ville = $ville;
    }

    /**
     * Get ville
     *
     * @return string 
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set tel
     *
     * @param string $tel
     */
    public function setTel($tel)
    {
        $this->tel = $tel;
    }

    /**
     * Get tel
     *
     * @return string 
     */
    public function getTel()
    {
        return $this->tel;
    }
	
	/**
     * Set tel2
     *
     * @param string $tel2
     */
    public function setTel2($tel2)
    {
        $this->tel2 = $tel2;
    }

    /**
     * Get tel2
     *
     * @return string 
     */
    public function getTel2()
    {
        return $this->tel2;
    }

    /**
     * Set fax
     *
     * @param string $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * Get fax
     *
     * @return string 
     */
    public function getFax()
    {
        return $this->fax;
    }
	
	/**
     * Set mail
     *
     * @param string $mail
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    /**
     * Get mail
     *
     * @return string 
     */
    public function getMail()
    {
        return $this->mail;
    }
	
	/**
     * Set sitenet
     *
     * @param string $sitenet
     */
    public function setSitenet($sitenet)
    {
        $this->sitenet = $sitenet;
    }

    /**
     * Get sitenet
     *
     * @return string 
     */
    public function getSitenet()
    {
        return $this->sitenet;
    }
	
	/**
     * Set pays
     *
     * @param string $pays
     */
    public function setPays($pays)
    {
        $this->pays = $pays;
    }

    /**
     * Get pays
     *
     * @return string 
     */
    public function getPays()
    {
        return $this->pays;
    }
}