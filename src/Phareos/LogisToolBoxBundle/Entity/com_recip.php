<?php

namespace Phareos\LogisToolBoxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\LogisToolBoxBundle\Entity\com_recip
 *
 * @ORM\Table(name="too_com_recip")
 * @ORM\Entity(repositoryClass="Phareos\LogisToolBoxBundle\Entity\com_recipRepository")
 */
class com_recip
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer $idcomrecips
     *
     * @ORM\Column(name="idcomrecips", type="integer")
     */
    private $idcomrecips;

    /**
     * @var integer $idarticles
     *
     * @ORM\Column(name="idarticles", type="integer")
     */
    private $idarticles;

    /**
     * @var integer $qtte
     *
     * @ORM\Column(name="qtte", type="integer")
     */
    private $qtte;
	
	/**
     * @var integer $qttereception
     *
     * @ORM\Column(name="qttereception", type="integer", nullable=true)
     */
    private $qttereception;
	
	/**
     * @var integer $qtteattente
     *
     * @ORM\Column(name="qtteattente", type="integer", nullable=true)
     */
    private $qtteattente;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idcomrecips
     *
     * @param integer $idcomrecips
     */
    public function setIdcomrecips($idcomrecips)
    {
        $this->idcomrecips = $idcomrecips;
    }

    /**
     * Get idcomrecips
     *
     * @return integer 
     */
    public function getIdcomrecips()
    {
        return $this->idcomrecips;
    }

    /**
     * Set idarticles
     *
     * @param integer $idarticles
     */
    public function setIdarticles($idarticles)
    {
        $this->idarticles = $idarticles;
    }

    /**
     * Get idarticles
     *
     * @return integer 
     */
    public function getIdarticles()
    {
        return $this->idarticles;
    }

    /**
     * Set qtte
     *
     * @param integer $qtte
     */
    public function setQtte($qtte)
    {
        $this->qtte = $qtte;
    }

    /**
     * Get qtte
     *
     * @return integer 
     */
    public function getQtte()
    {
        return $this->qtte;
    }
	
	/**
     * Set qttereception
     *
     * @param integer $qttereception
     */
    public function setQttereception($qttereception)
    {
        $this->qttereception = $qttereception;
    }

    /**
     * Get qttereception
     *
     * @return integer 
     */
    public function getQttereception()
    {
        return $this->qttereception;
    }
	
	/**
     * Set qtteattente
     *
     * @param integer $qtteattente
     */
    public function setQtteattente($qtteattente)
    {
        $this->qtteattente = $qtteattente;
    }

    /**
     * Get qtteattente
     *
     * @return integer 
     */
    public function getQtteattente()
    {
        return $this->qtteattente;
    }
}