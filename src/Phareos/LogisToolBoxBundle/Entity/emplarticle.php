<?php

namespace Phareos\LogisToolBoxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\LogisToolBoxBundle\Entity\emplarticle
 *
 * @ORM\Table(name="too_emplarticle")
 * @ORM\Entity(repositoryClass="Phareos\LogisToolBoxBundle\Entity\emplarticleRepository")
 */
class emplarticle
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
	 * @ORM\ManyToOne(targetEntity="emplacement", inversedBy="emplemplarticle", cascade={"remove"})
	 * @ORM\JoinColumn(name="emplacement_id", referencedColumnName="id")
	 */
	protected $emplacement;
	
	/**
	 * @ORM\ManyToOne(targetEntity="articles", inversedBy="articlemplarticle", cascade={"remove"})
	 * @ORM\JoinColumn(name="articles_id", referencedColumnName="id")
	 */
	protected $articles;
	
	/**
	 * @ORM\ManyToOne(targetEntity="ordrecstock", inversedBy="ordrecemplarticle", cascade={"remove"})
	 * @ORM\JoinColumn(name="ordrecstock_id", referencedColumnName="id")
	 */
	protected $ordrecstock;

    /**
     * @var integer $qtteemp
     *
     * @ORM\Column(name="qtteemp", type="integer")
     */
    private $qtteemp;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set qtteemp
     *
     * @param integer $qtteemp
     */
    public function setQtteemp($qtteemp)
    {
        $this->qtteemp = $qtteemp;
    }

    /**
     * Get qtteemp
     *
     * @return integer 
     */
    public function getQtteemp()
    {
        return $this->qtteemp;
    }

    /**
     * Set emplacement
     *
     * @param Phareos\LogisToolBoxBundle\Entity\emplacement $emplacement
     */
    public function setEmplacement(\Phareos\LogisToolBoxBundle\Entity\emplacement $emplacement)
    {
        $this->emplacement = $emplacement;
    }

    /**
     * Get emplacement
     *
     * @return Phareos\LogisToolBoxBundle\Entity\emplacement 
     */
    public function getEmplacement()
    {
        return $this->emplacement;
    }

    /**
     * Set articles
     *
     * @param Phareos\LogisToolBoxBundle\Entity\articles $articles
     */
    public function setArticles(\Phareos\LogisToolBoxBundle\Entity\articles $articles)
    {
        $this->articles = $articles;
    }

    /**
     * Get articles
     *
     * @return Phareos\LogisToolBoxBundle\Entity\articles 
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * Set ordrecstock
     *
     * @param Phareos\LogisToolBoxBundle\Entity\ordrecstock $ordrecstock
     */
    public function setOrdrecstock(\Phareos\LogisToolBoxBundle\Entity\ordrecstock $ordrecstock)
    {
        $this->ordrecstock = $ordrecstock;
    }

    /**
     * Get ordrecstock
     *
     * @return Phareos\LogisToolBoxBundle\Entity\ordrecstock 
     */
    public function getOrdrecstock()
    {
        return $this->ordrecstock;
    }
}