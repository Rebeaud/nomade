<?php

namespace Phareos\LogisToolBoxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Phareos\LogisToolBoxBundle\Entity\commanderecip
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="too_commanderecip")
 * @ORM\Entity(repositoryClass="Phareos\LogisToolBoxBundle\Entity\commanderecipRepository")
 */
class commanderecip
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
	 * @ORM\OneToMany(targetEntity="ordrecstock", mappedBy="commanderecip", cascade={"remove", "persist"})
	 */
	protected $commordrecstock;

    /**
     * @var datetime $datecom
     *
     * @ORM\Column(name="datecom", type="datetime", nullable=true)
     */
    private $datecom;
	
	/**
     * @var string $numor
     *
     * @ORM\Column(name="numor", type="string", length=255, nullable=true)
     */
    private $numor;
	
	/**
     * @var boolean $archive
     *
     * @ORM\Column(name="archive", type="boolean")
     */
    private $archive;
	
	/**
     * @var boolean $archive2
     *
     * @ORM\Column(name="archive2", type="boolean", nullable=true)
     */
    private $archive2;

    /**
     * @var text $comm
     *
     * @ORM\Column(name="comm", type="text", nullable=true)
     */
    private $comm;

    /**
     * @var string $compdf
     * @Assert\File( maxSize = "2048k", mimeTypesMessage = "Please upload a valid PDF")
     * @ORM\Column(name="compdf", type="string", length=255, nullable=true)
     */
    private $compdf;

    /**
     * @var string $client
     *
     * @ORM\Column(name="client", type="string", length=255, nullable=true)
     */
    private $client;

    /**
     * @var string $etat
     *
     * @ORM\Column(name="etat", type="string", length=255, nullable=true)
     */
    private $etat;

    /**
     * @var datetime $datelivrprev
     *
     * @ORM\Column(name="datelivrprev", type="datetime", nullable=true)
     */
    private $datelivrprev;

	/**
     * @var datetime $datereception
     *
     * @ORM\Column(name="datereception", type="datetime", nullable=true)
     */
    private $datereception;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datecom
     *
     * @param datetime $datecom
     */
    public function setDatecom($datecom)
    {
        $this->datecom = $datecom;
    }

    /**
     * Get datecom
     *
     * @return datetime 
     */
    public function getDatecom()
    {
        return $this->datecom;
    }
	
	/**
     * Set numor
     *
     * @param string $numor
     */
    public function setNumor($numor)
    {
        $this->numor = $numor;
    }

    /**
     * Get numor
     *
     * @return string 
     */
    public function getNumor()
    {
        return $this->numor;
    }

    /**
     * Set comm
     *
     * @param text $comm
     */
    public function setComm($comm)
    {
        $this->comm = $comm;
    }

    /**
     * Get comm
     *
     * @return text 
     */
    public function getComm()
    {
        return $this->comm;
    }

    /**
     * Set compdf
     *
     * @param string $compdf
     */
    public function setCompdf($compdf)
    {
        $this->compdf = $compdf;
    }

    /**
     * Get compdf
     *
     * @return string 
     */
    public function getCompdf()
    {
        return $this->compdf;
    }

    /**
     * Set client
     *
     * @param string $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * Get client
     *
     * @return string 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set etat
     *
     * @param string $etat
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;
    }

    /**
     * Get etat
     *
     * @return string 
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set datelivrprev
     *
     * @param datetime $datelivrprev
     */
    public function setDatelivrprev($datelivrprev)
    {
        $this->datelivrprev = $datelivrprev;
    }

    /**
     * Get datelivrprev
     *
     * @return datetime 
     */
    public function getDatelivrprev()
    {
        return $this->datelivrprev;
    }
	
	/**
     * Set datereception
     *
     * @param datetime $datereception
     */
    public function setDatereception($datereception)
    {
        $this->datereception = $datereception;
    }

    /**
     * Get datereception
     *
     * @return datetime 
     */
    public function getDatereception()
    {
        return $this->datereception;
    }
	
	public function __construct()
	{
		$this->datecom = new \DateTime('now');
	}
	
	public function getFullPdfPath() {
        return null === $this->compdf ? null : $this->getUploadRootDir(). $this->compdf;
    }
 
    protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return $this->getTmpUploadRootDir().$this->getId()."/";
    }
 
    protected function getTmpUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/upload/toolbox/recip/client/';
    }
 
    /**
     * @ORM\PrePersist()
     * 
     */
    public function uploadPdf() {
        // the file property can be empty if the field is not required
        if (null === $this->compdf) {
            return;
        }
        if(!$this->id){
            $this->compdf->move($this->getTmpUploadRootDir(), $this->compdf->getClientOriginalName());
        }else{
            $this->compdf->move($this->getUploadRootDir(), $this->compdf->getClientOriginalName());
        }
        $this->setCompdf($this->compdf->getClientOriginalName());
    }
 
    /**
     * @ORM\PostPersist()
     */
    public function movePdf()
    {
        if (null === $this->compdf) {
            return;
        }
        if(!is_dir($this->getUploadRootDir())){
            mkdir($this->getUploadRootDir());
        }
        copy($this->getTmpUploadRootDir().$this->compdf, $this->getFullPdfPath());
        unlink($this->getTmpUploadRootDir().$this->compdf);
    }
 
    /**
     * @ORM\PreRemove()
     */
    public function removePdf()
    {
        unlink($this->getFullPdfPath());
        rmdir($this->getUploadRootDir());
    }

    /**
     * Set archive
     *
     * @param boolean $archive
     */
    public function setArchive($archive)
    {
        $this->archive = $archive;
    }

    /**
     * Get archive
     *
     * @return boolean 
     */
    public function getArchive()
    {
        return $this->archive;
    }
	
	/**
     * Set archive2
     *
     * @param boolean $archive2
     */
    public function setArchive2($archive2)
    {
        $this->archive2 = $archive2;
    }

    /**
     * Get archive2
     *
     * @return boolean 
     */
    public function getArchive2()
    {
        return $this->archive2;
    }

    /**
     * Add commordrecstock
     *
     * @param Phareos\LogisToolBoxBundle\Entity\ordrecstock $commordrecstock
     */
    public function addordrecstock(\Phareos\LogisToolBoxBundle\Entity\ordrecstock $commordrecstock)
    {
        $this->commordrecstock[] = $commordrecstock;
    }

    /**
     * Get commordrecstock
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getCommordrecstock()
    {
        return $this->commordrecstock;
    }
}