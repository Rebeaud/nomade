<?php

namespace Phareos\LogisToolBoxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Phareos\LogisToolBoxBundle\Entity\articles
 *
 * @ORM\Table(name="too_article")
 * @ORM\Entity(repositoryClass="Phareos\LogisToolBoxBundle\Entity\articlesRepository")
 */
class articles
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
	 * @ORM\OneToMany(targetEntity="emplarticle", mappedBy="articles", cascade={"remove", "persist"})
	 */
	protected $articlemplarticle;
	
	/**
	 * @ORM\OneToMany(targetEntity="ordrecstock", mappedBy="articles", cascade={"remove", "persist"})
	 */
	protected $articordrecstock;

    /**
     * @var string $nom
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @var string $designat
     *
     * @ORM\Column(name="designat", type="string", length=255)
     */
    private $designat;

    /**
     * @var string $reference
     *
     * @ORM\Column(name="reference", type="string", length=255)
     */
    private $reference;

    /**
     * @var string $typeunit
     *
     * @ORM\Column(name="typeunit", type="string", length=255, nullable=true)
     */
    private $typeunit;

    /**
     * @var decimal $poids
     * @Assert\Type(type="float", message="Nombre décimal avec virgule: ,")
     * @ORM\Column(name="poids", type="decimal", length="10", scale="2", nullable=true)
     */
    private $poids;

    /**
     * @var decimal $haut
     * @Assert\Type(type="float", message="Nombre décimal avec virgule: ,")
     * @ORM\Column(name="haut", type="decimal", length="10", scale="2", nullable=true)
     */
    private $haut;

    /**
     * @var decimal $large
     * @Assert\Type(type="float", message="Nombre décimal avec virgule: ,")
     * @ORM\Column(name="large", type="decimal", length="7", scale="2" ,nullable=true)
     */
    private $large;

    /**
     * @var decimal $profond
     * @Assert\Type(type="float", message="Nombre décimal avec virgule: ,")
     * @ORM\Column(name="profond", type="decimal", length="7", scale="2", nullable=true)
     */
    private $profond;

    /**
     * @var integer $qtetot
     *
     * @ORM\Column(name="qtetot", type="integer")
     */
    private $qtetot;
	
	/**
     * @var integer $nbpalette
     *
     * @ORM\Column(name="nbpalette", type="integer", nullable=true)
     */
    private $nbpalette;

    /**
     * @var integer $qteencours
     *
     * @ORM\Column(name="qteencours", type="integer", nullable=true)
     */
    private $qteencours;
	
	/**
     * @var integer $qtephys
     *
     * @ORM\Column(name="qtephys", type="integer", nullable=true)
     */
    private $qtephys;
	
	/**
     * @var integer $qtereserve
     *
     * @ORM\Column(name="qtereserve", type="integer", nullable=true)
     */
    private $qtereserve;

    /**
     * @var integer $qtequarant
     *
     * @ORM\Column(name="qtequarant", type="integer", nullable=true)
     */
    private $qtequarant;

    /**
     * @var date $daterecep
     *
     * @ORM\Column(name="daterecep", type="date", nullable=true)
     */
    private $daterecep;

    /**
     * @var date $dateprepa
     *
     * @ORM\Column(name="dateprepa", type="date", nullable=true)
     */
    private $dateprepa;
	
	/**
     * @var string $fourniss
     *
     * @ORM\Column(name="fourniss", type="string", length=255)
     */
    private $fourniss;
	
	/**
     * @var string $client
     *
     * @ORM\Column(name="client", type="string", length=255)
     */
    private $client;
	
	/**
     * @var string $categorie
     *
     * @ORM\Column(name="categorie", type="string", length=255)
     */
    private $categorie;
	
	
	/**
     * @var string $allee
     *
     * @ORM\Column(name="allee", type="string", length=255, nullable=true)
     */
    private $allee;
	
	/**
     * @var string $travee
     *
     * @ORM\Column(name="travee", type="string", length=255, nullable=true)
     */
    private $travee;
	
	/**
     * @var string $niveau
     *
     * @ORM\Column(name="niveau", type="string", length=255, nullable=true)
     */
    private $niveau;
	
	/**
     * @var string $emplacement
     *
     * @ORM\Column(name="emplacement", type="string", length=255, nullable=true)
     */
    private $emplacement;
	
	


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set designat
     *
     * @param string $designat
     */
    public function setDesignat($designat)
    {
        $this->designat = $designat;
    }

    /**
     * Get designat
     *
     * @return string 
     */
    public function getDesignat()
    {
        return $this->designat;
    }

    /**
     * Set reference
     *
     * @param string $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set typeunit
     *
     * @param string $typeunit
     */
    public function setTypeunit($typeunit)
    {
        $this->typeunit = $typeunit;
    }

    /**
     * Get typeunit
     *
     * @return string 
     */
    public function getTypeunit()
    {
        return $this->typeunit;
    }

    /**
     * Set poids
     *
     * @param decimal $poids
     */
    public function setPoids($poids)
    {
        $this->poids = $poids;
    }

    /**
     * Get poids
     *
     * @return decimal 
     */
    public function getPoids()
    {
        return $this->poids;
    }

    /**
     * Set haut
     *
     * @param decimal $haut
     */
    public function setHaut($haut)
    {
        $this->haut = $haut;
    }

    /**
     * Get haut
     *
     * @return decimal 
     */
    public function getHaut()
    {
        return $this->haut;
    }

    /**
     * Set large
     *
     * @param decimal $large
     */
    public function setLarge($large)
    {
        $this->large = $large;
    }

    /**
     * Get large
     *
     * @return decimal 
     */
    public function getLarge()
    {
        return $this->large;
    }

    /**
     * Set profond
     *
     * @param decimal $profond
     */
    public function setProfond($profond)
    {
        $this->profond = $profond;
    }

    /**
     * Get profond
     *
     * @return decimal 
     */
    public function getProfond()
    {
        return $this->profond;
    }

    /**
     * Set qtetot
     *
     * @param integer $qtetot
     */
    public function setQtetot($qtetot)
    {
        $this->qtetot = $qtetot;
    }

    /**
     * Get qtetot
     *
     * @return integer 
     */
    public function getQtetot()
    {
        return $this->qtetot;
    }
	
	/**
     * Set nbpalette
     *
     * @param integer $nbpalette
     */
    public function setNbpalette($nbpalette)
    {
        $this->nbpalette = $nbpalette;
    }

    /**
     * Get nbpalette
     *
     * @return integer 
     */
    public function getNbpalette()
    {
        return $this->nbpalette;
    }

    /**
     * Set qteencours
     *
     * @param integer $qteencours
     */
    public function setQteencours($qteencours)
    {
        $this->qteencours = $qteencours;
    }

    /**
     * Get qteencours
     *
     * @return integer 
     */
    public function getQteencours()
    {
        return $this->qteencours;
    }
	
	/**
     * Set qtephys
     *
     * @param integer $qtephys
     */
    public function setQtephys($qtephys)
    {
        $this->qtephys = $qtephys;
    }

    /**
     * Get qtephys
     *
     * @return integer 
     */
    public function getQtephys()
    {
        return $this->qtephys;
    }
	
	/**
     * Set qtereserve
     *
     * @param integer $qtereserve
     */
    public function setQtereserve($qtereserve)
    {
        $this->qtereserve = $qtereserve;
    }

    /**
     * Get qtereserve
     *
     * @return integer 
     */
    public function getQtereserve()
    {
        return $this->qtereserve;
    }

    /**
     * Set qtequarant
     *
     * @param integer $qtequarant
     */
    public function setQtequarant($qtequarant)
    {
        $this->qtequarant = $qtequarant;
    }

    /**
     * Get qtequarant
     *
     * @return integer 
     */
    public function getQtequarant()
    {
        return $this->qtequarant;
    }

    /**
     * Set daterecep
     *
     * @param date $daterecep
     */
    public function setDaterecep($daterecep)
    {
        $this->daterecep = $daterecep;
    }

    /**
     * Get daterecep
     *
     * @return date 
     */
    public function getDaterecep()
    {
        return $this->daterecep;
    }

    /**
     * Set dateprepa
     *
     * @param date $dateprepa
     */
    public function setDateprepa($dateprepa)
    {
        $this->dateprepa = $dateprepa;
    }

    /**
     * Get dateprepa
     *
     * @return date 
     */
    public function getDateprepa()
    {
        return $this->dateprepa;
    }
	
	public function __toString()
    {
        return $this->designat;
		
    }
	
	/**
     * Set fourniss
     *
     * @param string $fourniss
     */
    public function setFourniss($fourniss)
    {
        $this->fourniss = $fourniss;
    }

    /**
     * Get fourniss
     *
     * @return string 
     */
    public function getFourniss()
    {
        return $this->fourniss;
    }
	
	/**
     * Set client
     *
     * @param string $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * Get client
     *
     * @return string 
     */
    public function getClient()
    {
        return $this->client;
    }
	
	/**
     * Set categorie
     *
     * @param string $categorie
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;
    }

    /**
     * Get categorie
     *
     * @return string 
     */
    public function getCategorie()
    {
        return $this->categorie;
    }
	
	
	

    /**
     * Set allee
     *
     * @param string $allee
     */
    public function setAllee($allee)
    {
        $this->allee = $allee;
    }

    /**
     * Get allee
     *
     * @return string 
     */
    public function getAllee()
    {
        return $this->allee;
    }

    /**
     * Set travee
     *
     * @param string $travee
     */
    public function setTravee($travee)
    {
        $this->travee = $travee;
    }

    /**
     * Get travee
     *
     * @return string 
     */
    public function getTravee()
    {
        return $this->travee;
    }

    /**
     * Set niveau
     *
     * @param string $niveau
     */
    public function setNiveau($niveau)
    {
        $this->niveau = $niveau;
    }

    /**
     * Get niveau
     *
     * @return string 
     */
    public function getNiveau()
    {
        return $this->niveau;
    }

    /**
     * Set emplacement
     *
     * @param string $emplacement
     */
    public function setEmplacement($emplacement)
    {
        $this->emplacement = $emplacement;
    }

    /**
     * Get emplacement
     *
     * @return string 
     */
    public function getEmplacement()
    {
        return $this->emplacement;
    }

    
    public function __construct()
    {
        $this->articordrecstock = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add articordrecstock
     *
     * @param Phareos\LogisToolBoxBundle\Entity\ordrecstock $articordrecstock
     */
    public function addordrecstock(\Phareos\LogisToolBoxBundle\Entity\ordrecstock $articordrecstock)
    {
        $this->articordrecstock[] = $articordrecstock;
    }

    /**
     * Get articordrecstock
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getArticordrecstock()
    {
        return $this->articordrecstock;
    }

    /**
     * Add articlemplarticle
     *
     * @param Phareos\LogisToolBoxBundle\Entity\emplarticle $articlemplarticle
     */
    public function addemplarticle(\Phareos\LogisToolBoxBundle\Entity\emplarticle $articlemplarticle)
    {
        $this->articlemplarticle[] = $articlemplarticle;
    }

    /**
     * Get articlemplarticle
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getArticlemplarticle()
    {
        return $this->articlemplarticle;
    }
}