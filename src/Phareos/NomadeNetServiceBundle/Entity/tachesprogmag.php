<?php

namespace Phareos\NomadeNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Phareos\NomadeNetServiceBundle\Entity\audtprog;

/**
 * Phareos\NomadeNetServiceBundle\Entity\tachesprogmag
 *
 * @ORM\Table(name="nom_tachesprogmag")
 * @ORM\Entity(repositoryClass="Phareos\NomadeNetServiceBundle\Entity\tachesprogmagRepository")
 */
class tachesprogmag
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
	 * @ORM\OneToMany(targetEntity="audtprog", mappedBy="tachesprogmag", cascade={"remove", "persist"})
	 */
	protected $audtechtachepros;
	
	protected $audtech;
	
	/**
	 * @ORM\OneToMany(targetEntity="planjs", mappedBy="tachesprogmag", cascade={"remove", "persist"})
	 */
	protected $tachesproplanjs;
	
	/**
	 * @ORM\ManyToOne(targetEntity="frequence", inversedBy="freqtachprogmag", cascade={"remove"})
	 * @ORM\JoinColumn(name="frequence_id", referencedColumnName="id")
	 */
	protected $frequence;
	
	/**
	 * @ORM\ManyToOne(targetEntity="fonction", inversedBy="fonctachprogmag", cascade={"remove"})
	 * @ORM\JoinColumn(name="fonction_id", referencedColumnName="id")
	 */
	protected $fonction;
	
	/**
	 * @ORM\ManyToOne(targetEntity="tachesprog", inversedBy="tachesprogtachprogmag", cascade={"remove"})
	 * @ORM\JoinColumn(name="tachesprog_id", referencedColumnName="id")
	 */
	protected $tachesprog;
	
	
	/**
	 * @ORM\ManyToOne(targetEntity="lieux", inversedBy="lieuxtachprogmag", cascade={"remove"})
	 * @ORM\JoinColumn(name="lieux_id", referencedColumnName="id")
	 */
	protected $lieux;
	
	/**
     * @var integer $lieuxid2
     *
     * @ORM\Column(name="lieuxid2", type="integer", nullable=true)
     */
    private $lieuxid2;

    /**
     * @var string $tache
     *
     * @ORM\Column(name="tache", type="string", length=255)
     */
    private $tache;

    /**
     * @var time $duree
     *
     * @ORM\Column(name="duree", type="time")
     */
    private $duree;
	
	/**
     * @var datetime $dureetot
     *
     * @ORM\Column(name="dureetot", type="datetime", nullable=true)
     */
    private $dureetot;
	
	/**
     * @var datetime $dureerest
     *
     * @ORM\Column(name="dureerest", type="datetime", nullable=true)
     */
    private $dureerest;

    /**
     * @var string $nomgc
     *
     * @ORM\Column(name="nomgc", type="string", length=255)
     */
    private $nomgc;

    /**
     * @var integer $qte
     *
     * @ORM\Column(name="qte", type="integer", nullable=true)
     */
    private $qte;
	
	/**
     * @var string $marqtype
     *
     * @ORM\Column(name="marqtype", type="string", length=255, nullable=true)
     */
    private $marqtype;
	
	/**
     * @var boolean $archive
     *
     * @ORM\Column(name="archive", type="boolean", nullable=true)
     */
    private $archive;
	
	/**
     * @var boolean $delcheck
     *
     * @ORM\Column(name="delcheck", type="boolean", nullable=true)
     */
    private $delcheck;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
	
	/**
     * Set delcheck
     *
     * @param boolean $delcheck
     */
    public function setDelcheck($delcheck)
    {
        $this->delcheck = $delcheck;
    }

    /**
     * Get delcheck
     *
     * @return boolean 
     */
    public function getDelcheck()
    {
        return $this->delcheck;
    }
	
	// Important 
    public function getAudtech()
    {
        $audtech = new ArrayCollection();
        
        foreach($this->audtechtachepros as $p)
        {
            $audtech[] = $p->getAudtech();
        }

        return $audtech;
    }
	
	// Important
    public function setAudtech($audtech)
    {
        foreach($audtech as $p)
        {
            $audtechtachepros = new audtprog();

            $audtechtachepros->setTachesprogmag($this);
            $audtechtachepros->setAudtech($p);

            $this->addAudtechtachepros($audtechtachepros);
        }

    }
	
	public function getTachesprogmag()
	{
		return $this;
	}
	
	public function addAudtechtachepros($audtprog)
    {
        $this->audtechtachepros[] = $audtprog;
    }
    
    public function removeAudtechtachepros($audtprog)
    {
        return $this->audtechtachepros->removeElement($audtprog);
    }
	
	/**
     * Set lieuxid2
     *
     * @param integer $lieuxid2
     */
    public function setLieuxid2($lieuxid2)
    {
        $this->lieuxid2 = $lieuxid2;
    }

    /**
     * Get lieuxid2
     *
     * @return integer 
     */
    public function getLieuxid2()
    {
        return $this->lieuxid2;
    }

    /**
     * Set tache
     *
     * @param string $tache
     */
    public function setTache($tache)
    {
        $this->tache = $tache;
    }

    /**
     * Get tache
     *
     * @return string 
     */
    public function getTache()
    {
        return $this->tache;
    }

    /**
     * Set duree
     *
     * @param time $duree
     */
    public function setDuree($duree)
    {
        $this->duree = $duree;
    }

    /**
     * Get duree
     *
     * @return time 
     */
    public function getDuree()
    {
        return $this->duree;
    }

    /**
     * Set nomgc
     *
     * @param string $nomgc
     */
    public function setNomgc($nomgc)
    {
        $this->nomgc = $nomgc;
    }

    /**
     * Get nomgc
     *
     * @return string 
     */
    public function getNomgc()
    {
        return $this->nomgc;
    }

    /**
     * Set qte
     *
     * @param integer $qte
     */
    public function setQte($qte)
    {
        $this->qte = $qte;
    }

    /**
     * Get qte
     *
     * @return integer 
     */
    public function getQte()
    {
        return $this->qte;
    }

    /**
     * Set frequence
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\frequence $frequence
     */
    public function setFrequence(\Phareos\NomadeNetServiceBundle\Entity\frequence $frequence)
    {
        $this->frequence = $frequence;
    }

    /**
     * Get frequence
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\frequence 
     */
    public function getFrequence()
    {
        return $this->frequence;
    }

    /**
     * Set fonction
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\fonction $fonction
     */
    public function setFonction(\Phareos\NomadeNetServiceBundle\Entity\fonction $fonction)
    {
        $this->fonction = $fonction;
    }

    /**
     * Get fonction
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\fonction 
     */
    public function getFonction()
    {
        return $this->fonction;
    }

    /**
     * Set lieux
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\lieux $lieux
     */
    public function setLieux(\Phareos\NomadeNetServiceBundle\Entity\lieux $lieux)
    {
        $this->lieux = $lieux;
    }

    /**
     * Get lieux
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\lieux 
     */
    public function getLieux()
    {
        return $this->lieux;
    }

    /**
     * Set marqtype
     *
     * @param string $marqtype
     */
    public function setMarqtype($marqtype)
    {
        $this->marqtype = $marqtype;
    }

    /**
     * Get marqtype
     *
     * @return string 
     */
    public function getMarqtype()
    {
        return $this->marqtype;
    }
    public function __construct()
    {
        $this->audtechtachepros = new \Doctrine\Common\Collections\ArrayCollection();
		$this->audtech = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add audtechtachepros
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\audtprog $audtechtachepros
     */
    public function addaudtprog(\Phareos\NomadeNetServiceBundle\Entity\audtprog $audtechtachepros)
    {
        $this->audtechtachepros[] = $audtechtachepros;
    }

    /**
     * Get audtechtachepros
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getAudtechtachepros()
    {
        return $this->audtechtachepros;
    }

    /**
     * Set tachesprog
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\tachesprog $tachesprog
     */
    public function setTachesprog(\Phareos\NomadeNetServiceBundle\Entity\tachesprog $tachesprog)
    {
        $this->tachesprog = $tachesprog;
    }

    /**
     * Get tachesprog
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\tachesprog 
     */
    public function getTachesprog()
    {
        return $this->tachesprog;
    }

    /**
     * Set dureetot
     *
     * @param datetime $dureetot
     */
    public function setDureetot($dureetot)
    {
        $this->dureetot = $dureetot;
    }

    /**
     * Get dureetot
     *
     * @return datetime
     */
    public function getDureetot()
    {
        return $this->dureetot;
    }

    /**
     * Set dureerest
     *
     * @param datetime $dureerest
     */
    public function setDureerest($dureerest)
    {
        $this->dureerest = $dureerest;
    }

    /**
     * Get dureerest
     *
     * @return datetime 
     */
    public function getDureerest()
    {
        return $this->dureerest;
    }
	
	/**
     * Set archive
     *
     * @param boolean $archive
     */
    public function setArchive($archive)
    {
        $this->archive = $archive;
    }

    /**
     * Get archive
     *
     * @return boolean 
     */
    public function getArchive()
    {
        return $this->archive;
    }

    /**
     * Add tachesproplanjs
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\planjs $tachesproplanjs
     */
    public function addplanjs(\Phareos\NomadeNetServiceBundle\Entity\planjs $tachesproplanjs)
    {
        $this->tachesproplanjs[] = $tachesproplanjs;
    }

    /**
     * Get tachesproplanjs
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getTachesproplanjs()
    {
        return $this->tachesproplanjs;
    }
}