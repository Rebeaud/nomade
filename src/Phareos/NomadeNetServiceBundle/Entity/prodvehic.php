<?php

namespace Phareos\NomadeNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\NomadeNetServiceBundle\Entity\prodvehic
 *
 * @ORM\Table(name="nom_prodvehic")
 * @ORM\Entity(repositoryClass="Phareos\NomadeNetServiceBundle\Entity\prodvehicRepository")
 */
class prodvehic
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
	 * @ORM\ManyToOne(targetEntity="prod", inversedBy="prodprodvehic", cascade={"remove"})
	 * @ORM\JoinColumn(name="prod_id", referencedColumnName="id")
	 */
	protected $prod;
	
	/**
	 * @ORM\ManyToOne(targetEntity="grand_compte", inversedBy="grdcpteprodvehic", cascade={"remove"})
	 * @ORM\JoinColumn(name="grdcompte_id", referencedColumnName="id")
	 */
	protected $grdcompte;
	
	/**
	 * @ORM\ManyToOne(targetEntity="respsect", inversedBy="respsectprodvehic", cascade={"remove"})
	 * @ORM\JoinColumn(name="respsect_id", referencedColumnName="id")
	 */
	protected $respsect;
	
	/**
	 * @ORM\OneToMany(targetEntity="stockmag", mappedBy="prodmag", cascade={"remove", "persist"})
	 */
	protected $prodvehicstock;

    /**
     * @var string $societeuser
     *
     * @ORM\Column(name="societeuser", type="string", length=255, nullable=true)
     */
    private $societeuser;
	
	/**
     * @var integer $stock
     *
     * @ORM\Column(name="stock", type="integer", nullable=true)
     */
    private $stock;

    /**
     * @var integer $seuil
     *
     * @ORM\Column(name="seuil", type="integer", nullable=true)
     */
    private $seuil;
	
	/**
     * @var boolean $archive
     *
     * @ORM\Column(name="archive", type="boolean", nullable=true)
     */
    private $archive;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
	
	/**
     * Set societeuser
     *
     * @param string $societeuser
     */
    public function setSocieteuser($societeuser)
    {
        $this->societeuser = $societeuser;
    }

    /**
     * Get societeuser
     *
     * @return string 
     */
    public function getSocieteuser()
    {
        return $this->societeuser;
    }

    /**
     * Set stock
     *
     * @param integer $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * Get stock
     *
     * @return integer 
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set seuil
     *
     * @param integer $seuil
     */
    public function setSeuil($seuil)
    {
        $this->seuil = $seuil;
    }

    /**
     * Get seuil
     *
     * @return integer 
     */
    public function getSeuil()
    {
        return $this->seuil;
    }
	
	/**
     * Set archive
     *
     * @param boolean $archive
     */
    public function setArchive($archive)
    {
        $this->archive = $archive;
    }

    /**
     * Get archive
     *
     * @return boolean 
     */
    public function getArchive()
    {
        return $this->archive;
    }
	
	
    public function __construct()
    {
        $this->prodvehicstock = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set prod
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\prod $prod
     */
    public function setProd(\Phareos\NomadeNetServiceBundle\Entity\prod $prod)
    {
        $this->prod = $prod;
    }

    /**
     * Get prod
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\prod 
     */
    public function getProd()
    {
        return $this->prod;
    }

    /**
     * Set grdcompte
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\grand_compte $grdcompte
     */
    public function setGrdcompte(\Phareos\NomadeNetServiceBundle\Entity\grand_compte $grdcompte)
    {
        $this->grdcompte = $grdcompte;
    }

    /**
     * Get grdcompte
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\grand_compte 
     */
    public function getGrdcompte()
    {
        return $this->grdcompte;
    }

    /**
     * Set respsect
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\respsect $respsect
     */
    public function setRespsect(\Phareos\NomadeNetServiceBundle\Entity\respsect $respsect)
    {
        $this->respsect = $respsect;
    }

    /**
     * Get respsect
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\respsect 
     */
    public function getRespsect()
    {
        return $this->respsect;
    }

    /**
     * Add prodvehicstock
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\stockmag $prodvehicstock
     */
    public function addstockmag(\Phareos\NomadeNetServiceBundle\Entity\stockmag $prodvehicstock)
    {
        $this->prodvehicstock[] = $prodvehicstock;
    }

    /**
     * Get prodvehicstock
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getProdvehicstock()
    {
        return $this->prodvehicstock;
    }
}