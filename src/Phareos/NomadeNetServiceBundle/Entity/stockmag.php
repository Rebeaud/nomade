<?php

namespace Phareos\NomadeNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\NomadeNetServiceBundle\Entity\stockmag
 *
 * @ORM\Table(name="nom_stockmag")
 * @ORM\Entity(repositoryClass="Phareos\NomadeNetServiceBundle\Entity\stockmagRepository")
 */
class stockmag
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
	 * @ORM\ManyToOne(targetEntity="prodmag", inversedBy="prodmagstock", cascade={"remove"})
	 * @ORM\JoinColumn(name="prodmag_id", referencedColumnName="id")
	 */
	protected $prodmag;
	
	/**
	 * @ORM\ManyToOne(targetEntity="prodvehic", inversedBy="prodvehicstock", cascade={"remove"})
	 * @ORM\JoinColumn(name="prodvehic_id", referencedColumnName="id")
	 */
	protected $prodvehic;
	
	/**
	 * @ORM\ManyToOne(targetEntity="lieux", inversedBy="lieuxstock", cascade={"remove"})
	 * @ORM\JoinColumn(name="lieux_id", referencedColumnName="id")
	 */
	protected $lieux;

    /**
     * @var date $date
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var integer $depotmag
     *
     * @ORM\Column(name="depotmag", type="integer")
     */
    private $depotmag;

    /**
     * @var string $typlivr
     *
     * @ORM\Column(name="typlivr", type="string", length=255)
     */
    private $typlivr;

    /**
     * @var integer $stockve
     *
     * @ORM\Column(name="stockve", type="integer")
     */
    private $stockve;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param date $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * Get date
     *
     * @return date 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set depotmag
     *
     * @param integer $depotmag
     */
    public function setDepotmag($depotmag)
    {
        $this->depotmag = $depotmag;
    }

    /**
     * Get depotmag
     *
     * @return integer 
     */
    public function getDepotmag()
    {
        return $this->depotmag;
    }

    /**
     * Set typlivr
     *
     * @param string $typlivr
     */
    public function setTyplivr($typlivr)
    {
        $this->typlivr = $typlivr;
    }

    /**
     * Get typlivr
     *
     * @return string 
     */
    public function getTyplivr()
    {
        return $this->typlivr;
    }

    /**
     * Set stockve
     *
     * @param integer $stockve
     */
    public function setStockve($stockve)
    {
        $this->stockve = $stockve;
    }

    /**
     * Get stockve
     *
     * @return integer 
     */
    public function getStockve()
    {
        return $this->stockve;
    }

    /**
     * Set prodmag
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\prodmag $prodmag
     */
    public function setProdmag(\Phareos\NomadeNetServiceBundle\Entity\prodmag $prodmag)
    {
        $this->prodmag = $prodmag;
    }

    /**
     * Get prodmag
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\prodmag 
     */
    public function getProdmag()
    {
        return $this->prodmag;
    }

    /**
     * Set lieux
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\lieux $lieux
     */
    public function setLieux(\Phareos\NomadeNetServiceBundle\Entity\lieux $lieux)
    {
        $this->lieux = $lieux;
    }

    /**
     * Get lieux
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\lieux 
     */
    public function getLieux()
    {
        return $this->lieux;
    }

    /**
     * Set prodvehic
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\prodvehic $prodvehic
     */
    public function setProdvehic(\Phareos\NomadeNetServiceBundle\Entity\prodvehic $prodvehic)
    {
        $this->prodvehic = $prodvehic;
    }

    /**
     * Get prodvehic
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\prodvehic 
     */
    public function getProdvehic()
    {
        return $this->prodvehic;
    }
}