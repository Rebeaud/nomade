<?php

namespace Phareos\NomadeNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\NomadeNetServiceBundle\Entity\grand_compte
 *
 * @ORM\Table(name="nom_grand_compte")
 * @ORM\Entity(repositoryClass="Phareos\NomadeNetServiceBundle\Entity\grand_compteRepository")
 */
class grand_compte
{
    
	/**
	* @ORM\OneToMany(targetEntity="lieux", mappedBy="grand_compte")
	*/
	protected $lieuxgc;
	
	/**
	 * @ORM\OneToMany(targetEntity="prodmag", mappedBy="grand_compte", cascade={"remove", "persist"})
	 */
	protected $grdcpteprodmag;
	
	/**
	 * @ORM\OneToMany(targetEntity="prodvehic", mappedBy="grand_compte", cascade={"remove", "persist"})
	 */
	protected $grdcpteprodvehic;
	
	/**
	 * @ORM\OneToMany(targetEntity="prod", mappedBy="grand_compte", cascade={"remove", "persist"})
	 */
	protected $grdcpteprod;
	
	
	
	
	
	/**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $MATRICULE_GC
     *
     * @ORM\Column(name="MATRICULE_GC", type="string", length=4, nullable=true)
     */
    private $MATRICULE_GC;
	
	/**
     * @var string $societeuser
     *
     * @ORM\Column(name="societeuser", type="string", length=255, nullable=true)
     */
    private $societeuser;

    /**
     * @var string $CODE_VILLE
     *
     * @ORM\Column(name="CODE_VILLE", type="string", length=5, nullable=true)
     */
    private $CODE_VILLE;

    /**
     * @var string $NOM_GC
     *
     * @ORM\Column(name="NOM_GC", type="string", length=32, nullable=true)
     */
    private $NOM_GC;

    /**
     * @var text $ADRUE_GC
     *
     * @ORM\Column(name="ADRUE_GC", type="text", nullable=true)
     */
    private $ADRUE_GC;

    /**
     * @var string $TELL_GC
     *
     * @ORM\Column(name="TELL_GC", type="string", length=14, nullable=true)
     */
    private $TELL_GC;

    /**
     * @var string $FAX_GC
     *
     * @ORM\Column(name="FAX_GC", type="string", length=14, nullable=true)
     */
    private $FAX_GC;

    /**
     * @var string $EMAIL_GC
     *
     * @ORM\Column(name="EMAIL_GC", type="string", length=50, nullable=true)
     */
    private $EMAIL_GC;

    /**
     * @var date $DATE_OUVERTURE
     *
     * @ORM\Column(name="DATE_OUVERTURE", type="date", nullable=true)
     */
    private $DATE_OUVERTURE;

    /**
     * @var smallint $SITE
     *
     * @ORM\Column(name="SITE", type="smallint", nullable=true)
     */
    private $SITE;

    /**
     * @var text $COMPTE
     *
     * @ORM\Column(name="COMPTE", type="text", nullable=true)
     */
    private $COMPTE;

    /**
     * @var text $COMMENTAIRE
     *
     * @ORM\Column(name="COMMENTAIRE", type="text", nullable=true)
     */
    private $COMMENTAIRE;

    /**
     * @var date $DATE_FERMETURE
     *
     * @ORM\Column(name="DATE_FERMETURE", type="date", nullable=true)
     */
    private $DATE_FERMETURE;

    /**
     * @var smallint $JANVIER
     *
     * @ORM\Column(name="JANVIER", type="smallint", nullable=true)
     */
    private $JANVIER;

    /**
     * @var smallint $FEVRIER
     *
     * @ORM\Column(name="FEVRIER", type="smallint", nullable=true)
     */
    private $FEVRIER;

    /**
     * @var smallint $MARS
     *
     * @ORM\Column(name="MARS", type="smallint", nullable=true)
     */
    private $MARS;

    /**
     * @var smallint $AVRIL
     *
     * @ORM\Column(name="AVRIL", type="smallint", nullable=true)
     */
    private $AVRIL;

    /**
     * @var smallint $MAI
     *
     * @ORM\Column(name="MAI", type="smallint", nullable=true)
     */
    private $MAI;

    /**
     * @var smallint $JUIN
     *
     * @ORM\Column(name="JUIN", type="smallint", nullable=true)
     */
    private $JUIN;

    /**
     * @var smallint $JUILLET
     *
     * @ORM\Column(name="JUILLET", type="smallint", nullable=true)
     */
    private $JUILLET;

    /**
     * @var smallint $AOUT
     *
     * @ORM\Column(name="AOUT", type="smallint", nullable=true)
     */
    private $AOUT;

    /**
     * @var smallint $SEPTEMBRE
     *
     * @ORM\Column(name="SEPTEMBRE", type="smallint", nullable=true)
     */
    private $SEPTEMBRE;

    /**
     * @var smallint $OCTOBRE
     *
     * @ORM\Column(name="OCTOBRE", type="smallint", nullable=true)
     */
    private $OCTOBRE;

    /**
     * @var smallint $NOVEMBRE
     *
     * @ORM\Column(name="NOVEMBRE", type="smallint", nullable=true)
     */
    private $NOVEMBRE;

    /**
     * @var smallint $DECEMBRE
     *
     * @ORM\Column(name="DECEMBRE", type="smallint", nullable=true)
     */
    private $DECEMBRE;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set MATRICULE_GC
     *
     * @param string $mATRICULEGC
     */
    public function setMATRICULEGC($mATRICULEGC)
    {
        $this->MATRICULE_GC = $mATRICULEGC;
    }

    /**
     * Get MATRICULE_GC
     *
     * @return string 
     */
    public function getMATRICULEGC()
    {
        return $this->MATRICULE_GC;
    }
	
	/**
     * Set societeuser
     *
     * @param string $societeuser
     */
    public function setSocieteuser($societeuser)
    {
        $this->societeuser = $societeuser;
    }

    /**
     * Get societeuser
     *
     * @return string 
     */
    public function getSocieteuser()
    {
        return $this->societeuser;
    }

    /**
     * Set CODE_VILLE
     *
     * @param string $cODEVILLE
     */
    public function setCODEVILLE($cODEVILLE)
    {
        $this->CODE_VILLE = $cODEVILLE;
    }

    /**
     * Get CODE_VILLE
     *
     * @return string 
     */
    public function getCODEVILLE()
    {
        return $this->CODE_VILLE;
    }

    /**
     * Set NOM_GC
     *
     * @param string $nOMGC
     */
    public function setNOMGC($nOMGC)
    {
        $this->NOM_GC = $nOMGC;
    }

    /**
     * Get NOM_GC
     *
     * @return string 
     */
    public function getNOMGC()
    {
        return $this->NOM_GC;
    }

    /**
     * Set ADRUE_GC
     *
     * @param text $aDRUEGC
     */
    public function setADRUEGC($aDRUEGC)
    {
        $this->ADRUE_GC = $aDRUEGC;
    }

    /**
     * Get ADRUE_GC
     *
     * @return text 
     */
    public function getADRUEGC()
    {
        return $this->ADRUE_GC;
    }

    /**
     * Set TELL_GC
     *
     * @param string $tELLGC
     */
    public function setTELLGC($tELLGC)
    {
        $this->TELL_GC = $tELLGC;
    }

    /**
     * Get TELL_GC
     *
     * @return string 
     */
    public function getTELLGC()
    {
        return $this->TELL_GC;
    }

    /**
     * Set FAX_GC
     *
     * @param string $fAXGC
     */
    public function setFAXGC($fAXGC)
    {
        $this->FAX_GC = $fAXGC;
    }

    /**
     * Get FAX_GC
     *
     * @return string 
     */
    public function getFAXGC()
    {
        return $this->FAX_GC;
    }

    /**
     * Set EMAIL_GC
     *
     * @param string $eMAILGC
     */
    public function setEMAILGC($eMAILGC)
    {
        $this->EMAIL_GC = $eMAILGC;
    }

    /**
     * Get EMAIL_GC
     *
     * @return string 
     */
    public function getEMAILGC()
    {
        return $this->EMAIL_GC;
    }

    /**
     * Set DATE_OUVERTURE
     *
     * @param date $dATEOUVERTURE
     */
    public function setDATEOUVERTURE($dATEOUVERTURE)
    {
        $this->DATE_OUVERTURE = $dATEOUVERTURE;
    }

    /**
     * Get DATE_OUVERTURE
     *
     * @return date 
     */
    public function getDATEOUVERTURE()
    {
        return $this->DATE_OUVERTURE;
    }

    /**
     * Set SITE
     *
     * @param smallint $sITE
     */
    public function setSITE($sITE)
    {
        $this->SITE = $sITE;
    }

    /**
     * Get SITE
     *
     * @return smallint 
     */
    public function getSITE()
    {
        return $this->SITE;
    }

    /**
     * Set COMPTE
     *
     * @param text $cOMPTE
     */
    public function setCOMPTE($cOMPTE)
    {
        $this->COMPTE = $cOMPTE;
    }

    /**
     * Get COMPTE
     *
     * @return text 
     */
    public function getCOMPTE()
    {
        return $this->COMPTE;
    }

    /**
     * Set COMMENTAIRE
     *
     * @param text $cOMMENTAIRE
     */
    public function setCOMMENTAIRE($cOMMENTAIRE)
    {
        $this->COMMENTAIRE = $cOMMENTAIRE;
    }

    /**
     * Get COMMENTAIRE
     *
     * @return text 
     */
    public function getCOMMENTAIRE()
    {
        return $this->COMMENTAIRE;
    }

    /**
     * Set DATE_FERMETURE
     *
     * @param date $dATEFERMETURE
     */
    public function setDATEFERMETURE($dATEFERMETURE)
    {
        $this->DATE_FERMETURE = $dATEFERMETURE;
    }

    /**
     * Get DATE_FERMETURE
     *
     * @return date 
     */
    public function getDATEFERMETURE()
    {
        return $this->DATE_FERMETURE;
    }

    /**
     * Set JANVIER
     *
     * @param smallint $jANVIER
     */
    public function setJANVIER($jANVIER)
    {
        $this->JANVIER = $jANVIER;
    }

    /**
     * Get JANVIER
     *
     * @return smallint 
     */
    public function getJANVIER()
    {
        return $this->JANVIER;
    }

    /**
     * Set FEVRIER
     *
     * @param smallint $fEVRIER
     */
    public function setFEVRIER($fEVRIER)
    {
        $this->FEVRIER = $fEVRIER;
    }

    /**
     * Get FEVRIER
     *
     * @return smallint 
     */
    public function getFEVRIER()
    {
        return $this->FEVRIER;
    }

    /**
     * Set MARS
     *
     * @param smallint $mARS
     */
    public function setMARS($mARS)
    {
        $this->MARS = $mARS;
    }

    /**
     * Get MARS
     *
     * @return smallint 
     */
    public function getMARS()
    {
        return $this->MARS;
    }

    /**
     * Set AVRIL
     *
     * @param smallint $aVRIL
     */
    public function setAVRIL($aVRIL)
    {
        $this->AVRIL = $aVRIL;
    }

    /**
     * Get AVRIL
     *
     * @return smallint 
     */
    public function getAVRIL()
    {
        return $this->AVRIL;
    }

    /**
     * Set MAI
     *
     * @param smallint $mAI
     */
    public function setMAI($mAI)
    {
        $this->MAI = $mAI;
    }

    /**
     * Get MAI
     *
     * @return smallint 
     */
    public function getMAI()
    {
        return $this->MAI;
    }

    /**
     * Set JUIN
     *
     * @param smallint $jUIN
     */
    public function setJUIN($jUIN)
    {
        $this->JUIN = $jUIN;
    }

    /**
     * Get JUIN
     *
     * @return smallint 
     */
    public function getJUIN()
    {
        return $this->JUIN;
    }

    /**
     * Set JUILLET
     *
     * @param smallint $jUILLET
     */
    public function setJUILLET($jUILLET)
    {
        $this->JUILLET = $jUILLET;
    }

    /**
     * Get JUILLET
     *
     * @return smallint 
     */
    public function getJUILLET()
    {
        return $this->JUILLET;
    }

    /**
     * Set AOUT
     *
     * @param smallint $aOUT
     */
    public function setAOUT($aOUT)
    {
        $this->AOUT = $aOUT;
    }

    /**
     * Get AOUT
     *
     * @return smallint 
     */
    public function getAOUT()
    {
        return $this->AOUT;
    }

    /**
     * Set SEPTEMBRE
     *
     * @param smallint $sEPTEMBRE
     */
    public function setSEPTEMBRE($sEPTEMBRE)
    {
        $this->SEPTEMBRE = $sEPTEMBRE;
    }

    /**
     * Get SEPTEMBRE
     *
     * @return smallint 
     */
    public function getSEPTEMBRE()
    {
        return $this->SEPTEMBRE;
    }

    /**
     * Set OCTOBRE
     *
     * @param smallint $oCTOBRE
     */
    public function setOCTOBRE($oCTOBRE)
    {
        $this->OCTOBRE = $oCTOBRE;
    }

    /**
     * Get OCTOBRE
     *
     * @return smallint 
     */
    public function getOCTOBRE()
    {
        return $this->OCTOBRE;
    }

    /**
     * Set NOVEMBRE
     *
     * @param smallint $nOVEMBRE
     */
    public function setNOVEMBRE($nOVEMBRE)
    {
        $this->NOVEMBRE = $nOVEMBRE;
    }

    /**
     * Get NOVEMBRE
     *
     * @return smallint 
     */
    public function getNOVEMBRE()
    {
        return $this->NOVEMBRE;
    }

    /**
     * Set DECEMBRE
     *
     * @param smallint $dECEMBRE
     */
    public function setDECEMBRE($dECEMBRE)
    {
        $this->DECEMBRE = $dECEMBRE;
    }

    /**
     * Get DECEMBRE
     *
     * @return smallint 
     */
    public function getDECEMBRE()
    {
        return $this->DECEMBRE;
    }
    public function __construct()
    {
        $this->lieuxgc = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add lieuxgc
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\lieux $lieuxgc
     */
    public function addlieux(\Phareos\NomadeNetServiceBundle\Entity\lieux $lieuxgc)
    {
        $this->lieuxgc[] = $lieuxgc;
    }

    /**
     * Get lieuxgc
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getLieuxgc()
    {
        return $this->lieuxgc;
    }
	
	/**
	* Set lieuxgc
	*
	* @param \Doctrine\Common\Collections\Collection $lieuxgc
	*/
	public function setLieux(\Doctrine\Common\Collections\Collection $lieuxgc)
	{
    $this->lieuxgc = $lieuxgc;
	}
	


    /**
     * Add grdcpteprodmag
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\prodmag $grdcpteprodmag
     */
    public function addprodmag(\Phareos\NomadeNetServiceBundle\Entity\prodmag $grdcpteprodmag)
    {
        $this->grdcpteprodmag[] = $grdcpteprodmag;
    }

    /**
     * Get grdcpteprodmag
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getGrdcpteprodmag()
    {
        return $this->grdcpteprodmag;
    }

    /**
     * Add grdcpteprod
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\prod $grdcpteprod
     */
    public function addprod(\Phareos\NomadeNetServiceBundle\Entity\prod $grdcpteprod)
    {
        $this->grdcpteprod[] = $grdcpteprod;
    }

    /**
     * Get grdcpteprod
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getGrdcpteprod()
    {
        return $this->grdcpteprod;
    }

    /**
     * Add grdcpteprodvehic
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\prodvehic $grdcpteprodvehic
     */
    public function addprodvehic(\Phareos\NomadeNetServiceBundle\Entity\prodvehic $grdcpteprodvehic)
    {
        $this->grdcpteprodvehic[] = $grdcpteprodvehic;
    }

    /**
     * Get grdcpteprodvehic
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getGrdcpteprodvehic()
    {
        return $this->grdcpteprodvehic;
    }
}