<?php

namespace Phareos\NomadeNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\NomadeNetServiceBundle\Entity\nom_responsmag
 *
 * @ORM\Table(name="nom_responsmag")
 * @ORM\Entity(repositoryClass="Phareos\NomadeNetServiceBundle\Entity\nom_responsmagRepository")
 */
class nom_responsmag
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bigint $NUM_PERS
     *
     * @ORM\Column(name="NUM_PERS", type="bigint", nullable=true)
     */
    private $NUM_PERS;

    /**
     * @var string $CODE_VILLE
     *
     * @ORM\Column(name="CODE_VILLE", type="string", length=5, nullable=true)
     */
    private $CODE_VILLE;

    /**
     * @var string $FAX_DR
     *
     * @ORM\Column(name="FAX_DR", type="string", length=4, nullable=true)
     */
    private $FAX_DR;

    /**
     * @var string $NOM_PERS
     *
     * @ORM\Column(name="NOM_PERS", type="string", length=32, nullable=true)
     */
    private $NOM_PERS;

    /**
     * @var string $PRENOM_PERS
     *
     * @ORM\Column(name="PRENOM_PERS", type="string", length=32, nullable=true)
     */
    private $PRENOM_PERS;

    /**
     * @var string $TELFIX_PERS
     *
     * @ORM\Column(name="TELFIX_PERS", type="string", length=14, nullable=true)
     */
    private $TELFIX_PERS;

    /**
     * @var string $TELPOR_PERS
     *
     * @ORM\Column(name="TELPOR_PERS", type="string", length=14, nullable=true)
     */
    private $TELPOR_PERS;

    /**
     * @var string $EMAIL_PERS
     *
     * @ORM\Column(name="EMAIL_PERS", type="string", length=50, nullable=true)
     */
    private $EMAIL_PERS;

    /**
     * @var string $AD_RUE
     *
     * @ORM\Column(name="AD_RUE", type="string", length=50, nullable=true)
     */
    private $AD_RUE;

    /**
     * @var string $SUPPL_AD
     *
     * @ORM\Column(name="SUPPL_AD", type="string", length=50, nullable=true)
     */
    private $SUPPL_AD;
	
	/**
     * @var string $codag
     *
     * @ORM\Column(name="codag", type="string", length=50, nullable=true)
     */
    private $codag;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set NUM_PERS
     *
     * @param bigint $nUMPERS
     */
    public function setNUMPERS($nUMPERS)
    {
        $this->NUM_PERS = $nUMPERS;
    }

    /**
     * Get NUM_PERS
     *
     * @return bigint 
     */
    public function getNUMPERS()
    {
        return $this->NUM_PERS;
    }

    /**
     * Set CODE_VILLE
     *
     * @param string $cODEVILLE
     */
    public function setCODEVILLE($cODEVILLE)
    {
        $this->CODE_VILLE = $cODEVILLE;
    }

    /**
     * Get CODE_VILLE
     *
     * @return string 
     */
    public function getCODEVILLE()
    {
        return $this->CODE_VILLE;
    }

    /**
     * Set FAX_DR
     *
     * @param string $fAXDR
     */
    public function setFAXDR($fAXDR)
    {
        $this->FAX_DR = $fAXDR;
    }

    /**
     * Get FAX_DR
     *
     * @return string 
     */
    public function getFAXDR()
    {
        return $this->FAX_DR;
    }

    /**
     * Set NOM_PERS
     *
     * @param string $nOMPERS
     */
    public function setNOMPERS($nOMPERS)
    {
        $this->NOM_PERS = $nOMPERS;
    }

    /**
     * Get NOM_PERS
     *
     * @return string 
     */
    public function getNOMPERS()
    {
        return $this->NOM_PERS;
    }

    /**
     * Set PRENOM_PERS
     *
     * @param string $pRENOMPERS
     */
    public function setPRENOMPERS($pRENOMPERS)
    {
        $this->PRENOM_PERS = $pRENOMPERS;
    }

    /**
     * Get PRENOM_PERS
     *
     * @return string 
     */
    public function getPRENOMPERS()
    {
        return $this->PRENOM_PERS;
    }

    /**
     * Set TELFIX_PERS
     *
     * @param string $tELFIXPERS
     */
    public function setTELFIXPERS($tELFIXPERS)
    {
        $this->TELFIX_PERS = $tELFIXPERS;
    }

    /**
     * Get TELFIX_PERS
     *
     * @return string 
     */
    public function getTELFIXPERS()
    {
        return $this->TELFIX_PERS;
    }

    /**
     * Set TELPOR_PERS
     *
     * @param string $tELPORPERS
     */
    public function setTELPORPERS($tELPORPERS)
    {
        $this->TELPOR_PERS = $tELPORPERS;
    }

    /**
     * Get TELPOR_PERS
     *
     * @return string 
     */
    public function getTELPORPERS()
    {
        return $this->TELPOR_PERS;
    }

    /**
     * Set EMAIL_PERS
     *
     * @param string $eMAILPERS
     */
    public function setEMAILPERS($eMAILPERS)
    {
        $this->EMAIL_PERS = $eMAILPERS;
    }

    /**
     * Get EMAIL_PERS
     *
     * @return string 
     */
    public function getEMAILPERS()
    {
        return $this->EMAIL_PERS;
    }

    /**
     * Set AD_RUE
     *
     * @param string $aDRUE
     */
    public function setADRUE($aDRUE)
    {
        $this->AD_RUE = $aDRUE;
    }

    /**
     * Get AD_RUE
     *
     * @return string 
     */
    public function getADRUE()
    {
        return $this->AD_RUE;
    }

    /**
     * Set SUPPL_AD
     *
     * @param string $sUPPLAD
     */
    public function setSUPPLAD($sUPPLAD)
    {
        $this->SUPPL_AD = $sUPPLAD;
    }

    /**
     * Get SUPPL_AD
     *
     * @return string 
     */
    public function getSUPPLAD()
    {
        return $this->SUPPL_AD;
    }

    /**
     * Set codag
     *
     * @param string $codag
     */
    public function setCodag($codag)
    {
        $this->codag = $codag;
    }

    /**
     * Get codag
     *
     * @return string 
     */
    public function getCodag()
    {
        return $this->codag;
    }
}