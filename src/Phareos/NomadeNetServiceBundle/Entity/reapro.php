<?php

namespace Phareos\NomadeNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\NomadeNetServiceBundle\Entity\reapro
 *
 * @ORM\Table(name="nom_reapro")
 * @ORM\Entity(repositoryClass="Phareos\NomadeNetServiceBundle\Entity\reaproRepository")
 */
class reapro
{
    /**
	* @ORM\ManyToOne(targetEntity="lieux", inversedBy="reaprolieux", cascade={"remove"})
	* @ORM\JoinColumn(name="lieux_id", referencedColumnName="id")
	*/
	protected $lieux;
	
	/**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $site
     *
     * @ORM\Column(name="site", type="string", length=255)
     */
    private $site;

    /**
     * @var string $nomrespsect
     *
     * @ORM\Column(name="nomrespsect", type="string", length=255)
     */
    private $nomrespsect;

    /**
     * @var date $datedemande
     *
     * @ORM\Column(name="datedemande", type="date")
     */
    private $datedemande;

    /**
     * @var string $prod01
     *
     * @ORM\Column(name="prod01", type="string", length=255)
     */
    private $prod01;

    /**
     * @var integer $prod01qte
     *
     * @ORM\Column(name="prod01qte", type="integer", nullable=true)
     */
    private $prod01qte;

    /**
     * @var string $prod02
     *
     * @ORM\Column(name="prod02", type="string", length=255, nullable=true)
     */
    private $prod02;

    /**
     * @var integer $prod02qte
     *
     * @ORM\Column(name="prod02qte", type="integer", nullable=true)
     */
    private $prod02qte;

    /**
     * @var string $prod03
     *
     * @ORM\Column(name="prod03", type="string", length=255, nullable=true)
     */
    private $prod03;

    /**
     * @var integer $prod03qte
     *
     * @ORM\Column(name="prod03qte", type="integer", nullable=true)
     */
    private $prod03qte;

    /**
     * @var string $prod04
     *
     * @ORM\Column(name="prod04", type="string", length=255, nullable=true)
     */
    private $prod04;

    /**
     * @var integer $prod04qte
     *
     * @ORM\Column(name="prod04qte", type="integer", nullable=true)
     */
    private $prod04qte;

    /**
     * @var string $prod05
     *
     * @ORM\Column(name="prod05", type="string", length=255, nullable=true)
     */
    private $prod05;

    /**
     * @var integer $prod05qte
     *
     * @ORM\Column(name="prod05qte", type="integer", nullable=true)
     */
    private $prod05qte;

    /**
     * @var string $mat01
     *
     * @ORM\Column(name="mat01", type="string", length=255, nullable=true)
     */
    private $mat01;

    /**
     * @var integer $mat01qte
     *
     * @ORM\Column(name="mat01qte", type="integer", nullable=true)
     */
    private $mat01qte;

    /**
     * @var string $mat02
     *
     * @ORM\Column(name="mat02", type="string", length=255, nullable=true)
     */
    private $mat02;

    /**
     * @var integer $mat02qte
     *
     * @ORM\Column(name="mat02qte", type="integer", nullable=true)
     */
    private $mat02qte;

    /**
     * @var string $mat03
     *
     * @ORM\Column(name="mat03", type="string", length=255, nullable=true)
     */
    private $mat03;

    /**
     * @var integer $mat03qte
     *
     * @ORM\Column(name="mat03qte", type="integer", nullable=true)
     */
    private $mat03qte;

    /**
     * @var string $mat04
     *
     * @ORM\Column(name="mat04", type="string", length=255, nullable=true)
     */
    private $mat04;

    /**
     * @var integer $mat04qte
     *
     * @ORM\Column(name="mat04qte", type="integer", nullable=true)
     */
    private $mat04qte;

    /**
     * @var string $mat05
     *
     * @ORM\Column(name="mat05", type="string", length=255, nullable=true)
     */
    private $mat05;

    /**
     * @var integer $mat05qte
     *
     * @ORM\Column(name="mat05qte", type="integer", nullable=true)
     */
    private $mat05qte;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set site
     *
     * @param string $site
     */
    public function setSite($site)
    {
        $this->site = $site;
    }

    /**
     * Get site
     *
     * @return string 
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set nomrespsect
     *
     * @param string $nomrespsect
     */
    public function setNomrespsect($nomrespsect)
    {
        $this->nomrespsect = $nomrespsect;
    }

    /**
     * Get nomrespsect
     *
     * @return string 
     */
    public function getNomrespsect()
    {
        return $this->nomrespsect;
    }

    /**
     * Set datedemande
     *
     * @param date $datedemande
     */
    public function setDatedemande($datedemande)
    {
        $this->datedemande = $datedemande;
    }

    /**
     * Get datedemande
     *
     * @return date 
     */
    public function getDatedemande()
    {
        return $this->datedemande;
    }

    /**
     * Set prod01
     *
     * @param string $prod01
     */
    public function setProd01($prod01)
    {
        $this->prod01 = $prod01;
    }

    /**
     * Get prod01
     *
     * @return string 
     */
    public function getProd01()
    {
        return $this->prod01;
    }

    /**
     * Set prod01qte
     *
     * @param integer $prod01qte
     */
    public function setProd01qte($prod01qte)
    {
        $this->prod01qte = $prod01qte;
    }

    /**
     * Get prod01qte
     *
     * @return integer 
     */
    public function getProd01qte()
    {
        return $this->prod01qte;
    }

    /**
     * Set prod02
     *
     * @param string $prod02
     */
    public function setProd02($prod02)
    {
        $this->prod02 = $prod02;
    }

    /**
     * Get prod02
     *
     * @return string 
     */
    public function getProd02()
    {
        return $this->prod02;
    }

    /**
     * Set prod02qte
     *
     * @param integer $prod02qte
     */
    public function setProd02qte($prod02qte)
    {
        $this->prod02qte = $prod02qte;
    }

    /**
     * Get prod02qte
     *
     * @return integer 
     */
    public function getProd02qte()
    {
        return $this->prod02qte;
    }

    /**
     * Set prod03
     *
     * @param string $prod03
     */
    public function setProd03($prod03)
    {
        $this->prod03 = $prod03;
    }

    /**
     * Get prod03
     *
     * @return string 
     */
    public function getProd03()
    {
        return $this->prod03;
    }

    /**
     * Set prod03qte
     *
     * @param integer $prod03qte
     */
    public function setProd03qte($prod03qte)
    {
        $this->prod03qte = $prod03qte;
    }

    /**
     * Get prod03qte
     *
     * @return integer 
     */
    public function getProd03qte()
    {
        return $this->prod03qte;
    }

    /**
     * Set prod04
     *
     * @param string $prod04
     */
    public function setProd04($prod04)
    {
        $this->prod04 = $prod04;
    }

    /**
     * Get prod04
     *
     * @return string 
     */
    public function getProd04()
    {
        return $this->prod04;
    }

    /**
     * Set prod04qte
     *
     * @param integer $prod04qte
     */
    public function setProd04qte($prod04qte)
    {
        $this->prod04qte = $prod04qte;
    }

    /**
     * Get prod04qte
     *
     * @return integer 
     */
    public function getProd04qte()
    {
        return $this->prod04qte;
    }

    /**
     * Set prod05
     *
     * @param string $prod05
     */
    public function setProd05($prod05)
    {
        $this->prod05 = $prod05;
    }

    /**
     * Get prod05
     *
     * @return string 
     */
    public function getProd05()
    {
        return $this->prod05;
    }

    /**
     * Set prod05qte
     *
     * @param integer $prod05qte
     */
    public function setProd05qte($prod05qte)
    {
        $this->prod05qte = $prod05qte;
    }

    /**
     * Get prod05qte
     *
     * @return integer 
     */
    public function getProd05qte()
    {
        return $this->prod05qte;
    }

    /**
     * Set mat01
     *
     * @param string $mat01
     */
    public function setMat01($mat01)
    {
        $this->mat01 = $mat01;
    }

    /**
     * Get mat01
     *
     * @return string 
     */
    public function getMat01()
    {
        return $this->mat01;
    }

    /**
     * Set mat01qte
     *
     * @param integer $mat01qte
     */
    public function setMat01qte($mat01qte)
    {
        $this->mat01qte = $mat01qte;
    }

    /**
     * Get mat01qte
     *
     * @return integer 
     */
    public function getMat01qte()
    {
        return $this->mat01qte;
    }

    /**
     * Set mat02
     *
     * @param string $mat02
     */
    public function setMat02($mat02)
    {
        $this->mat02 = $mat02;
    }

    /**
     * Get mat02
     *
     * @return string 
     */
    public function getMat02()
    {
        return $this->mat02;
    }

    /**
     * Set mat02qte
     *
     * @param integer $mat02qte
     */
    public function setMat02qte($mat02qte)
    {
        $this->mat02qte = $mat02qte;
    }

    /**
     * Get mat02qte
     *
     * @return integer 
     */
    public function getMat02qte()
    {
        return $this->mat02qte;
    }

    /**
     * Set mat03
     *
     * @param string $mat03
     */
    public function setMat03($mat03)
    {
        $this->mat03 = $mat03;
    }

    /**
     * Get mat03
     *
     * @return string 
     */
    public function getMat03()
    {
        return $this->mat03;
    }

    /**
     * Set mat03qte
     *
     * @param integer $mat03qte
     */
    public function setMat03qte($mat03qte)
    {
        $this->mat03qte = $mat03qte;
    }

    /**
     * Get mat03qte
     *
     * @return integer 
     */
    public function getMat03qte()
    {
        return $this->mat03qte;
    }

    /**
     * Set mat04
     *
     * @param string $mat04
     */
    public function setMat04($mat04)
    {
        $this->mat04 = $mat04;
    }

    /**
     * Get mat04
     *
     * @return string 
     */
    public function getMat04()
    {
        return $this->mat04;
    }

    /**
     * Set mat04qte
     *
     * @param integer $mat04qte
     */
    public function setMat04qte($mat04qte)
    {
        $this->mat04qte = $mat04qte;
    }

    /**
     * Get mat04qte
     *
     * @return integer 
     */
    public function getMat04qte()
    {
        return $this->mat04qte;
    }

    /**
     * Set mat05
     *
     * @param string $mat05
     */
    public function setMat05($mat05)
    {
        $this->mat05 = $mat05;
    }

    /**
     * Get mat05
     *
     * @return string 
     */
    public function getMat05()
    {
        return $this->mat05;
    }

    /**
     * Set mat05qte
     *
     * @param integer $mat05qte
     */
    public function setMat05qte($mat05qte)
    {
        $this->mat05qte = $mat05qte;
    }

    /**
     * Get mat05qte
     *
     * @return integer 
     */
    public function getMat05qte()
    {
        return $this->mat05qte;
    }
	
	/**
     * Set lieux
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\lieux $lieux
     */
    public function setLieux(\Phareos\NomadeNetServiceBundle\Entity\lieux $lieux)
    {
        $this->lieux = $lieux;
    }
	
    /**
     * Get lieux
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\lieux 
     */
    public function getLieux()
    {
        return $this->lieux;
    }
	
	public function __construct()
	{
		$this->datedemande = new \DateTime('now');
	}
}