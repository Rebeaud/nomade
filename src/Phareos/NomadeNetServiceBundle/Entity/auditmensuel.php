<?php

namespace Phareos\NomadeNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Phareos\NomadeNetServiceBundle\Entity\auditmensuel
 *
 * @ORM\Table(name="nom_auditmensuel")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Phareos\NomadeNetServiceBundle\Entity\auditmensuelRepository")
 */
class auditmensuel
{
    
	/**
	* @ORM\ManyToOne(targetEntity="lieux", inversedBy="auditmensuellieux", cascade={"remove"})
	* @ORM\JoinColumn(name="lieux_id", referencedColumnName="id")
	*/
	protected $lieux;
	
	
	/**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $ptvente
     *
     * @ORM\Column(name="ptvente", type="string", length=255)
     */
    private $ptvente;

    /**
     * @var datetime $datereunion
     *
     * @ORM\Column(name="datereunion", type="datetime")
     */
    private $datereunion;

    /**
     * @var string $responsecteur
     *
     * @ORM\Column(name="responsecteur", type="string", length=255)
     */
    private $responsecteur;

    /**
     * @var string $directeur
     *
     * @ORM\Column(name="directeur", type="string", length=255, nullable=true)
     */
    private $directeur;

    /**
     * @var string $respqual
     *
     * @ORM\Column(name="respqual", type="string", length=255, nullable=true)
     */
    private $respqual;

    /**
     * @var string $autrpart
     *
     * @ORM\Column(name="autrpart", type="string", length=255, nullable=true)
     */
    private $autrpart;

    /**
     * @var boolean $pannemur
     *
     * @ORM\Column(name="pannemur", type="boolean", nullable=true)
     */
    private $pannemur;

    /**
     * @var boolean $chliason
     *
     * @ORM\Column(name="chliason", type="boolean", nullable=true)
     */
    private $chliason;

    /**
     * @var boolean $planmag
     *
     * @ORM\Column(name="planmag", type="boolean", nullable=true)
     */
    private $planmag;

    /**
     * @var boolean $chliasor
     *
     * @ORM\Column(name="chliasor", type="boolean", nullable=true)
     */
    private $chliasor;

    /**
     * @var boolean $fichprod
     *
     * @ORM\Column(name="fichprod", type="boolean", nullable=true)
     */
    private $fichprod;

    /**
     * @var boolean $recepagent
     *
     * @ORM\Column(name="recepagent", type="boolean", nullable=true)
     */
    private $recepagent;

    /**
     * @var boolean $tenutrav
     *
     * @ORM\Column(name="tenutrav", type="boolean", nullable=true)
     */
    private $tenutrav;

    /**
     * @var boolean $pointagev
     *
     * @ORM\Column(name="pointagev", type="boolean", nullable=true)
     */
    private $pointagev;

    /**
     * @var boolean $visitrs
     *
     * @ORM\Column(name="visitrs", type="boolean", nullable=true)
     */
    private $visitrs;

    /**
     * @var boolean $absence
     *
     * @ORM\Column(name="absence", type="boolean", nullable=true)
     */
    private $absence;
	
	/**
     * @var boolean $planrespect
     *
     * @ORM\Column(name="planrespect", type="boolean", nullable=true)
     */
    private $planrespect;
	
	/**
     * @var boolean $absrempl
     *
     * @ORM\Column(name="absrempl", type="boolean", nullable=true)
     */
    private $absrempl;

    /**
     * @var string $qualitserv
     *
     * @ORM\Column(name="qualitserv", type="string", length=255, nullable=true)
     */
    private $qualitserv;
	
	/**
     * @var string $qualitrotf
     *
     * @ORM\Column(name="qualitrotf", type="string", length=255, nullable=true)
     */
    private $qualitrotf;
	
	/**
     * @var string $qualitsol
     *
     * @ORM\Column(name="qualitsol", type="string", length=255, nullable=true)
     */
    private $qualitsol;

    /**
     * @var text $observqual
     *
     * @ORM\Column(name="observqual", type="text", nullable=true)
     */
    private $observqual;

    /**
     * @var boolean $corection
     *
     * @ORM\Column(name="corection", type="boolean", nullable=true)
     */
    private $corection;
	
	/**
     * @var boolean $corectionras
     *
     * @ORM\Column(name="corectionras", type="boolean", nullable=true)
     */
    private $corectionras;
	
	/**
     * @var boolean $dispodir
     *
     * @ORM\Column(name="dispodir", type="boolean", nullable=true)
     */
    private $dispodir;
	
	/**
     * @var text $motifindispo
     *
     * @ORM\Column(name="motifindispo", type="text", nullable=true)
     */
    private $motifindispo;
	
	/**
     * @var string $pointdirres
     *
     * @ORM\Column(name="pointdirres", type="string", length=255, nullable=true)
     */
    private $pointdirres;
	
	/**
     * @var string $pointagent
     *
     * @ORM\Column(name="pointagent", type="string", length=255, nullable=true)
     */
    private $pointagent;


    /**
     * @var string $evolprest
     *
     * @ORM\Column(name="evolprest", type="string", length=255, nullable=true)
     */
    private $evolprest;

    /**
     * @var text $observevol
     *
     * @ORM\Column(name="observevol", type="text", nullable=true)
     */
    private $observevol;

    /**
     * @var datetime $dateprochre
     *
     * @ORM\Column(name="dateprochre", type="datetime")
     */
    private $dateprochre;

    /**
     * @var string $filepdf
     *
     * @ORM\Column(name="filepdf", type="string", length=255, nullable=true)
     */
    private $filepdf;

	/**
     * @var string $nomsignaltea
     *
     * @ORM\Column(name="nomsignaltea", type="string", nullable=true)
     */
    private $nomsignaltea;
	
	/**
     * @var string $imgsignaltea
     *
     * @ORM\Column(name="imgsignaltea", type="string", nullable=true)
     */
    private $imgsignaltea;
	
	/**
     * @var string $nomsignclient
     *
     * @ORM\Column(name="nomsignclient", type="string", nullable=true)
     */
    private $nomsignclient;
	
	
	/**
     * @var string $imgsignclient
     *
     * @ORM\Column(name="imgsignclient", type="string", nullable=true)
     */
    private $imgsignclient;
	
	/**
     * @var string $foncsignaltea
     *
     * @ORM\Column(name="foncsignaltea", type="string", nullable=true)
     */
    private $foncsignaltea;
	
	/**
     * @var string $foncsignclient
     *
     * @ORM\Column(name="foncsignclient", type="string", nullable=true)
     */
    private $foncsignclient;
	
	/**
     * @var string $nummois
     *
     * @ORM\Column(name="nummois", type="string", length=255, nullable=true)
     */
    private $nummois;
	
	/**
     * @var string $fileimgpaper
     * @Assert\File( maxSize = "4M", mimeTypesMessage = "SVP Uploader un fichier img")
     * @ORM\Column(name="fileimgpaper", type="string", length=255, nullable=true)
     */
    private $fileimgpaper;
	

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ptvente
     *
     * @param string $ptvente
     */
    public function setPtvente($ptvente)
    {
        $this->ptvente = $ptvente;
    }

    /**
     * Get ptvente
     *
     * @return string 
     */
    public function getPtvente()
    {
        return $this->ptvente;
    }

    /**
     * Set datereunion
     *
     * @param datetime $datereunion
     */
    public function setDatereunion($datereunion)
    {
        $this->datereunion = $datereunion;
    }

    /**
     * Get datereunion
     *
     * @return datetime 
     */
    public function getDatereunion()
    {
        return $this->datereunion;
    }

    /**
     * Set responsecteur
     *
     * @param string $responsecteur
     */
    public function setResponsecteur($responsecteur)
    {
        $this->responsecteur = $responsecteur;
    }

    /**
     * Get responsecteur
     *
     * @return string 
     */
    public function getResponsecteur()
    {
        return $this->responsecteur;
    }

    /**
     * Set directeur
     *
     * @param string $directeur
     */
    public function setDirecteur($directeur)
    {
        $this->directeur = $directeur;
    }

    /**
     * Get directeur
     *
     * @return string 
     */
    public function getDirecteur()
    {
        return $this->directeur;
    }

    /**
     * Set respqual
     *
     * @param string $respqual
     */
    public function setRespqual($respqual)
    {
        $this->respqual = $respqual;
    }

    /**
     * Get respqual
     *
     * @return string 
     */
    public function getRespqual()
    {
        return $this->respqual;
    }

    /**
     * Set autrpart
     *
     * @param string $autrpart
     */
    public function setAutrpart($autrpart)
    {
        $this->autrpart = $autrpart;
    }

    /**
     * Get autrpart
     *
     * @return string 
     */
    public function getAutrpart()
    {
        return $this->autrpart;
    }

    /**
     * Set pannemur
     *
     * @param boolean $pannemur
     */
    public function setPannemur($pannemur)
    {
        $this->pannemur = $pannemur;
    }

    /**
     * Get pannemur
     *
     * @return boolean 
     */
    public function getPannemur()
    {
        return $this->pannemur;
    }

    /**
     * Set chliason
     *
     * @param boolean $chliason
     */
    public function setChliason($chliason)
    {
        $this->chliason = $chliason;
    }

    /**
     * Get chliason
     *
     * @return boolean 
     */
    public function getChliason()
    {
        return $this->chliason;
    }

    /**
     * Set planmag
     *
     * @param boolean $planmag
     */
    public function setPlanmag($planmag)
    {
        $this->planmag = $planmag;
    }

    /**
     * Get planmag
     *
     * @return boolean 
     */
    public function getPlanmag()
    {
        return $this->planmag;
    }

    /**
     * Set chliasor
     *
     * @param boolean $chliasor
     */
    public function setChliasor($chliasor)
    {
        $this->chliasor = $chliasor;
    }

    /**
     * Get chliasor
     *
     * @return boolean 
     */
    public function getChliasor()
    {
        return $this->chliasor;
    }

    /**
     * Set fichprod
     *
     * @param boolean $fichprod
     */
    public function setFichprod($fichprod)
    {
        $this->fichprod = $fichprod;
    }

    /**
     * Get fichprod
     *
     * @return boolean 
     */
    public function getFichprod()
    {
        return $this->fichprod;
    }

    /**
     * Set recepagent
     *
     * @param boolean $recepagent
     */
    public function setRecepagent($recepagent)
    {
        $this->recepagent = $recepagent;
    }

    /**
     * Get recepagent
     *
     * @return boolean 
     */
    public function getRecepagent()
    {
        return $this->recepagent;
    }

    /**
     * Set tenutrav
     *
     * @param boolean $tenutrav
     */
    public function setTenutrav($tenutrav)
    {
        $this->tenutrav = $tenutrav;
    }

    /**
     * Get tenutrav
     *
     * @return boolean 
     */
    public function getTenutrav()
    {
        return $this->tenutrav;
    }

    /**
     * Set pointagev
     *
     * @param boolean $pointagev
     */
    public function setPointagev($pointagev)
    {
        $this->pointagev = $pointagev;
    }

    /**
     * Get pointagev
     *
     * @return boolean 
     */
    public function getPointagev()
    {
        return $this->pointagev;
    }

    /**
     * Set visitrs
     *
     * @param boolean $visitrs
     */
    public function setVisitrs($visitrs)
    {
        $this->visitrs = $visitrs;
    }

    /**
     * Get visitrs
     *
     * @return boolean 
     */
    public function getVisitrs()
    {
        return $this->visitrs;
    }

    /**
     * Set absence
     *
     * @param boolean $absence
     */
    public function setAbsence($absence)
    {
        $this->absence = $absence;
    }

    /**
     * Get absence
     *
     * @return boolean 
     */
    public function getAbsence()
    {
        return $this->absence;
    }
	
	/**
     * Set planrespect
     *
     * @param boolean $planrespect
     */
    public function setPlanrespect($planrespect)
    {
        $this->planrespect = $planrespect;
    }

    /**
     * Get planrespect
     *
     * @return boolean 
     */
    public function getPlanrespect()
    {
        return $this->planrespect;
    }
	
	/**
     * Set absrempl
     *
     * @param boolean $absrempl
     */
    public function setAbsrempl($absrempl)
    {
        $this->absrempl = $absrempl;
    }

    /**
     * Get absrempl
     *
     * @return boolean 
     */
    public function getAbsrempl()
    {
        return $this->absrempl;
    }

    /**
     * Set qualitserv
     *
     * @param string $qualitserv
     */
    public function setQualitserv($qualitserv)
    {
        $this->qualitserv = $qualitserv;
    }

    /**
     * Get qualitserv
     *
     * @return string 
     */
    public function getQualitserv()
    {
        return $this->qualitserv;
    }
	
	/**
     * Set qualitrotf
     *
     * @param string $qualitrotf
     */
    public function setQualitrotf($qualitrotf)
    {
        $this->qualitrotf = $qualitrotf;
    }

    /**
     * Get qualitrotf
     *
     * @return string 
     */
    public function getQualitrotf()
    {
        return $this->qualitrotf;
    }
	
	/**
     * Set qualitsol
     *
     * @param string $qualitsol
     */
    public function setQualitsol($qualitsol)
    {
        $this->qualitsol = $qualitsol;
    }

    /**
     * Get qualitsol
     *
     * @return string 
     */
    public function getQualitsol()
    {
        return $this->qualitsol;
    }
	
	

    /**
     * Set observqual
     *
     * @param text $observqual
     */
    public function setObservqual($observqual)
    {
        $this->observqual = $observqual;
    }

    /**
     * Get observqual
     *
     * @return text 
     */
    public function getObservqual()
    {
        return $this->observqual;
    }

    /**
     * Set corection
     *
     * @param boolean $corection
     */
    public function setCorection($corection)
    {
        $this->corection = $corection;
    }

    /**
     * Get corection
     *
     * @return boolean 
     */
    public function getCorection()
    {
        return $this->corection;
    }
	
	/**
     * Set dispodir
     *
     * @param boolean $dispodir
     */
    public function setDispodir($dispodir)
    {
        $this->dispodir = $dispodir;
    }

    /**
     * Get dispodir
     *
     * @return boolean 
     */
    public function getDispodir()
    {
        return $this->dispodir;
    }
	
	/**
     * Set motifindispo
     *
     * @param text $motifindispo
     */
    public function setMotifindispo($motifindispo)
    {
        $this->motifindispo = $motifindispo;
    }

    /**
     * Get motifindispo
     *
     * @return text 
     */
    public function getMotifindispo()
    {
        return $this->motifindispo;
    }

    /**
     * Set evolprest
     *
     * @param string $evolprest
     */
    public function setEvolprest($evolprest)
    {
        $this->evolprest = $evolprest;
    }

    /**
     * Get evolprest
     *
     * @return string 
     */
    public function getEvolprest()
    {
        return $this->evolprest;
    }
	
	/**
     * Set pointdirres
     *
     * @param string $pointdirres
     */
    public function setPointdirres($pointdirres)
    {
        $this->pointdirres = $pointdirres;
    }

    /**
     * Get pointdirres
     *
     * @return string 
     */
    public function getPointdirres()
    {
        return $this->pointdirres;
    }
	
	/**
     * Set pointagent
     *
     * @param string $pointagent
     */
    public function setPointagent($pointagent)
    {
        $this->pointagent = $pointagent;
    }

    /**
     * Get pointagent
     *
     * @return string 
     */
    public function getPointagent()
    {
        return $this->pointagent;
    }

    /**
     * Set observevol
     *
     * @param text $observevol
     */
    public function setObservevol($observevol)
    {
        $this->observevol = $observevol;
    }

    /**
     * Get observevol
     *
     * @return text 
     */
    public function getObservevol()
    {
        return $this->observevol;
    }

    /**
     * Set dateprochre
     *
     * @param datetime $dateprochre
     */
    public function setDateprochre($dateprochre)
    {
        $this->dateprochre = $dateprochre;
    }

    /**
     * Get dateprochre
     *
     * @return datetime 
     */
    public function getDateprochre()
    {
        return $this->dateprochre;
    }

    /**
     * Set filepdf
     *
     * @param string $filepdf
     */
    public function setFilepdf($filepdf)
    {
        $this->filepdf = $filepdf;
    }

    /**
     * Get filepdf
     *
     * @return string 
     */
    public function getFilepdf()
    {
        return $this->filepdf;
    }
	
	/**
     * Set lieux
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\lieux $lieux
     */
    public function setLieux(\Phareos\NomadeNetServiceBundle\Entity\lieux $lieux)
    {
        $this->lieux = $lieux;
    }
	
    /**
     * Get lieux
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\lieux 
     */
    public function getLieux()
    {
        return $this->lieux;
    }
	
	/**
     * Set nomsignaltea
     *
     * @param string $nomsignaltea
     */
    public function setNomsignaltea($nomsignaltea)
    {
        $this->nomsignaltea = $nomsignaltea;
    }

    /**
     * Get nomsignaltea
     *
     * @return string 
     */
    public function getNomsignaltea()
    {
        return $this->nomsignaltea;
    }
	
	/**
     * Set nomsignclient
     *
     * @param string $nomsignclient
     */
    public function setNomsignclient($nomsignclient)
    {
        $this->nomsignclient = $nomsignclient;
    }

    /**
     * Get nomsignclient
     *
     * @return string 
     */
    public function getNomsignclient()
    {
        return $this->nomsignclient;
    }
	
	/**
     * Set imgsignaltea
     *
     * @param string $imgsignaltea
     */
    public function setImgsignaltea($imgsignaltea)
    {
        $this->imgsignaltea = $imgsignaltea;
    }

    /**
     * Get imgsignaltea
     *
     * @return string 
     */
    public function getImgsignaltea()
    {
        return $this->imgsignaltea;
    }
	
	/**
     * Set imgsignclient
     *
     * @param string $imgsignclient
     */
    public function setImgsignclient($imgsignclient)
    {
        $this->imgsignclient = $imgsignclient;
    }

    /**
     * Get imgsignclient
     *
     * @return string 
     */
    public function getImgsignclient()
    {
        return $this->imgsignclient;
    }
	
	/**
     * Set foncsignaltea
     *
     * @param string $foncsignaltea
     */
    public function setFoncsignaltea($foncsignaltea)
    {
        $this->foncsignaltea = $foncsignaltea;
    }

    /**
     * Get foncsignaltea
     *
     * @return string 
     */
    public function getFoncsignaltea()
    {
        return $this->foncsignaltea;
    }
	
	/**
     * Set foncsignclient
     *
     * @param string $foncsignclient
     */
    public function setFoncsignclient($foncsignclient)
    {
        $this->foncsignclient = $foncsignclient;
    }

    /**
     * Get foncsignclient
     *
     * @return string 
     */
    public function getFoncsignclient()
    {
        return $this->foncsignclient;
    }
	
	/**
     * Set nummois
     *
     * @param string $nummois
     */
    public function setNummois($nummois)
    {
        $this->nummois = $nummois;
    }

    /**
     * Get nummois
     *
     * @return string 
     */
    public function getNummois()
    {
        return $this->nummois;
    }
	
	public function __construct()
	{
		//$this->datereunion = new \DateTime('now');
		//$this->dateprochre = new \DateTime('now');
	}

    /**
     * Set corectionras
     *
     * @param boolean $corectionras
     */
    public function setCorectionras($corectionras)
    {
        $this->corectionras = $corectionras;
    }

    /**
     * Get corectionras
     *
     * @return boolean 
     */
    public function getCorectionras()
    {
        return $this->corectionras;
    }
	
	/**
     * Set fileimgpaper
     *
     * @param string $fileimgpaper
     */
    public function setFileimgpaper($fileimgpaper)
    {
        $this->fileimgpaper = $fileimgpaper;
    }

    /**
     * Get fileimgpaper
     *
     * @return string 
     */
    public function getFileimgpaper()
    {
        return $this->fileimgpaper;
    }
	
	public function getFullPdfPath() {
        return null === $this->fileimgpaper ? null : $this->getUploadRootDir(). $this->fileimgpaper;
    }
	
	protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return $this->getTmpUploadRootDir().$this->getId()."/";
    }
	
	protected function getTmpUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/upload/nomade/auditmensuel/paper/';
    }
	
	/**
     * @ORM\PrePersist()
	 * @ORM\PreUpdate()
     * 
     */
    public function uploadPdf() {
        // the file property can be empty if the field is not required
        if (null === $this->fileimgpaper) {
            return;
        }
        if(!$this->id){
            $this->fileimgpaper->move($this->getTmpUploadRootDir(), $this->fileimgpaper->getClientOriginalName());
        }else{
            $this->fileimgpaper->move($this->getUploadRootDir(), $this->fileimgpaper->getClientOriginalName());
        }
        $this->setFileimgpaper($this->fileimgpaper->getClientOriginalName());
    }
 
    /**
     * @ORM\PostPersist()
	 * 
     */
    public function movePdf()
    {
        if (null === $this->fileimgpaper) {
            return;
        }
        if(!is_dir($this->getUploadRootDir())){
            mkdir($this->getUploadRootDir());
        }
        copy($this->getTmpUploadRootDir().$this->fileimgpaper, $this->getFullPdfPath());
        unlink($this->getTmpUploadRootDir().$this->fileimgpaper);
    }
	
	/**
     * @ORM\PreRemove()
     */
    public function removePdf()
    {
        unlink($this->getFullPdfPath());
        rmdir($this->getUploadRootDir());
    }
}