<?php

namespace Phareos\NomadeNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\NomadeNetServiceBundle\Entity\audit
 *
 * @ORM\Table(name="nom_audit")
 * @ORM\Entity(repositoryClass="Phareos\NomadeNetServiceBundle\Entity\auditRepository")
 */
class audit
{
    
	/**
	* @ORM\ManyToOne(targetEntity="lieux", inversedBy="auditlieux")
	* @ORM\JoinColumn(name="lieux_id", referencedColumnName="id")
	*/
	protected $lieux;
	
	/**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var array $parvis_sol
     *
     * @ORM\Column(name="parvis_sol", type="array", length=20, nullable=true)
     */
    private $parvis_sol;

    /**
     * @var text $parvis_sol_com
     *
     * @ORM\Column(name="parvis_sol_com", type="text", nullable=true)
     */
    private $parvis_sol_com;

    /**
     * @var array $tapis_entree
     *
     * @ORM\Column(name="tapis_entree", type="array", length=20, nullable=true)
     */
    private $tapis_entree;

    /**
     * @var string $tapis_ent_com
     *
     * @ORM\Column(name="tapis_ent_com", type="string", length=255, nullable=true)
     */
    private $tapis_ent_com;

    /**
     * @var array $portes_vit
     *
     * @ORM\Column(name="portes_vit", type="array", length=20, nullable=true)
     */
    private $portes_vit;

    /**
     * @var string $portes_vit_com
     *
     * @ORM\Column(name="portes_vit_com", type="string", length=255, nullable=true)
     */
    private $portes_vit_com;

    /**
     * @var array $bornes_antivol
     *
     * @ORM\Column(name="bornes_antivol", type="array", length=20, nullable=true)
     */
    private $bornes_antivol;

    /**
     * @var string $bornes_ant_com
     *
     * @ORM\Column(name="bornes_ant_com", type="string", length=255, nullable=true)
     */
    private $bornes_ant_com;

    /**
     * @var array $miroir_vit
     *
     * @ORM\Column(name="miroir_vit", type="array", length=20, nullable=true)
     */
    private $miroir_vit;

    /**
     * @var string $miroir_vit_com
     *
     * @ORM\Column(name="miroir_vit_com", type="string", length=255, nullable=true)
     */
    private $miroir_vit_com;

    /**
     * @var array $porte_mont_pil
     *
     * @ORM\Column(name="porte_mont_pil", type="array", length=20, nullable=true)
     */
    private $porte_mont_pil;

    /**
     * @var string $porte_mont_pilcom
     *
     * @ORM\Column(name="porte_mont_pilcom", type="string", length=255, nullable=true)
     */
    private $porte_mont_pilcom;

    /**
     * @var array $caisses
     *
     * @ORM\Column(name="caisses", type="array", length=20, nullable=true)
     */
    private $caisses;

    /**
     * @var string $caisses_com
     *
     * @ORM\Column(name="caisses_com", type="string", length=255, nullable=true)
     */
    private $caisses_com;

    /**
     * @var array $dess_fac_tiroir
     *
     * @ORM\Column(name="dess_fac_tiroir", type="array", length=20, nullable=true)
     */
    private $dess_fac_tiroir;

    /**
     * @var string $de_fac_tir_com
     *
     * @ORM\Column(name="de_fac_tir_com", type="string", length=255, nullable=true)
     */
    private $de_fac_tir_com;

    /**
     * @var array $maqu_rap_pinc
     *
     * @ORM\Column(name="maqu_rap_pinc", type="array", length=20, nullable=true)
     */
    private $maqu_rap_pinc;

    /**
     * @var string $ma_ra_pi_com
     *
     * @ORM\Column(name="ma_ra_pi_com", type="string", length=255, nullable=true)
     */
    private $ma_ra_pi_com;

    /**
     * @var array $present_maqu
     *
     * @ORM\Column(name="present_maqu", type="array", length=20, nullable=true)
     */
    private $present_maqu;

    /**
     * @var string $pres_maq_com
     *
     * @ORM\Column(name="pres_maq_com", type="string", length=255, nullable=true)
     */
    private $pres_maq_com;

    /**
     * @var array $edg
     *
     * @ORM\Column(name="edg", type="array", length=20, nullable=true)
     */
    private $edg;

    /**
     * @var string $edg_com
     *
     * @ORM\Column(name="edg_com", type="string", length=255, nullable=true)
     */
    private $edg_com;

    /**
     * @var array $mobilier_mobani
     *
     * @ORM\Column(name="mobilier_mobani", type="array", length=20, nullable=true)
     */
    private $mobilier_mobani;

    /**
     * @var string $mob_ani_com
     *
     * @ORM\Column(name="mob_ani_com", type="string", length=255, nullable=true)
     */
    private $mob_ani_com;

    /**
     * @var array $rehau_joue_gond
     *
     * @ORM\Column(name="rehau_joue_gond", type="array", length=20, nullable=true)
     */
    private $rehau_joue_gond;

    /**
     * @var string $reh_jo_go_com
     *
     * @ORM\Column(name="reh_jo_go_com", type="string", length=255, nullable=true)
     */
    private $reh_jo_go_com;

    /**
     * @var array $bar_beaut_nail
     *
     * @ORM\Column(name="bar_beaut_nail", type="array", length=20, nullable=true)
     */
    private $bar_beaut_nail;

    /**
     * @var string $bar_beaut_com
     *
     * @ORM\Column(name="bar_beaut_com", type="string", length=255, nullable=true)
     */
    private $bar_beaut_com;

    /**
     * @var array $tab_bain
     *
     * @ORM\Column(name="tab_bain", type="array", length=20, nullable=true)
     */
    private $tab_bain;

    /**
     * @var string $tab_bain_com
     *
     * @ORM\Column(name="tab_bain_com", type="string", length=255, nullable=true)
     */
    private $tab_bain_com;

    /**
     * @var array $sol_surf_vente
     *
     * @ORM\Column(name="sol_surf_vente", type="array", length=20, nullable=true)
     */
    private $sol_surf_vente;

    /**
     * @var text $sol_surf_v_com
     *
     * @ORM\Column(name="sol_surf_v_com", type="text", nullable=true)
     */
    private $sol_surf_v_com;

    /**
     * @var array $bur_plat_tab_ecr
     *
     * @ORM\Column(name="bur_plat_tab_ecr", type="array", length=20, nullable=true)
     */
    private $bur_plat_tab_ecr;

    /**
     * @var string $bur_p_t_com
     *
     * @ORM\Column(name="bur_p_t_com", type="string", length=255, nullable=true)
     */
    private $bur_p_t_com;

    /**
     * @var array $corbeilles
     *
     * @ORM\Column(name="corbeilles", type="array", length=20, nullable=true)
     */
    private $corbeilles;

    /**
     * @var string $corb_com
     *
     * @ORM\Column(name="corb_com", type="string", length=255, nullable=true)
     */
    private $corb_com;

    /**
     * @var array $wc_lav_corb
     *
     * @ORM\Column(name="wc_lav_corb", type="array", length=20, nullable=true)
     */
    private $wc_lav_corb;

    /**
     * @var string $wc_la_com
     *
     * @ORM\Column(name="wc_la_com", type="string", length=255, nullable=true)
     */
    private $wc_la_com;

    /**
     * @var array $miroirs_sanit
     *
     * @ORM\Column(name="miroirs_sanit", type="array", length=20, nullable=true)
     */
    private $miroirs_sanit;

    /**
     * @var string $mir_sa_com
     *
     * @ORM\Column(name="mir_sa_com", type="string", length=255, nullable=true)
     */
    private $mir_sa_com;

    /**
     * @var array $cuisine_siege
     *
     * @ORM\Column(name="cuisine_siege", type="array", length=20, nullable=true)
     */
    private $cuisine_siege;

    /**
     * @var string $cuis_sieg_com
     *
     * @ORM\Column(name="cuis_sieg_com", type="string", length=255, nullable=true)
     */
    private $cuis_sieg_com;

    /**
     * @var array $evier_robin
     *
     * @ORM\Column(name="evier_robin", type="array", length=20, nullable=true)
     */
    private $evier_robin;

    /**
     * @var string $evier_ro_com
     *
     * @ORM\Column(name="evier_ro_com", type="string", length=255, nullable=true)
     */
    private $evier_ro_com;

    /**
     * @var array $sol_loc_soc
     *
     * @ORM\Column(name="sol_loc_soc", type="array", length=20, nullable=true)
     */
    private $sol_loc_soc;

    /**
     * @var string $sol_loc_so_com
     *
     * @ORM\Column(name="sol_loc_so_com", type="string", length=255, nullable=true)
     */
    private $sol_loc_so_com;

    /**
     * @var array $sol_meca_reno
     *
     * @ORM\Column(name="sol_meca_reno", type="array", length=20, nullable=true)
     */
    private $sol_meca_reno;

    /**
     * @var text $sol_mec_re_com
     *
     * @ORM\Column(name="sol_mec_re_com", type="text", nullable=true)
     */
    private $sol_mec_re_com;

    /**
     * @var array $vitrerie
     *
     * @ORM\Column(name="vitrerie", type="array", length=20, nullable=true)
     */
    private $vitrerie;

    /**
     * @var string $vitreri_com
     *
     * @ORM\Column(name="vitreri_com", type="string", length=255, nullable=true)
     */
    private $vitreri_com;

    /**
     * @var array $caiss_nail_prof
     *
     * @ORM\Column(name="caiss_nail_prof", type="array", length=20, nullable=true)
     */
    private $caiss_nail_prof;

    /**
     * @var array $tour_ver_bout
     *
     * @ORM\Column(name="tour_ver_bout", type="array", length=20, nullable=true)
     */
    private $tour_ver_bout;

    /**
     * @var string $tour_ver_com
     *
     * @ORM\Column(name="tour_ver_com", type="string", length=255, nullable=true)
     */
    private $tour_ver_com;

    /**
     * @var string $caiss_nail_com
     *
     * @ORM\Column(name="caiss_nail_com", type="string", length=255, nullable=true)
     */
    private $caiss_nail_com;

    /**
     * @var array $line_mur
     *
     * @ORM\Column(name="line_mur", type="array", length=20, nullable=true)
     */
    private $line_mur;

    /**
     * @var string $line_mur_com
     *
     * @ORM\Column(name="line_mur_com", type="string", length=255, nullable=true)
     */
    private $line_mur_com;

    /**
     * @var array $gondole_cent
     *
     * @ORM\Column(name="gondole_cent", type="array", length=20, nullable=true)
     */
    private $gondole_cent;

    /**
     * @var text $gondole_cent_com
     *
     * @ORM\Column(name="gondole_cent_com", type="text", length=255, nullable=true)
     */
    private $gondole_cent_com;

    /**
     * @var boolean $trait_prio
     *
     * @ORM\Column(name="trait_prio", type="boolean", nullable=true)
     */
    private $trait_prio;

    /**
     * @var boolean $poin_mag
     *
     * @ORM\Column(name="poin_mag", type="boolean", nullable=true)
     */
    private $poin_mag;

    /**
     * @var boolean $reapro
     *
     * @ORM\Column(name="reapro", type="boolean", nullable=true)
     */
    private $reapro;

    /**
     * @var boolean $form_po_agen
     *
     * @ORM\Column(name="form_po_agen", type="boolean", nullable=true)
     */
    private $form_po_agen;

    
	
	/**
     * @var date $date_maj
     *
     * @ORM\Column(name="date_maj", type="date")
     */
    private $date_maj;
	
	/**
     * @var time $heure_maj
     *
     * @ORM\Column(name="heure_maj", type="time", nullable=true)
     */
    private $heure_maj;
	
	/**
     * @var string $nomsignaltea
     *
     * @ORM\Column(name="nomsignaltea", type="string", nullable=true)
     */
    private $nomsignaltea;
	
	/**
     * @var string $imgsignaltea
     *
     * @ORM\Column(name="imgsignaltea", type="string", nullable=true)
     */
    private $imgsignaltea;
	
	/**
     * @var string $nomsignclient
     *
     * @ORM\Column(name="nomsignclient", type="string", nullable=true)
     */
    private $nomsignclient;
	
	
	/**
     * @var string $imgsignclient
     *
     * @ORM\Column(name="imgsignclient", type="string", nullable=true)
     */
    private $imgsignclient;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set parvis_sol
     *
     * @param array $parvisSol
     */
    public function setParvisSol($parvisSol)
    {
        $this->parvis_sol = $parvisSol;
    }

    /**
     * Get parvis_sol
     *
     * @return array 
     */
    public function getParvisSol()
    {
        return $this->parvis_sol;
    }

    /**
     * Set parvis_sol_com
     *
     * @param string $parvisSolCom
     */
    public function setParvisSolCom($parvisSolCom)
    {
        $this->parvis_sol_com = $parvisSolCom;
    }

    /**
     * Get parvis_sol_com
     *
     * @return string 
     */
    public function getParvisSolCom()
    {
        return $this->parvis_sol_com;
    }

    /**
     * Set tapis_entree
     *
     * @param array $tapisEntree
     */
    public function setTapisEntree($tapisEntree)
    {
        $this->tapis_entree = $tapisEntree;
    }

    /**
     * Get tapis_entree
     *
     * @return array 
     */
    public function getTapisEntree()
    {
        return $this->tapis_entree;
    }

    /**
     * Set tapis_ent_com
     *
     * @param string $tapisEntCom
     */
    public function setTapisEntCom($tapisEntCom)
    {
        $this->tapis_ent_com = $tapisEntCom;
    }

    /**
     * Get tapis_ent_com
     *
     * @return string 
     */
    public function getTapisEntCom()
    {
        return $this->tapis_ent_com;
    }

    /**
     * Set portes_vit
     *
     * @param array $portesVit
     */
    public function setPortesVit($portesVit)
    {
        $this->portes_vit = $portesVit;
    }

    /**
     * Get portes_vit
     *
     * @return array 
     */
    public function getPortesVit()
    {
        return $this->portes_vit;
    }

    /**
     * Set portes_vit_com
     *
     * @param string $portesVitCom
     */
    public function setPortesVitCom($portesVitCom)
    {
        $this->portes_vit_com = $portesVitCom;
    }

    /**
     * Get portes_vit_com
     *
     * @return string 
     */
    public function getPortesVitCom()
    {
        return $this->portes_vit_com;
    }

    /**
     * Set bornes_antivol
     *
     * @param array $bornesAntivol
     */
    public function setBornesAntivol($bornesAntivol)
    {
        $this->bornes_antivol = $bornesAntivol;
    }

    /**
     * Get bornes_antivol
     *
     * @return array 
     */
    public function getBornesAntivol()
    {
        return $this->bornes_antivol;
    }

    /**
     * Set bornes_ant_com
     *
     * @param string $bornesAntCom
     */
    public function setBornesAntCom($bornesAntCom)
    {
        $this->bornes_ant_com = $bornesAntCom;
    }

    /**
     * Get bornes_ant_com
     *
     * @return string 
     */
    public function getBornesAntCom()
    {
        return $this->bornes_ant_com;
    }

    /**
     * Set miroir_vit
     *
     * @param array $miroirVit
     */
    public function setMiroirVit($miroirVit)
    {
        $this->miroir_vit = $miroirVit;
    }

    /**
     * Get miroir_vit
     *
     * @return array 
     */
    public function getMiroirVit()
    {
        return $this->miroir_vit;
    }

    /**
     * Set miroir_vit_com
     *
     * @param string $miroirVitCom
     */
    public function setMiroirVitCom($miroirVitCom)
    {
        $this->miroir_vit_com = $miroirVitCom;
    }

    /**
     * Get miroir_vit_com
     *
     * @return string 
     */
    public function getMiroirVitCom()
    {
        return $this->miroir_vit_com;
    }

    /**
     * Set porte_mont_pil
     *
     * @param array $porteMontPil
     */
    public function setPorteMontPil($porteMontPil)
    {
        $this->porte_mont_pil = $porteMontPil;
    }

    /**
     * Get porte_mont_pil
     *
     * @return array 
     */
    public function getPorteMontPil()
    {
        return $this->porte_mont_pil;
    }

    /**
     * Set porte_mont_pilcom
     *
     * @param string $porteMontPilcom
     */
    public function setPorteMontPilcom($porteMontPilcom)
    {
        $this->porte_mont_pilcom = $porteMontPilcom;
    }

    /**
     * Get porte_mont_pilcom
     *
     * @return string 
     */
    public function getPorteMontPilcom()
    {
        return $this->porte_mont_pilcom;
    }

    /**
     * Set caisses
     *
     * @param array $caisses
     */
    public function setCaisses($caisses)
    {
        $this->caisses = $caisses;
    }

    /**
     * Get caisses
     *
     * @return array 
     */
    public function getCaisses()
    {
        return $this->caisses;
    }

    /**
     * Set caisses_com
     *
     * @param string $caissesCom
     */
    public function setCaissesCom($caissesCom)
    {
        $this->caisses_com = $caissesCom;
    }

    /**
     * Get caisses_com
     *
     * @return string 
     */
    public function getCaissesCom()
    {
        return $this->caisses_com;
    }

    /**
     * Set dess_fac_tiroir
     *
     * @param array $dessFacTiroir
     */
    public function setDessFacTiroir($dessFacTiroir)
    {
        $this->dess_fac_tiroir = $dessFacTiroir;
    }

    /**
     * Get dess_fac_tiroir
     *
     * @return array 
     */
    public function getDessFacTiroir()
    {
        return $this->dess_fac_tiroir;
    }

    /**
     * Set de_fac_tir_com
     *
     * @param string $deFacTirCom
     */
    public function setDeFacTirCom($deFacTirCom)
    {
        $this->de_fac_tir_com = $deFacTirCom;
    }

    /**
     * Get de_fac_tir_com
     *
     * @return string 
     */
    public function getDeFacTirCom()
    {
        return $this->de_fac_tir_com;
    }

    /**
     * Set maqu_rap_pinc
     *
     * @param array $maquRapPinc
     */
    public function setMaquRapPinc($maquRapPinc)
    {
        $this->maqu_rap_pinc = $maquRapPinc;
    }

    /**
     * Get maqu_rap_pinc
     *
     * @return array 
     */
    public function getMaquRapPinc()
    {
        return $this->maqu_rap_pinc;
    }

    /**
     * Set ma_ra_pi_com
     *
     * @param string $maRaPiCom
     */
    public function setMaRaPiCom($maRaPiCom)
    {
        $this->ma_ra_pi_com = $maRaPiCom;
    }

    /**
     * Get ma_ra_pi_com
     *
     * @return string 
     */
    public function getMaRaPiCom()
    {
        return $this->ma_ra_pi_com;
    }

    /**
     * Set present_maqu
     *
     * @param array $presentMaqu
     */
    public function setPresentMaqu($presentMaqu)
    {
        $this->present_maqu = $presentMaqu;
    }

    /**
     * Get present_maqu
     *
     * @return array 
     */
    public function getPresentMaqu()
    {
        return $this->present_maqu;
    }

    /**
     * Set pres_maq_com
     *
     * @param string $presMaqCom
     */
    public function setPresMaqCom($presMaqCom)
    {
        $this->pres_maq_com = $presMaqCom;
    }

    /**
     * Get pres_maq_com
     *
     * @return string 
     */
    public function getPresMaqCom()
    {
        return $this->pres_maq_com;
    }

    /**
     * Set edg
     *
     * @param array $edg
     */
    public function setEdg($edg)
    {
        $this->edg = $edg;
    }

    /**
     * Get edg
     *
     * @return array 
     */
    public function getEdg()
    {
        return $this->edg;
    }

    /**
     * Set edg_com
     *
     * @param string $edgCom
     */
    public function setEdgCom($edgCom)
    {
        $this->edg_com = $edgCom;
    }

    /**
     * Get edg_com
     *
     * @return string 
     */
    public function getEdgCom()
    {
        return $this->edg_com;
    }

    /**
     * Set mobilier_mobani
     *
     * @param array $mobilierMobani
     */
    public function setMobilierMobani($mobilierMobani)
    {
        $this->mobilier_mobani = $mobilierMobani;
    }

    /**
     * Get mobilier_mobani
     *
     * @return array 
     */
    public function getMobilierMobani()
    {
        return $this->mobilier_mobani;
    }

    /**
     * Set mob_ani_com
     *
     * @param string $mobAniCom
     */
    public function setMobAniCom($mobAniCom)
    {
        $this->mob_ani_com = $mobAniCom;
    }

    /**
     * Get mob_ani_com
     *
     * @return string 
     */
    public function getMobAniCom()
    {
        return $this->mob_ani_com;
    }

    /**
     * Set rehau_joue_gond
     *
     * @param array $rehauJoueGond
     */
    public function setRehauJoueGond($rehauJoueGond)
    {
        $this->rehau_joue_gond = $rehauJoueGond;
    }

    /**
     * Get rehau_joue_gond
     *
     * @return array 
     */
    public function getRehauJoueGond()
    {
        return $this->rehau_joue_gond;
    }

    /**
     * Set reh_jo_go_com
     *
     * @param string $rehJoGoCom
     */
    public function setRehJoGoCom($rehJoGoCom)
    {
        $this->reh_jo_go_com = $rehJoGoCom;
    }

    /**
     * Get reh_jo_go_com
     *
     * @return string 
     */
    public function getRehJoGoCom()
    {
        return $this->reh_jo_go_com;
    }

    /**
     * Set bar_beaut_nail
     *
     * @param array $barBeautNail
     */
    public function setBarBeautNail($barBeautNail)
    {
        $this->bar_beaut_nail = $barBeautNail;
    }

    /**
     * Get bar_beaut_nail
     *
     * @return array 
     */
    public function getBarBeautNail()
    {
        return $this->bar_beaut_nail;
    }

    /**
     * Set bar_beaut_com
     *
     * @param string $barBeautCom
     */
    public function setBarBeautCom($barBeautCom)
    {
        $this->bar_beaut_com = $barBeautCom;
    }

    /**
     * Get bar_beaut_com
     *
     * @return string 
     */
    public function getBarBeautCom()
    {
        return $this->bar_beaut_com;
    }

    /**
     * Set tab_bain
     *
     * @param array $tabBain
     */
    public function setTabBain($tabBain)
    {
        $this->tab_bain = $tabBain;
    }

    /**
     * Get tab_bain
     *
     * @return array 
     */
    public function getTabBain()
    {
        return $this->tab_bain;
    }

    /**
     * Set tab_bain_com
     *
     * @param string $tabBainCom
     */
    public function setTabBainCom($tabBainCom)
    {
        $this->tab_bain_com = $tabBainCom;
    }

    /**
     * Get tab_bain_com
     *
     * @return string 
     */
    public function getTabBainCom()
    {
        return $this->tab_bain_com;
    }

    /**
     * Set sol_surf_vente
     *
     * @param array $solSurfVente
     */
    public function setSolSurfVente($solSurfVente)
    {
        $this->sol_surf_vente = $solSurfVente;
    }

    /**
     * Get sol_surf_vente
     *
     * @return array 
     */
    public function getSolSurfVente()
    {
        return $this->sol_surf_vente;
    }

    /**
     * Set sol_surf_v_com
     *
     * @param string $solSurfVCom
     */
    public function setSolSurfVCom($solSurfVCom)
    {
        $this->sol_surf_v_com = $solSurfVCom;
    }

    /**
     * Get sol_surf_v_com
     *
     * @return string 
     */
    public function getSolSurfVCom()
    {
        return $this->sol_surf_v_com;
    }

    /**
     * Set bur_plat_tab_ecr
     *
     * @param array $burPlatTabEcr
     */
    public function setBurPlatTabEcr($burPlatTabEcr)
    {
        $this->bur_plat_tab_ecr = $burPlatTabEcr;
    }

    /**
     * Get bur_plat_tab_ecr
     *
     * @return array 
     */
    public function getBurPlatTabEcr()
    {
        return $this->bur_plat_tab_ecr;
    }

    /**
     * Set bur_p_t_com
     *
     * @param string $burPTCom
     */
    public function setBurPTCom($burPTCom)
    {
        $this->bur_p_t_com = $burPTCom;
    }

    /**
     * Get bur_p_t_com
     *
     * @return string 
     */
    public function getBurPTCom()
    {
        return $this->bur_p_t_com;
    }

    /**
     * Set corbeilles
     *
     * @param array $corbeilles
     */
    public function setCorbeilles($corbeilles)
    {
        $this->corbeilles = $corbeilles;
    }

    /**
     * Get corbeilles
     *
     * @return array 
     */
    public function getCorbeilles()
    {
        return $this->corbeilles;
    }

    /**
     * Set corb_com
     *
     * @param string $corbCom
     */
    public function setCorbCom($corbCom)
    {
        $this->corb_com = $corbCom;
    }

    /**
     * Get corb_com
     *
     * @return string 
     */
    public function getCorbCom()
    {
        return $this->corb_com;
    }

    /**
     * Set wc_lav_corb
     *
     * @param array $wcLavCorb
     */
    public function setWcLavCorb($wcLavCorb)
    {
        $this->wc_lav_corb = $wcLavCorb;
    }

    /**
     * Get wc_lav_corb
     *
     * @return array 
     */
    public function getWcLavCorb()
    {
        return $this->wc_lav_corb;
    }

    /**
     * Set wc_la_com
     *
     * @param string $wcLaCom
     */
    public function setWcLaCom($wcLaCom)
    {
        $this->wc_la_com = $wcLaCom;
    }

    /**
     * Get wc_la_com
     *
     * @return string 
     */
    public function getWcLaCom()
    {
        return $this->wc_la_com;
    }

    /**
     * Set miroirs_sanit
     *
     * @param array $miroirsSanit
     */
    public function setMiroirsSanit($miroirsSanit)
    {
        $this->miroirs_sanit = $miroirsSanit;
    }

    /**
     * Get miroirs_sanit
     *
     * @return array 
     */
    public function getMiroirsSanit()
    {
        return $this->miroirs_sanit;
    }

    /**
     * Set mir_sa_com
     *
     * @param string $mirSaCom
     */
    public function setMirSaCom($mirSaCom)
    {
        $this->mir_sa_com = $mirSaCom;
    }

    /**
     * Get mir_sa_com
     *
     * @return string 
     */
    public function getMirSaCom()
    {
        return $this->mir_sa_com;
    }

    /**
     * Set cuisine_siege
     *
     * @param array $cuisineSiege
     */
    public function setCuisineSiege($cuisineSiege)
    {
        $this->cuisine_siege = $cuisineSiege;
    }

    /**
     * Get cuisine_siege
     *
     * @return array 
     */
    public function getCuisineSiege()
    {
        return $this->cuisine_siege;
    }

    /**
     * Set cuis_sieg_com
     *
     * @param string $cuisSiegCom
     */
    public function setCuisSiegCom($cuisSiegCom)
    {
        $this->cuis_sieg_com = $cuisSiegCom;
    }

    /**
     * Get cuis_sieg_com
     *
     * @return string 
     */
    public function getCuisSiegCom()
    {
        return $this->cuis_sieg_com;
    }

    /**
     * Set evier_robin
     *
     * @param array $evierRobin
     */
    public function setEvierRobin($evierRobin)
    {
        $this->evier_robin = $evierRobin;
    }

    /**
     * Get evier_robin
     *
     * @return array 
     */
    public function getEvierRobin()
    {
        return $this->evier_robin;
    }

    /**
     * Set evier_ro_com
     *
     * @param string $evierRoCom
     */
    public function setEvierRoCom($evierRoCom)
    {
        $this->evier_ro_com = $evierRoCom;
    }

    /**
     * Get evier_ro_com
     *
     * @return string 
     */
    public function getEvierRoCom()
    {
        return $this->evier_ro_com;
    }

    /**
     * Set sol_loc_soc
     *
     * @param array $solLocSoc
     */
    public function setSolLocSoc($solLocSoc)
    {
        $this->sol_loc_soc = $solLocSoc;
    }

    /**
     * Get sol_loc_soc
     *
     * @return array 
     */
    public function getSolLocSoc()
    {
        return $this->sol_loc_soc;
    }

    /**
     * Set sol_loc_so_com
     *
     * @param string $solLocSoCom
     */
    public function setSolLocSoCom($solLocSoCom)
    {
        $this->sol_loc_so_com = $solLocSoCom;
    }

    /**
     * Get sol_loc_so_com
     *
     * @return string 
     */
    public function getSolLocSoCom()
    {
        return $this->sol_loc_so_com;
    }

    /**
     * Set sol_meca_reno
     *
     * @param array $solMecaReno
     */
    public function setSolMecaReno($solMecaReno)
    {
        $this->sol_meca_reno = $solMecaReno;
    }

    /**
     * Get sol_meca_reno
     *
     * @return array 
     */
    public function getSolMecaReno()
    {
        return $this->sol_meca_reno;
    }

    /**
     * Set sol_mec_re_com
     *
     * @param string $solMecReCom
     */
    public function setSolMecReCom($solMecReCom)
    {
        $this->sol_mec_re_com = $solMecReCom;
    }

    /**
     * Get sol_mec_re_com
     *
     * @return string 
     */
    public function getSolMecReCom()
    {
        return $this->sol_mec_re_com;
    }

    /**
     * Set vitrerie
     *
     * @param array $vitrerie
     */
    public function setVitrerie($vitrerie)
    {
        $this->vitrerie = $vitrerie;
    }

    /**
     * Get vitrerie
     *
     * @return array 
     */
    public function getVitrerie()
    {
        return $this->vitrerie;
    }

    /**
     * Set vitreri_com
     *
     * @param string $vitreriCom
     */
    public function setVitreriCom($vitreriCom)
    {
        $this->vitreri_com = $vitreriCom;
    }

    /**
     * Get vitreri_com
     *
     * @return string 
     */
    public function getVitreriCom()
    {
        return $this->vitreri_com;
    }

    /**
     * Set caiss_nail_prof
     *
     * @param array $caissNailProf
     */
    public function setCaissNailProf($caissNailProf)
    {
        $this->caiss_nail_prof = $caissNailProf;
    }

    /**
     * Get caiss_nail_prof
     *
     * @return array 
     */
    public function getCaissNailProf()
    {
        return $this->caiss_nail_prof;
    }

    /**
     * Set tour_ver_bout
     *
     * @param array $tourVerBout
     */
    public function setTourVerBout($tourVerBout)
    {
        $this->tour_ver_bout = $tourVerBout;
    }

    /**
     * Get tour_ver_bout
     *
     * @return array 
     */
    public function getTourVerBout()
    {
        return $this->tour_ver_bout;
    }

    /**
     * Set tour_ver_com
     *
     * @param string $tourVerCom
     */
    public function setTourVerCom($tourVerCom)
    {
        $this->tour_ver_com = $tourVerCom;
    }

    /**
     * Get tour_ver_com
     *
     * @return string 
     */
    public function getTourVerCom()
    {
        return $this->tour_ver_com;
    }

    /**
     * Set caiss_nail_com
     *
     * @param string $caissNailCom
     */
    public function setCaissNailCom($caissNailCom)
    {
        $this->caiss_nail_com = $caissNailCom;
    }

    /**
     * Get caiss_nail_com
     *
     * @return string 
     */
    public function getCaissNailCom()
    {
        return $this->caiss_nail_com;
    }

    /**
     * Set line_mur
     *
     * @param array $lineMur
     */
    public function setLineMur($lineMur)
    {
        $this->line_mur = $lineMur;
    }

    /**
     * Get line_mur
     *
     * @return array 
     */
    public function getLineMur()
    {
        return $this->line_mur;
    }

    /**
     * Set line_mur_com
     *
     * @param string $lineMurCom
     */
    public function setLineMurCom($lineMurCom)
    {
        $this->line_mur_com = $lineMurCom;
    }

    /**
     * Get line_mur_com
     *
     * @return string 
     */
    public function getLineMurCom()
    {
        return $this->line_mur_com;
    }

    /**
     * Set gondole_cent
     *
     * @param array $gondoleCent
     */
    public function setGondoleCent($gondoleCent)
    {
        $this->gondole_cent = $gondoleCent;
    }

    /**
     * Get gondole_cent
     *
     * @return array 
     */
    public function getGondoleCent()
    {
        return $this->gondole_cent;
    }

    /**
     * Set gondole_cent_com
     *
     * @param string $gondoleCentCom
     */
    public function setGondoleCentCom($gondoleCentCom)
    {
        $this->gondole_cent_com = $gondoleCentCom;
    }

    /**
     * Get gondole_cent_com
     *
     * @return string 
     */
    public function getGondoleCentCom()
    {
        return $this->gondole_cent_com;
    }

    /**
     * Set trait_prio
     *
     * @param string $traitPrio
     */
    public function setTraitPrio($traitPrio)
    {
        $this->trait_prio = $traitPrio;
    }

    /**
     * Get trait_prio
     *
     * @return string 
     */
    public function getTraitPrio()
    {
        return $this->trait_prio;
    }

    /**
     * Set poin_mag
     *
     * @param string $poinMag
     */
    public function setPoinMag($poinMag)
    {
        $this->poin_mag = $poinMag;
    }

    /**
     * Get poin_mag
     *
     * @return string 
     */
    public function getPoinMag()
    {
        return $this->poin_mag;
    }

    /**
     * Set reapro
     *
     * @param string $reapro
     */
    public function setReapro($reapro)
    {
        $this->reapro = $reapro;
    }

    /**
     * Get reapro
     *
     * @return string 
     */
    public function getReapro()
    {
        return $this->reapro;
    }

    /**
     * Set form_po_agen
     *
     * @param string $formPoAgen
     */
    public function setFormPoAgen($formPoAgen)
    {
        $this->form_po_agen = $formPoAgen;
    }

    /**
     * Get form_po_agen
     *
     * @return string 
     */
    public function getFormPoAgen()
    {
        return $this->form_po_agen;
    }

    
	
	/**
     * Set date_maj
     *
     * @param date $dateMaj
     */
    public function setDateMaj($dateMaj)
    {
        $this->date_maj = $dateMaj;
    }

    /**
     * Get date_maj
     *
     * @return date 
     */
    public function getDateMaj()
    {
        return $this->date_maj;
    }
	
	/**
     * Set heure_maj
     *
     * @param time $heureMaj
     */
    public function setHeureMaj($heureMaj)
    {
        $this->heure_maj = $heureMaj;
    }

    /**
     * Get heure_maj
     *
     * @return time 
     */
    public function getHeureMaj()
    {
        return $this->heure_maj;
    }

    /**
     * Set lieux
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\lieux $lieux
     */
    public function setLieux(\Phareos\NomadeNetServiceBundle\Entity\lieux $lieux)
    {
        $this->lieux = $lieux;
    }
	
    /**
     * Get lieux
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\lieux 
     */
    public function getLieux()
    {
        return $this->lieux;
    }
	
	/**
     * Set nomsignaltea
     *
     * @param string $nomsignaltea
     */
    public function setNomsignaltea($nomsignaltea)
    {
        $this->nomsignaltea = $nomsignaltea;
    }

    /**
     * Get nomsignaltea
     *
     * @return string 
     */
    public function getNomsignaltea()
    {
        return $this->nomsignaltea;
    }
	
	/**
     * Set nomsignclient
     *
     * @param string $nomsignclient
     */
    public function setNomsignclient($nomsignclient)
    {
        $this->nomsignclient = $nomsignclient;
    }

    /**
     * Get nomsignclient
     *
     * @return string 
     */
    public function getNomsignclient()
    {
        return $this->nomsignclient;
    }
	
	/**
     * Set imgsignaltea
     *
     * @param string $imgsignaltea
     */
    public function setImgsignaltea($imgsignaltea)
    {
        $this->imgsignaltea = $imgsignaltea;
    }

    /**
     * Get imgsignaltea
     *
     * @return string 
     */
    public function getImgsignaltea()
    {
        return $this->imgsignaltea;
    }
	
	/**
     * Set imgsignclient
     *
     * @param string $imgsignclient
     */
    public function setImgsignclient($imgsignclient)
    {
        $this->imgsignclient = $imgsignclient;
    }

    /**
     * Get imgsignclient
     *
     * @return string 
     */
    public function getImgsignclient()
    {
        return $this->imgsignclient;
    }
	
	public function __construct()
	{
		//$this->date_maj = new \DateTime('now');
		//$this->heure_maj = new \DateTime('now');
	}
}