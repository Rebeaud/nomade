<?php

namespace Phareos\NomadeNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\NomadeNetServiceBundle\Entity\testarray
 *
 * @ORM\Table(name="nom_testarray")
 * @ORM\Entity(repositoryClass="Phareos\NomadeNetServiceBundle\Entity\testarrayRepository")
 */
class testarray
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var array $arr1
     *
     * @ORM\Column(name="arr1", type="array")
     */
    private $arr1;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set arr1
     *
     * @param array $arr1
     */
    public function setArr1($arr1)
    {
        $this->arr1 = $arr1;
    }

    /**
     * Get arr1
     *
     * @return array 
     */
    public function getArr1()
    {
        return $this->arr1;
    }
}