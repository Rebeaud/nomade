<?php

namespace Phareos\NomadeNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\NomadeNetServiceBundle\Entity\agent
 *
 * @ORM\Table(name="nom_agent")
 * @ORM\Entity(repositoryClass="Phareos\NomadeNetServiceBundle\Entity\agentRepository")
 */
class agent
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
	 * @ORM\OneToMany(targetEntity="recadrage", mappedBy="agent", cascade={"remove", "persist"})
	 */
	protected $agentrecadre;
	
	/**
	 * @ORM\OneToMany(targetEntity="planingdate", mappedBy="agent", cascade={"remove", "persist"})
	 */
	protected $plandagent;

    /**
     * @var bigint $NUM_PERS
     *
     * @ORM\Column(name="NUM_PERS", type="bigint", nullable=true)
     */
    private $NUM_PERS;

    /**
     * @var string $CODE_VILLE
     *
     * @ORM\Column(name="CODE_VILLE", type="string", length=5, nullable=true)
     */
    private $CODE_VILLE;

    /**
     * @var string $SEXE
     *
     * @ORM\Column(name="SEXE", type="string", length=1, nullable=true)
     */
    private $SEXE;

    /**
     * @var string $SITUATION_FAMILLE
     *
     * @ORM\Column(name="SITUATION_FAMILLE", type="string", length=12, nullable=true)
     */
    private $SITUATION_FAMILLE;

    /**
     * @var string $NOM_JEUNE_FILLE
     *
     * @ORM\Column(name="NOM_JEUNE_FILLE", type="string", length=32, nullable=true)
     */
    private $NOM_JEUNE_FILLE;

    /**
     * @var date $DATE_NAISSANCE
     *
     * @ORM\Column(name="DATE_NAISSANCE", type="date", nullable=true)
     */
    private $DATE_NAISSANCE;

    /**
     * @var string $LIEU_NAISSANCE
     *
     * @ORM\Column(name="LIEU_NAISSANCE", type="string", length=32, nullable=true)
     */
    private $LIEU_NAISSANCE;

    /**
     * @var string $NUM_SS
     *
     * @ORM\Column(name="NUM_SS", type="string", length=15, nullable=true)
     */
    private $NUM_SS;

    /**
     * @var string $NATIONALITE
     *
     * @ORM\Column(name="NATIONALITE", type="string", length=32, nullable=true)
     */
    private $NATIONALITE;

    /**
     * @var string $NUM_CS
     *
     * @ORM\Column(name="NUM_CS", type="string", length=32, nullable=true)
     */
    private $NUM_CS;

    /**
     * @var date $VALIDITE_CS
     *
     * @ORM\Column(name="VALIDITE_CS", type="date", nullable=true)
     */
    private $VALIDITE_CS;

    /**
     * @var smallint $COPIE_CS
     *
     * @ORM\Column(name="COPIE_CS", type="smallint", nullable=true)
     */
    private $COPIE_CS;

    /**
     * @var text $NUM_ID
     *
     * @ORM\Column(name="NUM_ID", type="text", nullable=true)
     */
    private $NUM_ID;

    /**
     * @var date $DATE_ID
     *
     * @ORM\Column(name="DATE_ID", type="date", nullable=true)
     */
    private $DATE_ID;

    /**
     * @var smallint $ANNEXE7
     *
     * @ORM\Column(name="ANNEXE7", type="smallint", nullable=true)
     */
    private $ANNEXE7;

    /**
     * @var smallint $HANDICAPE
     *
     * @ORM\Column(name="HANDICAPE", type="smallint", nullable=true)
     */
    private $HANDICAPE;

    /**
     * @var date $DATE_F_HANDICAPE
     *
     * @ORM\Column(name="DATE_F_HANDICAPE", type="date", nullable=true)
     */
    private $DATE_F_HANDICAPE;

    /**
     * @var date $NB_AN_EXPERIENCE
     *
     * @ORM\Column(name="NB_AN_EXPERIENCE", type="date", nullable=true)
     */
    private $NB_AN_EXPERIENCE;

    /**
     * @var text $EXPERIENCE
     *
     * @ORM\Column(name="EXPERIENCE", type="text", nullable=true)
     */
    private $EXPERIENCE;

    /**
     * @var smallint $PERMIS
     *
     * @ORM\Column(name="PERMIS", type="smallint", nullable=true)
     */
    private $PERMIS;

    /**
     * @var text $COMMENTAIRE
     *
     * @ORM\Column(name="COMMENTAIRE", type="text", nullable=true)
     */
    private $COMMENTAIRE;

    /**
     * @var text $APPRECIATION
     *
     * @ORM\Column(name="APPRECIATION", type="text", nullable=true)
     */
    private $APPRECIATION;

    /**
     * @var smallint $VEHICULE
     *
     * @ORM\Column(name="VEHICULE", type="smallint", nullable=true)
     */
    private $VEHICULE;

    /**
     * @var smallint $COMPLEMENT_HRS
     *
     * @ORM\Column(name="COMPLEMENT_HRS", type="smallint", nullable=true)
     */
    private $COMPLEMENT_HRS;

    /**
     * @var smallint $VITRAGE
     *
     * @ORM\Column(name="VITRAGE", type="smallint", nullable=true)
     */
    private $VITRAGE;

    /**
     * @var string $NOM_PERS
     *
     * @ORM\Column(name="NOM_PERS", type="string", length=60, nullable=true)
     */
    private $NOM_PERS;

    /**
     * @var string $PRENOM_PERS
     *
     * @ORM\Column(name="PRENOM_PERS", type="string", length=50, nullable=true)
     */
    private $PRENOM_PERS;

    /**
     * @var string $TELFIX_PERS
     *
     * @ORM\Column(name="TELFIX_PERS", type="string", length=14, nullable=true)
     */
    private $TELFIX_PERS;

    /**
     * @var string $TELPOR_PERS
     *
     * @ORM\Column(name="TELPOR_PERS", type="string", length=14, nullable=true)
     */
    private $TELPOR_PERS;

    /**
     * @var string $EMAIL_PERS
     *
     * @ORM\Column(name="EMAIL_PERS", type="string", length=50, nullable=true)
     */
    private $EMAIL_PERS;

    /**
     * @var string $AD_RUE
     *
     * @ORM\Column(name="AD_RUE", type="string", length=50, nullable=true)
     */
    private $AD_RUE;

    /**
     * @var string $SUPPL_AD
     *
     * @ORM\Column(name="SUPPL_AD", type="string", length=50, nullable=true)
     */
    private $SUPPL_AD;

    /**
     * @var smallint $TRAVEX
     *
     * @ORM\Column(name="TRAVEX", type="smallint", nullable=true)
     */
    private $TRAVEX;

    /**
     * @var string $CODE_CLAS
     *
     * @ORM\Column(name="CODE_CLAS", type="string", length=4, nullable=true)
     */
    private $CODE_CLAS;

    /**
     * @var smallint $ECHELON
     *
     * @ORM\Column(name="ECHELON", type="smallint", nullable=true)
     */
    private $ECHELON;

    /**
     * @var text $POSITIONNEMENT
     *
     * @ORM\Column(name="POSITIONNEMENT", type="text", nullable=true)
     */
    private $POSITIONNEMENT;

    /**
     * @var date $DATE_CANDI
     *
     * @ORM\Column(name="DATE_CANDI", type="date", nullable=true)
     */
    private $DATE_CANDI;

    /**
     * @var text $MODE
     *
     * @ORM\Column(name="MODE", type="text", nullable=true)
     */
    private $MODE;

    /**
     * @var text $SOURCE
     *
     * @ORM\Column(name="SOURCE", type="text", nullable=true)
     */
    private $SOURCE;

    /**
     * @var text $APPGEN
     *
     * @ORM\Column(name="APPGEN", type="text", nullable=true)
     */
    private $APPGEN;

    /**
     * @var smallint $LETTRECV
     *
     * @ORM\Column(name="LETTRECV", type="smallint", nullable=true)
     */
    private $LETTRECV;

    /**
     * @var date $DATELET
     *
     * @ORM\Column(name="DATELET", type="date", nullable=true)
     */
    private $DATELET;

    /**
     * @var date $DATE_ENVOI_CONTRAT
     *
     * @ORM\Column(name="DATE_ENVOI_CONTRAT", type="date", nullable=true)
     */
    private $DATE_ENVOI_CONTRAT;

    /**
     * @var date $DATE_ENVOI_AVENANT
     *
     * @ORM\Column(name="DATE_ENVOI_AVENANT", type="date", nullable=true)
     */
    private $DATE_ENVOI_AVENANT;

    /**
     * @var date $DATE_ENVOI_CONTRATSIG
     *
     * @ORM\Column(name="DATE_ENVOI_CONTRATSIG", type="date", nullable=true)
     */
    private $DATE_ENVOI_CONTRATSIG;

    /**
     * @var smallint $ACTIF
     *
     * @ORM\Column(name="ACTIF", type="smallint", nullable=true)
     */
    private $ACTIF;

    /**
     * @var bigint $PERS
     *
     * @ORM\Column(name="PERS", type="bigint", nullable=true)
     */
    private $PERS;

    /**
     * @var smallint $RETRAITE
     *
     * @ORM\Column(name="RETRAITE", type="smallint", nullable=true)
     */
    private $RETRAITE;

    /**
     * @var smallint $SAMEDI
     *
     * @ORM\Column(name="SAMEDI", type="smallint", nullable=true)
     */
    private $SAMEDI;

    /**
     * @var smallint $RPLT
     *
     * @ORM\Column(name="RPLT", type="smallint", nullable=true)
     */
    private $RPLT;

    /**
     * @var smallint $FORMVIT
     *
     * @ORM\Column(name="FORMVIT", type="smallint", nullable=true)
     */
    private $FORMVIT;

    /**
     * @var smallint $DISPOJ
     *
     * @ORM\Column(name="DISPOJ", type="smallint", nullable=true)
     */
    private $DISPOJ;

    /**
     * @var smallint $TPSP
     *
     * @ORM\Column(name="TPSP", type="smallint", nullable=true)
     */
    private $TPSP;

    /**
     * @var smallint $NONINTER
     *
     * @ORM\Column(name="NONINTER", type="smallint", nullable=true)
     */
    private $NONINTER;

    /**
     * @var smallint $NUMNONATT
     *
     * @ORM\Column(name="NUMNONATT", type="smallint", nullable=true)
     */
    private $NUMNONATT;

    /**
     * @var date $TJSDISPO
     *
     * @ORM\Column(name="TJSDISPO", type="date", nullable=true)
     */
    private $TJSDISPO;

    /**
     * @var smallint $BURO
     *
     * @ORM\Column(name="BURO", type="smallint", nullable=true)
     */
    private $BURO;

    /**
     * @var smallint $MAG
     *
     * @ORM\Column(name="MAG", type="smallint", nullable=true)
     */
    private $MAG;

    /**
     * @var smallint $GDSURF
     *
     * @ORM\Column(name="GDSURF", type="smallint", nullable=true)
     */
    private $GDSURF;

    /**
     * @var smallint $HOP
     *
     * @ORM\Column(name="HOP", type="smallint", nullable=true)
     */
    private $HOP;

    /**
     * @var smallint $PART
     *
     * @ORM\Column(name="PART", type="smallint", nullable=true)
     */
    private $PART;

    /**
     * @var smallint $ECO
     *
     * @ORM\Column(name="ECO", type="smallint", nullable=true)
     */
    private $ECO;

    /**
     * @var smallint $HOT
     *
     * @ORM\Column(name="HOT", type="smallint", nullable=true)
     */
    private $HOT;

    /**
     * @var smallint $ESPV
     *
     * @ORM\Column(name="ESPV", type="smallint", nullable=true)
     */
    private $ESPV;

    /**
     * @var smallint $MAISRET
     *
     * @ORM\Column(name="MAISRET", type="smallint", nullable=true)
     */
    private $MAISRET;

    /**
     * @var smallint $RESTO
     *
     * @ORM\Column(name="RESTO", type="smallint", nullable=true)
     */
    private $RESTO;

    /**
     * @var smallint $REMISEETAT
     *
     * @ORM\Column(name="REMISEETAT", type="smallint", nullable=true)
     */
    private $REMISEETAT;

    /**
     * @var smallint $DECAPAGE
     *
     * @ORM\Column(name="DECAPAGE", type="smallint", nullable=true)
     */
    private $DECAPAGE;

    /**
     * @var smallint $PLUSCONTACT
     *
     * @ORM\Column(name="PLUSCONTACT", type="smallint", nullable=true)
     */
    private $PLUSCONTACT;

    /**
     * @var text $MOTIFPLUSCONTACT
     *
     * @ORM\Column(name="MOTIFPLUSCONTACT", type="text", nullable=true)
     */
    private $MOTIFPLUSCONTACT;

    /**
     * @var date $DATE_COPIE_CS
     *
     * @ORM\Column(name="DATE_COPIE_CS", type="date", nullable=true)
     */
    private $DATE_COPIE_CS;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set NUM_PERS
     *
     * @param bigint $nUMPERS
     */
    public function setNUMPERS($nUMPERS)
    {
        $this->NUM_PERS = $nUMPERS;
    }

    /**
     * Get NUM_PERS
     *
     * @return bigint 
     */
    public function getNUMPERS()
    {
        return $this->NUM_PERS;
    }

    /**
     * Set CODE_VILLE
     *
     * @param string $cODEVILLE
     */
    public function setCODEVILLE($cODEVILLE)
    {
        $this->CODE_VILLE = $cODEVILLE;
    }

    /**
     * Get CODE_VILLE
     *
     * @return string 
     */
    public function getCODEVILLE()
    {
        return $this->CODE_VILLE;
    }

    /**
     * Set SEXE
     *
     * @param string $sEXE
     */
    public function setSEXE($sEXE)
    {
        $this->SEXE = $sEXE;
    }

    /**
     * Get SEXE
     *
     * @return string 
     */
    public function getSEXE()
    {
        return $this->SEXE;
    }

    /**
     * Set SITUATION_FAMILLE
     *
     * @param string $sITUATIONFAMILLE
     */
    public function setSITUATIONFAMILLE($sITUATIONFAMILLE)
    {
        $this->SITUATION_FAMILLE = $sITUATIONFAMILLE;
    }

    /**
     * Get SITUATION_FAMILLE
     *
     * @return string 
     */
    public function getSITUATIONFAMILLE()
    {
        return $this->SITUATION_FAMILLE;
    }

    /**
     * Set NOM_JEUNE_FILLE
     *
     * @param string $nOMJEUNEFILLE
     */
    public function setNOMJEUNEFILLE($nOMJEUNEFILLE)
    {
        $this->NOM_JEUNE_FILLE = $nOMJEUNEFILLE;
    }

    /**
     * Get NOM_JEUNE_FILLE
     *
     * @return string 
     */
    public function getNOMJEUNEFILLE()
    {
        return $this->NOM_JEUNE_FILLE;
    }

    /**
     * Set DATE_NAISSANCE
     *
     * @param date $dATENAISSANCE
     */
    public function setDATENAISSANCE($dATENAISSANCE)
    {
        $this->DATE_NAISSANCE = $dATENAISSANCE;
    }

    /**
     * Get DATE_NAISSANCE
     *
     * @return date 
     */
    public function getDATENAISSANCE()
    {
        return $this->DATE_NAISSANCE;
    }

    /**
     * Set LIEU_NAISSANCE
     *
     * @param string $lIEUNAISSANCE
     */
    public function setLIEUNAISSANCE($lIEUNAISSANCE)
    {
        $this->LIEU_NAISSANCE = $lIEUNAISSANCE;
    }

    /**
     * Get LIEU_NAISSANCE
     *
     * @return string 
     */
    public function getLIEUNAISSANCE()
    {
        return $this->LIEU_NAISSANCE;
    }

    /**
     * Set NUM_SS
     *
     * @param string $nUMSS
     */
    public function setNUMSS($nUMSS)
    {
        $this->NUM_SS = $nUMSS;
    }

    /**
     * Get NUM_SS
     *
     * @return string 
     */
    public function getNUMSS()
    {
        return $this->NUM_SS;
    }

    /**
     * Set NATIONALITE
     *
     * @param string $nATIONALITE
     */
    public function setNATIONALITE($nATIONALITE)
    {
        $this->NATIONALITE = $nATIONALITE;
    }

    /**
     * Get NATIONALITE
     *
     * @return string 
     */
    public function getNATIONALITE()
    {
        return $this->NATIONALITE;
    }

    /**
     * Set NUM_CS
     *
     * @param string $nUMCS
     */
    public function setNUMCS($nUMCS)
    {
        $this->NUM_CS = $nUMCS;
    }

    /**
     * Get NUM_CS
     *
     * @return string 
     */
    public function getNUMCS()
    {
        return $this->NUM_CS;
    }

    /**
     * Set VALIDITE_CS
     *
     * @param date $vALIDITECS
     */
    public function setVALIDITECS($vALIDITECS)
    {
        $this->VALIDITE_CS = $vALIDITECS;
    }

    /**
     * Get VALIDITE_CS
     *
     * @return date 
     */
    public function getVALIDITECS()
    {
        return $this->VALIDITE_CS;
    }

    /**
     * Set COPIE_CS
     *
     * @param smallint $cOPIECS
     */
    public function setCOPIECS($cOPIECS)
    {
        $this->COPIE_CS = $cOPIECS;
    }

    /**
     * Get COPIE_CS
     *
     * @return smallint 
     */
    public function getCOPIECS()
    {
        return $this->COPIE_CS;
    }

    /**
     * Set NUM_ID
     *
     * @param text $nUMID
     */
    public function setNUMID($nUMID)
    {
        $this->NUM_ID = $nUMID;
    }

    /**
     * Get NUM_ID
     *
     * @return text 
     */
    public function getNUMID()
    {
        return $this->NUM_ID;
    }

    /**
     * Set DATE_ID
     *
     * @param date $dATEID
     */
    public function setDATEID($dATEID)
    {
        $this->DATE_ID = $dATEID;
    }

    /**
     * Get DATE_ID
     *
     * @return date 
     */
    public function getDATEID()
    {
        return $this->DATE_ID;
    }

    /**
     * Set ANNEXE7
     *
     * @param smallint $aNNEXE7
     */
    public function setANNEXE7($aNNEXE7)
    {
        $this->ANNEXE7 = $aNNEXE7;
    }

    /**
     * Get ANNEXE7
     *
     * @return smallint 
     */
    public function getANNEXE7()
    {
        return $this->ANNEXE7;
    }

    /**
     * Set HANDICAPE
     *
     * @param smallint $hANDICAPE
     */
    public function setHANDICAPE($hANDICAPE)
    {
        $this->HANDICAPE = $hANDICAPE;
    }

    /**
     * Get HANDICAPE
     *
     * @return smallint 
     */
    public function getHANDICAPE()
    {
        return $this->HANDICAPE;
    }

    /**
     * Set DATE_F_HANDICAPE
     *
     * @param date $dATEFHANDICAPE
     */
    public function setDATEFHANDICAPE($dATEFHANDICAPE)
    {
        $this->DATE_F_HANDICAPE = $dATEFHANDICAPE;
    }

    /**
     * Get DATE_F_HANDICAPE
     *
     * @return date 
     */
    public function getDATEFHANDICAPE()
    {
        return $this->DATE_F_HANDICAPE;
    }

    /**
     * Set NB_AN_EXPERIENCE
     *
     * @param date $nBANEXPERIENCE
     */
    public function setNBANEXPERIENCE($nBANEXPERIENCE)
    {
        $this->NB_AN_EXPERIENCE = $nBANEXPERIENCE;
    }

    /**
     * Get NB_AN_EXPERIENCE
     *
     * @return date 
     */
    public function getNBANEXPERIENCE()
    {
        return $this->NB_AN_EXPERIENCE;
    }

    /**
     * Set EXPERIENCE
     *
     * @param text $eXPERIENCE
     */
    public function setEXPERIENCE($eXPERIENCE)
    {
        $this->EXPERIENCE = $eXPERIENCE;
    }

    /**
     * Get EXPERIENCE
     *
     * @return text 
     */
    public function getEXPERIENCE()
    {
        return $this->EXPERIENCE;
    }

    /**
     * Set PERMIS
     *
     * @param smallint $pERMIS
     */
    public function setPERMIS($pERMIS)
    {
        $this->PERMIS = $pERMIS;
    }

    /**
     * Get PERMIS
     *
     * @return smallint 
     */
    public function getPERMIS()
    {
        return $this->PERMIS;
    }

    /**
     * Set COMMENTAIRE
     *
     * @param text $cOMMENTAIRE
     */
    public function setCOMMENTAIRE($cOMMENTAIRE)
    {
        $this->COMMENTAIRE = $cOMMENTAIRE;
    }

    /**
     * Get COMMENTAIRE
     *
     * @return text 
     */
    public function getCOMMENTAIRE()
    {
        return $this->COMMENTAIRE;
    }

    /**
     * Set APPRECIATION
     *
     * @param text $aPPRECIATION
     */
    public function setAPPRECIATION($aPPRECIATION)
    {
        $this->APPRECIATION = $aPPRECIATION;
    }

    /**
     * Get APPRECIATION
     *
     * @return text 
     */
    public function getAPPRECIATION()
    {
        return $this->APPRECIATION;
    }

    /**
     * Set VEHICULE
     *
     * @param smallint $vEHICULE
     */
    public function setVEHICULE($vEHICULE)
    {
        $this->VEHICULE = $vEHICULE;
    }

    /**
     * Get VEHICULE
     *
     * @return smallint 
     */
    public function getVEHICULE()
    {
        return $this->VEHICULE;
    }

    /**
     * Set COMPLEMENT_HRS
     *
     * @param smallint $cOMPLEMENTHRS
     */
    public function setCOMPLEMENTHRS($cOMPLEMENTHRS)
    {
        $this->COMPLEMENT_HRS = $cOMPLEMENTHRS;
    }

    /**
     * Get COMPLEMENT_HRS
     *
     * @return smallint 
     */
    public function getCOMPLEMENTHRS()
    {
        return $this->COMPLEMENT_HRS;
    }

    /**
     * Set VITRAGE
     *
     * @param smallint $vITRAGE
     */
    public function setVITRAGE($vITRAGE)
    {
        $this->VITRAGE = $vITRAGE;
    }

    /**
     * Get VITRAGE
     *
     * @return smallint 
     */
    public function getVITRAGE()
    {
        return $this->VITRAGE;
    }

    /**
     * Set NOM_PERS
     *
     * @param string $nOMPERS
     */
    public function setNOMPERS($nOMPERS)
    {
        $this->NOM_PERS = $nOMPERS;
    }

    /**
     * Get NOM_PERS
     *
     * @return string 
     */
    public function getNOMPERS()
    {
        return $this->NOM_PERS;
    }

    /**
     * Set PRENOM_PERS
     *
     * @param string $pRENOMPERS
     */
    public function setPRENOMPERS($pRENOMPERS)
    {
        $this->PRENOM_PERS = $pRENOMPERS;
    }

    /**
     * Get PRENOM_PERS
     *
     * @return string 
     */
    public function getPRENOMPERS()
    {
        return $this->PRENOM_PERS;
    }

    /**
     * Set TELFIX_PERS
     *
     * @param string $tELFIXPERS
     */
    public function setTELFIXPERS($tELFIXPERS)
    {
        $this->TELFIX_PERS = $tELFIXPERS;
    }

    /**
     * Get TELFIX_PERS
     *
     * @return string 
     */
    public function getTELFIXPERS()
    {
        return $this->TELFIX_PERS;
    }

    /**
     * Set TELPOR_PERS
     *
     * @param string $tELPORPERS
     */
    public function setTELPORPERS($tELPORPERS)
    {
        $this->TELPOR_PERS = $tELPORPERS;
    }

    /**
     * Get TELPOR_PERS
     *
     * @return string 
     */
    public function getTELPORPERS()
    {
        return $this->TELPOR_PERS;
    }

    /**
     * Set EMAIL_PERS
     *
     * @param string $eMAILPERS
     */
    public function setEMAILPERS($eMAILPERS)
    {
        $this->EMAIL_PERS = $eMAILPERS;
    }

    /**
     * Get EMAIL_PERS
     *
     * @return string 
     */
    public function getEMAILPERS()
    {
        return $this->EMAIL_PERS;
    }

    /**
     * Set AD_RUE
     *
     * @param string $aDRUE
     */
    public function setADRUE($aDRUE)
    {
        $this->AD_RUE = $aDRUE;
    }

    /**
     * Get AD_RUE
     *
     * @return string 
     */
    public function getADRUE()
    {
        return $this->AD_RUE;
    }

    /**
     * Set SUPPL_AD
     *
     * @param string $sUPPLAD
     */
    public function setSUPPLAD($sUPPLAD)
    {
        $this->SUPPL_AD = $sUPPLAD;
    }

    /**
     * Get SUPPL_AD
     *
     * @return string 
     */
    public function getSUPPLAD()
    {
        return $this->SUPPL_AD;
    }

    /**
     * Set TRAVEX
     *
     * @param smallint $tRAVEX
     */
    public function setTRAVEX($tRAVEX)
    {
        $this->TRAVEX = $tRAVEX;
    }

    /**
     * Get TRAVEX
     *
     * @return smallint 
     */
    public function getTRAVEX()
    {
        return $this->TRAVEX;
    }

    /**
     * Set CODE_CLAS
     *
     * @param string $cODECLAS
     */
    public function setCODECLAS($cODECLAS)
    {
        $this->CODE_CLAS = $cODECLAS;
    }

    /**
     * Get CODE_CLAS
     *
     * @return string 
     */
    public function getCODECLAS()
    {
        return $this->CODE_CLAS;
    }

    /**
     * Set ECHELON
     *
     * @param smallint $eCHELON
     */
    public function setECHELON($eCHELON)
    {
        $this->ECHELON = $eCHELON;
    }

    /**
     * Get ECHELON
     *
     * @return smallint 
     */
    public function getECHELON()
    {
        return $this->ECHELON;
    }

    /**
     * Set POSITIONNEMENT
     *
     * @param text $pOSITIONNEMENT
     */
    public function setPOSITIONNEMENT($pOSITIONNEMENT)
    {
        $this->POSITIONNEMENT = $pOSITIONNEMENT;
    }

    /**
     * Get POSITIONNEMENT
     *
     * @return text 
     */
    public function getPOSITIONNEMENT()
    {
        return $this->POSITIONNEMENT;
    }

    /**
     * Set DATE_CANDI
     *
     * @param date $dATECANDI
     */
    public function setDATECANDI($dATECANDI)
    {
        $this->DATE_CANDI = $dATECANDI;
    }

    /**
     * Get DATE_CANDI
     *
     * @return date 
     */
    public function getDATECANDI()
    {
        return $this->DATE_CANDI;
    }

    /**
     * Set MODE
     *
     * @param text $mODE
     */
    public function setMODE($mODE)
    {
        $this->MODE = $mODE;
    }

    /**
     * Get MODE
     *
     * @return text 
     */
    public function getMODE()
    {
        return $this->MODE;
    }

    /**
     * Set SOURCE
     *
     * @param text $sOURCE
     */
    public function setSOURCE($sOURCE)
    {
        $this->SOURCE = $sOURCE;
    }

    /**
     * Get SOURCE
     *
     * @return text 
     */
    public function getSOURCE()
    {
        return $this->SOURCE;
    }

    /**
     * Set APPGEN
     *
     * @param text $aPPGEN
     */
    public function setAPPGEN($aPPGEN)
    {
        $this->APPGEN = $aPPGEN;
    }

    /**
     * Get APPGEN
     *
     * @return text 
     */
    public function getAPPGEN()
    {
        return $this->APPGEN;
    }

    /**
     * Set LETTRECV
     *
     * @param smallint $lETTRECV
     */
    public function setLETTRECV($lETTRECV)
    {
        $this->LETTRECV = $lETTRECV;
    }

    /**
     * Get LETTRECV
     *
     * @return smallint 
     */
    public function getLETTRECV()
    {
        return $this->LETTRECV;
    }

    /**
     * Set DATELET
     *
     * @param date $dATELET
     */
    public function setDATELET($dATELET)
    {
        $this->DATELET = $dATELET;
    }

    /**
     * Get DATELET
     *
     * @return date 
     */
    public function getDATELET()
    {
        return $this->DATELET;
    }

    /**
     * Set DATE_ENVOI_CONTRAT
     *
     * @param date $dATEENVOICONTRAT
     */
    public function setDATEENVOICONTRAT($dATEENVOICONTRAT)
    {
        $this->DATE_ENVOI_CONTRAT = $dATEENVOICONTRAT;
    }

    /**
     * Get DATE_ENVOI_CONTRAT
     *
     * @return date 
     */
    public function getDATEENVOICONTRAT()
    {
        return $this->DATE_ENVOI_CONTRAT;
    }

    /**
     * Set DATE_ENVOI_AVENANT
     *
     * @param date $dATEENVOIAVENANT
     */
    public function setDATEENVOIAVENANT($dATEENVOIAVENANT)
    {
        $this->DATE_ENVOI_AVENANT = $dATEENVOIAVENANT;
    }

    /**
     * Get DATE_ENVOI_AVENANT
     *
     * @return date 
     */
    public function getDATEENVOIAVENANT()
    {
        return $this->DATE_ENVOI_AVENANT;
    }

    /**
     * Set DATE_ENVOI_CONTRATSIG
     *
     * @param date $dATEENVOICONTRATSIG
     */
    public function setDATEENVOICONTRATSIG($dATEENVOICONTRATSIG)
    {
        $this->DATE_ENVOI_CONTRATSIG = $dATEENVOICONTRATSIG;
    }

    /**
     * Get DATE_ENVOI_CONTRATSIG
     *
     * @return date 
     */
    public function getDATEENVOICONTRATSIG()
    {
        return $this->DATE_ENVOI_CONTRATSIG;
    }

    /**
     * Set ACTIF
     *
     * @param smallint $aCTIF
     */
    public function setACTIF($aCTIF)
    {
        $this->ACTIF = $aCTIF;
    }

    /**
     * Get ACTIF
     *
     * @return smallint 
     */
    public function getACTIF()
    {
        return $this->ACTIF;
    }

    /**
     * Set PERS
     *
     * @param bigint $pERS
     */
    public function setPERS($pERS)
    {
        $this->PERS = $pERS;
    }

    /**
     * Get PERS
     *
     * @return bigint 
     */
    public function getPERS()
    {
        return $this->PERS;
    }

    /**
     * Set RETRAITE
     *
     * @param smallint $rETRAITE
     */
    public function setRETRAITE($rETRAITE)
    {
        $this->RETRAITE = $rETRAITE;
    }

    /**
     * Get RETRAITE
     *
     * @return smallint 
     */
    public function getRETRAITE()
    {
        return $this->RETRAITE;
    }

    /**
     * Set SAMEDI
     *
     * @param smallint $sAMEDI
     */
    public function setSAMEDI($sAMEDI)
    {
        $this->SAMEDI = $sAMEDI;
    }

    /**
     * Get SAMEDI
     *
     * @return smallint 
     */
    public function getSAMEDI()
    {
        return $this->SAMEDI;
    }

    /**
     * Set RPLT
     *
     * @param smallint $rPLT
     */
    public function setRPLT($rPLT)
    {
        $this->RPLT = $rPLT;
    }

    /**
     * Get RPLT
     *
     * @return smallint 
     */
    public function getRPLT()
    {
        return $this->RPLT;
    }

    /**
     * Set FORMVIT
     *
     * @param smallint $fORMVIT
     */
    public function setFORMVIT($fORMVIT)
    {
        $this->FORMVIT = $fORMVIT;
    }

    /**
     * Get FORMVIT
     *
     * @return smallint 
     */
    public function getFORMVIT()
    {
        return $this->FORMVIT;
    }

    /**
     * Set DISPOJ
     *
     * @param smallint $dISPOJ
     */
    public function setDISPOJ($dISPOJ)
    {
        $this->DISPOJ = $dISPOJ;
    }

    /**
     * Get DISPOJ
     *
     * @return smallint 
     */
    public function getDISPOJ()
    {
        return $this->DISPOJ;
    }

    /**
     * Set TPSP
     *
     * @param smallint $tPSP
     */
    public function setTPSP($tPSP)
    {
        $this->TPSP = $tPSP;
    }

    /**
     * Get TPSP
     *
     * @return smallint 
     */
    public function getTPSP()
    {
        return $this->TPSP;
    }

    /**
     * Set NONINTER
     *
     * @param smallint $nONINTER
     */
    public function setNONINTER($nONINTER)
    {
        $this->NONINTER = $nONINTER;
    }

    /**
     * Get NONINTER
     *
     * @return smallint 
     */
    public function getNONINTER()
    {
        return $this->NONINTER;
    }

    /**
     * Set NUMNONATT
     *
     * @param smallint $nUMNONATT
     */
    public function setNUMNONATT($nUMNONATT)
    {
        $this->NUMNONATT = $nUMNONATT;
    }

    /**
     * Get NUMNONATT
     *
     * @return smallint 
     */
    public function getNUMNONATT()
    {
        return $this->NUMNONATT;
    }

    /**
     * Set TJSDISPO
     *
     * @param date $tJSDISPO
     */
    public function setTJSDISPO($tJSDISPO)
    {
        $this->TJSDISPO = $tJSDISPO;
    }

    /**
     * Get TJSDISPO
     *
     * @return date 
     */
    public function getTJSDISPO()
    {
        return $this->TJSDISPO;
    }

    /**
     * Set BURO
     *
     * @param smallint $bURO
     */
    public function setBURO($bURO)
    {
        $this->BURO = $bURO;
    }

    /**
     * Get BURO
     *
     * @return smallint 
     */
    public function getBURO()
    {
        return $this->BURO;
    }

    /**
     * Set MAG
     *
     * @param smallint $mAG
     */
    public function setMAG($mAG)
    {
        $this->MAG = $mAG;
    }

    /**
     * Get MAG
     *
     * @return smallint 
     */
    public function getMAG()
    {
        return $this->MAG;
    }

    /**
     * Set GDSURF
     *
     * @param smallint $gDSURF
     */
    public function setGDSURF($gDSURF)
    {
        $this->GDSURF = $gDSURF;
    }

    /**
     * Get GDSURF
     *
     * @return smallint 
     */
    public function getGDSURF()
    {
        return $this->GDSURF;
    }

    /**
     * Set HOP
     *
     * @param smallint $hOP
     */
    public function setHOP($hOP)
    {
        $this->HOP = $hOP;
    }

    /**
     * Get HOP
     *
     * @return smallint 
     */
    public function getHOP()
    {
        return $this->HOP;
    }

    /**
     * Set PART
     *
     * @param smallint $pART
     */
    public function setPART($pART)
    {
        $this->PART = $pART;
    }

    /**
     * Get PART
     *
     * @return smallint 
     */
    public function getPART()
    {
        return $this->PART;
    }

    /**
     * Set ECO
     *
     * @param smallint $eCO
     */
    public function setECO($eCO)
    {
        $this->ECO = $eCO;
    }

    /**
     * Get ECO
     *
     * @return smallint 
     */
    public function getECO()
    {
        return $this->ECO;
    }

    /**
     * Set HOT
     *
     * @param smallint $hOT
     */
    public function setHOT($hOT)
    {
        $this->HOT = $hOT;
    }

    /**
     * Get HOT
     *
     * @return smallint 
     */
    public function getHOT()
    {
        return $this->HOT;
    }

    /**
     * Set ESPV
     *
     * @param smallint $eSPV
     */
    public function setESPV($eSPV)
    {
        $this->ESPV = $eSPV;
    }

    /**
     * Get ESPV
     *
     * @return smallint 
     */
    public function getESPV()
    {
        return $this->ESPV;
    }

    /**
     * Set MAISRET
     *
     * @param smallint $mAISRET
     */
    public function setMAISRET($mAISRET)
    {
        $this->MAISRET = $mAISRET;
    }

    /**
     * Get MAISRET
     *
     * @return smallint 
     */
    public function getMAISRET()
    {
        return $this->MAISRET;
    }

    /**
     * Set RESTO
     *
     * @param smallint $rESTO
     */
    public function setRESTO($rESTO)
    {
        $this->RESTO = $rESTO;
    }

    /**
     * Get RESTO
     *
     * @return smallint 
     */
    public function getRESTO()
    {
        return $this->RESTO;
    }

    /**
     * Set REMISEETAT
     *
     * @param smallint $rEMISEETAT
     */
    public function setREMISEETAT($rEMISEETAT)
    {
        $this->REMISEETAT = $rEMISEETAT;
    }

    /**
     * Get REMISEETAT
     *
     * @return smallint 
     */
    public function getREMISEETAT()
    {
        return $this->REMISEETAT;
    }

    /**
     * Set DECAPAGE
     *
     * @param smallint $dECAPAGE
     */
    public function setDECAPAGE($dECAPAGE)
    {
        $this->DECAPAGE = $dECAPAGE;
    }

    /**
     * Get DECAPAGE
     *
     * @return smallint 
     */
    public function getDECAPAGE()
    {
        return $this->DECAPAGE;
    }

    /**
     * Set PLUSCONTACT
     *
     * @param smallint $pLUSCONTACT
     */
    public function setPLUSCONTACT($pLUSCONTACT)
    {
        $this->PLUSCONTACT = $pLUSCONTACT;
    }

    /**
     * Get PLUSCONTACT
     *
     * @return smallint 
     */
    public function getPLUSCONTACT()
    {
        return $this->PLUSCONTACT;
    }

    /**
     * Set MOTIFPLUSCONTACT
     *
     * @param text $mOTIFPLUSCONTACT
     */
    public function setMOTIFPLUSCONTACT($mOTIFPLUSCONTACT)
    {
        $this->MOTIFPLUSCONTACT = $mOTIFPLUSCONTACT;
    }

    /**
     * Get MOTIFPLUSCONTACT
     *
     * @return text 
     */
    public function getMOTIFPLUSCONTACT()
    {
        return $this->MOTIFPLUSCONTACT;
    }

    /**
     * Set DATE_COPIE_CS
     *
     * @param date $dATECOPIECS
     */
    public function setDATECOPIECS($dATECOPIECS)
    {
        $this->DATE_COPIE_CS = $dATECOPIECS;
    }

    /**
     * Get DATE_COPIE_CS
     *
     * @return date 
     */
    public function getDATECOPIECS()
    {
        return $this->DATE_COPIE_CS;
    }
    public function __construct()
    {
        $this->agentrecadre = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add agentrecadre
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\recadrage $agentrecadre
     */
    public function addrecadrage(\Phareos\NomadeNetServiceBundle\Entity\recadrage $agentrecadre)
    {
        $this->agentrecadre[] = $agentrecadre;
    }

    /**
     * Get agentrecadre
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getAgentrecadre()
    {
        return $this->agentrecadre;
    }

    /**
     * Add plandagent
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\planingdate $plandagent
     */
    public function addplaningdate(\Phareos\NomadeNetServiceBundle\Entity\planingdate $plandagent)
    {
        $this->plandagent[] = $plandagent;
    }

    /**
     * Get plandagent
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPlandagent()
    {
        return $this->plandagent;
    }
}