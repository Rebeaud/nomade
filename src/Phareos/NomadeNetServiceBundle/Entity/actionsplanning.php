<?php

namespace Phareos\NomadeNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\NomadeNetServiceBundle\Entity\actionsplanning
 *
 * @ORM\Table(name="nom_actionsplanning")
 * @ORM\Entity(repositoryClass="Phareos\NomadeNetServiceBundle\Entity\actionsplanningRepository")
 */
class actionsplanning
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $actions
     *
     * @ORM\Column(name="actions", type="string", length=255)
     */
    private $actions;

    /**
     * @var string $temps_prev
     *
     * @ORM\Column(name="temps_prev", type="string", length=255, nullable=true)
     */
    private $temps_prev;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set actions
     *
     * @param string $actions
     */
    public function setActions($actions)
    {
        $this->actions = $actions;
    }

    /**
     * Get actions
     *
     * @return string 
     */
    public function getActions()
    {
        return $this->actions;
    }

    /**
     * Set temps_prev
     *
     * @param string $tempsPrev
     */
    public function setTempsPrev($tempsPrev)
    {
        $this->temps_prev = $tempsPrev;
    }

    /**
     * Get temps_prev
     *
     * @return string 
     */
    public function getTempsPrev()
    {
        return $this->temps_prev;
    }
}