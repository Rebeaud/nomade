<?php

namespace Phareos\NomadeNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\NomadeNetServiceBundle\Entity\ville
 *
 * @ORM\Table(name="nom_ville")
 * @ORM\Entity(repositoryClass="Phareos\NomadeNetServiceBundle\Entity\villeRepository")
 */
class ville
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $CODE_VILLE
     *
     * @ORM\Column(name="CODE_VILLE", type="string", length=5)
     */
    private $CODE_VILLE;

    /**
     * @var string $VILLE
     *
     * @ORM\Column(name="VILLE", type="string", length=50, nullable=true)
     */
    private $VILLE;

    /**
     * @var string $CP_VILLE
     *
     * @ORM\Column(name="CP_VILLE", type="string", length=5, nullable=true)
     */
    private $CP_VILLE;

    /**
     * @var decimal $LATITUDE
     *
     * @ORM\Column(name="LATITUDE", type="decimal")
     */
    private $LATITUDE;

    /**
     * @var decimal $LONGITUDE
     *
     * @ORM\Column(name="LONGITUDE", type="decimal")
     */
    private $LONGITUDE;

    /**
     * @var decimal $ELOIGNEMENT
     *
     * @ORM\Column(name="ELOIGNEMENT", type="decimal")
     */
    private $ELOIGNEMENT;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set CODE_VILLE
     *
     * @param string $cODEVILLE
     */
    public function setCODEVILLE($cODEVILLE)
    {
        $this->CODE_VILLE = $cODEVILLE;
    }

    /**
     * Get CODE_VILLE
     *
     * @return string 
     */
    public function getCODEVILLE()
    {
        return $this->CODE_VILLE;
    }

    /**
     * Set VILLE
     *
     * @param string $vILLE
     */
    public function setVILLE($vILLE)
    {
        $this->VILLE = $vILLE;
    }

    /**
     * Get VILLE
     *
     * @return string 
     */
    public function getVILLE()
    {
        return $this->VILLE;
    }

    /**
     * Set CP_VILLE
     *
     * @param string $cPVILLE
     */
    public function setCPVILLE($cPVILLE)
    {
        $this->CP_VILLE = $cPVILLE;
    }

    /**
     * Get CP_VILLE
     *
     * @return string 
     */
    public function getCPVILLE()
    {
        return $this->CP_VILLE;
    }

    /**
     * Set LATITUDE
     *
     * @param decimal $lATITUDE
     */
    public function setLATITUDE($lATITUDE)
    {
        $this->LATITUDE = $lATITUDE;
    }

    /**
     * Get LATITUDE
     *
     * @return decimal 
     */
    public function getLATITUDE()
    {
        return $this->LATITUDE;
    }

    /**
     * Set LONGITUDE
     *
     * @param decimal $lONGITUDE
     */
    public function setLONGITUDE($lONGITUDE)
    {
        $this->LONGITUDE = $lONGITUDE;
    }

    /**
     * Get LONGITUDE
     *
     * @return decimal 
     */
    public function getLONGITUDE()
    {
        return $this->LONGITUDE;
    }

    /**
     * Set ELOIGNEMENT
     *
     * @param decimal $eLOIGNEMENT
     */
    public function setELOIGNEMENT($eLOIGNEMENT)
    {
        $this->ELOIGNEMENT = $eLOIGNEMENT;
    }

    /**
     * Get ELOIGNEMENT
     *
     * @return decimal 
     */
    public function getELOIGNEMENT()
    {
        return $this->ELOIGNEMENT;
    }
}