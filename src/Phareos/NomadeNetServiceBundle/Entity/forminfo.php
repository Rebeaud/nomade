<?php

namespace Phareos\NomadeNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\NomadeNetServiceBundle\Entity\forminfo
 *
 * @ORM\Table(name="nom_forminfo")
 * @ORM\Entity(repositoryClass="Phareos\NomadeNetServiceBundle\Entity\forminfoRepository")
 */
class forminfo
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nom
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @var string $prenom
     *
     * @ORM\Column(name="prenom", type="string", length=255, nullable=true)
     */
    private $prenom;

    /**
     * @var string $adressse1
     *
     * @ORM\Column(name="adressse1", type="string", length=255, nullable=true)
     */
    private $adressse1;

    /**
     * @var string $adresse2
     *
     * @ORM\Column(name="adresse2", type="string", length=255, nullable=true)
     */
    private $adresse2;

    /**
     * @var string $codepostal
     *
     * @ORM\Column(name="codepostal", type="string", length=255, nullable=true)
     */
    private $codepostal;

    /**
     * @var string $ville
     *
     * @ORM\Column(name="ville", type="string", length=255, nullable=true)
     */
    private $ville;

    /**
     * @var string $nationa
     *
     * @ORM\Column(name="nationa", type="string", length=255, nullable=true)
     */
    private $nationa;

    /**
     * @var string $ressortce
     *
     * @ORM\Column(name="ressortce", type="string", length=255, nullable=true)
     */
    private $ressortce;

    /**
     * @var string $carteident
     *
     * @ORM\Column(name="carteident", type="string", length=255, nullable=true)
     */
    private $carteident;

    /**
     * @var string $delivrpar
     *
     * @ORM\Column(name="delivrpar", type="string", length=255, nullable=true)
     */
    private $delivrpar;

    /**
     * @var string $titresejour
     *
     * @ORM\Column(name="titresejour", type="string", length=255, nullable=true)
     */
    private $titresejour;

    /**
     * @var date $datevalide
     *
     * @ORM\Column(name="datevalide", type="date", nullable=true)
     */
    private $datevalide;

    /**
     * @var string $autotravil
     *
     * @ORM\Column(name="autotravil", type="string", length=255, nullable=true)
     */
    private $autotravil;

    /**
     * @var string $delivattrav
     *
     * @ORM\Column(name="delivattrav", type="string", length=255, nullable=true)
     */
    private $delivattrav;

    /**
     * @var string $notrepresent
     *
     * @ORM\Column(name="notrepresent", type="string", length=255, nullable=true)
     */
    private $notrepresent;

    /**
     * @var integer $notepres
     *
     * @ORM\Column(name="notepres", type="integer", nullable=true)
     */
    private $notepres;

    /**
     * @var integer $noteeloc
     *
     * @ORM\Column(name="noteeloc", type="integer", nullable=true)
     */
    private $noteeloc;

    /**
     * @var integer $notedynam
     *
     * @ORM\Column(name="notedynam", type="integer", nullable=true)
     */
    private $notedynam;

    /**
     * @var integer $noteanaly
     *
     * @ORM\Column(name="noteanaly", type="integer", nullable=true)
     */
    private $noteanaly;

    /**
     * @var integer $noteorga
     *
     * @ORM\Column(name="noteorga", type="integer", nullable=true)
     */
    private $noteorga;

    /**
     * @var integer $noteexp
     *
     * @ORM\Column(name="noteexp", type="integer", nullable=true)
     */
    private $noteexp;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * Get prenom
     *
     * @return string 
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set adressse1
     *
     * @param string $adressse1
     */
    public function setAdressse1($adressse1)
    {
        $this->adressse1 = $adressse1;
    }

    /**
     * Get adressse1
     *
     * @return string 
     */
    public function getAdressse1()
    {
        return $this->adressse1;
    }

    /**
     * Set adresse2
     *
     * @param string $adresse2
     */
    public function setAdresse2($adresse2)
    {
        $this->adresse2 = $adresse2;
    }

    /**
     * Get adresse2
     *
     * @return string 
     */
    public function getAdresse2()
    {
        return $this->adresse2;
    }

    /**
     * Set codepostal
     *
     * @param string $codepostal
     */
    public function setCodepostal($codepostal)
    {
        $this->codepostal = $codepostal;
    }

    /**
     * Get codepostal
     *
     * @return string 
     */
    public function getCodepostal()
    {
        return $this->codepostal;
    }

    /**
     * Set ville
     *
     * @param string $ville
     */
    public function setVille($ville)
    {
        $this->ville = $ville;
    }

    /**
     * Get ville
     *
     * @return string 
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set nationa
     *
     * @param string $nationa
     */
    public function setNationa($nationa)
    {
        $this->nationa = $nationa;
    }

    /**
     * Get nationa
     *
     * @return string 
     */
    public function getNationa()
    {
        return $this->nationa;
    }

    /**
     * Set ressortce
     *
     * @param string $ressortce
     */
    public function setRessortce($ressortce)
    {
        $this->ressortce = $ressortce;
    }

    /**
     * Get ressortce
     *
     * @return string 
     */
    public function getRessortce()
    {
        return $this->ressortce;
    }

    /**
     * Set carteident
     *
     * @param string $carteident
     */
    public function setCarteident($carteident)
    {
        $this->carteident = $carteident;
    }

    /**
     * Get carteident
     *
     * @return string 
     */
    public function getCarteident()
    {
        return $this->carteident;
    }

    /**
     * Set delivrpar
     *
     * @param string $delivrpar
     */
    public function setDelivrpar($delivrpar)
    {
        $this->delivrpar = $delivrpar;
    }

    /**
     * Get delivrpar
     *
     * @return string 
     */
    public function getDelivrpar()
    {
        return $this->delivrpar;
    }

    /**
     * Set titresejour
     *
     * @param string $titresejour
     */
    public function setTitresejour($titresejour)
    {
        $this->titresejour = $titresejour;
    }

    /**
     * Get titresejour
     *
     * @return string 
     */
    public function getTitresejour()
    {
        return $this->titresejour;
    }

    /**
     * Set datevalide
     *
     * @param date $datevalide
     */
    public function setDatevalide($datevalide)
    {
        $this->datevalide = $datevalide;
    }

    /**
     * Get datevalide
     *
     * @return date 
     */
    public function getDatevalide()
    {
        return $this->datevalide;
    }

    /**
     * Set autotravil
     *
     * @param string $autotravil
     */
    public function setAutotravil($autotravil)
    {
        $this->autotravil = $autotravil;
    }

    /**
     * Get autotravil
     *
     * @return string 
     */
    public function getAutotravil()
    {
        return $this->autotravil;
    }

    /**
     * Set delivattrav
     *
     * @param string $delivattrav
     */
    public function setDelivattrav($delivattrav)
    {
        $this->delivattrav = $delivattrav;
    }

    /**
     * Get delivattrav
     *
     * @return string 
     */
    public function getDelivattrav()
    {
        return $this->delivattrav;
    }

    /**
     * Set notrepresent
     *
     * @param string $notrepresent
     */
    public function setNotrepresent($notrepresent)
    {
        $this->notrepresent = $notrepresent;
    }

    /**
     * Get notrepresent
     *
     * @return string 
     */
    public function getNotrepresent()
    {
        return $this->notrepresent;
    }

    /**
     * Set notepres
     *
     * @param integer $notepres
     */
    public function setNotepres($notepres)
    {
        $this->notepres = $notepres;
    }

    /**
     * Get notepres
     *
     * @return integer 
     */
    public function getNotepres()
    {
        return $this->notepres;
    }

    /**
     * Set noteeloc
     *
     * @param integer $noteeloc
     */
    public function setNoteeloc($noteeloc)
    {
        $this->noteeloc = $noteeloc;
    }

    /**
     * Get noteeloc
     *
     * @return integer 
     */
    public function getNoteeloc()
    {
        return $this->noteeloc;
    }

    /**
     * Set notedynam
     *
     * @param integer $notedynam
     */
    public function setNotedynam($notedynam)
    {
        $this->notedynam = $notedynam;
    }

    /**
     * Get notedynam
     *
     * @return integer 
     */
    public function getNotedynam()
    {
        return $this->notedynam;
    }

    /**
     * Set noteanaly
     *
     * @param integer $noteanaly
     */
    public function setNoteanaly($noteanaly)
    {
        $this->noteanaly = $noteanaly;
    }

    /**
     * Get noteanaly
     *
     * @return integer 
     */
    public function getNoteanaly()
    {
        return $this->noteanaly;
    }

    /**
     * Set noteorga
     *
     * @param integer $noteorga
     */
    public function setNoteorga($noteorga)
    {
        $this->noteorga = $noteorga;
    }

    /**
     * Get noteorga
     *
     * @return integer 
     */
    public function getNoteorga()
    {
        return $this->noteorga;
    }

    /**
     * Set noteexp
     *
     * @param integer $noteexp
     */
    public function setNoteexp($noteexp)
    {
        $this->noteexp = $noteexp;
    }

    /**
     * Get noteexp
     *
     * @return integer 
     */
    public function getNoteexp()
    {
        return $this->noteexp;
    }
}