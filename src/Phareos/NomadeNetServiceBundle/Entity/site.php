<?php

namespace Phareos\NomadeNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\NomadeNetServiceBundle\Entity\site
 *
 * @ORM\Table(name="nom_site")
 * @ORM\Entity(repositoryClass="Phareos\NomadeNetServiceBundle\Entity\siteRepository")
 */
class site
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var date $datevit
     *
     * @ORM\Column(name="datevit", type="date" , nullable=true)
     */
    private $datevit;
	
	/**
     * @var string $MATRICULE_LIEUX
     *
     * @ORM\Column(name="MATRICULE_LIEUX", type="string", length=6)
     */
    private $MATRICULE_LIEUX;

    /**
     * @var smallint $CENTRE_COMMERCIAL
     *
     * @ORM\Column(name="CENTRE_COMMERCIAL", type="smallint", nullable=true)
     */
    private $CENTRE_COMMERCIAL;

    /**
     * @var smallint $CENTRE_VILLE
     *
     * @ORM\Column(name="CENTRE_VILLE", type="smallint", nullable=true)
     */
    private $CENTRE_VILLE;

    /**
     * @var smallint $ZONE
     *
     * @ORM\Column(name="ZONE", type="smallint", nullable=true)
     */
    private $ZONE;

    /**
     * @var string $TEL_SITE
     *
     * @ORM\Column(name="TEL_SITE", type="string", length=14, nullable=true)
     */
    private $TEL_SITE;

    /**
     * @var string $TEL2_SITE
     *
     * @ORM\Column(name="TEL2_SITE", type="string", length=14, nullable=true)
     */
    private $TEL2_SITE;

    /**
     * @var string $TEL3_SITE
     *
     * @ORM\Column(name="TEL3_SITE", type="string", length=14, nullable=true)
     */
    private $TEL3_SITE;

    /**
     * @var string $TEL4_SITE
     *
     * @ORM\Column(name="TEL4_SITE", type="string", length=14, nullable=true)
     */
    private $TEL4_SITE;

    /**
     * @var string $FAX_SITE
     *
     * @ORM\Column(name="FAX_SITE", type="string", length=14, nullable=true)
     */
    private $FAX_SITE;

    /**
     * @var string $EMAIL_SITE
     *
     * @ORM\Column(name="EMAIL_SITE", type="string", length=75, nullable=true)
     */
    private $EMAIL_SITE;

    /**
     * @var integer $NB_PERMANENT
     *
     * @ORM\Column(name="NB_PERMANENT", type="integer", nullable=true)
     */
    private $NB_PERMANENT;

    /**
     * @var integer $NB_SANITAIRE
     *
     * @ORM\Column(name="NB_SANITAIRE", type="integer", nullable=true)
     */
    private $NB_SANITAIRE;

    /**
     * @var integer $SURFACE_BO
     *
     * @ORM\Column(name="SURFACE_BO", type="integer", nullable=true)
     */
    private $SURFACE_BO;

    /**
     * @var integer $SURFACE_MAG
     *
     * @ORM\Column(name="SURFACE_MAG", type="integer", nullable=true)
     */
    private $SURFACE_MAG;

    /**
     * @var decimal $SURFACE_VITRE
     *
     * @ORM\Column(name="SURFACE_VITRE", type="decimal", nullable=true)
     */
    private $SURFACE_VITRE;

    /**
     * @var integer $SURFACE_TAPIS
     *
     * @ORM\Column(name="SURFACE_TAPIS", type="integer", nullable=true)
     */
    private $SURFACE_TAPIS;

    /**
     * @var integer $NOMBRE_ENSEIGNE
     *
     * @ORM\Column(name="NOMBRE_ENSEIGNE", type="integer", nullable=true)
     */
    private $NOMBRE_ENSEIGNE;

    /**
     * @var decimal $LONGUEUR_ENSEIGNE
     *
     * @ORM\Column(name="LONGUEUR_ENSEIGNE", type="decimal", nullable=true)
     */
    private $LONGUEUR_ENSEIGNE;

    /**
     * @var integer $NOMBRE_ENSEIGNE2
     *
     * @ORM\Column(name="NOMBRE_ENSEIGNE2", type="integer", nullable=true)
     */
    private $NOMBRE_ENSEIGNE2;

    /**
     * @var decimal $LONGUEUR_ENSEIGNE2
     *
     * @ORM\Column(name="LONGUEUR_ENSEIGNE2", type="decimal", nullable=true)
     */
    private $LONGUEUR_ENSEIGNE2;

    /**
     * @var integer $NOMBRE_ENSEIGNE3
     *
     * @ORM\Column(name="NOMBRE_ENSEIGNE3", type="integer", nullable=true)
     */
    private $NOMBRE_ENSEIGNE3;

    /**
     * @var decimal $LONGUEUR_ENSEIGNE3
     *
     * @ORM\Column(name="LONGUEUR_ENSEIGNE3", type="decimal", nullable=true)
     */
    private $LONGUEUR_ENSEIGNE3;

    /**
     * @var smallint $FOURNITURE_CONSO
     *
     * @ORM\Column(name="FOURNITURE_CONSO", type="smallint", nullable=true)
     */
    private $FOURNITURE_CONSO;

    /**
     * @var text $HORAIRE
     *
     * @ORM\Column(name="HORAIRE", type="text", nullable=true)
     */
    private $HORAIRE;

    /**
     * @var integer $SURFACE_VT
     *
     * @ORM\Column(name="SURFACE_VT", type="integer", nullable=true)
     */
    private $SURFACE_VT;

    /**
     * @var integer $SURFACE_BURO
     *
     * @ORM\Column(name="SURFACE_BURO", type="integer", nullable=true)
     */
    private $SURFACE_BURO;

    /**
     * @var integer $NB_BURO
     *
     * @ORM\Column(name="NB_BURO", type="integer", nullable=true)
     */
    private $NB_BURO;

    /**
     * @var integer $SURFACE_CUIS
     *
     * @ORM\Column(name="SURFACE_CUIS", type="integer", nullable=true)
     */
    private $SURFACE_CUIS;

    /**
     * @var integer $SURFACE_VEST
     *
     * @ORM\Column(name="SURFACE_VEST", type="integer", nullable=true)
     */
    private $SURFACE_VEST;

    /**
     * @var integer $SURFACE_RES
     *
     * @ORM\Column(name="SURFACE_RES", type="integer", nullable=true)
     */
    private $SURFACE_RES;

    /**
     * @var text $PART_VIT
     *
     * @ORM\Column(name="PART_VIT", type="text", nullable=true)
     */
    private $PART_VIT;

    /**
     * @var decimal $HAUT_VIT
     *
     * @ORM\Column(name="HAUT_VIT", type="decimal", nullable=true)
     */
    private $HAUT_VIT;

    /**
     * @var decimal $SUP_CLOISON_VIT
     *
     * @ORM\Column(name="SUP_CLOISON_VIT", type="decimal", nullable=true)
     */
    private $SUP_CLOISON_VIT;

    /**
     * @var decimal $SUP_MIROIR
     *
     * @ORM\Column(name="SUP_MIROIR", type="decimal", nullable=true)
     */
    private $SUP_MIROIR;

    /**
     * @var text $TYPE_INTERVENANT
     *
     * @ORM\Column(name="TYPE_INTERVENANT", type="text", nullable=true)
     */
    private $TYPE_INTERVENANT;

    /**
     * @var bigint $NUM_PERS
     *
     * @ORM\Column(name="NUM_PERS", type="bigint")
     */
    private $NUM_PERS;

    /**
     * @var integer $NUM_RS
     *
     * @ORM\Column(name="NUM_RS", type="integer")
     */
    private $NUM_RS;

    /**
     * @var integer $NUM_ST
     *
     * @ORM\Column(name="NUM_ST", type="integer")
     */
    private $NUM_ST;

    /**
     * @var text $PRECISER_INT
     *
     * @ORM\Column(name="PRECISER_INT", type="text", nullable=true)
     */
    private $PRECISER_INT;

    /**
     * @var smallint $AVENANT_VIT
     *
     * @ORM\Column(name="AVENANT_VIT", type="smallint")
     */
    private $AVENANT_VIT;

    /**
     * @var smallint $JR_INTER
     *
     * @ORM\Column(name="JR_INTER", type="smallint")
     */
    private $JR_INTER;

    /**
     * @var string $VITRAGE_INT
     *
     * @ORM\Column(name="VITRAGE_INT", type="string", length=12, nullable=true)
     */
    private $VITRAGE_INT;

    /**
     * @var string $VITRAGE_EXT
     *
     * @ORM\Column(name="VITRAGE_EXT", type="string", length=12, nullable=true)
     */
    private $VITRAGE_EXT;

    /**
     * @var smallint $NB_DIM
     *
     * @ORM\Column(name="NB_DIM", type="smallint", nullable=true)
     */
    private $NB_DIM;

    /**
     * @var smallint $NB_JR
     *
     * @ORM\Column(name="NB_JR", type="smallint", nullable=true)
     */
    private $NB_JR;

    /**
     * @var bigint $AGENT1_NUM_PERS
     *
     * @ORM\Column(name="AGENT1_NUM_PERS", type="bigint", nullable=true)
     */
    private $AGENT1_NUM_PERS;

    /**
     * @var bigint $AGENT2_NUM_PERS
     *
     * @ORM\Column(name="AGENT2_NUM_PERS", type="bigint", nullable=true)
     */
    private $AGENT2_NUM_PERS;

    /**
     * @var bigint $AGENT3_NUM_PERS
     *
     * @ORM\Column(name="AGENT3_NUM_PERS", type="bigint", nullable=true)
     */
    private $AGENT3_NUM_PERS;

    /**
     * @var bigint $AGENT4_NUM_PERS
     *
     * @ORM\Column(name="AGENT4_NUM_PERS", type="bigint", nullable=true)
     */
    private $AGENT4_NUM_PERS;

    /**
     * @var bigint $AGENT5_NUM_PERS
     *
     * @ORM\Column(name="AGENT5_NUM_PERS", type="bigint", nullable=true)
     */
    private $AGENT5_NUM_PERS;

    /**
     * @var bigint $AGENT6_NUM_PERS
     *
     * @ORM\Column(name="AGENT6_NUM_PERS", type="bigint", nullable=true)
     */
    private $AGENT6_NUM_PERS;

    /**
     * @var bigint $AGENT7_NUM_PERS
     *
     * @ORM\Column(name="AGENT7_NUM_PERS", type="bigint", nullable=true)
     */
    private $AGENT7_NUM_PERS;

    /**
     * @var bigint $AGENT8_NUM_PERS
     *
     * @ORM\Column(name="AGENT8_NUM_PERS", type="bigint", nullable=true)
     */
    private $AGENT8_NUM_PERS;
	
	/**
     * @var string $vitpresta
     *
     * @ORM\Column(name="vitpresta", type="string", nullable=true)
     */
    private $vitpresta;
	
	/**
     * @var string $solbrille
     *
     * @ORM\Column(name="solbrille", type="string", nullable=true)
     */
    private $solbrille;
	
	/**
     * @var text $obssite
     *
     * @ORM\Column(name="obssite", type="text", nullable=true)
     */
    private $obssite;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set MATRICULE_LIEUX
     *
     * @param string $mATRICULELIEUX
     */
    public function setMATRICULELIEUX($mATRICULELIEUX)
    {
        $this->MATRICULE_LIEUX = $mATRICULELIEUX;
    }

    /**
     * Get MATRICULE_LIEUX
     *
     * @return string 
     */
    public function getMATRICULELIEUX()
    {
        return $this->MATRICULE_LIEUX;
    }

    /**
     * Set CENTRE_COMMERCIAL
     *
     * @param smallint $cENTRECOMMERCIAL
     */
    public function setCENTRECOMMERCIAL($cENTRECOMMERCIAL)
    {
        $this->CENTRE_COMMERCIAL = $cENTRECOMMERCIAL;
    }

    /**
     * Get CENTRE_COMMERCIAL
     *
     * @return smallint 
     */
    public function getCENTRECOMMERCIAL()
    {
        return $this->CENTRE_COMMERCIAL;
    }

    /**
     * Set CENTRE_VILLE
     *
     * @param smallint $cENTREVILLE
     */
    public function setCENTREVILLE($cENTREVILLE)
    {
        $this->CENTRE_VILLE = $cENTREVILLE;
    }

    /**
     * Get CENTRE_VILLE
     *
     * @return smallint 
     */
    public function getCENTREVILLE()
    {
        return $this->CENTRE_VILLE;
    }

    /**
     * Set ZONE
     *
     * @param smallint $zONE
     */
    public function setZONE($zONE)
    {
        $this->ZONE = $zONE;
    }

    /**
     * Get ZONE
     *
     * @return smallint 
     */
    public function getZONE()
    {
        return $this->ZONE;
    }

    /**
     * Set TEL_SITE
     *
     * @param string $tELSITE
     */
    public function setTELSITE($tELSITE)
    {
        $this->TEL_SITE = $tELSITE;
    }

    /**
     * Get TEL_SITE
     *
     * @return string 
     */
    public function getTELSITE()
    {
        return $this->TEL_SITE;
    }

    /**
     * Set TEL2_SITE
     *
     * @param string $tEL2SITE
     */
    public function setTEL2SITE($tEL2SITE)
    {
        $this->TEL2_SITE = $tEL2SITE;
    }

    /**
     * Get TEL2_SITE
     *
     * @return string 
     */
    public function getTEL2SITE()
    {
        return $this->TEL2_SITE;
    }

    /**
     * Set TEL3_SITE
     *
     * @param string $tEL3SITE
     */
    public function setTEL3SITE($tEL3SITE)
    {
        $this->TEL3_SITE = $tEL3SITE;
    }

    /**
     * Get TEL3_SITE
     *
     * @return string 
     */
    public function getTEL3SITE()
    {
        return $this->TEL3_SITE;
    }

    /**
     * Set TEL4_SITE
     *
     * @param string $tEL4SITE
     */
    public function setTEL4SITE($tEL4SITE)
    {
        $this->TEL4_SITE = $tEL4SITE;
    }

    /**
     * Get TEL4_SITE
     *
     * @return string 
     */
    public function getTEL4SITE()
    {
        return $this->TEL4_SITE;
    }

    /**
     * Set FAX_SITE
     *
     * @param string $fAXSITE
     */
    public function setFAXSITE($fAXSITE)
    {
        $this->FAX_SITE = $fAXSITE;
    }

    /**
     * Get FAX_SITE
     *
     * @return string 
     */
    public function getFAXSITE()
    {
        return $this->FAX_SITE;
    }

    /**
     * Set EMAIL_SITE
     *
     * @param string $eMAILSITE
     */
    public function setEMAILSITE($eMAILSITE)
    {
        $this->EMAIL_SITE = $eMAILSITE;
    }

    /**
     * Get EMAIL_SITE
     *
     * @return string 
     */
    public function getEMAILSITE()
    {
        return $this->EMAIL_SITE;
    }

    /**
     * Set NB_PERMANENT
     *
     * @param integer $nBPERMANENT
     */
    public function setNBPERMANENT($nBPERMANENT)
    {
        $this->NB_PERMANENT = $nBPERMANENT;
    }

    /**
     * Get NB_PERMANENT
     *
     * @return integer 
     */
    public function getNBPERMANENT()
    {
        return $this->NB_PERMANENT;
    }

    /**
     * Set NB_SANITAIRE
     *
     * @param integer $nBSANITAIRE
     */
    public function setNBSANITAIRE($nBSANITAIRE)
    {
        $this->NB_SANITAIRE = $nBSANITAIRE;
    }

    /**
     * Get NB_SANITAIRE
     *
     * @return integer 
     */
    public function getNBSANITAIRE()
    {
        return $this->NB_SANITAIRE;
    }

    /**
     * Set SURFACE_BO
     *
     * @param integer $sURFACEBO
     */
    public function setSURFACEBO($sURFACEBO)
    {
        $this->SURFACE_BO = $sURFACEBO;
    }

    /**
     * Get SURFACE_BO
     *
     * @return integer 
     */
    public function getSURFACEBO()
    {
        return $this->SURFACE_BO;
    }

    /**
     * Set SURFACE_MAG
     *
     * @param integer $sURFACEMAG
     */
    public function setSURFACEMAG($sURFACEMAG)
    {
        $this->SURFACE_MAG = $sURFACEMAG;
    }

    /**
     * Get SURFACE_MAG
     *
     * @return integer 
     */
    public function getSURFACEMAG()
    {
        return $this->SURFACE_MAG;
    }

    /**
     * Set SURFACE_VITRE
     *
     * @param decimal $sURFACEVITRE
     */
    public function setSURFACEVITRE($sURFACEVITRE)
    {
        $this->SURFACE_VITRE = $sURFACEVITRE;
    }

    /**
     * Get SURFACE_VITRE
     *
     * @return decimal 
     */
    public function getSURFACEVITRE()
    {
        return $this->SURFACE_VITRE;
    }

    /**
     * Set SURFACE_TAPIS
     *
     * @param integer $sURFACETAPIS
     */
    public function setSURFACETAPIS($sURFACETAPIS)
    {
        $this->SURFACE_TAPIS = $sURFACETAPIS;
    }

    /**
     * Get SURFACE_TAPIS
     *
     * @return integer 
     */
    public function getSURFACETAPIS()
    {
        return $this->SURFACE_TAPIS;
    }

    /**
     * Set NOMBRE_ENSEIGNE
     *
     * @param integer $nOMBREENSEIGNE
     */
    public function setNOMBREENSEIGNE($nOMBREENSEIGNE)
    {
        $this->NOMBRE_ENSEIGNE = $nOMBREENSEIGNE;
    }

    /**
     * Get NOMBRE_ENSEIGNE
     *
     * @return integer 
     */
    public function getNOMBREENSEIGNE()
    {
        return $this->NOMBRE_ENSEIGNE;
    }

    /**
     * Set LONGUEUR_ENSEIGNE
     *
     * @param decimal $lONGUEURENSEIGNE
     */
    public function setLONGUEURENSEIGNE($lONGUEURENSEIGNE)
    {
        $this->LONGUEUR_ENSEIGNE = $lONGUEURENSEIGNE;
    }

    /**
     * Get LONGUEUR_ENSEIGNE
     *
     * @return decimal 
     */
    public function getLONGUEURENSEIGNE()
    {
        return $this->LONGUEUR_ENSEIGNE;
    }

    /**
     * Set NOMBRE_ENSEIGNE2
     *
     * @param integer $nOMBREENSEIGNE2
     */
    public function setNOMBREENSEIGNE2($nOMBREENSEIGNE2)
    {
        $this->NOMBRE_ENSEIGNE2 = $nOMBREENSEIGNE2;
    }

    /**
     * Get NOMBRE_ENSEIGNE2
     *
     * @return integer 
     */
    public function getNOMBREENSEIGNE2()
    {
        return $this->NOMBRE_ENSEIGNE2;
    }

    /**
     * Set LONGUEUR_ENSEIGNE2
     *
     * @param decimal $lONGUEURENSEIGNE2
     */
    public function setLONGUEURENSEIGNE2($lONGUEURENSEIGNE2)
    {
        $this->LONGUEUR_ENSEIGNE2 = $lONGUEURENSEIGNE2;
    }

    /**
     * Get LONGUEUR_ENSEIGNE2
     *
     * @return decimal 
     */
    public function getLONGUEURENSEIGNE2()
    {
        return $this->LONGUEUR_ENSEIGNE2;
    }

    /**
     * Set NOMBRE_ENSEIGNE3
     *
     * @param integer $nOMBREENSEIGNE3
     */
    public function setNOMBREENSEIGNE3($nOMBREENSEIGNE3)
    {
        $this->NOMBRE_ENSEIGNE3 = $nOMBREENSEIGNE3;
    }

    /**
     * Get NOMBRE_ENSEIGNE3
     *
     * @return integer 
     */
    public function getNOMBREENSEIGNE3()
    {
        return $this->NOMBRE_ENSEIGNE3;
    }

    /**
     * Set LONGUEUR_ENSEIGNE3
     *
     * @param decimal $lONGUEURENSEIGNE3
     */
    public function setLONGUEURENSEIGNE3($lONGUEURENSEIGNE3)
    {
        $this->LONGUEUR_ENSEIGNE3 = $lONGUEURENSEIGNE3;
    }

    /**
     * Get LONGUEUR_ENSEIGNE3
     *
     * @return decimal 
     */
    public function getLONGUEURENSEIGNE3()
    {
        return $this->LONGUEUR_ENSEIGNE3;
    }

    /**
     * Set FOURNITURE_CONSO
     *
     * @param smallint $fOURNITURECONSO
     */
    public function setFOURNITURECONSO($fOURNITURECONSO)
    {
        $this->FOURNITURE_CONSO = $fOURNITURECONSO;
    }

    /**
     * Get FOURNITURE_CONSO
     *
     * @return smallint 
     */
    public function getFOURNITURECONSO()
    {
        return $this->FOURNITURE_CONSO;
    }

    /**
     * Set HORAIRE
     *
     * @param text $hORAIRE
     */
    public function setHORAIRE($hORAIRE)
    {
        $this->HORAIRE = $hORAIRE;
    }

    /**
     * Get HORAIRE
     *
     * @return text 
     */
    public function getHORAIRE()
    {
        return $this->HORAIRE;
    }

    /**
     * Set SURFACE_VT
     *
     * @param integer $sURFACEVT
     */
    public function setSURFACEVT($sURFACEVT)
    {
        $this->SURFACE_VT = $sURFACEVT;
    }

    /**
     * Get SURFACE_VT
     *
     * @return integer 
     */
    public function getSURFACEVT()
    {
        return $this->SURFACE_VT;
    }

    /**
     * Set SURFACE_BURO
     *
     * @param integer $sURFACEBURO
     */
    public function setSURFACEBURO($sURFACEBURO)
    {
        $this->SURFACE_BURO = $sURFACEBURO;
    }

    /**
     * Get SURFACE_BURO
     *
     * @return integer 
     */
    public function getSURFACEBURO()
    {
        return $this->SURFACE_BURO;
    }

    /**
     * Set NB_BURO
     *
     * @param integer $nBBURO
     */
    public function setNBBURO($nBBURO)
    {
        $this->NB_BURO = $nBBURO;
    }

    /**
     * Get NB_BURO
     *
     * @return integer 
     */
    public function getNBBURO()
    {
        return $this->NB_BURO;
    }

    /**
     * Set SURFACE_CUIS
     *
     * @param integer $sURFACECUIS
     */
    public function setSURFACECUIS($sURFACECUIS)
    {
        $this->SURFACE_CUIS = $sURFACECUIS;
    }

    /**
     * Get SURFACE_CUIS
     *
     * @return integer 
     */
    public function getSURFACECUIS()
    {
        return $this->SURFACE_CUIS;
    }

    /**
     * Set SURFACE_VEST
     *
     * @param integer $sURFACEVEST
     */
    public function setSURFACEVEST($sURFACEVEST)
    {
        $this->SURFACE_VEST = $sURFACEVEST;
    }

    /**
     * Get SURFACE_VEST
     *
     * @return integer 
     */
    public function getSURFACEVEST()
    {
        return $this->SURFACE_VEST;
    }

    /**
     * Set SURFACE_RES
     *
     * @param integer $sURFACERES
     */
    public function setSURFACERES($sURFACERES)
    {
        $this->SURFACE_RES = $sURFACERES;
    }

    /**
     * Get SURFACE_RES
     *
     * @return integer 
     */
    public function getSURFACERES()
    {
        return $this->SURFACE_RES;
    }

    /**
     * Set PART_VIT
     *
     * @param text $pARTVIT
     */
    public function setPARTVIT($pARTVIT)
    {
        $this->PART_VIT = $pARTVIT;
    }

    /**
     * Get PART_VIT
     *
     * @return text 
     */
    public function getPARTVIT()
    {
        return $this->PART_VIT;
    }

    /**
     * Set HAUT_VIT
     *
     * @param decimal $hAUTVIT
     */
    public function setHAUTVIT($hAUTVIT)
    {
        $this->HAUT_VIT = $hAUTVIT;
    }

    /**
     * Get HAUT_VIT
     *
     * @return decimal 
     */
    public function getHAUTVIT()
    {
        return $this->HAUT_VIT;
    }

    /**
     * Set SUP_CLOISON_VIT
     *
     * @param decimal $sUPCLOISONVIT
     */
    public function setSUPCLOISONVIT($sUPCLOISONVIT)
    {
        $this->SUP_CLOISON_VIT = $sUPCLOISONVIT;
    }

    /**
     * Get SUP_CLOISON_VIT
     *
     * @return decimal 
     */
    public function getSUPCLOISONVIT()
    {
        return $this->SUP_CLOISON_VIT;
    }

    /**
     * Set SUP_MIROIR
     *
     * @param decimal $sUPMIROIR
     */
    public function setSUPMIROIR($sUPMIROIR)
    {
        $this->SUP_MIROIR = $sUPMIROIR;
    }

    /**
     * Get SUP_MIROIR
     *
     * @return decimal 
     */
    public function getSUPMIROIR()
    {
        return $this->SUP_MIROIR;
    }

    /**
     * Set TYPE_INTERVENANT
     *
     * @param text $tYPEINTERVENANT
     */
    public function setTYPEINTERVENANT($tYPEINTERVENANT)
    {
        $this->TYPE_INTERVENANT = $tYPEINTERVENANT;
    }

    /**
     * Get TYPE_INTERVENANT
     *
     * @return text 
     */
    public function getTYPEINTERVENANT()
    {
        return $this->TYPE_INTERVENANT;
    }

    /**
     * Set NUM_PERS
     *
     * @param bigint $nUMPERS
     */
    public function setNUMPERS($nUMPERS)
    {
        $this->NUM_PERS = $nUMPERS;
    }

    /**
     * Get NUM_PERS
     *
     * @return bigint 
     */
    public function getNUMPERS()
    {
        return $this->NUM_PERS;
    }

    /**
     * Set NUM_RS
     *
     * @param integer $nUMRS
     */
    public function setNUMRS($nUMRS)
    {
        $this->NUM_RS = $nUMRS;
    }

    /**
     * Get NUM_RS
     *
     * @return integer 
     */
    public function getNUMRS()
    {
        return $this->NUM_RS;
    }

    /**
     * Set NUM_ST
     *
     * @param integer $nUMST
     */
    public function setNUMST($nUMST)
    {
        $this->NUM_ST = $nUMST;
    }

    /**
     * Get NUM_ST
     *
     * @return integer 
     */
    public function getNUMST()
    {
        return $this->NUM_ST;
    }

    /**
     * Set PRECISER_INT
     *
     * @param text $pRECISERINT
     */
    public function setPRECISERINT($pRECISERINT)
    {
        $this->PRECISER_INT = $pRECISERINT;
    }

    /**
     * Get PRECISER_INT
     *
     * @return text 
     */
    public function getPRECISERINT()
    {
        return $this->PRECISER_INT;
    }

    /**
     * Set AVENANT_VIT
     *
     * @param smallint $aVENANTVIT
     */
    public function setAVENANTVIT($aVENANTVIT)
    {
        $this->AVENANT_VIT = $aVENANTVIT;
    }

    /**
     * Get AVENANT_VIT
     *
     * @return smallint 
     */
    public function getAVENANTVIT()
    {
        return $this->AVENANT_VIT;
    }

    /**
     * Set JR_INTER
     *
     * @param smallint $jRINTER
     */
    public function setJRINTER($jRINTER)
    {
        $this->JR_INTER = $jRINTER;
    }

    /**
     * Get JR_INTER
     *
     * @return smallint 
     */
    public function getJRINTER()
    {
        return $this->JR_INTER;
    }

    /**
     * Set VITRAGE_INT
     *
     * @param string $vITRAGEINT
     */
    public function setVITRAGEINT($vITRAGEINT)
    {
        $this->VITRAGE_INT = $vITRAGEINT;
    }

    /**
     * Get VITRAGE_INT
     *
     * @return string 
     */
    public function getVITRAGEINT()
    {
        return $this->VITRAGE_INT;
    }

    /**
     * Set VITRAGE_EXT
     *
     * @param string $vITRAGEEXT
     */
    public function setVITRAGEEXT($vITRAGEEXT)
    {
        $this->VITRAGE_EXT = $vITRAGEEXT;
    }

    /**
     * Get VITRAGE_EXT
     *
     * @return string 
     */
    public function getVITRAGEEXT()
    {
        return $this->VITRAGE_EXT;
    }

    /**
     * Set NB_DIM
     *
     * @param smallint $nBDIM
     */
    public function setNBDIM($nBDIM)
    {
        $this->NB_DIM = $nBDIM;
    }

    /**
     * Get NB_DIM
     *
     * @return smallint 
     */
    public function getNBDIM()
    {
        return $this->NB_DIM;
    }

    /**
     * Set NB_JR
     *
     * @param smallint $nBJR
     */
    public function setNBJR($nBJR)
    {
        $this->NB_JR = $nBJR;
    }

    /**
     * Get NB_JR
     *
     * @return smallint 
     */
    public function getNBJR()
    {
        return $this->NB_JR;
    }

    /**
     * Set AGENT1_NUM_PERS
     *
     * @param bigint $aGENT1NUMPERS
     */
    public function setAGENT1NUMPERS($aGENT1NUMPERS)
    {
        $this->AGENT1_NUM_PERS = $aGENT1NUMPERS;
    }

    /**
     * Get AGENT1_NUM_PERS
     *
     * @return bigint 
     */
    public function getAGENT1NUMPERS()
    {
        return $this->AGENT1_NUM_PERS;
    }

    /**
     * Set AGENT2_NUM_PERS
     *
     * @param bigint $aGENT2NUMPERS
     */
    public function setAGENT2NUMPERS($aGENT2NUMPERS)
    {
        $this->AGENT2_NUM_PERS = $aGENT2NUMPERS;
    }

    /**
     * Get AGENT2_NUM_PERS
     *
     * @return bigint 
     */
    public function getAGENT2NUMPERS()
    {
        return $this->AGENT2_NUM_PERS;
    }

    /**
     * Set AGENT3_NUM_PERS
     *
     * @param bigint $aGENT3NUMPERS
     */
    public function setAGENT3NUMPERS($aGENT3NUMPERS)
    {
        $this->AGENT3_NUM_PERS = $aGENT3NUMPERS;
    }

    /**
     * Get AGENT3_NUM_PERS
     *
     * @return bigint 
     */
    public function getAGENT3NUMPERS()
    {
        return $this->AGENT3_NUM_PERS;
    }

    /**
     * Set AGENT4_NUM_PERS
     *
     * @param bigint $aGENT4NUMPERS
     */
    public function setAGENT4NUMPERS($aGENT4NUMPERS)
    {
        $this->AGENT4_NUM_PERS = $aGENT4NUMPERS;
    }

    /**
     * Get AGENT4_NUM_PERS
     *
     * @return bigint 
     */
    public function getAGENT4NUMPERS()
    {
        return $this->AGENT4_NUM_PERS;
    }

    /**
     * Set AGENT5_NUM_PERS
     *
     * @param bigint $aGENT5NUMPERS
     */
    public function setAGENT5NUMPERS($aGENT5NUMPERS)
    {
        $this->AGENT5_NUM_PERS = $aGENT5NUMPERS;
    }

    /**
     * Get AGENT5_NUM_PERS
     *
     * @return bigint 
     */
    public function getAGENT5NUMPERS()
    {
        return $this->AGENT5_NUM_PERS;
    }

    /**
     * Set AGENT6_NUM_PERS
     *
     * @param bigint $aGENT6NUMPERS
     */
    public function setAGENT6NUMPERS($aGENT6NUMPERS)
    {
        $this->AGENT6_NUM_PERS = $aGENT6NUMPERS;
    }

    /**
     * Get AGENT6_NUM_PERS
     *
     * @return bigint 
     */
    public function getAGENT6NUMPERS()
    {
        return $this->AGENT6_NUM_PERS;
    }

    /**
     * Set AGENT7_NUM_PERS
     *
     * @param bigint $aGENT7NUMPERS
     */
    public function setAGENT7NUMPERS($aGENT7NUMPERS)
    {
        $this->AGENT7_NUM_PERS = $aGENT7NUMPERS;
    }

    /**
     * Get AGENT7_NUM_PERS
     *
     * @return bigint 
     */
    public function getAGENT7NUMPERS()
    {
        return $this->AGENT7_NUM_PERS;
    }

    /**
     * Set AGENT8_NUM_PERS
     *
     * @param bigint $aGENT8NUMPERS
     */
    public function setAGENT8NUMPERS($aGENT8NUMPERS)
    {
        $this->AGENT8_NUM_PERS = $aGENT8NUMPERS;
    }

    /**
     * Get AGENT8_NUM_PERS
     *
     * @return bigint 
     */
    public function getAGENT8NUMPERS()
    {
        return $this->AGENT8_NUM_PERS;
    }
	
	/**
     * Set vitpresta
     *
     * @param string $vitpresta
     */
    public function setVitpresta($vitpresta)
    {
        $this->vitpresta = $vitpresta;
    }

    /**
     * Get vitpresta
     *
     * @return string 
     */
    public function getVitpresta()
    {
        return $this->vitpresta;
    }
	
	/**
     * Set solbrille
     *
     * @param string $solbrille
     */
    public function setSolbrille($solbrille)
    {
        $this->solbrille = $solbrille;
    }

    /**
     * Get solbrille
     *
     * @return string 
     */
    public function getSolbrille()
    {
        return $this->solbrille;
    }
	
	/**
     * Set obssite
     *
     * @param text $obssite
     */
    public function setObssite($obssite)
    {
        $this->obssite = $obssite;
    }

    /**
     * Get obssite
     *
     * @return text 
     */
    public function getObssite()
    {
        return $this->obssite;
    }

    /**
     * Set datevit
     *
     * @param date $datevit
     */
    public function setDatevit($datevit)
    {
        $this->datevit = $datevit;
    }

    /**
     * Get datevit
     *
     * @return date 
     */
    public function getDatevit()
    {
        return $this->datevit;
    }
}