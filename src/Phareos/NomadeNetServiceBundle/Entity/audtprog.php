<?php

namespace Phareos\NomadeNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\NomadeNetServiceBundle\Entity\audtprog
 *
 * @ORM\Table(name="nom_audtprog")
 * @ORM\Entity(repositoryClass="Phareos\NomadeNetServiceBundle\Entity\audtprogRepository")
 */
class audtprog
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
	 * @ORM\ManyToOne(targetEntity="audtech", inversedBy="audtechtachepros", cascade={"remove"})
	 * @ORM\JoinColumn(name="audtech_id", referencedColumnName="id")
	 */
	protected $audtech;
	
	/**
	 * @ORM\ManyToOne(targetEntity="tachesprogmag", inversedBy="audtechtachepros", cascade={"remove"})
	 * @ORM\JoinColumn(name="tachepros_id", referencedColumnName="id")
	 */
	protected $tachesprogmag;

    /**
     * @var boolean $propre
     *
     * @ORM\Column(name="propre", type="boolean", nullable=true)
     */
    private $propre;

    /**
     * @var boolean $traces
     *
     * @ORM\Column(name="traces", type="boolean", nullable=true)
     */
    private $traces;

    /**
     * @var boolean $poussiere
     *
     * @ORM\Column(name="poussiere", type="boolean", nullable=true)
     */
    private $poussiere;

    /**
     * @var boolean $tache
     *
     * @ORM\Column(name="tache", type="boolean", nullable=true)
     */
    private $tache;

    /**
     * @var boolean $fatuse
     *
     * @ORM\Column(name="fatuse", type="boolean", nullable=true)
     */
    private $fatuse;

    /**
     * @var text $com
     *
     * @ORM\Column(name="com", type="text", nullable=true)
     */
    private $com;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set propre
     *
     * @param boolean $propre
     */
    public function setPropre($propre)
    {
        $this->propre = $propre;
    }

    /**
     * Get propre
     *
     * @return boolean 
     */
    public function getPropre()
    {
        return $this->propre;
    }

    /**
     * Set traces
     *
     * @param boolean $traces
     */
    public function setTraces($traces)
    {
        $this->traces = $traces;
    }

    /**
     * Get traces
     *
     * @return boolean 
     */
    public function getTraces()
    {
        return $this->traces;
    }

    /**
     * Set poussiere
     *
     * @param boolean $poussiere
     */
    public function setPoussiere($poussiere)
    {
        $this->poussiere = $poussiere;
    }

    /**
     * Get poussiere
     *
     * @return boolean 
     */
    public function getPoussiere()
    {
        return $this->poussiere;
    }

    /**
     * Set tache
     *
     * @param boolean $tache
     */
    public function setTache($tache)
    {
        $this->tache = $tache;
    }

    /**
     * Get tache
     *
     * @return boolean 
     */
    public function getTache()
    {
        return $this->tache;
    }

    /**
     * Set fatuse
     *
     * @param boolean $fatuse
     */
    public function setFatuse($fatuse)
    {
        $this->fatuse = $fatuse;
    }

    /**
     * Get fatuse
     *
     * @return boolean 
     */
    public function getFatuse()
    {
        return $this->fatuse;
    }

    /**
     * Set com
     *
     * @param text $com
     */
    public function setCom($com)
    {
        $this->com = $com;
    }

    /**
     * Get com
     *
     * @return text 
     */
    public function getCom()
    {
        return $this->com;
    }

    /**
     * Set audtech
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\audtech $audtech
     */
    public function setAudtech(\Phareos\NomadeNetServiceBundle\Entity\audtech $audtech)
    {
        $this->audtech = $audtech;
    }

    /**
     * Get audtech
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\audtech 
     */
    public function getAudtech()
    {
        return $this->audtech;
    }

    /**
     * Set tachesprogmag
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\tachesprogmag $tachesprogmag
     */
    public function setTachesprogmag(\Phareos\NomadeNetServiceBundle\Entity\tachesprogmag $tachesprogmag)
    {
        $this->tachesprogmag = $tachesprogmag;
    }

    /**
     * Get tachesprogmag
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\tachesprogmag 
     */
    public function getTachesprogmag()
    {
        return $this->tachesprogmag;
    }
}