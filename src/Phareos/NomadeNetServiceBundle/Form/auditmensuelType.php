<?php

namespace Phareos\NomadeNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class auditmensuelType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            //->add('ptvente')
            //->add('datereunion')
            //->add('responsecteur')
            //->add('directeur')
            //->add('respqual')
            //->add('autrpart')
            //->add('pannemur', 'choice', array('choices' => array(1 => "Oui", 0 => "Non"), 
                                            //'multiple' => false, 
                                            //'expanded' => true, 
                                            //'preferred_choices' => array(0),
                                            //'empty_value' => '- Choisissez une option -',
                                            //'empty_data'  => null,
											//'required' => false
                                            //))
            //->add('chliason', 'choice', array('choices' => array(1 => "Oui", 0 => "Non"), 
                                            //'multiple' => false, 
                                            //'expanded' => true, 
                                            //'preferred_choices' => array(0),
                                            //'empty_value' => '- Choisissez une option -',
                                            //'empty_data'  => null,
											//'required' => false
                                            //))
            //->add('planmag', 'choice', array('choices' => array(1 => "Oui", 0 => "Non"), 
                                            //'multiple' => false, 
                                            //'expanded' => true, 
                                            //'preferred_choices' => array(0),
                                            //'empty_value' => '- Choisissez une option -',
                                            //'empty_data'  => null,
											//'required' => false
                                            //))
            //->add('chliasor', 'choice', array('choices' => array(1 => "Oui", 0 => "Non"), 
                                            //'multiple' => false, 
                                            //'expanded' => true, 
                                            //'preferred_choices' => array(0),
                                            //'empty_value' => '- Choisissez une option -',
                                            //'empty_data'  => null,
											//'required' => false
                                            //))
            //->add('fichprod', 'choice', array('choices' => array(1 => "Oui", 0 => "Non"), 
                                            //'multiple' => false, 
                                            //'expanded' => true, 
                                            //'preferred_choices' => array(0),
                                            //'empty_value' => '- Choisissez une option -',
                                            //'empty_data'  => null,
											//'required' => false
                                            //))
            ->add('recepagent', 'choice', array('choices' => array(1 => "Oui", 0 => "Non"), 
                                            'multiple' => false, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('tenutrav', 'choice', array('choices' => array(1 => "Oui", 0 => "Non"), 
                                            'multiple' => false, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('pointagev', 'choice', array('choices' => array(1 => "Oui", 0 => "Non"), 
                                            'multiple' => false, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            //->add('visitrs', 'choice', array('choices' => array(1 => "Oui", 0 => "Non"), 
                                            //'multiple' => false, 
                                            //'expanded' => true, 
                                            //'preferred_choices' => array(0),
                                            //'empty_value' => '- Choisissez une option -',
                                            //'empty_data'  => null,
											//'required' => false
                                            //))
            ->add('absence', 'choice', array('choices' => array(1 => "Oui", 0 => "Non"), 
                                            'multiple' => false, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
			->add('planrespect', 'choice', array('choices' => array(1 => "Oui", 0 => "Non"), 
                                            'multiple' => false, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
			->add('absrempl', 'choice', array('choices' => array(1 => "Oui", 0 => "Non"), 
                                            'multiple' => false, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('qualitserv', 'choice', array('choices' => array('TRES SATISFAISANTE' => "TRES SATISFAISANTE", 'SATISFAISANTE' => "SATISFAISANTE", 'PLUTOT SATISFAISANTE' => "PLUTOT SATISFAISANTE", 'PAS SATISFAISANTE' => "PAS SATISFAISANTE"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
			->add('qualitrotf', 'choice', array('choices' => array('TRES SATISFAISANTE' => "TRES SATISFAISANTE", 'SATISFAISANTE' => "SATISFAISANTE", 'PLUTOT SATISFAISANTE' => "PLUTOT SATISFAISANTE", 'PAS SATISFAISANTE' => "PAS SATISFAISANTE"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
			->add('qualitsol', 'choice', array('choices' => array('TRES SATISFAISANTE' => "TRES SATISFAISANTE", 'SATISFAISANTE' => "SATISFAISANTE", 'PLUTOT SATISFAISANTE' => "PLUTOT SATISFAISANTE", 'PAS SATISFAISANTE' => "PAS SATISFAISANTE"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('observqual')
            ->add('corection', 'choice', array('choices' => array(1 => "Oui", 0 => "Non"), 
                                            'multiple' => false, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
			->add('corectionras', 'choice', array('choices' => array(1 => 'RAS, aucun problème à signalé'), 
                                            'multiple' => false, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => 0,
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('evolprest', 'choice', array('choices' => array('EN AMELIORATION' => "EN AMELIORATION", 'EN CONTINUITE' => "EN CONTINUITE", 'EN REGRESSION' => "EN REGRESSION"), 
                                            'multiple' => false, 
                                            'expanded' => FALSE, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('observevol')
            //->add('dateprochre')
            ->add('filepdf', 'hidden')
        ;
    }

    public function getName()
    {
        return 'phareos_nomadenetservicebundle_auditmensueltype';
    }
}
