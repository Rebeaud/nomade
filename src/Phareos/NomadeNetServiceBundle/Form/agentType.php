<?php

namespace Phareos\NomadeNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class agentType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('NUM_PERS')
            ->add('CODE_VILLE')
            ->add('SEXE')
            ->add('SITUATION_FAMILLE')
            ->add('NOM_JEUNE_FILLE')
            ->add('DATE_NAISSANCE')
            ->add('LIEU_NAISSANCE')
            ->add('NUM_SS')
            ->add('NATIONALITE')
            ->add('NUM_CS')
            ->add('VALIDITE_CS')
            ->add('COPIE_CS')
            ->add('NUM_ID')
            ->add('DATE_ID')
            ->add('ANNEXE7')
            ->add('HANDICAPE')
            ->add('DATE_F_HANDICAPE')
            ->add('NB_AN_EXPERIENCE')
            ->add('EXPERIENCE')
            ->add('PERMIS')
            ->add('COMMENTAIRE')
            ->add('APPRECIATION')
            ->add('VEHICULE')
            ->add('COMPLEMENT_HRS')
            ->add('VITRAGE')
            ->add('NOM_PERS')
            ->add('PRENOM_PERS')
            ->add('TELFIX_PERS')
            ->add('TELPOR_PERS')
            ->add('EMAIL_PERS')
            ->add('AD_RUE')
            ->add('SUPPL_AD')
            ->add('TRAVEX')
            ->add('CODE_CLAS')
            ->add('ECHELON')
            ->add('POSITIONNEMENT')
            ->add('DATE_CANDI')
            ->add('MODE')
            ->add('SOURCE')
            ->add('APPGEN')
            ->add('LETTRECV')
            ->add('DATELET')
            ->add('DATE_ENVOI_CONTRAT')
            ->add('DATE_ENVOI_AVENANT')
            ->add('DATE_ENVOI_CONTRATSIG')
            ->add('ACTIF')
            ->add('PERS')
            ->add('RETRAITE')
            ->add('SAMEDI')
            ->add('RPLT')
            ->add('FORMVIT')
            ->add('DISPOJ')
            ->add('TPSP')
            ->add('NONINTER')
            ->add('NUMNONATT')
            ->add('TJSDISPO')
            ->add('BURO')
            ->add('MAG')
            ->add('GDSURF')
            ->add('HOP')
            ->add('PART')
            ->add('ECO')
            ->add('HOT')
            ->add('ESPV')
            ->add('MAISRET')
            ->add('RESTO')
            ->add('REMISEETAT')
            ->add('DECAPAGE')
            ->add('PLUSCONTACT')
            ->add('MOTIFPLUSCONTACT')
            ->add('DATE_COPIE_CS')
        ;
    }

    public function getName()
    {
        return 'phareos_nomadenetservicebundle_agenttype';
    }
}
