<?php

namespace Phareos\NomadeNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class actionsplanningType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('actions')
            ->add('temps_prev')
        ;
    }

    public function getName()
    {
        return 'phareos_nomadenetservicebundle_actionsplanningtype';
    }
}
