<?php

namespace Phareos\NomadeNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class formationType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            //->add('site')
            //->add('dateform')
            ->add('adresse1', 'hidden')
            ->add('adresse2', 'hidden')
            ->add('cp', 'hidden')
            ->add('ville', 'hidden')
            //->add('nomform')
            //->add('prenomform')
            ->add('detailform')
            ->add('typeform', 'choice', array('choices' => array('MENAGE' => "MENAGE", 'VITRAGE' => "VITRAGE", 'COMPLETE' => "COMPLETE"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => true
                                            ))
            ->add('observ')
            ->add('recommand', 'choice', array('choices' => array('Client Premium' => "Client Premium", 'Client Standard' => "Client Standard"), 
                                            'multiple' => false, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => true
                                            ))
            ->add('critere')
            ->add('nomagent')
            ->add('prenomagent')
            ->add('adr1agent')
            ->add('adr2agent')
            ->add('cpagent')
            ->add('villeagent')
            ->add('nation')
            ->add('CEE', 'choice', array('choices' => array(1 => "Oui", 0 => "Non"), 
                                            'multiple' => false, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => true
                                            ))
            ->add('numci')
            ->add('pref')
            ->add('numtsej')
            //->add('datesej')
            ->add('autotravsal', 'choice', array('choices' => array(1 => "Oui", 0 => "Non"), 
                                            'multiple' => false, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('pref2')
            ->add('fileimg1')
			->add('fileimg2')
			->add('fileimg3')
			->add('notepres')
			->add('noteeloc')
			->add('notedynam')
			->add('noteanaly')
			->add('noteorga')
			->add('noteexp')
        ;
    }

    public function getName()
    {
        return 'phareos_nomadenetservicebundle_formationtype';
    }
}
