<?php

namespace Phareos\NomadeNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class planningType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('titre', 'choice', array('choices' => array('Semaine 1' => "Semaine 1", 'Semaine 2' => "Semaine 2", 'Semaine 3' => "Semaine 3", 'Semaine 4' => "Semaine 4", 'Semaine 5' => "Semaine 5", 'Semaine 6' => "Semaine 6", 'Semaine 7' => "Semaine 7", 'Semaine 8' => "Semaine 8", 'Semaine 9' => "Semaine 9"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('lundi_t1', 'choice', array('choices' => array('Aspiration des sols' => "Aspiration des sols", 'Lavage des sols' => "Lavage des sols", 'Sanitaires' => "Sanitaires"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('lundi_t2', 'choice', array('choices' => array('Aspiration des sols' => "Aspiration des sols", 'Lavage des sols' => "Lavage des sols", 'Sanitaires' => "Sanitaires"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(1),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('lundi_t3', 'choice', array('choices' => array('Aspiration des sols' => "Aspiration des sols", 'Lavage des sols' => "Lavage des sols", 'Sanitaires' => "Sanitaires"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('lundi_t4', 'hidden')
            ->add('lundi_t5', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('lundi_t6', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('lundi_t7', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('lundi_t8', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('lundi_t9', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('lundi_t10', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('mardi_t1', 'choice', array('choices' => array('Aspiration des sols' => "Aspiration des sols", 'Lavage des sols' => "Lavage des sols", 'Sanitaires' => "Sanitaires"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('mardi_t2', 'choice', array('choices' => array('Aspiration des sols' => "Aspiration des sols", 'Lavage des sols' => "Lavage des sols", 'Sanitaires' => "Sanitaires"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('mardi_t3', 'choice', array('choices' => array('Aspiration des sols' => "Aspiration des sols", 'Lavage des sols' => "Lavage des sols", 'Sanitaires' => "Sanitaires"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('mardi_t4', 'hidden')
            ->add('mardi_t5', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('mardi_t6', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('mardi_t7', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('mardi_t8', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('mardi_t9', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('mardi_t10', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('mercredi_t1', 'choice', array('choices' => array('Aspiration des sols' => "Aspiration des sols", 'Lavage des sols' => "Lavage des sols", 'Sanitaires' => "Sanitaires"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('mercredi_t2', 'choice', array('choices' => array('Aspiration des sols' => "Aspiration des sols", 'Lavage des sols' => "Lavage des sols", 'Sanitaires' => "Sanitaires"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('mercredi_t3', 'choice', array('choices' => array('Aspiration des sols' => "Aspiration des sols", 'Lavage des sols' => "Lavage des sols", 'Sanitaires' => "Sanitaires"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('mercredi_t4', 'hidden')
            ->add('mercredi_t5', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('mercredi_t6', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('mercredi_t7', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('mercredi_t8', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('mercredi_t9', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('mercredi_t10', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('jeudi_t1', 'choice', array('choices' => array('Aspiration des sols' => "Aspiration des sols", 'Lavage des sols' => "Lavage des sols", 'Sanitaires' => "Sanitaires"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('jeudi_t2', 'choice', array('choices' => array('Aspiration des sols' => "Aspiration des sols", 'Lavage des sols' => "Lavage des sols", 'Sanitaires' => "Sanitaires"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('jeudi_t3', 'choice', array('choices' => array('Aspiration des sols' => "Aspiration des sols", 'Lavage des sols' => "Lavage des sols", 'Sanitaires' => "Sanitaires"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('jeudi_t4', 'hidden')
            ->add('jeudi_t5', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('jeudi_t6', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('jeudi_t7', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('jeudi_t8', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('jeudi_t9', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('jeudi_t10', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('vendredi_t1', 'choice', array('choices' => array('Aspiration des sols' => "Aspiration des sols", 'Lavage des sols' => "Lavage des sols", 'Sanitaires' => "Sanitaires"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('vendredi_t2', 'choice', array('choices' => array('Aspiration des sols' => "Aspiration des sols", 'Lavage des sols' => "Lavage des sols", 'Sanitaires' => "Sanitaires"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('vendredi_t3', 'choice', array('choices' => array('Aspiration des sols' => "Aspiration des sols", 'Lavage des sols' => "Lavage des sols", 'Sanitaires' => "Sanitaires"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('vendredi_t4', 'hidden')
            ->add('vendredi_t5', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('vendredi_t6', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('vendredi_t7', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('vendredi_t8', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('vendredi_t9', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('vendredi_t10', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('samedi_t1', 'choice', array('choices' => array('Aspiration des sols' => "Aspiration des sols", 'Lavage des sols' => "Lavage des sols", 'Sanitaires' => "Sanitaires"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('samedi_t2', 'choice', array('choices' => array('Aspiration des sols' => "Aspiration des sols", 'Lavage des sols' => "Lavage des sols", 'Sanitaires' => "Sanitaires"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('samedi_t3', 'choice', array('choices' => array('Aspiration des sols' => "Aspiration des sols", 'Lavage des sols' => "Lavage des sols", 'Sanitaires' => "Sanitaires"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('samedi_t4', 'hidden')
            ->add('samedi_t5', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('samedi_t6', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('samedi_t7', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('samedi_t8', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('samedi_t9', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('samedi_t10', 'choice', array('choices' => array('Accessoires' => "Accessoires", 'Arrières' => "Arrières", 'Bains suggestion' => "Bains suggestion", 'Bare minéral' => "Bare minéral",
'Boutique' => "Boutique", 'Capillaire' => "Capillaire", 'Chanel' => "Chanel", 'Clinique By Terry' => "Clinique By Terry", 'Coffret gadget' => "Coffret gadget", 'Crayon  gloss' => "Crayon  gloss",
'Crayon' => "Crayon", 'Dior' => "Dior", 'Enfants' => "Enfants", 'Fond teint' => "Fond teint", 'Giverchy' => "Giverchy", 'Gloss' => "Gloss", 'Gondole Bains' => "Gondole Bains",
'Gondoles' => "Gondoles", 'Gondoles Bare' => "Gondoles Bare", 'Gondoles Bénéfit' => "Gondoles Bénéfit", 'Gondoles Chanel' => "Gondoles Chanel", 'Gondole Clarins' => "Gondole Clarins", 'Gondoles Clinique Lauder' => "Gondoles Clinique Lauder",
'Gondoles Dior' => "Gondoles Dior", 'Gondoles Enfant' => "Gondoles Enfant", 'Gondoles Guerlain' => "Gondoles Guerlain", 'Gondoles Lancôme' => "Gondoles Lancôme", 'Gondoles Make up' => "Gondoles Make up",
'Gondoles Mufe' => "Gondoles Mufe", 'Gondoles Sisley' => "Gondoles Sisley", 'Gondoles soins ongles' => "Gondoles soins ongles", 'Gondoles soins Sephora' => "Gondoles soins Sephora", 'Gondoles Tablettes' => "Gondoles Tablettes",
'Gondoles YSL' => "Gondoles YSL", 'Guerlain' => "Guerlain", 'Lancôme' => "Lancôme", 'Lauder' => "Lauder", 'Linéaires' => "Linéaires", 'Lineaires Muraux' => "Lineaires Muraux", 'Loréal' => "Loréal",
'Maquillage' => "Maquillage", 'Mobiliers Mobile' => "Mobiliers Mobile", 'Sisley' => "Sisley", 'Soins cheveux' => "Soins cheveux", 'Soins ongles' => "Soins ongles", 'Soins Sephora' => "Soins Sephora",
'Trousses' => "Trousses", 'Vebar' => "Vebar", 'Vernis et Rouge' => "Vernis et Rouge", 'Vernis' => "Vernis", 'Yeux' => "Yeux", 'YSL' => "YSL"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('lundi_t5com')
			->add('lundi_t6com')
			->add('lundi_t7com')
			->add('lundi_t8com')
			->add('lundi_t9com')
			->add('lundi_t10com')
			->add('mardi_t5com')
			->add('mardi_t6com')
			->add('mardi_t7com')
			->add('mardi_t8com')
			->add('mardi_t9com')
			->add('mardi_t10com')
			->add('mercredi_t5com')
			->add('mercredi_t6com')
			->add('mercredi_t7com')
			->add('mercredi_t8com')
			->add('mercredi_t9com')
			->add('mercredi_t10com')
			->add('jeudi_t5com')
			->add('jeudi_t6com')
			->add('jeudi_t7com')
			->add('jeudi_t8com')
			->add('jeudi_t9com')
			->add('jeudi_t10com')
			->add('vendredi_t5com')
			->add('vendredi_t6com')
			->add('vendredi_t7com')
			->add('vendredi_t8com')
			->add('vendredi_t9com')
			->add('vendredi_t10com')
			->add('samedi_t5com')
			->add('samedi_t6com')
			->add('samedi_t7com')
			->add('samedi_t8com')
			->add('samedi_t9com')
			->add('samedi_t10com')
			->add('temps_nr')
            ->add('temps_cv')
            ->add('temps_nf')
            //->add('date_maj')
        ;
    }

    public function getName()
    {
        return 'phareos_nomadenetservicebundle_planningtype';
    }
}
