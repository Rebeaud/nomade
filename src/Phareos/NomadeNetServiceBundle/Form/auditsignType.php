<?php

namespace Phareos\NomadeNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class auditsignType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
			->add('heure_maj')
        ;
    }

    public function getName()
    {
        return 'phareos_nomadenetservicebundle_auditsigntype';
    }
}
