<?php

namespace Phareos\NomadeNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class prodvehicType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('stock')
			->add('seuil')
        ;
    }

    public function getName()
    {
        return 'phareos_nomadenetservicebundle_prodvehictype';
    }
}
