<?php

namespace Phareos\NomadeNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

use Phareos\NomadeNetServiceBundle\Entity\prodmag;
use Phareos\NomadeNetServiceBundle\Entity\prodmagRepository;

class stockmagType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $idLIEUX = $_SESSION['idLIEUX'];
		
		$builder
            //->add('date')
            ->add('depotmag')
            ->add('typlivr', 'choice', array('choices' => array('Déposé' => "Déposé", 'Livraison 3 - 4 jours' => "Livraison 3 - 4 jours"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => true
                                            ))
			->add('prodmag', 'entity', array('class' => 'PhareosNomadeNetServiceBundle:prodmag',
												'property' => 'nom',
												'query_builder' => function(prodmagRepository $er) {
												return $er->createQueryBuilder ('u')
												->where ('u.lieuxid2 = :idLIEUX')
												->setParameter('idLIEUX', $_SESSION['idLIEUX'])
												->andWhere ('u.archive = :archive')
												->setParameter('archive', 0)
												->orderBY ('u.nom', 'ASC');
												},
												'multiple' => false,
												'expanded' => false
												))
        ;
    }

    public function getName()
    {
        return 'phareos_nomadenetservicebundle_stockmagtype';
    }
}
