<?php

namespace Phareos\NomadeNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class notescomType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('com')
        ;
    }

    public function getName()
    {
        return 'phareos_nomadenetservicebundle_notescomtype';
    }
}
