<?php

namespace Phareos\NomadeNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class fonctionType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('nom')
			->add('nomafficher')
        ;
    }

    public function getName()
    {
        return 'phareos_nomadenetservicebundle_fonctiontype';
    }
}
