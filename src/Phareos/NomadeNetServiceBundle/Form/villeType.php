<?php

namespace Phareos\NomadeNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class villeType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('CODE_VILLE')
            ->add('VILLE')
            ->add('CP_VILLE')
            ->add('LATITUDE')
            ->add('LONGITUDE')
            ->add('ELOIGNEMENT')
        ;
    }

    public function getName()
    {
        return 'phareos_nomadenetservicebundle_villetype';
    }
}
