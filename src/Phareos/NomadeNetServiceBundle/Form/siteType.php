<?php

namespace Phareos\NomadeNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class siteType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('CENTRECOMMERCIAL', 'choice', array('choices' => array(0 => "Non", 1 => "Oui"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
			->add('CENTREVILLE', 'choice', array('choices' => array(0 => "Non", 1 => "Oui"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
			->add('ZONE', 'choice', array('choices' => array(0 => "Non", 1 => "Oui"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
			->add('NBPERMANENT')
			->add('NBSANITAIRE')
			->add('SURFACEMAG')
			->add('FOURNITURECONSO', 'choice', array('choices' => array(0 => "Non", 1 => "Oui"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
			->add('SURFACEBO')
			->add('SURFACETAPIS')
			->add('VITRAGEINT')
			->add('VITRAGEEXT')
			->add('NOMBREENSEIGNE')
			->add('LONGUEURENSEIGNE')
			->add('SURFACEVITRE')
			->add('HAUTVIT')
			->add('SUPCLOISONVIT')
			->add('SUPMIROIR')
			->add('PARTVIT')
			->add('obssite')
			->add('vitpresta', 'choice', array('choices' => array('Agent' => "Agent", 'Prestataire' => "Prestataire"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
			->add('solbrille', 'choice', array('choices' => array('Brillant' => "Brillant", 'Mat' => "Mat"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
        ;
    }

    public function getName()
    {
        return 'phareos_nomadenetservicebundle_sitetype';
    }
}
