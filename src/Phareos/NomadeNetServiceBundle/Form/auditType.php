<?php

namespace Phareos\NomadeNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class auditType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('parvis_sol', 'choice', array('choices' => array('Propre' => " ", 'Traces' => " ", 'Poussières' => " ", 'Tâches' => " ", 'Usé' => " " ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('parvis_sol_com')
            ->add('tapis_entree', 'choice', array('choices' => array('Propre' => " ", 'Traces' => " ", 'Poussières' => " ", 'Tâches' => " ", 'Usé' => " " ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('tapis_ent_com', 'hidden')
            ->add('portes_vit', 'choice', array('choices' => array('Propre' => " ", 'Traces' => " ", 'Poussières' => " ", 'Tâches' => " ", 'Usé' => " " ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('portes_vit_com', 'hidden')
            ->add('bornes_antivol', 'choice', array('choices' => array('Propre' => " ", 'Traces' => " ", 'Poussières' => " ", 'Tâches' => " ", 'Usé' => " " ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('bornes_ant_com', 'hidden')
            ->add('miroir_vit', 'choice', array('choices' => array('Propre' => " ", 'Traces' => " ", 'Poussières' => " ", 'Tâches' => " ", 'Usé' => " " ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('miroir_vit_com', 'hidden')
            ->add('porte_mont_pil', 'choice', array('choices' => array('Propre' => " ", 'Traces' => " ", 'Poussières' => " ", 'Tâches' => " ", 'Usé' => " " ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('porte_mont_pilcom', 'hidden')
            ->add('caisses', 'choice', array('choices' => array('Propre' => " ", 'Traces' => " ", 'Poussières' => " ", 'Tâches' => " ", 'Usé' => " " ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('caisses_com', 'hidden')
            ->add('dess_fac_tiroir', 'choice', array('choices' => array('Propre' => " ", 'Traces' => " ", 'Poussières' => " ", 'Tâches' => " ", 'Usé' => " " ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('de_fac_tir_com', 'hidden')
            ->add('maqu_rap_pinc', 'choice', array('choices' => array('Propre' => " ", 'Traces' => " ", 'Poussières' => " ", 'Tâches' => " ", 'Usé' => " " ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('ma_ra_pi_com', 'hidden')
            ->add('present_maqu', 'choice', array('choices' => array('Propre' => " ", 'Traces' => " ", 'Poussières' => " ", 'Tâches' => " ", 'Usé' => " " ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('pres_maq_com', 'hidden')
            ->add('edg', 'choice', array('choices' => array('Propre' => " ", 'Traces' => " ", 'Poussières' => " ", 'Tâches' => " ", 'Usé' => " " ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('edg_com', 'hidden')
            ->add('mobilier_mobani', 'choice', array('choices' => array('Propre' => " ", 'Traces' => " ", 'Poussières' => " ", 'Tâches' => " ", 'Usé' => " " ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('mob_ani_com', 'hidden')
            ->add('rehau_joue_gond', 'choice', array('choices' => array('Propre' => " ", 'Traces' => " ", 'Poussières' => " ", 'Tâches' => " ", 'Usé' => " " ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('reh_jo_go_com', 'hidden')
            ->add('bar_beaut_nail', 'choice', array('choices' => array('Propre' => " ", 'Traces' => " ", 'Poussières' => " ", 'Tâches' => " ", 'Usé' => " " ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('bar_beaut_com', 'hidden')
            ->add('tab_bain', 'choice', array('choices' => array('Propre' => " ", 'Traces' => " ", 'Poussières' => " ", 'Tâches' => " ", 'Usé' => " " ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('tab_bain_com', 'hidden')
            ->add('sol_surf_vente', 'choice', array('choices' => array('Propre' => " ", 'Traces' => " ", 'Poussières' => " ", 'Tâches' => " ", 'Usé' => " " ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('sol_surf_v_com')
            ->add('bur_plat_tab_ecr', 'choice', array('choices' => array('Propre' => " ", 'Traces' => " ", 'Poussières' => " ", 'Tâches' => " ", 'Usé' => " " ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('bur_p_t_com', 'hidden')
            ->add('corbeilles', 'choice', array('choices' => array('Propre' => " ", 'Traces' => " ", 'Poussières' => " ", 'Tâches' => " ", 'Usé' => " " ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('corb_com', 'hidden')
            ->add('wc_lav_corb', 'choice', array('choices' => array('Propre' => " ", 'Traces' => " ", 'Poussières' => " ", 'Tâches' => " ", 'Usé' => " " ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('wc_la_com', 'hidden')
            ->add('miroirs_sanit', 'choice', array('choices' => array('Propre' => " ", 'Traces' => " ", 'Poussières' => " ", 'Tâches' => " ", 'Usé' => " " ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('mir_sa_com', 'hidden')
            ->add('cuisine_siege', 'choice', array('choices' => array('Propre' => " ", 'Traces' => " ", 'Poussières' => " ", 'Tâches' => " ", 'Usé' => " " ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('cuis_sieg_com', 'hidden')
            ->add('evier_robin', 'choice', array('choices' => array('Propre' => " ", 'Traces' => " ", 'Poussières' => " ", 'Tâches' => " ", 'Usé' => " " ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('evier_ro_com', 'hidden')
            ->add('sol_loc_soc', 'choice', array('choices' => array('Propre' => " ", 'Traces' => " ", 'Poussières' => " ", 'Tâches' => " ", 'Usé' => " " ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('sol_loc_so_com', 'hidden')
            ->add('sol_meca_reno', 'choice', array('choices' => array('Propre' => " ", 'Traces' => " ", 'Poussières' => " ", 'Tâches' => " ", 'Usé' => " " ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('sol_mec_re_com')
            ->add('vitrerie', 'choice', array('choices' => array('Propre' => " ", 'Traces' => " ", 'Poussières' => " ", 'Tâches' => " ", 'Usé' => " " ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('vitreri_com', 'hidden')
            ->add('caiss_nail_prof', 'choice', array('choices' => array('Propre' => " ", 'Traces' => " ", 'Poussières' => " ", 'Tâches' => " ", 'Usé' => " " ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('tour_ver_bout', 'choice', array('choices' => array('Propre' => " ", 'Traces' => " ", 'Poussières' => " ", 'Tâches' => " ", 'Usé' => " " ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('tour_ver_com', 'hidden')
            ->add('caiss_nail_com', 'hidden')
            ->add('line_mur', 'choice', array('choices' => array('Propre' => " ", 'Traces' => " ", 'Poussières' => " ", 'Tâches' => " ", 'Usé' => " " ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('line_mur_com', 'hidden')
            ->add('gondole_cent', 'choice', array('choices' => array('Propre' => " ", 'Traces' => " ", 'Poussières' => " ", 'Tâches' => " ", 'Usé' => " " ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('gondole_cent_com')
            ->add('trait_prio', 'checkbox', array("label" => "Publier l'actu ?", "required" => false, "value" => "ValeurCheckbox"))
            ->add('poin_mag', 'checkbox', array("label" => "Publier l'actu ?", "required" => false, "value" => "ValeurCheckbox"))
            ->add('reapro', 'checkbox', array("label" => "Publier l'actu ?", "required" => false, "value" => "ValeurCheckbox"))
            ->add('form_po_agen', 'checkbox', array("label" => "Publier l'actu ?", "required" => false, "value" => "ValeurCheckbox"))
            
			//->add('date_maj')//, 'date', array(   'widget' => 'single_text',
                                                //'input' => 'datetime',
                                               // 'format' => 'dd-MM-yyyy',
                                                //'attr' => array('class' => 'date'),
                                                //))
			//->add('heure_maj')
        ;
    }

    public function getName()
    {
        return 'phareos_nomadenetservicebundle_audittype';
    }
}
