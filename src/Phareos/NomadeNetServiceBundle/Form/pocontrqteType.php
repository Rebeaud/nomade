<?php

namespace Phareos\NomadeNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

use Phareos\NomadeNetServiceBundle\Entity\tachesprog;
use Phareos\NomadeNetServiceBundle\Entity\tachesprogRepository;

class pocontrqteType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('qte')
			->add('tachesprog', 'entity', array('class' => 'PhareosNomadeNetServiceBundle:tachesprog',
												'property' => 'tache',
												'query_builder' => function(tachesprogRepository $er) {
												return $er->createQueryBuilder ('u')
												->orderBY ('u.tache', 'ASC');
												},
												))
        ;
    }

    public function getName()
    {
        return 'phareos_nomadenetservicebundle_pocontrqtetype';
    }
}
