<?php

namespace Phareos\NomadeNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

use Phareos\NomadeNetServiceBundle\Entity\agent;
use Phareos\NomadeNetServiceBundle\Entity\agentRepository;


class planingdateType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            //->add('datedeb')
			//->add('agent', 'entity', array('class' => 'PhareosNomadeNetServiceBundle:agent',
												//'property' => 'NOM_PERS',
												//'query_builder' => function(agentRepository $er) {
												//return $er->createQueryBuilder ('u')
												//->orderBY ('u.NOM_PERS', 'ASC');
												//},
												//))
			->add('agent2', 'choice', array('choices' => array('Agent 1' => "Agent 1", 'Agent 2' => "Agent 2", 'Agent 3' => "Agent 3", 'Agent 4' => "Agent 4" ), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(1),
                                            'empty_value' => '- Choisissez un agent -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            //->add('tlsj', 'choice', array('choices' => array(' ' => " "), 
                                            //'multiple' => true, 
                                            //'expanded' => true, 
                                            //'preferred_choices' => array(1),
                                            //'empty_value' => '- Choisissez une option -',
                                            //'empty_data'  => null,
											//'required' => false
                                            //))
            
            //->add('nbrsemaine')
            ->add('vitrage')
			//->add('lundid')
            //->add('mardid')
            //->add('mercredid')
            //->add('jeudid')
            //->add('vendredid')
            //->add('samedid')
			//->add('numsemaine')
        ;
    }

    public function getName()
    {
        return 'phareos_nomadenetservicebundle_planingdatetype';
    }
}
