<?php

namespace Phareos\NomadeNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class nom_responsmagType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            //->add('NUM_PERS')
            //->add('CODE_VILLE')
            //->add('FAX_DR')
            ->add('NOM_PERS')
            ->add('PRENOM_PERS')
            ->add('TELFIX_PERS')
            ->add('TELPOR_PERS')
            ->add('EMAIL_PERS')
            //->add('AD_RUE')
            //->add('SUPPL_AD')
        ;
    }

    public function getName()
    {
        return 'phareos_nomadenetservicebundle_nom_responsmagtype';
    }
}
