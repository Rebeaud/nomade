<?php

namespace Phareos\NomadeNetServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Phareos\NomadeNetServiceBundle\Entity\stockmag;
use Phareos\NomadeNetServiceBundle\Form\stockmagType;

use Phareos\NomadeNetServiceBundle\Entity\prodmag;
use Phareos\NomadeNetServiceBundle\Form\prodmagType;

use Phareos\NomadeNetServiceBundle\Entity\prodvehic;
use Phareos\NomadeNetServiceBundle\Form\prodvehicType;


/**
 * stockmag controller.
 *
 */
class stockmagController extends Controller
{
    /**
     * Lists all stockmag entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

		
		
        
		
		$request = $this->get('request');
		//$idMag = $request->query->get('idMag');
		$session = $this->get('session');
		$idMag = $session->get('idMag');
		$idGC = $session->get('idGC');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		

		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		
		$lieux = $repository_lieux->find($idMag);
		
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}

        $entities = $em->getRepository('PhareosNomadeNetServiceBundle:stockmag')->findBy(array('lieux' => $idMag),
                                      array('id' => 'desc'));
									  
		$entitiesprodmag = $em->getRepository('PhareosNomadeNetServiceBundle:prodmag')->findBy(array(
											'lieux' => $idMag,
											'archive' => 0));
		
		$entity = new stockmag();
		$_SESSION['idLIEUX'] = $idMag;
        $form   = $this->createForm(new stockmagType(), $entity);

        return $this->render('PhareosNomadeNetServiceBundle:stockmag:index.html.twig', array(
            'idMag' => $idMag,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'lieux'=> $lieux,
			'site'=>$site,
			'entity' => $entity,
            'form'   => $form->createView(),
			'entitiesprodmag' => $entitiesprodmag,
			'entities' => $entities
        ));
    }
	
	    public function indexadminAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

		
		
        
		
		$request = $this->get('request');
		//$idMag = $request->query->get('idMag');
		$session = $this->get('session');
		$idMag = $session->get('idMag');
		$idGC = $session->get('idGC');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		

		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		
		$lieux = $repository_lieux->find($idMag);
		
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}

        $entities = $em->getRepository('PhareosNomadeNetServiceBundle:stockmag')->findBy(array('typlivr' => 'Déposé'),
                                      array('id' => 'desc'));
									  
		

        return $this->render('PhareosNomadeNetServiceBundle:stockmag:indexadmin.html.twig', array(
            'idMag' => $idMag,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'lieux'=> $lieux,
			'site'=>$site,
			'entities' => $entities
        ));
    }

    /**
     * Finds and displays a stockmag entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:stockmag')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find stockmag entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosNomadeNetServiceBundle:stockmag:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new stockmag entity.
     *
     */
    public function newAction()
    {
        $entity = new stockmag();
        $form   = $this->createForm(new stockmagType(), $entity);

        return $this->render('PhareosNomadeNetServiceBundle:stockmag:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new stockmag entity.
     *
     */
    public function createAction()
    {
        $request = $this->get('request');
		
		$session = $this->get('session');
		$idMag = $session->get('idMag');
		$idGC = $session->get('idGC');
		
		
		$datedepot = $request->request->get('datedepot');
		$datedepot2 = new \DateTime($datedepot);
		
		$entity  = new stockmag();
        $request = $this->getRequest();
        $form    = $this->createForm(new stockmagType(), $entity);
        $form->bindRequest($request);
		
		$depot = $entity->getDepotmag();
		$typelivre = $entity->getTyplivr();
		$seuilvehic = $entity->getProdmag()->getSeuilveh();
		
		$nouvseuilvehic = $seuilvehic;
		
		

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
			
			$monlieux = $this->getDoctrine()->getRepository('PhareosNomadeNetServiceBundle:lieux')->find($idMag);
			
			$monnom = $this->getDoctrine()->getRepository('PhareosNomadeNetServiceBundle:respsect')->find($idMag);
			
			
			
			$monprodmag = $entity->getProdmag();
			
			$monprod = $monprodmag->getProd();
			
			$monprodId = $monprod->getId();
			
			$user = $this->container->get('security.context')->getToken()->getUser();
			
			$userid = $user->getid();
			
			$respsect = $this->getDoctrine()->getRepository('PhareosNomadeNetServiceBundle:respsect')->findOneBy(array(
											'idfos' => $userid
											));
											
			$idrespsect = $respsect->getId();
			
			
			$query = $em->createQuery(
					'SELECT p FROM PhareosNomadeNetServiceBundle:prodvehic p WHERE (p.grdcompte = :grdcompte OR p.grdcompte is null) AND p.prod = :prod AND p.respsect = :respsect  ORDER BY p.id ASC'
					);
			$query -> setParameters(array (
						'grdcompte' => $idGC,
						'prod' => $monprodId,
						'respsect' => $idrespsect,
						));
						
			$monstockvehic = $query->getSingleResult();
			
			//$monstockvehic = $this->getDoctrine()->getRepository('PhareosNomadeNetServiceBundle:prodvehic')->findOneBy(array(
											//'grdcompte' => $idGC,
											//'prod' => $monprodId,
											//'respsect' => $idrespsect
											//));
											
			//$monstockvehic2 = $this->getDoctrine()->getRepository('PhareosNomadeNetServiceBundle:prodvehic')->find($monstockvehic);
											
			$monstock = $monstockvehic->getId();
			
			$monstockreel = $this->getDoctrine()->getRepository('PhareosNomadeNetServiceBundle:prodvehic')->find($monstock);
			
			$monstockreel2 = $monstockreel->getStock();
			
			
			$entity->setLieux($monlieux);
			$entity->setStockve($nouvseuilvehic);
			
			$entity->setProdvehic($monstockvehic);
			
			$entity->setDate($datedepot2);
			
            $em->persist($entity);
            $em->flush();
			
			if ($typelivre == 'Déposé')
				{
					$nouvstockvehic =  $monstockreel2 - $depot;
				}
			else
				{
					$nouvstockvehic = $monstockreel2;
				}
				
			$monstockvehic->setStock($nouvstockvehic);
			
			 $em->persist($monstockvehic);
            $em->flush();

            return $this->redirect($this->generateUrl('stockmag'));
            
        }

        return $this->render('PhareosNomadeNetServiceBundle:stockmag:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }
	
	public function createrecaudAction()
    {
        $request = $this->get('request');
		
		$session = $this->get('session');
		$idMag = $session->get('idMag');
		$idGC = $session->get('idGC');
		
		$datedepot = $request->request->get('datedepot');
		$datedepot2 = new \DateTime($datedepot);
		
		$entity  = new stockmag();
        $request = $this->getRequest();
        $form    = $this->createForm(new stockmagType(), $entity);
        $form->bindRequest($request);
		
		$depot = $entity->getDepotmag();
		$typelivre = $entity->getTyplivr();
		$seuilvehic = $entity->getProdmag()->getSeuilveh();
		
		$nouvseuilvehic = $seuilvehic;
		
		

        if ($form->isValid()) {
		
			$em = $this->getDoctrine()->getEntityManager();
            $monlieux = $this->getDoctrine()->getRepository('PhareosNomadeNetServiceBundle:lieux')->find($idMag);
			
			$monnom = $this->getDoctrine()->getRepository('PhareosNomadeNetServiceBundle:respsect')->find($idMag);
			
			
			
			$monprodmag = $entity->getProdmag();
			
			$monprod = $monprodmag->getProd();
			
			$monprodId = $monprod->getId();
			
			$user = $this->container->get('security.context')->getToken()->getUser();
			
			$userid = $user->getid();
			
			$respsect = $this->getDoctrine()->getRepository('PhareosNomadeNetServiceBundle:respsect')->findOneBy(array(
											'idfos' => $userid
											));
											
			$idrespsect = $respsect->getId();
			
			
			$query = $em->createQuery(
					'SELECT p FROM PhareosNomadeNetServiceBundle:prodvehic p WHERE (p.grdcompte = :grdcompte OR p.grdcompte is null) AND p.prod = :prod AND p.respsect = :respsect  ORDER BY p.id ASC'
					);
			$query -> setParameters(array (
						'grdcompte' => $idGC,
						'prod' => $monprodId,
						'respsect' => $idrespsect,
						));
						
			$monstockvehic = $query->getSingleResult();
			
			//$monstockvehic = $this->getDoctrine()->getRepository('PhareosNomadeNetServiceBundle:prodvehic')->findOneBy(array(
											//'grdcompte' => $idGC,
											//'prod' => $monprodId,
											//'respsect' => $idrespsect
											//));
											
			//$monstockvehic = $this->getDoctrine()->getRepository('PhareosNomadeNetServiceBundle:prodvehic')->find(2);
											
			//$monstock = $monstockvehic->getStock();
			
			$monstock = $monstockvehic->getId();
			
			$monstockreel = $this->getDoctrine()->getRepository('PhareosNomadeNetServiceBundle:prodvehic')->find($monstock);
			
			$monstockreel2 = $monstockreel->getStock();
			
			$entity->setLieux($monlieux);
			$entity->setStockve($nouvseuilvehic);
			
			$entity->setProdvehic($monstockvehic);
			
			$entity->setDate($datedepot2);
			
            $em->persist($entity);
            $em->flush();
			
			if ($typelivre == 'Déposé')
				{
					$nouvstockvehic =  $monstockreel2 - $depot;
				}
			else
				{
					$nouvstockvehic = $monstockreel2;
				}
				
			$monstockvehic->setStock($nouvstockvehic);
			
			 $em->persist($monstockvehic);
            $em->flush();

            return $this->redirect($this->generateUrl('audtech_new'));
            
        }

        return $this->render('PhareosNomadeNetServiceBundle:stockmag:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing stockmag entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:stockmag')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find stockmag entity.');
        }

        $editForm = $this->createForm(new stockmagType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosNomadeNetServiceBundle:stockmag:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing stockmag entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:stockmag')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find stockmag entity.');
        }

        $editForm   = $this->createForm(new stockmagType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('stockmag_edit', array('id' => $id)));
        }

        return $this->render('PhareosNomadeNetServiceBundle:stockmag:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a stockmag entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosNomadeNetServiceBundle:stockmag')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find stockmag entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('stockmag'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
