<?php

namespace Phareos\NomadeNetServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\NomadeNetServiceBundle\Entity\prod;
use Phareos\NomadeNetServiceBundle\Form\prodType;

use Phareos\NomadeNetServiceBundle\Entity\prodmag;
use Phareos\NomadeNetServiceBundle\Form\progmagType;

use Phareos\NomadeNetServiceBundle\Entity\prodvehic;
use Phareos\NomadeNetServiceBundle\Form\progvehicType;

use Phareos\NomadeNetServiceBundle\Entity\respsect;

/**
 * prod controller.
 *
 */
class prodController extends Controller
{
    /**
     * Lists all prod entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
		
		$request = $this->get('request');
		$session = $this->get('session');
		$idMag = $session->get('idMag');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		
		$nomGC = $session->get('nomGC');
		

		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		
		$lieux = $repository_lieux->find($idMag);
		
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}

        
		
		$idGC = $session->get('idGC');
		
		$query = $em->createQuery(
					'SELECT p FROM PhareosNomadeNetServiceBundle:prod p WHERE (p.grdcompte = :grdcexist OR p.grdcompte is null) AND p.archive = :archive ORDER BY p.id ASC'
					);
		$query -> setParameters(array (
						'grdcexist' => $idGC,
						'archive' => 0,
						));
						
		$entities = $query->getResult();
		
		//$entities = $em->getRepository('PhareosNomadeNetServiceBundle:prod')->findBy(array(
										//'grdcompte' => $idGC,
										//'archive' => 0));
		
		$entity = new prod();
		$_SESSION['idgc'] = $idGC;
        $form   = $this->createForm(new prodType(), $entity);

        return $this->render('PhareosNomadeNetServiceBundle:prod:index.html.twig', array(
            'idMag' => $idMag,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'lieux'=> $lieux,
			'site'=>$site,
			'entity' => $entity,
			'form'   => $form->createView(),
			'entities' => $entities
        ));
    }

    /**
     * Finds and displays a prod entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
		
		$request = $this->get('request');
		$session = $this->get('session');
		$idMag = $session->get('idMag');
		
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		

		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		
		$lieux = $repository_lieux->find($idMag);
		
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:prod')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find prod entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosNomadeNetServiceBundle:prod:show.html.twig', array(
            'idMag' => $idMag,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'lieux'=> $lieux,
			'site'=>$site,
			'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new prod entity.
     *
     */
    public function newAction()
    {
        $session = $this->get('session');
		$request = $this->get('request');
		$idMag = $request->query->get('idMag');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		
		$nomGC = $session->get('nomGC');
		
		
		$em = $this->getDoctrine()->getEntityManager();
		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		
		$lieux = $repository_lieux->find($idMag);
		
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}
		
		$idGC = $session->get('idGC');
		
		$entities = $em->getRepository('PhareosNomadeNetServiceBundle:prod')->findBy(array('nomgc' => $nomGC));
		
		$entity = new prod();
		$_SESSION['idgc'] = $idGC;
        $form   = $this->createForm(new prodType(), $entity);

        return $this->render('PhareosNomadeNetServiceBundle:prod:new.html.twig', array(
            'idMag' => $idMag,
			'lieux'=> $lieux,
			'site'=> $site,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'entities' => $entities,
			'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new prod entity.
     *
     */
    public function createAction()
    {
        $session = $this->get('session');
		$nomGC = $session->get('nomGC');
		$idGC = $session->get('idGC');
		$idMag = $session->get('idMag');
		$societeUser = $session->get('societeUSER');
		
		
		
		$entity  = new prod();
        $request = $this->getRequest();
        $form    = $this->createForm(new prodType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
			
			$prodaffectglobal = $entity->getAffectglobal();
			
			if ($prodaffectglobal == 0)
			{
			
				$moncompte = $this->getDoctrine()->getRepository('PhareosNomadeNetServiceBundle:grand_compte')->find($idGC);
			
				$entity->setGrdcompte($moncompte);
			
				$entity->setGcid2($idGC);
				
			}
			
			$entity->setArchive(0);
			
            $em->persist($entity);
            $em->flush();
			
			
			
			if ($prodaffectglobal == 1)
			{
				$entitieslieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux')->findAll();
			}
			else
			{
				$entitieslieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux')->findBy(array('lieux' => $idGC));
			}
			
			if ($entitieslieux) {
			
				foreach ($entitieslieux as $entitylieu)
				{
					$prodnom = $entity->getNom();
					$prodseuilmag = $entity->getSeuilmag();
					$prodseuilveh = $entity->getSeuilveh();
					$prodtypemat = $entity->getTypemat();
					$prodgcid2 = $entity->getGcid2();
					
					$idlieux = $entitylieu->getId();
					
					$entitymag  = new prodmag();
					
					$entitymag->setNom($prodnom);
					$entitymag->setSeuilmag($prodseuilmag);
					$entitymag->setSeuilveh($prodseuilveh);
					$entitymag->setTypemat($prodtypemat);
					if ($prodaffectglobal == 0)
					{
						$entitymag->setGcid2($prodgcid2);
						$entitymag->setGrdcompte($moncompte);
					}
					$entitymag->setLieux($entitylieu);
					$entitymag->setLieuxid2($idlieux);
					$entitymag->setProd($entity);
					$entitymag->setArchive(0);
					$em->persist($entitymag);
					$em->flush();
				
				}
				
				
			
			}
			
			$entitiesrespsect = $em->getRepository('PhareosNomadeNetServiceBundle:respsect')->findBy(array('fonction' => 'Responsable Secteur'));
			
				foreach ($entitiesrespsect as $entitysrespsect)
				{
					$entityvehic  = new prodvehic();
					
					$entityvehic->setProd($entity);
					if ($prodaffectglobal == 0)
					{
						$entityvehic->setGrdcompte($moncompte);
					}
					
					$entityvehic->setRespsect($entitysrespsect);
					$entityvehic->setStock(0);
					$entityvehic->setArchive(0);
					$entityvehic->setSeuil($entity->getSeuilveh());
					$entityvehic->setSocieteuser($societeUser);
					
					$em->persist($entityvehic);
					$em->flush();
				}

            return $this->redirect($this->generateUrl('prod'));
			
            
        }

        return $this->render('PhareosNomadeNetServiceBundle:prod:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing prod entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:prod')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find prod entity.');
        }

        $editForm = $this->createForm(new prodType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosNomadeNetServiceBundle:prod:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing prod entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:prod')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find prod entity.');
        }

        $editForm   = $this->createForm(new prodType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('prod_edit', array('id' => $id)));
        }

        return $this->render('PhareosNomadeNetServiceBundle:prod:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a prod entity.
     *
     */
    public function deleteAction($id)
    {
        //$form = $this->createDeleteForm($id);
        //$request = $this->getRequest();

        //$form->bindRequest($request);

        //if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosNomadeNetServiceBundle:prod')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find prod entity.');
            }

            $entity->setArchive(1);
			
            $em->persist($entity);
            $em->flush();
			
			$idProd = $entity->getId();
			
			$entitiesprodmag = $em->getRepository('PhareosNomadeNetServiceBundle:prodmag')->findBy(array('prod' => $idProd));
			
			if ($entitiesprodmag) {
			
				foreach ($entitiesprodmag as $entityprodmag)
				{
					$entityprodmag->setArchive(1);
					$em->persist($entityprodmag);
					$em->flush();
				}
			
			}
			$entitiesprodvehic = $em->getRepository('PhareosNomadeNetServiceBundle:prodvehic')->findBy(array('prod' => $idProd));
			
				foreach ($entitiesprodvehic as $entityprodvehic)
				{
					$entityprodvehic->setArchive(1);
					$em->persist($entityprodvehic);
					$em->flush();
				}
        //}

        return $this->redirect($this->generateUrl('prod'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
