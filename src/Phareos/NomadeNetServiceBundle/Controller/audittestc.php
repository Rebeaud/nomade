<?php

//$html2pdf = new \Html2Pdf_Html2Pdf('P','A4','fr');
		//$html2pdf->pdf->SetDisplayMode('real');
		//$html2pdf->writeHTML('essai page pdf');
		//$html2pdf->Output('my-document-name.pdf');
		
		
		$html = $this->renderView('PhareosNomadeNetServiceBundle:audit:show.html.twig', array(
			'idMag' => $idMag,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'lieux'=> $lieux,
			'site'=>$site,
			'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
		
		//$html = ob_get_clean();

		$html2pdf = new \Html2Pdf_Html2Pdf('P','A4','fr');
		$html2pdf->pdf->SetDisplayMode('real');
		$html2pdf->writeHTML($html, isset($_GET['vuehtml']));
		$fichier = $html2pdf->Output('Synthese_'.'.pdf');

		$response = new Response();
		$response->clearHttpHeaders();
		$response->setContent(file_get_contents($fichier));
		$response->headers->set('Content-Type', 'application/force-download');
		$response->headers->set('Content-disposition', 'filename='. $fichier);

return $response;
?>