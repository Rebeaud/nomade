<?php

namespace Phareos\NomadeNetServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\NomadeNetServiceBundle\Entity\forminfo;
use Phareos\NomadeNetServiceBundle\Form\forminfoType;

/**
 * forminfo controller.
 *
 */
class forminfoController extends Controller
{
    /**
     * Lists all forminfo entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosNomadeNetServiceBundle:forminfo')->findAll();

        return $this->render('PhareosNomadeNetServiceBundle:forminfo:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a forminfo entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:forminfo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find forminfo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosNomadeNetServiceBundle:forminfo:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new forminfo entity.
     *
     */
    public function newAction()
    {
        $entity = new forminfo();
        $form   = $this->createForm(new forminfoType(), $entity);

        return $this->render('PhareosNomadeNetServiceBundle:forminfo:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new forminfo entity.
     *
     */
    public function createAction()
    {
        $entity  = new forminfo();
        $request = $this->getRequest();
        $form    = $this->createForm(new forminfoType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('forminfo_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosNomadeNetServiceBundle:forminfo:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing forminfo entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:forminfo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find forminfo entity.');
        }

        $editForm = $this->createForm(new forminfoType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosNomadeNetServiceBundle:forminfo:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing forminfo entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:forminfo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find forminfo entity.');
        }

        $editForm   = $this->createForm(new forminfoType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('forminfo_edit', array('id' => $id)));
        }

        return $this->render('PhareosNomadeNetServiceBundle:forminfo:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a forminfo entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosNomadeNetServiceBundle:forminfo')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find forminfo entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('forminfo'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
