<?php

namespace Phareos\NomadeNetServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Phareos\NomadeNetServiceBundle\Entity\auditmensuel;
use Phareos\NomadeNetServiceBundle\Form\auditmensuelType;
use Phareos\NomadeNetServiceBundle\Form\auditmensuelpaperType;
use Phareos\NomadeNetServiceBundle\Form\auditmensueleditType;

use Phareos\NomadeNetServiceBundle\Entity\controlag;

require_once __DIR__ . '/../Resources/public/signaturepad/signature-to-image.php';

/**
 * auditmensuel controller.
 *
 */
class auditmensuelController extends Controller
{
    /**
     * Lists all auditmensuel entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
		
		
		$request = $this->get('request');
		$idMag = $request->query->get('idMag');
		$session = $this->get('session');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		
		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		
		$lieux = $repository_lieux->find($idMag);
		
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}
		
		
		// ancien entité tous
		//$entities = $em->getRepository('PhareosNomadeNetServiceBundle:auditmensuel')->findAll();
		//remplacer par nouvelle pour le filtrage par apartenance mag:
		$entities = $em->getRepository('PhareosNomadeNetServiceBundle:auditmensuel')->findBy(array('lieux' => $idMag),
                                      array('datereunion' => 'desc'));

        

        return $this->render('PhareosNomadeNetServiceBundle:auditmensuel:index.html.twig', array(
            'idMag' => $idMag,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'lieux'=> $lieux,
			'site'=>$site,
			'entities' => $entities
        ));
    }
	
	public function indexadminAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
		
		
		$request = $this->get('request');
		$idMag = $request->query->get('idMag');
		$session = $this->get('session');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		
		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		
		$lieux = $repository_lieux->find($idMag);
		
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}
		
		
		// ancien entité tous
		//$entities = $em->getRepository('PhareosNomadeNetServiceBundle:auditmensuel')->findAll();
		//remplacer par nouvelle pour le filtrage par apartenance mag:
		$entities = $em->getRepository('PhareosNomadeNetServiceBundle:auditmensuel')->findAll();

        

        return $this->render('PhareosNomadeNetServiceBundle:auditmensuel:indexadmin.html.twig', array(
            'idMag' => $idMag,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'lieux'=> $lieux,
			'site'=>$site,
			'entities' => $entities
        ));
    }

    /**
     * Finds and displays a auditmensuel entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:auditmensuel')->find($id);
		
		$request = $this->get('request');
		$session = $this->get('session');
		$idMag = $session->get('idMag');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		
		$prenomUSER = $session->get('prenomUSER');
		$nomUSER = $session->get('nomUSER');
		
		$nomGC = $session->get('nomGC');
		
		$respSecteur = $nomUSER . ' - ' . $prenomUSER;
		

		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		
		$lieux = $repository_lieux->find($idMag);
		
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}
		
		if ($responsmag1->getNompers()) {
			$responsmag = $responsmag1->getNompers() . ' - ' . $responsmag1->getPrenompers();
		}
		else
		{
			$responsmag = null;
		}
		
		
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find auditmensuel entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
		
		$editForm = $this->createForm(new auditmensuelpaperType(), $entity);

        return $this->render('PhareosNomadeNetServiceBundle:auditmensuel:show.html.twig', array(
            'idMag' => $idMag,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'lieux'=> $lieux,
			'site'=>$site,
			'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
			'edit_form'   => $editForm->createView(),
			'respSecteur' => $respSecteur,
			'responsmag' => $responsmag

        ));
    }
	
	public function showclientAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:auditmensuel')->find($id);
		
		$request = $this->get('request');
		$session = $this->get('session');
		$idMag = $session->get('idMag');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		
		$prenomUSER = $session->get('prenomUSER');
		$nomUSER = $session->get('nomUSER');
		
		$nomGC = $session->get('nomGC');
		
		$respSecteur = $nomUSER . ' - ' . $prenomUSER;
		

		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		
		$lieux = $repository_lieux->find($idMag);
		
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}
		
		if ($responsmag1->getNompers()) {
			$responsmag = $responsmag1->getNompers() . ' - ' . $responsmag1->getPrenompers();
		}
		else
		{
			$responsmag = null;
		}
		
		
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find auditmensuel entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosNomadeNetServiceBundle:auditmensuel:showclient.html.twig', array(
            'idMag' => $idMag,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'lieux'=> $lieux,
			'site'=>$site,
			'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
			'respSecteur' => $respSecteur,
			'responsmag' => $responsmag

        ));
    }

    /**
     * Displays a form to create a new auditmensuel entity.
     *
     */
    public function newAction()
    {
        $session = $this->get('session');
		$request = $this->get('request');
		$idMag = $request->query->get('idMag');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		
		$prenomUSER = $session->get('prenomUSER');
		$nomUSER = $session->get('nomUSER');
		
		$nomGC = $session->get('nomGC');
		
		$respSecteur = $nomUSER . ' - ' . $prenomUSER;
		
		$em = $this->getDoctrine()->getEntityManager();
		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		
		$lieux = $repository_lieux->find($idMag);
		
		$nomville = $lieux->getNomville();
		
		$codeAG = $lieux->getCodag();
		
		$pointdevente = $nomGC . ' - ' . $nomville . ' - ' . $codeAG;
		
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}
		
		
		$entity = new auditmensuel();
        $form   = $this->createForm(new auditmensuelType(), $entity);
		$form_paper   = $this->createForm(new auditmensuelpaperType(), $entity);

        return $this->render('PhareosNomadeNetServiceBundle:auditmensuel:new.html.twig', array(
            'idMag' => $idMag,
			'lieux'=> $lieux,
			'site'=>$site,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'entity' => $entity,
            'form'   => $form->createView(),
			'form_paper'   => $form_paper->createView(),
			'poinvente' => $pointdevente,
			'respSecteur' => $respSecteur
        ));
    }

    /**
     * Creates a new auditmensuel entity.
     *
     */
    public function createAction()
    {
        $entity  = new auditmensuel();
        $request = $this->getRequest();
        $form    = $this->createForm(new auditmensuelType(), $entity);
        $form->bindRequest($request);
		$ptvente = $request->request->get('ptvente');
		$responsecteur = $request->request->get('responsecteur');
		
		$session = $this->get('session');
		$idMag = $session->get('idMag');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		//$datekeep = $request->request->get('datepicker2');
		//$datekeep2 = new \DateTime($datekeep);
		//$session->set('datekeep', $datekeep2);
		//$session->set('date', $entity->getDateMaj());
		
		
		
		$em = $this->getDoctrine()->getEntityManager();
			$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
			$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
			$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
			$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
			
			$lieux = $repository_lieux->find($idMag);
		
			$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
			$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
			
			$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		
		
		
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}
		
		
		$datereunion = $request->request->get('datereunion');
		$heurereunion = $request->request->get('heurereunion');
		$dateheurereunion = $datereunion . ' ' . $heurereunion;
		$datereunion2 = new \DateTime($dateheurereunion);
		$dateprochre = $request->request->get('dateprochre');
		$heureprochre = $request->request->get('heureprochre');
		$dateheureprochre = $dateprochre . ' ' . $heureprochre;
		$dateprochre2 = new \DateTime($dateheureprochre);
		$nummois = $datereunion2->format('m');
		
		$jourreunion = date('D', strtotime($datereunion));
		$jourprochre = date('D', strtotime($dateprochre));
		
		
		
		
		
        if ($form->isValid()) {
            
			$monlieux = $this->getDoctrine()->getRepository('PhareosNomadeNetServiceBundle:lieux')->find($idMag);
			$entity->setLieux($monlieux);
			$entity->setPtvente($ptvente);
			$entity->setResponsecteur($responsecteur);
			$entity->setDatereunion($datereunion2);
			$entity->setDateprochre($dateprochre2);
			$entity->setNummois($nummois);
            //$entity->setDateMaj($datekeep2);
			
            $em->persist($entity);
            $em->flush();
			
			$dateanne = new \DateTime();
			$numannee = $dateanne->format('Y');
			
			$nummois = $datereunion2->format('m');
			
			$dernumag = $entity->getId();
			
			$controlag = $em->getRepository('PhareosNomadeNetServiceBundle:controlag')->findOneBy(array('annee' => $numannee, 'moi' => $nummois, 'lieux' => $idMag));
			
			if ($controlag)
			{
				$controlag->setRdvous(1);
				$controlag->setDernumag($dernumag);
				$em->persist($controlag);
				$em->flush();
			}
			
			
			if ($jourreunion == $jourprochre)
				{
				}
			else
				{
					$message2 = \Swift_Message::newInstance()
							->setSubject('Message application Nomade')
							//->setFrom('mail.application@phareoscloud.org')
							//->setTo('admin.dev.toolbox@phareoscloud.org')
							//code alexis setfrom et setto
							->setFrom('mail.application@faqslife.org')
							->setTo('phareos.test@gmail.com')
							->setBody($this->renderView('PhareosNomadeNetServiceBundle:auditmensuel:showmail.html.twig', array(
																	 'datereunion2' => $datereunion2,
																	 'rsecteur' => $responsecteur,
																	 'ptvente'=> $ptvente
																	 )))
					;
					
					//$this->get('mailer')->send($message2);
					
					$message3 = \Swift_Message::newInstance()
							->setSubject('Message application Nomade')
							//->setFrom('mail.application@phareoscloud.org')
							//->setTo('nomade@altea-proprete.com')
							//code alexis setfrom et setto
							->setFrom('mail.application@faqslife.org')
							->setTo('phareos.test@gmail.com')
							->setBody($this->renderView('PhareosNomadeNetServiceBundle:auditmensuel:showmail.html.twig', array(
																	 'datereunion2' => $datereunion2,
																	 'rsecteur' => $responsecteur,
																	 'ptvente'=> $ptvente
																	 )))
					;
					
					//$this->get('mailer')->send($message3);
				}

            return $this->redirect($this->generateUrl('auditmensuel_show', array(
				'idMag' => $idMag,
				'agent1'=>$agent1,
				'agent2'=>$agent2,
				'agent3'=>$agent3,
				'agent4'=>$agent4,
				'agent5'=>$agent5,
				'agent6'=>$agent6,
				'agent7'=>$agent7,
				'agent8'=>$agent8,
				'agent9'=>$agent9,
				'agent10'=>$agent10,
				'responsmag1'=>$responsmag1,
				'responsmag2'=>$responsmag2,
				'lieux'=> $lieux,
				'site'=>$site,
				'id' => $entity->getId()
					)));
            
        }

        return $this->render('PhareosNomadeNetServiceBundle:auditmensuel:new.html.twig', array(
            'idMag' => $idMag,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'lieux'=> $lieux,
			'site'=>$site,
			'entity' => $entity,
            'form'   => $form->createView()
        ));
    }
	
	public function createpaperAction()
    {
        $entity  = new auditmensuel();
        $request = $this->getRequest();
        $form_paper    = $this->createForm(new auditmensuelpaperType(), $entity);
        $form_paper->bindRequest($request);
		
		
		$session = $this->get('session');
		$idMag = $session->get('idMag');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		//$datekeep = $request->request->get('datepicker2');
		//$datekeep2 = new \DateTime($datekeep);
		//$session->set('datekeep', $datekeep2);
		//$session->set('date', $entity->getDateMaj());
		
		
		
		$em = $this->getDoctrine()->getEntityManager();
			$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
			$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
			$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
			$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
			
			$prenomUSER = $session->get('prenomUSER');
			$nomUSER = $session->get('nomUSER');
		
			$nomGC = $session->get('nomGC');
			
			
			$lieux = $repository_lieux->find($idMag);
			
			$nomville = $lieux->getNomville();
		
			$codeAG = $lieux->getCodag();
		
			$ptvente = $nomGC . ' - ' . $nomville . ' - ' . $codeAG;
			
			
		
			$responsecteur = $nomUSER . ' - ' . $prenomUSER;
		
			$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
			$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
			
			$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		
		
		
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}
		
		
		
		$datereunion2 = new \DateTime();
		
		$dateprochre2 = new \DateTime();
		$nummois = $datereunion2->format('m');
		
		
		
		
		
		
		
        if ($form_paper->isValid()) {
            
			$monlieux = $this->getDoctrine()->getRepository('PhareosNomadeNetServiceBundle:lieux')->find($idMag);
			$entity->setLieux($monlieux);
			$entity->setPtvente($ptvente);
			$entity->setResponsecteur($responsecteur);
			$entity->setDatereunion($datereunion2);
			$entity->setDateprochre($dateprochre2);
			$entity->setNummois($nummois);
            //$entity->setDateMaj($datekeep2);
			
            $em->persist($entity);
            $em->flush();
			
			$dateanne = new \DateTime();
			$numannee = $dateanne->format('Y');
			
			$nummois = $datereunion2->format('m');
			
			$dernumag = $entity->getId();
			
			$controlag = $em->getRepository('PhareosNomadeNetServiceBundle:controlag')->findOneBy(array('annee' => $numannee, 'moi' => $nummois, 'lieux' => $idMag));
			
			if ($controlag)
			{
				$controlag->setRdvous(1);
				$controlag->setDernumag($dernumag);
				$em->persist($controlag);
				$em->flush();
			}
			
			
			

            return $this->redirect($this->generateUrl('auditmensuel_show', array(
				'idMag' => $idMag,
				'agent1'=>$agent1,
				'agent2'=>$agent2,
				'agent3'=>$agent3,
				'agent4'=>$agent4,
				'agent5'=>$agent5,
				'agent6'=>$agent6,
				'agent7'=>$agent7,
				'agent8'=>$agent8,
				'agent9'=>$agent9,
				'agent10'=>$agent10,
				'responsmag1'=>$responsmag1,
				'responsmag2'=>$responsmag2,
				'lieux'=> $lieux,
				'site'=>$site,
				'id' => $entity->getId()
					)));
            
        }

        return $this->render('PhareosNomadeNetServiceBundle:auditmensuel:new.html.twig', array(
            'idMag' => $idMag,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'lieux'=> $lieux,
			'site'=>$site,
			'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing auditmensuel entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:auditmensuel')->find($id);

        $request = $this->get('request');
		$session = $this->get('session');
		$idMag = $session->get('idMag');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		
		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		
		$lieux = $repository_lieux->find($idMag);
		
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		
		$prenomUSER = $session->get('prenomUSER');
		$nomUSER = $session->get('nomUSER');
		
		$nomGC = $session->get('nomGC');
		
		$respSecteur = $nomUSER . ' - ' . $prenomUSER;
		
		$nomville = $lieux->getNomville();
		
		$codeAG = $lieux->getCodag();
		
		$pointdevente = $nomGC . ' - ' . $nomville . ' - ' . $codeAG;
		
		if (!$entity) {
            throw $this->createNotFoundException('Unable to find auditmensuel entity.');
        }

        $editForm = $this->createForm(new auditmensueleditType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}
		
		return $this->render('PhareosNomadeNetServiceBundle:auditmensuel:edit.html.twig', array(
            'idMag' => $idMag,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'lieux'=> $lieux,
			'site'=>$site,
			'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
			'poinvente' => $pointdevente,
			'respSecteur' => $respSecteur
        ));
    }

    /**
     * Edits an existing auditmensuel entity.
     *
     */
    public function updateAction($id)
    {
        
		$session = $this->get('session');
		$idMag = $session->get('idMag');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		
		$em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:auditmensuel')->find($id);

        $repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
			$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
			$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
			$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
			$repository_mail = $em->getRepository('PhareosNomadeNetServiceBundle:mailswift');
			
			$mail1 = $repository_mail->find(1)->getAdresse();
			$mail2 = $repository_mail->find(2)->getAdresse();
			$mail3 = $repository_mail->find(3)->getAdresse();
			
			$lieux = $repository_lieux->find($idMag);
		
			$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
			$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		
		
		if (!$entity) {
            throw $this->createNotFoundException('Unable to find auditmensuel entity.');
        }

        $editForm   = $this->createForm(new auditmensueleditType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);
		
		$ptvente = $request->request->get('ptvente');
		$responsecteur = $request->request->get('responsecteur');
		
		

        $idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		
		
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}
		
		
		$datereunion = $request->request->get('datereunion');
		$datereunion2 = new \DateTime($datereunion);
		$dateprochre = $request->request->get('dateprochre');
		$dateprochre2 = new \DateTime($dateprochre);
		$nummois = $datereunion2->format('m');
		
		$pointagent = $request->request->get('pointagent');
		
		
		
		$jourreunion = date('D', strtotime($datereunion));
		$jourprochre = date('D', strtotime($dateprochre));
		
		if ($editForm->isValid()) {
            $entity->setDatereunion($datereunion2);
			$entity->setDateprochre($dateprochre2);
			$entity->setPtvente($ptvente);
			$entity->setResponsecteur($responsecteur);
			$entity->setPointagent($pointagent);
			$entity->setNummois($nummois);
			$em->persist($entity);
            $em->flush();
			
			$dispodir = $entity->getDispodir();
			
			if ($jourreunion == $jourprochre)
				{
				}
			else
				{
					$message2 = \Swift_Message::newInstance()
							->setSubject('Message application Nomade')
							//->setFrom('mail.application@phareoscloud.org')
							//->setTo('admin.dev.toolbox@phareoscloud.org')
							//code alexis setfrom et setto
							->setFrom($mail1)
							->setTo($mail2)
							->setBody($this->renderView('PhareosNomadeNetServiceBundle:auditmensuel:showmail.html.twig', array(
																	 'datereunion2' => $datereunion2,
																	 'rsecteur' => $responsecteur,
																	 'ptvente'=> $ptvente
																	 )))
					;
					
					//$this->get('mailer')->send($message2);
					
					$message3 = \Swift_Message::newInstance()
							->setSubject('Message application Nomade')
							//->setFrom('mail.application@phareoscloud.org')
							//->setTo('nomade@altea-proprete.com')
							//code alexis setfrom et setto
							->setFrom($mail1)
							->setTo($mail3)
							->setBody($this->renderView('PhareosNomadeNetServiceBundle:auditmensuel:showmail.html.twig', array(
																	 'datereunion2' => $datereunion2,
																	 'rsecteur' => $responsecteur,
																	 'ptvente'=> $ptvente
																	 )))
					;
					
					//$this->get('mailer')->send($message3);
				}
				
				if ($dispodir == 1)
				{
					$message4 = \Swift_Message::newInstance()
							->setSubject('Directrice Indisponible sur Audit Global')
							//->setFrom('mail.application@phareoscloud.org')
							//->setTo('admin.dev.toolbox@phareoscloud.org')
							//code alexis setfrom et setto
							->setFrom($mail1)
							->setTo($mail2)
							->setBody($this->renderView('PhareosNomadeNetServiceBundle:auditmensuel:showmail2.html.twig', array(
																	 'datereunion2' => $datereunion2,
																	 'rsecteur' => $responsecteur,
																	 'ptvente'=> $ptvente
																	 )))
					;
					
					$this->get('mailer')->send($message4);
					
					$message5 = \Swift_Message::newInstance()
							->setSubject('Directrice Indisponible sur Audit Global')
							//->setFrom('mail.application@phareoscloud.org')
							//->setTo('nomade@altea-proprete.com')
							//code alexis setfrom et setto
							->setFrom($mail1)
							->setTo($mail3)
							->setBody($this->renderView('PhareosNomadeNetServiceBundle:auditmensuel:showmail2.html.twig', array(
																	 'datereunion2' => $datereunion2,
																	 'rsecteur' => $responsecteur,
																	 'ptvente'=> $ptvente
																	 )))
					;
					
					$this->get('mailer')->send($message5);
				}
				else
				{
					
				}

            return $this->redirect($this->generateUrl('auditmensuel_show', array(
				'id' => $id
				)));
        }

        return $this->render('PhareosNomadeNetServiceBundle:auditmensuel:edit.html.twig', array(
            'idMag' 	  => $idMag,
			'agent1'	  =>$agent1,
			'agent2'	  =>$agent2,
			'agent3'	  =>$agent3,
			'agent4'	  =>$agent4,
			'agent5' 	  =>$agent5,
			'agent6' 	  =>$agent6,
			'agent7'	  =>$agent7,
			'agent8'  	  =>$agent8,
			'agent9'	  =>$agent9,
			'agent10'	  =>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'lieux'		  => $lieux,
			'site'		  => $site,
			'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a auditmensuel entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosNomadeNetServiceBundle:auditmensuel')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find auditmensuel entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('auditmensuel'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
	
	public function updatesignAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:auditmensuel')->find($id);
		
			$repository_mail = $em->getRepository('PhareosNomadeNetServiceBundle:mailswift');
			
			$mail1 = $repository_mail->find(1)->getAdresse();
			$mail2 = $repository_mail->find(2)->getAdresse();
			$mail3 = $repository_mail->find(3)->getAdresse();
			$mail4 = $repository_mail->find(4)->getAdresse();
			$mail5 = $repository_mail->find(5)->getAdresse();
		
		$session = $this->get('session');
		
		$prenomUSER = $session->get('prenomUSER');
		$nomUSER = $session->get('nomUSER');
		
		$nomGC = $session->get('nomGC');
		
		$idMag = $session->get('idMag');
		
		$entitymag = $em->getRepository('PhareosNomadeNetServiceBundle:lieux')->find($idMag);
		
		$entitysite = $entitymag->getSitelieux();
		
		if ( $mail1 == 'mail.application@phareoscloud.org')
		{
			$emailMag = $entitysite->getEMAILSITE();
		}
		else
		{
			$emailMag = $mail2;
		}
		
		$respSecteur = $nomUSER . ' - ' . $prenomUSER;
		
		$nomsignaltea = $_POST['nomsignaltea'];
		$nomsignclient = $_POST['nomsignclient'];
		$foncsignaltea = $_POST['foncsignaltea'];
		$foncsignclient = $_POST['foncsignclient'];
		$motifindispo = $_POST['motifindispo'];
		
		$request = $this->getRequest();
		
		$dispodir = $request->request->get('dispodir');
		//$pointdirres = $request->request->get('pointdirres');
		
		$pointagent = $request->request->get('pointagent');
		
		$filesignaltea = 'img_altea_'.$id.'.png';
		$filesignclient = 'img_client_'.$id.'.png';

		$json1 = $_POST['output1'];
		$img1 = sigJsonToImage($json1, array('imageSize'=>array(298, 190)));

		imagepng($img1, 'imgsign/auditmensuel/'.$filesignaltea);
		imagedestroy($img1);
		
		$json2 = $_POST['output2'];
		$img2 = sigJsonToImage($json2, array('imageSize'=>array(298, 190)));

		imagepng($img2, 'imgsign/auditmensuel/'.$filesignclient);
		imagedestroy($img2);
		
		$entity->setNomsignaltea($nomsignaltea);
		$entity->setNomsignclient($nomsignclient);
		$entity->setImgsignaltea($filesignaltea);
		$entity->setImgsignclient($filesignclient);
		$entity->setFoncsignaltea($foncsignaltea);
		$entity->setFoncsignclient($foncsignclient);
		$entity->setDispodir($dispodir);
		//$entity->setPointdirres($pointdirres);
		$entity->setPointagent($pointagent);
		$entity->setMotifindispo($motifindispo);
		
		
		
		
		
		
		
		
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find auditmensuel entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
		$editForm   = $this->createForm(new auditmensuelpaperType(), $entity);
		$request = $this->getRequest();
		
		$editForm->bindRequest($request);
		
		

        //if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

			$dateanne = new \DateTime();
			$numannee = $dateanne->format('Y');
			
			$nummois = $entity->getDatereunion()->format('m');
			
			$controlag = $em->getRepository('PhareosNomadeNetServiceBundle:controlag')->findOneBy(array('annee' => $numannee, 'moi' => $nummois, 'lieux' => $idMag));
			
			if ($controlag)
			{
				$controlag->setSigne(1);
				
				if($dispodir == 1)
				{
					$controlag->setDispodir(1);
					$controlag->setMotifabs($motifindispo);
				}
				$em->persist($controlag);
				$em->flush();
			}


		   //Ecriture du fichier PDF
			
			$idLieuxaudit = $entity->getLieux()->getCodag();
			$dateReunion = $entity->getDatereunion()->format('d-m-Y');
			
			
			$observqualentier = wordwrap ($entity->getObservqual(),135,'***', true);
		$cutobservqual = explode ('***', $observqualentier);
		
		$icountobservqual = count($cutobservqual);
		
		$recepagent = $entity->getRecepagent();//
		$pointagev = $entity->getPointagev();
		$tenutrav = $entity->getTenutrav();
		$absence = $entity->getAbsence();
		$absrempl = $entity->getAbsrempl();
		$planrespect = $entity->getPlanrespect();
		$qualitserv = $entity->getQualitserv();
		$qualitrotf = $entity->getQualitrotf();
		$qualitsol = $entity->getQualitsol();
		
		$corection = $entity->getCorection();
		
		$evolprest = $entity->getEvolprest();
		
		$motifIndispo = $entity->getMotifindispo();
		
		$dispoDirect = $entity->getDispodir();
		
		if ($dispoDirect == 1){
			$message3 = \Swift_Message::newInstance()
							->setSubject('Directrice Indisponible sur Audit Global')
							//->setFrom('mail.application@phareoscloud.org')
							//->setTo('nomade@altea-proprete.com')
							//->setBcc('jfrebaud@yahoo.fr')
							//code alexis setfrom et setto
							->setFrom($mail1)
							->setTo($mail3)
							->setBody($this->renderView('PhareosNomadeNetServiceBundle:auditmensuel:showmail5.html.twig', array(
																	 'datereunion' => $dateReunion,
																	 'rsecteur' => $nomsignaltea,
																	 'ptvente'=> $idLieuxaudit,
																	 'entity'      => $entity,
																	 'motifindispo' => $motifIndispo,
																	 
																	 )))
					;
					
					$this->get('mailer')->send($message3);
					
		}
		
		if ($pointagev == 0 or $tenutrav == 0 or $absence == 0 or $absrempl == 0 or $planrespect == 0 or $qualitserv == 'PAS SATISFAISANTE' or $qualitrotf == 'PAS SATISFAISANTE' or $qualitsol == 'PAS SATISFAISANTE' or $corection == 0 or $evolprest == 'EN REGRESSION')
		{
			$mailnomade = 1;
		}
		else
		{
			$mailnomade = 0;
		}
		
		if ($recepagent == 0 or $absence == 0 or $absrempl == 0 )
		{
			$mailrh = 1;
		}
		else
		{
			$mailrh = 0;
		}
		
		if ($pointagev == 0 or $tenutrav == 0 or $absence == 0 or $absrempl == 0 or $planrespect == 0 or $evolprest == 'EN REGRESSION')
		{
			$mailcc = 1;
		}
		else
		{
			$mailcc = 0;
		}
		
		
		
		//echo $icountobservqual;
		
		switch ($icountobservqual){
		case 0:
			$observqual1 = '';
			$observqual2 = '';
			$observqual3 = '';
			$observqual4 = '';
			$observqual5 = '';
			$observqual6 = '';
			break;
		case 1:
			$observqual1 = $cutobservqual[0];
			$observqual2 = '';
			$observqual3 = '';
			$observqual4 = '';
			$observqual5 = '';
			$observqual6 = '';
			break;
		case 2:
			$observqual1 = $cutobservqual[0];
			$observqual2 = $cutobservqual[1];
			$observqual3 = '';
			$observqual4 = '';
			$observqual5 = '';
			$observqual6 = '';
			break;
		case 3:
			$observqual1 = $cutobservqual[0];
			$observqual2 = $cutobservqual[1];
			$observqual3 = $cutobservqual[2];
			$observqual4 = '';
			$observqual5 = '';
			$observqual6 = '';
			break;
		case 4:
			$observqual1 = $cutobservqual[0];
			$observqual2 = $cutobservqual[1];
			$observqual3 = $cutobservqual[2];
			$observqual4 = $cutobservqual[3];
			$observqual5 = '';
			$observqual6 = '';
			break;
		case 5:
			$observqual1 = $cutobservqual[0];
			$observqual2 = $cutobservqual[1];
			$observqual3 = $cutobservqual[2];
			$observqual4 = $cutobservqual[3];
			$observqual5 = $cutobservqual[4];
			$observqual6 = '';
			break;
		case 6:
			$observqual1 = $cutobservqual[0];
			$observqual2 = $cutobservqual[1];
			$observqual3 = $cutobservqual[2];
			$observqual4 = $cutobservqual[3];
			$observqual5 = $cutobservqual[4];
			$observqual6 = $cutobservqual[5];
			break;
		}
		
		if ($icountobservqual > 6)
		{
			$observqual1 = $cutobservqual[0];
			$observqual2 = $cutobservqual[1];
			$observqual3 = $cutobservqual[2];
			$observqual4 = $cutobservqual[3];
			$observqual5 = $cutobservqual[4];
			$observqual6 = $cutobservqual[5];
		}
		
		$rassignal = $entity->getCorectionras();
		
		
		
		if ($dispodir == 1)
		{
			$remarques = 'Directrice Indisponible Motif:' . $motifIndispo . ' ';
		}
		else
		{
			$remarques = '';
		}
		
		if ($rassignal == 1)
		{
			$remarques2 = ' RAS, Aucun problème à signalé sur la prestation ';
		}
		else
		{
			$remarques2 = '';
		}
		
		
		$observevolution = $remarques . $remarques2 . $entity->getObservevol();
		
		
		
		$observevolentier = wordwrap ($observevolution,135,'***', true);
		$cutobservevol = explode ('***', $observevolentier);
		
		$icountobservevol = count($cutobservevol);
		
		switch ($icountobservevol){
		case 0:
			$observevol1 = '';
			$observevol2 = '';
			$observevol3 = '';
			$observevol4 = '';
			$observevol5 = '';
			$observevol6 = '';
			break;
		case 1:
			$observevol1 = $cutobservevol[0];
			$observevol2 = '';
			$observevol3 = '';
			$observevol4 = '';
			$observevol5 = '';
			$observevol6 = '';
			break;
		case 2:
			$observevol1 = $cutobservevol[0];
			$observevol2 = $cutobservevol[1];
			$observevol3 = '';
			$observevol4 = '';
			$observevol5 = '';
			$observevol6 = '';
			break;
		case 3:
			$observevol1 = $cutobservevol[0];
			$observevol2 = $cutobservevol[1];
			$observevol3 = $cutobservevol[2];
			$observevol4 = '';
			$observevol5 = '';
			$observevol6 = '';
			break;
		case 4:
			$observevol1 = $cutobservevol[0];
			$observevol2 = $cutobservevol[1];
			$observevol3 = $cutobservevol[2];
			$observevol4 = $cutobservevol[3];
			$observevol5 = '';
			$observevol6 = '';
			break;
		case 5:
			$observevol1 = $cutobservevol[0];
			$observevol2 = $cutobservevol[1];
			$observevol3 = $cutobservevol[2];
			$observevol4 = $cutobservevol[3];
			$observevol5 = $cutobservevol[4];
			$observevol6 = '';
			break;
		case 6:
			$observevol1 = $cutobservevol[0];
			$observevol2 = $cutobservevol[1];
			$observevol3 = $cutobservevol[2];
			$observevol4 = $cutobservevol[3];
			$observevol5 = $cutobservevol[4];
			$observevol6 = $cutobservevol[5];
			break;
		}
		
		if ($icountobservevol > 6)
		{
			$observevol1 = $cutobservevol[0];
			$observevol2 = $cutobservevol[1];
			$observevol3 = $cutobservevol[2];
			$observevol4 = $cutobservevol[3];
			$observevol5 = $cutobservevol[4];
			$observevol6 = $cutobservevol[5];
		}
		
		

			$html = $this->render('PhareosNomadeNetServiceBundle:auditmensuel:show2pdf2.html.twig', array(
				'icount' => $icountobservqual,
				'observevol1' => $observevol1,
				'observevol2' => $observevol2,
				'observevol3' => $observevol3,
				'observevol4' => $observevol4,
				'observevol5' => $observevol5,
				'observevol6' => $observevol6,
				'observqual1' => $observqual1,
				'observqual2' => $observqual2,
				'observqual3' => $observqual3,
				'observqual4' => $observqual4,
				'observqual5' => $observqual5,
				'observqual6' => $observqual6,
				'entity'      => $entity,
				'delete_form' => $deleteForm->createView(),
			));
		
			$html2pdf = new \Html2Pdf_Html2Pdf('P','A4','en', false, 'ISO-8859-1');
			$html2pdf->pdf->SetDisplayMode('real');
			$html2pdf->setDefaultFont('Arial');
			$html2pdf->writeHTML(utf8_decode($html), isset($_GET['vuehtml']));
			ob_clean();
			$html2pdf->Output(__DIR__ . '/../../../../web/upload/nomade/auditmensuel/'.'Audit_Mensuel_Mag'.$idLieuxaudit.'_'.$dateReunion.'_'.$id.'.pdf', 'F');
			
			
			
			// envoi d'un mail de confirmation
			$message4 = \Swift_Message::newInstance()
							->setSubject('RDV chargé de secteur Altéa et Audit Global')
							//->setFrom('contact@altea-proprete.com')
							//->setTo($emailMag)
							//->setBcc('jfrebaud@yahoo.fr')
							//code alexis setfrom et setto
							->setFrom($mail4)
							->setTo($emailMag)
							->setBody($this->renderView('PhareosNomadeNetServiceBundle:auditmensuel:showmail3.html.twig', array(
																	 'datereunion' => $dateReunion,
																	 'rsecteur' => $nomsignaltea,
																	 'ptvente'=> $idLieuxaudit,
																	 'entity'      => $entity,
																	 )))
					;
					
					$attachment = \Swift_Attachment::fromPath(__DIR__ . '/../../../../web/upload/nomade/auditmensuel/'.'Audit_Mensuel_Mag'.$idLieuxaudit.'_'.$dateReunion.'_'.$id.'.pdf', 'application/pdf');
					
					$message4->attach($attachment);
					
					if ($nomGC == 'SEPHORA' and $dispodir == 0)
					{
						$this->get('mailer')->send($message4);
					}
					
					$message5 = \Swift_Message::newInstance()
							->setSubject('Point négatif sur Audit Global')
							//->setFrom('mail.application@phareoscloud.org')
							//->setTo('nomade@altea-proprete.com')
							//->setBcc('jfrebaud@yahoo.fr')
							//code alexis setfrom et setto
							->setFrom($mail1)
							->setTo($mail3)
							->setBody($this->renderView('PhareosNomadeNetServiceBundle:auditmensuel:showmail4.html.twig', array(
																	'observevol1' => $observevol1,
																	'observevol2' => $observevol2,
																	'observevol3' => $observevol3,
																	'observevol4' => $observevol4,
																	'observevol5' => $observevol5,
																	'observevol6' => $observevol6,
																	'observqual1' => $observqual1,
																	'observqual2' => $observqual2,
																	'observqual3' => $observqual3,
																	'observqual4' => $observqual4,
																	'observqual5' => $observqual5,
																	'observqual6' => $observqual6,
																	 'datereunion' => $dateReunion,
																	 'rsecteur' => $nomsignaltea,
																	 'ptvente'=> $idLieuxaudit,
																	 'entity'      => $entity,
																	 )))
					;
					
					$attachment = \Swift_Attachment::fromPath(__DIR__ . '/../../../../web/upload/nomade/auditmensuel/'.'Audit_Mensuel_Mag'.$idLieuxaudit.'_'.$dateReunion.'_'.$id.'.pdf', 'application/pdf');
					
					$message5->attach($attachment);
					
					if ($mailnomade == 1)
					{
						$this->get('mailer')->send($message5);
					}
					
					$message6 = \Swift_Message::newInstance()
							->setSubject('Point négatif sur Audit Global')
							//->setFrom('mail.application@phareoscloud.org')
							//->setTo('contact@altea-proprete.com')
							//->setBcc('jfrebaud@yahoo.fr')
							//code alexis setfrom et setto
							->setFrom($mail1)
							->setTo($mail5)
							->setBody($this->renderView('PhareosNomadeNetServiceBundle:auditmensuel:showmail4.html.twig', array(
																	'observevol1' => $observevol1,
																	'observevol2' => $observevol2,
																	'observevol3' => $observevol3,
																	'observevol4' => $observevol4,
																	'observevol5' => $observevol5,
																	'observevol6' => $observevol6,
																	'observqual1' => $observqual1,
																	'observqual2' => $observqual2,
																	'observqual3' => $observqual3,
																	'observqual4' => $observqual4,
																	'observqual5' => $observqual5,
																	'observqual6' => $observqual6,
																	 'datereunion' => $dateReunion,
																	 'rsecteur' => $nomsignaltea,
																	 'ptvente'=> $idLieuxaudit,
																	 'entity'      => $entity,
																	 )))
					;
					
					$attachment = \Swift_Attachment::fromPath(__DIR__ . '/../../../../web/upload/nomade/auditmensuel/'.'Audit_Mensuel_Mag'.$idLieuxaudit.'_'.$dateReunion.'_'.$id.'.pdf', 'application/pdf');
					
					$message6->attach($attachment);
					
					if ($mailrh == 1 or $mailcc == 1)
					{
						$this->get('mailer')->send($message6);
					}
			 
			
			
			
			
			
			
			
			
			
			return $this->redirect($this->generateUrl('auditmensuel_show', array(
			'id' => $id
			)));
        //}

        
    }
	
	public function showpdfAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:auditmensuel')->find($id);
		
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find auditmensuel entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
		
		$idLieuxaudit = $entity->getLieux()->getCodag();
		$dateReunion = $entity->getDatereunion()->format('d-m-Y');
		
		$observqualentier = wordwrap ($entity->getObservqual(),135,'***', true);
		$cutobservqual = explode ('***', $observqualentier);
		
		$icountobservqual = count($cutobservqual);
		
		//echo $icountobservqual;
		
		switch ($icountobservqual){
		case 0:
			$observqual1 = '';
			$observqual2 = '';
			$observqual3 = '';
			$observqual4 = '';
			$observqual5 = '';
			$observqual6 = '';
			break;
		case 1:
			$observqual1 = $cutobservqual[0];
			$observqual2 = '';
			$observqual3 = '';
			$observqual4 = '';
			$observqual5 = '';
			$observqual6 = '';
			break;
		case 2:
			$observqual1 = $cutobservqual[0];
			$observqual2 = $cutobservqual[1];
			$observqual3 = '';
			$observqual4 = '';
			$observqual5 = '';
			$observqual6 = '';
			break;
		case 3:
			$observqual1 = $cutobservqual[0];
			$observqual2 = $cutobservqual[1];
			$observqual3 = $cutobservqual[2];
			$observqual4 = '';
			$observqual5 = '';
			$observqual6 = '';
			break;
		case 4:
			$observqual1 = $cutobservqual[0];
			$observqual2 = $cutobservqual[1];
			$observqual3 = $cutobservqual[2];
			$observqual4 = $cutobservqual[3];
			$observqual5 = '';
			$observqual6 = '';
			break;
		case 5:
			$observqual1 = $cutobservqual[0];
			$observqual2 = $cutobservqual[1];
			$observqual3 = $cutobservqual[2];
			$observqual4 = $cutobservqual[3];
			$observqual5 = $cutobservqual[4];
			$observqual6 = '';
			break;
		case 6:
			$observqual1 = $cutobservqual[0];
			$observqual2 = $cutobservqual[1];
			$observqual3 = $cutobservqual[2];
			$observqual4 = $cutobservqual[3];
			$observqual5 = $cutobservqual[4];
			$observqual6 = $cutobservqual[5];
			break;
		}
		
		if ($icountobservqual > 6)
		{
			$observqual1 = $cutobservqual[0];
			$observqual2 = $cutobservqual[1];
			$observqual3 = $cutobservqual[2];
			$observqual4 = $cutobservqual[3];
			$observqual5 = $cutobservqual[4];
			$observqual6 = $cutobservqual[5];
		}
		
		$observevolentier = wordwrap ($entity->getObservevol(),135,'***', true);
		$cutobservevol = explode ('***', $observevolentier);
		
		$icountobservevol = count($cutobservevol);
		
		switch ($icountobservevol){
		case 0:
			$observevol1 = '';
			$observevol2 = '';
			$observevol3 = '';
			$observevol4 = '';
			$observevol5 = '';
			$observevol6 = '';
			break;
		case 1:
			$observevol1 = $cutobservevol[0];
			$observevol2 = '';
			$observevol3 = '';
			$observevol4 = '';
			$observevol5 = '';
			$observevol6 = '';
			break;
		case 2:
			$observevol1 = $cutobservevol[0];
			$observevol2 = $cutobservevol[1];
			$observevol3 = '';
			$observevol4 = '';
			$observevol5 = '';
			$observevol6 = '';
			break;
		case 3:
			$observevol1 = $cutobservevol[0];
			$observevol2 = $cutobservevol[1];
			$observevol3 = $cutobservevol[2];
			$observevol4 = '';
			$observevol5 = '';
			$observevol6 = '';
			break;
		case 4:
			$observevol1 = $cutobservevol[0];
			$observevol2 = $cutobservevol[1];
			$observevol3 = $cutobservevol[2];
			$observevol4 = $cutobservevol[3];
			$observevol5 = '';
			$observevol6 = '';
			break;
		case 5:
			$observevol1 = $cutobservevol[0];
			$observevol2 = $cutobservevol[1];
			$observevol3 = $cutobservevol[2];
			$observevol4 = $cutobservevol[3];
			$observevol5 = $cutobservevol[4];
			$observevol6 = '';
			break;
		case 6:
			$observevol1 = $cutobservevol[0];
			$observevol2 = $cutobservevol[1];
			$observevol3 = $cutobservevol[2];
			$observevol4 = $cutobservevol[3];
			$observevol5 = $cutobservevol[4];
			$observevol6 = $cutobservevol[5];
			break;
		}
		
		if ($icountobservevol > 6)
		{
			$observevol1 = $cutobservevol[0];
			$observevol2 = $cutobservevol[1];
			$observevol3 = $cutobservevol[2];
			$observevol4 = $cutobservevol[3];
			$observevol5 = $cutobservevol[4];
			$observevol6 = $cutobservevol[5];
		}
		
		
		
		
		
		
		

        $html = $this->render('PhareosNomadeNetServiceBundle:auditmensuel:show2pdf2.html.twig', array(
			'icount' => $icountobservqual,
			'observevol1' => $observevol1,
			'observevol2' => $observevol2,
			'observevol3' => $observevol3,
			'observevol4' => $observevol4,
			'observevol5' => $observevol5,
			'observevol6' => $observevol6,
			'observqual1' => $observqual1,
			'observqual2' => $observqual2,
			'observqual3' => $observqual3,
			'observqual4' => $observqual4,
			'observqual5' => $observqual5,
			'observqual6' => $observqual6,
			'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
		
		$html2pdf = new \Html2Pdf_Html2Pdf('P','A4','en', false, 'ISO-8859-1');
		$html2pdf->pdf->SetDisplayMode('real');
		$html2pdf->setDefaultFont('Arial');
		$html2pdf->writeHTML(utf8_decode($html), isset($_GET['vuehtml']));
		ob_clean();
		$fichier = $html2pdf->Output('Audit_Mensuel_Mag'.$idLieuxaudit.'_'.$dateReunion.'_'.$id.'.pdf', 'D');

		$response = new Response();
		$response->clearHttpHeaders();
		$response->setContent(file_get_contents($fichier));
		$response->headers->set('Content-Type', 'application/force-download');
		$response->headers->set('Content-disposition', 'filename='. $fichier);

		return $response;
    }
	
	public function showpdfclientAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:auditmensuel')->find($id);
		
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find auditmensuel entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
		
		$idLieuxaudit = $entity->getLieux()->getCodag();
		$dateReunion = $entity->getDatereunion()->format('d-m-Y');
		
		$observqualentier = wordwrap ($entity->getObservqual(),135,'***', true);
		$cutobservqual = explode ('***', $observqualentier);
		
		$icountobservqual = count($cutobservqual);
		
		//echo $icountobservqual;
		
		switch ($icountobservqual){
		case 0:
			$observqual1 = '';
			$observqual2 = '';
			$observqual3 = '';
			$observqual4 = '';
			$observqual5 = '';
			$observqual6 = '';
			break;
		case 1:
			$observqual1 = $cutobservqual[0];
			$observqual2 = '';
			$observqual3 = '';
			$observqual4 = '';
			$observqual5 = '';
			$observqual6 = '';
			break;
		case 2:
			$observqual1 = $cutobservqual[0];
			$observqual2 = $cutobservqual[1];
			$observqual3 = '';
			$observqual4 = '';
			$observqual5 = '';
			$observqual6 = '';
			break;
		case 3:
			$observqual1 = $cutobservqual[0];
			$observqual2 = $cutobservqual[1];
			$observqual3 = $cutobservqual[2];
			$observqual4 = '';
			$observqual5 = '';
			$observqual6 = '';
			break;
		case 4:
			$observqual1 = $cutobservqual[0];
			$observqual2 = $cutobservqual[1];
			$observqual3 = $cutobservqual[2];
			$observqual4 = $cutobservqual[3];
			$observqual5 = '';
			$observqual6 = '';
			break;
		case 5:
			$observqual1 = $cutobservqual[0];
			$observqual2 = $cutobservqual[1];
			$observqual3 = $cutobservqual[2];
			$observqual4 = $cutobservqual[3];
			$observqual5 = $cutobservqual[4];
			$observqual6 = '';
			break;
		case 6:
			$observqual1 = $cutobservqual[0];
			$observqual2 = $cutobservqual[1];
			$observqual3 = $cutobservqual[2];
			$observqual4 = $cutobservqual[3];
			$observqual5 = $cutobservqual[4];
			$observqual6 = $cutobservqual[5];
			break;
		}
		
		if ($icountobservqual > 6)
		{
			$observqual1 = $cutobservqual[0];
			$observqual2 = $cutobservqual[1];
			$observqual3 = $cutobservqual[2];
			$observqual4 = $cutobservqual[3];
			$observqual5 = $cutobservqual[4];
			$observqual6 = $cutobservqual[5];
		}
		
		$observevolentier = wordwrap ($entity->getObservevol(),135,'***', true);
		$cutobservevol = explode ('***', $observevolentier);
		
		$icountobservevol = count($cutobservevol);
		
		switch ($icountobservevol){
		case 0:
			$observevol1 = '';
			$observevol2 = '';
			$observevol3 = '';
			$observevol4 = '';
			$observevol5 = '';
			$observevol6 = '';
			break;
		case 1:
			$observevol1 = $cutobservevol[0];
			$observevol2 = '';
			$observevol3 = '';
			$observevol4 = '';
			$observevol5 = '';
			$observevol6 = '';
			break;
		case 2:
			$observevol1 = $cutobservevol[0];
			$observevol2 = $cutobservevol[1];
			$observevol3 = '';
			$observevol4 = '';
			$observevol5 = '';
			$observevol6 = '';
			break;
		case 3:
			$observevol1 = $cutobservevol[0];
			$observevol2 = $cutobservevol[1];
			$observevol3 = $cutobservevol[2];
			$observevol4 = '';
			$observevol5 = '';
			$observevol6 = '';
			break;
		case 4:
			$observevol1 = $cutobservevol[0];
			$observevol2 = $cutobservevol[1];
			$observevol3 = $cutobservevol[2];
			$observevol4 = $cutobservevol[3];
			$observevol5 = '';
			$observevol6 = '';
			break;
		case 5:
			$observevol1 = $cutobservevol[0];
			$observevol2 = $cutobservevol[1];
			$observevol3 = $cutobservevol[2];
			$observevol4 = $cutobservevol[3];
			$observevol5 = $cutobservevol[4];
			$observevol6 = '';
			break;
		case 6:
			$observevol1 = $cutobservevol[0];
			$observevol2 = $cutobservevol[1];
			$observevol3 = $cutobservevol[2];
			$observevol4 = $cutobservevol[3];
			$observevol5 = $cutobservevol[4];
			$observevol6 = $cutobservevol[5];
			break;
		}
		
		if ($icountobservevol > 6)
		{
			$observevol1 = $cutobservevol[0];
			$observevol2 = $cutobservevol[1];
			$observevol3 = $cutobservevol[2];
			$observevol4 = $cutobservevol[3];
			$observevol5 = $cutobservevol[4];
			$observevol6 = $cutobservevol[5];
		}
		
		
		
		
		
		
		

        $html = $this->render('PhareosNomadeNetServiceBundle:auditmensuel:show2pdf2.html.twig', array(
			'icount' => $icountobservqual,
			'observevol1' => $observevol1,
			'observevol2' => $observevol2,
			'observevol3' => $observevol3,
			'observevol4' => $observevol4,
			'observevol5' => $observevol5,
			'observevol6' => $observevol6,
			'observqual1' => $observqual1,
			'observqual2' => $observqual2,
			'observqual3' => $observqual3,
			'observqual4' => $observqual4,
			'observqual5' => $observqual5,
			'observqual6' => $observqual6,
			'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
		
		$html2pdf = new \Html2Pdf_Html2Pdf('P','A4','en', false, 'ISO-8859-1');
		$html2pdf->pdf->SetDisplayMode('real');
		$html2pdf->setDefaultFont('Arial');
		$html2pdf->writeHTML(utf8_decode($html), isset($_GET['vuehtml']));
		ob_clean();
		$fichier = $html2pdf->Output('Audit_Mensuel_Mag'.$idLieuxaudit.'_'.$dateReunion.'_'.$id.'.pdf', 'D');

		$response = new Response();
		$response->clearHttpHeaders();
		$response->setContent(file_get_contents($fichier));
		$response->headers->set('Content-Type', 'application/force-download');
		$response->headers->set('Content-disposition', 'filename='. $fichier);

		return $response;
    }
}
