<?php

namespace Phareos\UtilisateurBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class DefaultController extends Controller
{
    
    public function indexAction()
    {
        return $this->render('PhareosUtilisateurBundle::layout.html.twig');
    }
}
