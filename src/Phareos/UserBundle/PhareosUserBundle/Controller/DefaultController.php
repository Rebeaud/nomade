<?php

namespace Phareos\UserBundle\PhareosUserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class DefaultController extends Controller
{
    
    public function indexAction($name)
    {
        return $this->render('PhareosUserBundlePhareosUserBundle:Default:index.html.twig', array('name' => $name));
    }
}
